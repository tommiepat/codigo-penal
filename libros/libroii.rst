**************************************
LIBRO  SEGUNDO PARTE  ESPECIAL DELITOS
**************************************

(*) De conformidad con el Numeral 3.3 del Artículo 3 del Decreto Supremo N° 004-2020-JUS, publicado el 23 abril 2020, en los supuestos previstos en los literales d) y e) del numeral 3.1 del citado artículo no procede la recomendación de gracia presidencial respecto de las internas y los internos que han sido sentenciados por cualquiera de los delitos detallados en el citado numeral, contemplados en el Libro Segundo, Parte Especial del presente Código Penal.

TITULO I DELITOS CONTRA LA VIDA, EL CUERPO Y  LA  SALUD
=======================================================

CAPITULO  I HOMICIDIO
#####################

CONCORDANCIAS:     Ley N° 30901 (Ley que implementa un subregistro de condenas y establece la inhabilitación definitiva para desempeñar actividad, profesión, ocupación u oficio que implique el cuidado, vigilancia o atención de niñas, niños o adolescentes)

Artículo 106 Homicidio Simple
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que mata a otro será reprimido con pena privativa de libertad no menor de seis ni mayor de veinte años.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 107 Parricidio
~~~~~~~~~~~~~~~~~~~~~~~

El que, a sabiendas, mata a su ascendiente, descendiente, natural o adoptivo, o a su cónyuge o concubino, será reprimido con pena privativa de libertad no menor de quince años. (*)

(*) Artículo modificado por el Artículo Único de la Ley Nº 29819, publicada el 27 diciembre 2011, en los términos siguientes:

“Artículo 107. Parricidio / Feminicidio

El que, a sabiendas, mata a su ascendiente, descendiente, natural o adoptivo, o a quien es o ha sido su cónyuge, su conviviente, o con quien esté sosteniendo o haya sostenido una relación análoga será reprimido con pena privativa de libertad no menor de quince años.

La pena privativa de libertad será no menor de veinticinco años, cuando concurran cualquiera de las circunstancias agravantes previstas en los numerales 1, 2, 3 y 4 del artículo 108.

Si la víctima del delito descrito es o ha sido la cónyuge o la conviviente del autor, o estuvo ligada a él por una relación análoga el delito tendrá el nombre de feminicidio.”(*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 30068, publicada el 18 julio 2013, cuyo texto es el siguiente:

“Artículo 107.- Parricidio

El que, a sabiendas, mata a su ascendiente, descendiente, natural o adoptivo, o a una persona con quien sostiene o haya sostenido una relación conyugal o de convivencia, será reprimido con pena privativa de libertad no menor de quince años.

La pena privativa de libertad será no menor de veinticinco años, cuando concurra cualquiera de las circunstancias agravantes previstas en los numerales 1, 2, 3 y 4 del artículo 108.”

"En caso de que el agente tenga hijos con la víctima, además será reprimido con la pena de inhabilitación prevista en el inciso 5 del artículo 36."(*)(**)

(*) Artículo modificado  (*) NOTA SPIJ por el Artículo 1 de la Ley N° 30323, publicada el 07 mayo 2015.

(**) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

PROCESOS CONSTITUCIONALES
JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 108 Homicidio calificado. Asesinato
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no menor de quince años, el que mata a otro concurriendo cualquiera de las circunstancias siguientes:

1. Por ferocidad o por lucro.
2. Para facilitar u ocultar otro delito.
3. Con gran crueldad, alevosía o veneno.
4. Por fuego, explosión u otro medio capaz de poner en peligro la vida o salud de otras personas.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 896, publicado el 24 mayo 1998, expedido con arreglo a la Ley N° 26950, que otorga al Poder Ejecutivo facultades para legislar en materia de seguridad nacional, cuyo texto es el siguiente:

"Homicidio Calificado - Asesinato
Artículo 108.- Será reprimido con pena privativa de libertad no menor de veinticinco años el que mate a otro concurriendo cualquiera de las circunstancias siguientes:

1.- Por ferocidad, por lucro o por placer;
2.- Para facilitar u ocultar otro delito;
3.- Con gran crueldad o alevosía;
4.- Por fuego, explosión, veneno o por cualquier otro medio capaz de poner en peligro la vida o salud de otras personas." (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 27472, publicada el 05 junio 2001, cuyo texto es el siguiente:

"Artículo 108.- Homicidio calificado. Asesinato

Será reprimido con pena privativa de libertad no menor de quince años el que mata a otro concurriendo cualquiera de las circunstancias siguientes:

1. Por ferocidad, por lucro o por placer;

2. Para facilitar u ocultar otro delito;

3. Con gran crueldad o alevosía;

4. Por fuego, explosión, veneno o por cualquier otro medio capaz de poner en peligro la vida o salud de otras personas." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28878, publicada el 17 agosto 2006, cuyo texto es el siguiente:

“Artículo 108.- Homicidio Calificado - Asesinato

Será reprimido con pena privativa de libertad no menor de quince años el que mate a otro concurriendo cualquiera de las circunstancias siguientes:

1. Por ferocidad, por lucro o por placer;

2. Para facilitar u ocultar otro delito;

3. Con gran crueldad o alevosía;

4. Por fuego, explosión, veneno o por cualquier otro medio capaz de poner en peligro la vida o salud de otras personas;

5. Si la víctima es miembro de la Policía Nacional del Perú o de las Fuerzas Armadas, Magistrado del Poder Judicial o del Ministerio Público, en el cumplimiento de sus funciones."(*)

(*) Artículo modificado por el Artículo 2 de la Ley Nº 30054, publicada el 30 junio 2013, cuyo texto es el siguiente:

Artículo 108.- Homicidio calificado-asesinato

Será reprimido con pena privativa de libertad no menor de quince años el que mate a otro concurriendo cualquiera de las circunstancias siguientes:

1. Por ferocidad, por lucro o por placer;

2. Para facilitar u ocultar otro delito;

3. Con gran crueldad o alevosía;

4. Por fuego, explosión, veneno o por cualquier otro medio capaz de poner en peligro la vida o salud de otras personas." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 30253, publicada el 24 octubre 2014, cuyo texto es el siguiente:

“Artículo 108.- Homicidio calificado

Será reprimido con pena privativa de libertad no menor de quince años el que mate a otro concurriendo cualquiera de las circunstancias siguientes:

1. Por ferocidad, codicia, lucro o por placer.

2. Para facilitar u ocultar otro delito.

3. Con gran crueldad o alevosía.

4. Por fuego, explosión o cualquier otro medio capaz de poner en peligro la vida o salud de otras personas”.

CONCORDANCIAS:     D.S. Nº 015-2003-JUS, Art. 210.5 (Aprueban el Reglamento del Código de Ejecución Penal)

Ley Nº 30077, Arts. 3 (Delitos comprendidos) y 24 (Prohibición de beneficios penitenciarios)

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

“Artículo 108-A.- Homicidio calificado por la condición oficial del agente

El que mata a un miembro de la Policía Nacional, de las Fuerzas Armadas, a un magistrado del Poder Judicial o del Ministerio Público o a un miembro del Tribunal Constitucional o a cualquier autoridad elegida por mandato popular, en el ejercicio de sus funciones o como consecuencia de ellas, será reprimido con pena privativa de libertad no menor de veinte años.”(*)

(*) Artículo incorporado por el Artículo 1 de la Ley Nº 30054, publicada el 30 junio 2013.

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 108-A.- Homicidio Calificado por la Condición de la víctima

El que mata a uno de los altos funcionarios comprendidos en el artículo 39 de la Constitución Política del Perú, a un miembro de la Policía Nacional, de las Fuerzas Armadas, a un Magistrado del Poder Judicial o del Ministerio Publico o a un Miembro del Tribunal Constitucional o a cualquier autoridad elegida por mandato popular, en el ejercicio de sus funciones o como consecuencia de ellas, será reprimido con pena privativa de libertad no menor de 25 años ni mayor de 35 años."

“Artículo 108-B.- Feminicidio (*) RECTIFICADO POR FE DE ERRATAS

Será reprimido con pena privativa de libertad no menor de quince años el que mata a una mujer por su condición de tal, en cualquiera de los siguientes contextos:

1. Violencia familiar;

2. Coacción, hostigamiento o acoso sexual;

3. Abuso de poder, confianza o de cualquier otra posición o relación que le confiera autoridad al agente;

4. Cualquier forma de discriminación contra la mujer, independientemente de que exista o haya existido una relación conyugal o de convivencia con el agente.

La pena privativa de libertad será no menor de veinticinco años, cuando concurra cualquiera de las siguientes circunstancias agravantes:

1. Si la víctima era menor de edad;

2. Si la víctima se encontraba en estado de gestación;

3. Si la víctima se encontraba bajo cuidado o responsabilidad del agente;

4. Si la víctima fue sometida previamente a violación sexual o actos de mutilación;

5. Si al momento de cometerse el delito, la víctima padeciera cualquier tipo de discapacidad;

6. Si la víctima fue sometida para fines de trata de personas;

7. Cuando hubiera concurrido cualquiera de las circunstancias agravantes establecidas en el artículo 108.

La pena será de cadena perpetua cuando concurran dos o más circunstancias agravantes.”(1)

"En caso de que el agente tenga hijos con la víctima, además será reprimido con la pena de inhabilitación prevista en el inciso 5 del artículo 36."(2)(3)

(1) Artículo incorporado por el Artículo 2 de la Ley Nº 30068, publicada el 18 julio 2013.

(2) Artículo modificado  (*) NOTA SPIJ por el Artículo 1 de la Ley N° 30323, publicada el 07 mayo 2015.

(3) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1323, publicado el 06 enero 2017, cuyo texto es el siguiente:

“Artículo 108-B.- Feminicidio

Será reprimido con pena privativa de libertad no menor de quince años el que mata a una mujer por su condición de tal, en cualquiera de los siguientes contextos:

1. Violencia familiar;

2. Coacción, hostigamiento o acoso sexual;

3. Abuso de poder, confianza o de cualquier otra posición o relación que le confiera autoridad al agente;

4. Cualquier forma de discriminación contra la mujer, independientemente de que exista o haya existido una relación conyugal o de convivencia con el agente.

La pena privativa de libertad será no menor de veinticinco años, cuando concurra cualquiera de las siguientes circunstancias agravantes:

1. Si la víctima era menor de edad o adulta mayor.

2. Si la víctima se encontraba en estado de gestación.

3. Si la víctima se encontraba bajo cuidado o responsabilidad del agente.

4. Si la víctima fue sometida previamente a violación sexual o actos de mutilación.

5. Si al momento de cometerse el delito, la víctima tiene cualquier tipo de discapacidad.

6. Si la víctima fue sometida para fines de trata de personas o cualquier tipo de explotación humana.

7. Cuando hubiera concurrido cualquiera de las circunstancias agravantes establecidas en el artículo 108.

8. Cuando se comete a sabiendas de la presencia de las hijas o hijos de la víctima o de niños, niñas o adolescentes que se encuentren bajo su cuidado.

La pena será de cadena perpetua cuando concurran dos o más circunstancias agravantes.

En todas las circunstancias previstas en el presente artículo, se impondrá la pena de inhabilitación conforme al artículo 36.”(*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 30819, publicada el 13 julio 2018, cuyo texto es el siguiente:

“Artículo 108-B.- Feminicidio

Será reprimido con pena privativa de libertad no menor de veinte años el que mata a una mujer por su condición de tal, en cualquiera de los siguientes contextos:

1. Violencia familiar.

2. Coacción, hostigamiento o acoso sexual.

3. Abuso de poder, confianza o de cualquier otra posición o relación que le confiera autoridad al agente.

4. Cualquier forma de discriminación contra la mujer, independientemente de que exista o haya existido una relación conyugal o de convivencia con el agente.

La pena privativa de libertad será no menor de treinta años cuando concurra cualquiera de las siguientes circunstancias agravantes:

1. Si la víctima era menor de edad o adulta mayor.

2. Si la víctima se encontraba en estado de gestación.

3. Si la víctima se encontraba bajo cuidado o responsabilidad del agente.

4. Si la víctima fue sometida previamente a violación sexual o actos de mutilación.

5. Si al momento de cometerse el delito, la víctima tiene cualquier tipo de discapacidad.

6. Si la víctima fue sometida para fines de trata de personas o cualquier tipo de explotación humana.

7. Cuando hubiera concurrido cualquiera de las circunstancias agravantes establecidas en el artículo 108.

8. Si, en el momento de cometerse el delito, estuviera presente cualquier niña, niño o adolescente.

9. Si el agente actúa en estado de ebriedad, con presencia de alcohol en la sangre en proporción mayor de 0.25 gramos-litro, o bajo efecto de drogas tóxicas, estupefacientes, sustancias psicotrópicas o sintéticas.

La pena será de cadena perpetua cuando concurran dos o más circunstancias agravantes.

En todas las circunstancias previstas en el presente artículo, se impondrá la pena de inhabilitación conforme a los numerales 5 y 11 del artículo 36 del presente Código y los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda." (*)

(*) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

(*) De conformidad con el Literal a) del Artículo 3 del Decreto Legislativo N° 1368, publicado el 29 julio 2018, el sistema  es competente para conocer las medidas de protección y las medidas cautelares que se dicten en el marco de la Ley Nº 30364, así como los procesos penales que se siguen por la comisión del delito de Feminicidio, previsto en el presente artículo.

(*) De conformidad con Literal a) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del feminicidio, previsto en el presente artículo.

“Artículo 108-C.- Sicariato

El que mata a otro por orden, encargo o acuerdo, con el propósito de obtener para sí o para otro un beneficio económico o de cualquier otra índole, será reprimido con pena privativa de libertad no menor de veinticinco años y con inhabilitación establecida en el numeral 6 del artículo 36, según corresponda.

Las mismas penas se imponen a quien ordena, encarga, acuerda el sicariato o actúa como intermediario.

Será reprimido con pena privativa de libertad de cadena perpetua si la conducta descrita en el primer párrafo se realiza:

1. Valiéndose de un menor de edad o de otro inimputable para ejecutar la conducta

2. Para dar cumplimiento a la orden de una organización criminal

3. Cuando en la ejecución intervienen dos o más personas

4. Cuando las víctimas sean dos o más personas

5. Cuando las víctimas estén comprendidas en los artículos 107 primer párrafo, 108-A y 108-B primer párrafo.

6. Cuando se utilice armas de guerra.”(*)

(*) Artículo incorporado por el Artículo 1 del Decreto Legislativo N° 1181, publicado el 27 julio 2015.

(*) De conformidad con la Primera Disposición Complementaria Final del Decreto Legislativo N° 1181, publicado el 27 julio 2015, queda prohibido el derecho de gracia, amnistía, indulto y conmutación de la pena para los delitos previstos en el presente artículo.

(*) De conformidad con el Numeral 1 de la Segunda Disposición Complementaria Final del Decreto Legislativo N° 1181, publicado el 27 julio 2015, se prohibe los beneficios de semilibertad y liberación condicional a los sentenciados bajo los alcances del presente artículo.

(*) De conformidad con el Numeral 2 de la Segunda Disposición Complementaria Final del Decreto Legislativo N° 1181, publicado el 27 julio 2015, se dispone que en los casos señalados en el párrafo anterior de la citada disposición sólo se les aplicará la redención de pena por trabajo o educación en la modalidad del siete por uno.

“Artículo 108-D.- La conspiración y el ofrecimiento para el delito de sicariato

Será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años:

1. Quien participa en una conspiración para promover, favorecer o facilitar el delito de sicariato.

2. Quien solicita u ofrece a otros, cometer el delito de sicariato o actúa como intermediario.

La pena privativa de libertad será no menor de seis ni mayor de diez años, si las conductas antes descritas se realizan con la intervención de un menor de edad u otro inimputable”.”(*)

(*) Artículo incorporado por el Artículo 1 del Decreto Legislativo N° 1181, publicado el 27 julio 2015.

(*) De conformidad con la Primera Disposición Complementaria Final del Decreto Legislativo N° 1181, publicado el 27 julio 2015, queda prohibido el derecho de gracia, amnistía, indulto y conmutación de la pena para los delitos previstos en el presente artículo.

(*) De conformidad con el Numeral 1 de la Segunda Disposición Complementaria Final del Decreto Legislativo N° 1181, publicado el 27 julio 2015, se prohibe los beneficios de semilibertad y liberación condicional a los sentenciados bajo los alcances del presente artículo.

(*) De conformidad con el Numeral 2 de la Segunda Disposición Complementaria Final del Decreto Legislativo N° 1181, publicado el 27 julio 2015, se dispone que en los casos señalados en el párrafo anterior de la citada disposición sólo se les aplicará la redención de pena por trabajo o educación en la modalidad del siete por uno.

Artículo 109 Homicidio por emoción violenta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que mata a otro bajo el imperio de una emoción violenta que las circunstancias hacen excusable, será reprimido con pena privativa de libertad, no menor de tres ni mayor de cinco años.

Si concurre algunas de las circunstancias previstas en el artículo 107, la pena será no menor de cinco ni mayor de diez años.

Artículo 110 Infanticidio
~~~~~~~~~~~~~~~~~~~~~~~~~

La madre que mata a su hijo durante el parto o bajo la influencia del estado puerperal, será reprimida con pena privativa de libertad no menor de uno ni mayor de cuatro años, o con prestación de servicio comunitario de cincuentidós a ciento cuatro jornadas.

Artículo 111 Homicidio culposo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, por culpa, ocasiona la muerte de una persona, será reprimido con pena privativa de libertad no mayor de dos años o con prestación de servicio comunitario de cincuentidós a ciento cuatro jornadas.

Cuando son varias las víctimas del mismo hecho o el delito resulte de la inobservancia de reglas técnicas de profesión, de ocupación o industria, la pena privativa de libertad será no menor de dos ni mayor de seis años e inhabilitación conforme al artículo 36, incisos 4, 6 y 7.(*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 27753, publicada el 09 junio 2002, cuyo texto es el siguiente:

“Artículo 111.- Homicidio Culposo

El que, por culpa, ocasiona la muerte de una persona, será reprimido con pena privativa de libertad no mayor de dos años o con prestación de servicios comunitarios de cincuenta y dos a ciento cuatro jornadas.

La pena privativa de la libertad será no menor de cuatro años ni mayor de ocho años e inhabilitación, según corresponda, conforme al Artículo 36 incisos 4), 6) y 7), cuando el agente haya estado conduciendo un vehículo motorizado bajo el efecto de estupefacientes o en estado de ebriedad, con presencia de alcohol en la sangre en proporción mayor de 0.5 gramos-litro, o cuando sean varias las víctimas del mismo hecho o el delito resulte de la inobservancia de reglas técnicas de tránsito. (*)

(*) Segundo párrafo modificado por el Artículo 1 de la Ley N° 29439, publicada el 19 noviembre 2009, cuyo texto es el siguiente:

"La pena privativa de la libertad será no menor de un año ni mayor de cuatro años si el delito resulta de la inobservancia de reglas de profesión, de ocupación o industria y no menor de un año ni mayor de seis años cuando sean varias las víctimas del mismo hecho."

La pena será no mayor de cuatro años si el delito resulta de la inobservancia de reglas de profesión, de ocupación o industria y cuando sean varias las víctimas del mismo hecho, la pena será no mayor de seis años." (*)

(*) Tercer párrafo modificado por el Artículo 1 de la Ley N° 29439, publicada el 19 noviembre 2009, cuyo texto es el siguiente:

"La pena privativa de la libertad será no menor de cuatro años ni mayor de ocho años e inhabilitación, según corresponda, conforme al artículo 36 -incisos 4), 6) y 7)-, si la muerte se comete utilizando vehículo motorizado o arma de fuego, estando el agente bajo el efecto de drogas tóxicas, estupefacientes, sustancias psicotrópicas o sintéticas, o con presencia de alcohol en la sangre en proporción mayor de 0.5 gramos-litro, en el caso de transporte particular, o mayor de 0.25 gramoslitro en el caso de transporte público de pasajeros, mercancías o carga en general, o cuando el delito resulte de la inobservancia de reglas técnicas de tránsito."

CONCORDANCIAS:     Ley N° 27753, Art. 3, 4 y Anexo

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 112 Homicidio piadoso
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, por piedad, mata a un enfermo incurable que le solicita de manera expresa y consciente para poner fin a sus intolerables dolores, será reprimido con pena privativa de libertad no mayor de tres años.

Artículo 113 Instigación o ayuda al suicidio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que instiga a otro al suicidio o lo ayuda a cometerlo, será reprimido, si el suicidio se ha consumado o intentado, con pena privativa de libertad no menor de uno ni mayor de cuatro años.

La pena será no menor de dos ni mayor de cinco años, si el agente actuó por un móvil egoísta.

CAPITULO  II ABORTO
###################

Artículo 114 Autoaborto
~~~~~~~~~~~~~~~~~~~~~~~

La mujer que causa su aborto, o consiente que otro le practique, será reprimida con pena privativa de libertad no mayor de dos años o con prestación de servicio comunitario de cincuentidós a ciento cuatro jornadas.

Artículo 115 Aborto consentido
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que causa el aborto con el consentimiento de la gestante, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

Si sobreviene la muerte de la mujer y el agente pudo prever este resultado, la pena será no menor de dos ni mayor de cinco años.

Artículo 116 Aborto sin consentimiento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que hace abortar a una mujer sin su consentimiento, será reprimido con pena privativa de libertad no menor de tres ni mayor de cinco años.

Si sobreviene la muerte de la mujer y el agente pudo prever este resultado, la pena será no menor de cinco ni mayor de diez años.

Artículo 117 Agravación de la pena por la calidad del sujeto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El médico, obstetra, farmacéutico, o cualquier profesional sanitario, que abusa de su ciencia o arte para causar el aborto, será reprimido con la pena de los artículos 115 y 116 e inhabilitación conforme al artículo 36, incisos 4 y 8.

Artículo 118 Aborto preterintencional
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, con violencia, ocasiona un aborto, sin haber tenido el propósito de causarlo, siendo notorio o constándole el embarazo, será reprimido con pena privativa de libertad no mayor de dos años, o con prestación de servicio comunitario de cincuentidós a ciento cuatro jornadas.

Artículo 119 Aborto terapeútico
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No es punible el aborto practicado por un médico con el consentimiento de la mujer embarazada o de su representante legal, si lo tuviere, cuando es el único medio para salvar la vida de la gestante o para evitar en su salud un mal grave y permanente.

CONCORDANCIAS:     R.M. N° 486-2014-MINSA (Aprueban la “Guía Técnica Nacional para la estandarización del procedimiento de la Atención Integral de la gestante en la Interrupción Voluntaria por Indicación
Terapéutica del Embarazo menor de 22 semanas con consentimiento informado en el marco de lo dispuesto en el artículo 119 del Código Penal”)
PROYECTO DE LEY N° 3839-2014-IC (Proyecto de Ley que despenaliza el aborto en los casos de embarazos a consecuencia de una violación sexual, inseminación artificial o transferencia de óvulos no
consentidas)

Artículo 120 Aborto sentimental y eugenésico
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El aborto será reprimido con pena privativa de libertad no mayor de tres meses:

1.Cuando el embarazo sea consecuencia de violación sexual fuera de matrimonio o inseminación artificial no consentida y ocurrida fuera de matrimonio, siempre que los hechos hubieren sido denunciados o investigados, cuando menos policialmente; o

CONCORDANCIAS:     PROYECTO DE LEY N° 3839-2014-IC (Proyecto de Ley que despenaliza el aborto en los casos de embarazos a consecuencia de una violación sexual, inseminación artificial o transferencia de óvulos no
consentidas)

2. Cuando es probable que el ser en formación conlleve al nacimiento graves taras físicas o psíquicas, siempre que exista diagnóstico médico.

CAPITULO III LESIONES
#####################

(*) De conformidad con Literal b) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del delito de lesiones, previsto en los artículos 121-B, 122 y 122-B, en concordancia con el artículo 124-B del presente Código Penal, cuando la víctima es una mujer agredida por su condición de tal, o niños, niñas o adolescentes.

(*) De conformidad con el Literal b) del Artículo 3 del Decreto Legislativo N° 1368, publicado el 29 julio 2018, el sistema  es competente para conocer las medidas de protección y las medidas cautelares que se dicten en el marco de la Ley Nº 30364, así como los procesos penales que se siguen por la comisión del delito de Lesiones, previstos en los artículos 121-B, 122, 122-B, en concordancia con el artículo 124-B del Código Penal, cuando la víctima es una mujer agredida por su condición de tal, niños, niñas o adolescentes.

Artículo 121 Lesiones graves
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que causa a otro daño grave en el cuerpo o en la salud, será reprimido con pena privativa de libertad no menor de tres ni mayor de ocho años. Se consideran lesiones graves:

1. Las que ponen en peligro inminente la vida de la víctima.

2. Las que mutilan un miembro u órgano principal del cuerpo o lo hacen  impropio para su función, causan a una persona incapacidad para el trabajo, invalidez o anomalía psíquica permanente o la desfiguran de manera grave y permanente.

3. Las que infieren cualquier otro daño a la integridad corporal, o a la salud  física o mental de una persona que requiera treinta o más días de asistencia o descanso, según prescripción facultativa.

Cuando la víctima muere a consecuencia de la lesión y si el agente pudo prever este resultado, la pena será no menor de cinco ni mayor de diez años.(*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28878, publicada el 17 agosto 2006, cuyo texto es el siguiente:

"Artículo 121.- Lesiones graves

El que causa a otro daño grave en el cuerpo o en la salud, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años. Se consideran lesiones graves:

1. Las que ponen en peligro inminente la vida de la víctima.

2. Las que mutilan un miembro u órgano principal del cuerpo o lo hacen impropio para su función, causan a una persona incapacidad para el trabajo, invalidez o anomalía psíquica permanente o la desfiguran de manera grave y permanente.

3. Las que infieren cualquier otro daño a la integridad corporal, o a la salud física o mental de una persona que requiera treinta o más días de asistencia o descanso, según prescripción facultativa.

Cuando la víctima muere a consecuencia de la lesión y si el agente pudo prever este resultado, la pena será no menor de cinco ni mayor de diez años.

Cuando la víctima es miembro de la Policía Nacional del Perú o de las Fuerzas Armadas, Magistrado del Poder Judicial o del Ministerio Público, en el cumplimiento de sus funciones, se aplicará pena privativa de libertad no menor de cinco años ni mayor de doce años. "(*)

(*) Artículo modificado por el Artículo 2 de la Ley Nº 30054, publicada el 30 junio 2013, cuyo texto es el siguiente:

"Artículo 121.- Lesiones graves

El que causa a otro daño grave en el cuerpo o en la salud, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años. Se consideran lesiones graves:

1. Las que ponen en peligro inminente la vida de la víctima.

2. Las que mutilan un miembro u órgano principal del cuerpo o lo hacen impropio para su función, causan a una persona incapacidad para el trabajo, invalidez o anomalía psíquica permanente o la desfiguran de manera grave y permanente.

3. Las que infieren cualquier otro daño a la integridad corporal, o a la salud física o mental de una persona que requiera treinta o más días de asistencia o descanso, según prescripción facultativa.

En estos supuestos, cuando la víctima es miembro de la Policía Nacional del Perú o de las Fuerzas Armadas, magistrado del Poder Judicial o del Ministerio Público, miembro del Tribunal Constitucional o autoridad elegida por mandato popular, en ejercicio de sus funciones o como consecuencia de ellas, se aplica pena privativa de libertad no menor de seis años ni mayor de doce años.

Cuando la víctima muere a consecuencia de la lesión y si el agente pudo prever este resultado, la pena será no menor de ocho ni mayor de doce años. En este caso, si la víctima es miembro de la Policía Nacional o de las Fuerzas Armadas, magistrado del Poder Judicial o del Ministerio Público, miembro del Tribunal Constitucional o autoridad elegida por mandato popular, en ejercicio de sus funciones o como consecuencia de ellas, se aplica pena privativa de libertad no menor de doce ni mayor de quince años." (*)

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 121.- Lesiones graves

El que causa a otro daño grave en el cuerpo o en la salud, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años. Se consideran lesiones graves:

1. Las que ponen en peligro inminente la vida de la víctima.

2. Las que mutilan un miembro u órgano principal del cuerpo o lo hacen impropio para su función, causan a una persona incapacidad para el trabajo, invalidez o anomalía psíquica permanente o la desfiguran de manera grave y permanente.

3. Las que infieren cualquier otro daño a la integridad corporal, o a la salud física o mental de una persona que requiera treinta o más días de asistencia o descanso, según prescripción facultativa.

En estos supuestos, cuando la víctima es miembro de la Policía Nacional del Perú o de las Fuerzas Armadas, magistrado del Poder Judicial o del Ministerio Público, miembro del Tribunal Constitucional o autoridad elegida por mandato popular, en ejercicio de sus funciones o como consecuencia de ellas, se aplica pena privativa de libertad no menor de seis años ni mayor de doce años.

Cuando la víctima muere a consecuencia de la lesión y si el agente pudo prever este resultado, la pena será no menor de ocho ni mayor de doce años. En este caso, si la víctima es miembro de la Policía Nacional o de las Fuerzas Armadas, magistrado del Poder Judicial o del Ministerio Público, miembro del Tribunal Constitucional o autoridad elegida por mandato popular, en ejercicio de sus funciones o como consecuencia de ellas, se aplica pena privativa de libertad no menor de quince ni mayor de veinte años."(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1323, publicado el 06 enero 2017, cuyo texto es el siguiente:

“Artículo 121.- Lesiones graves

El que causa a otro daño grave en el cuerpo o en la salud, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años. Se consideran lesiones graves:

1. Las que ponen en peligro inminente la vida de la víctima.

2. Las que mutilan un miembro u órgano principal del cuerpo o lo hacen impropio para su función, causan a una persona incapacidad para el trabajo, invalidez o anomalía psíquica permanente o la desfiguran de manera grave y permanente.

3. Las que infieren cualquier otro daño a la integridad corporal, o a la salud física o mental de una persona que requiera treinta o más días de asistencia o descanso según prescripción facultativa, o se determina un nivel grave o muy grave de daño psíquico.

4. La afectación psicológica generada como consecuencia de que el agente obligue a otro a presenciar cualquier modalidad de homicidio doloso, lesión dolosa o violación sexual, o pudiendo evitar esta situación no lo hubiera hecho.

En los supuestos 1, 2 y 3 del primer párrafo, la pena privativa de libertad será no menor de seis años ni mayor de doce años, cuando concurra cualquiera de las siguientes circunstancias agravantes:

1. La víctima es miembro de la Policía Nacional del Perú o de las Fuerzas Armadas, magistrado del Poder Judicial o del Ministerio Público, magistrado del Tribunal Constitucional, autoridad elegida por mandato popular, o servidor civil, y es lesionada en ejercicio de sus funciones o como consecuencia de ellas.

2. La víctima es menor de edad, adulta mayor o tiene discapacidad y el agente se aprovecha de dicha condición.

3. Para cometer el delito se hubiera utilizado cualquier tipo de arma, objeto contundente o instrumento que ponga en riesgo la vida de la víctima.

4. El delito se hubiera realizado con ensañamiento o alevosía.

Cuando la víctima muere a consecuencia de la lesión y el agente pudo prever este resultado, la pena será no menor de ocho ni mayor de doce años. En este caso, si la muerte se produce como consecuencia de cualquiera de las agravantes del segundo párrafo se aplica pena privativa de libertad no menor de quince ni mayor de veinte años.” (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 30819, publicada el 13 julio 2018, cuyo texto es el siguiente:

"Artículo 121.- Lesiones graves

El que causa a otro daño grave en el cuerpo o en la salud física o mental, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años.

Se consideran lesiones graves:

1. Las que ponen en peligro inminente la vida de la víctima.

2. Las que mutilan un miembro u órgano principal del cuerpo o lo hacen impropio para su función, causan a una persona incapacidad para el trabajo, invalidez o anomalía psíquica permanente o la desfiguran de manera grave y permanente.

3. Las que infieren cualquier otro daño a la integridad corporal, o a la salud física o mental de una persona que requiera veinte o más días de asistencia o descanso según prescripción facultativa, o se determina un nivel grave o muy grave de daño psíquico.

4. La afectación psicológica generada como consecuencia de que el agente obligue a otro a presenciar cualquier modalidad de homicidio doloso, lesión dolosa o violación sexual, o pudiendo evitar esta situación no lo hubiera hecho.

Cuando la víctima muere a consecuencia de la lesión y el agente pudo prever este resultado, la pena será no menor de seis ni mayor de doce años.

En los supuestos 1, 2 y 3 del primer párrafo, la pena privativa de libertad será no menor de seis años ni mayor de doce años cuando concurra cualquiera de las siguientes circunstancias agravantes:

1. La víctima es miembro de la Policía Nacional del Perú o de las Fuerzas Armadas, magistrado del Poder Judicial o del Ministerio Público, magistrado del Tribunal Constitucional, autoridad elegida por mandato popular, o servidor civil, y es lesionada en ejercicio de sus funciones o como consecuencia de ellas.

2. La víctima es menor de edad, adulta mayor o tiene discapacidad y el agente se aprovecha de dicha condición.

3. Para cometer el delito se hubiera utilizado cualquier tipo de arma, objeto contundente o instrumento que ponga en riesgo la vida de la víctima.

4. El delito se hubiera realizado con ensañamiento o alevosía.

En este caso, si la muerte se produce como consecuencia de cualquiera de las agravantes del segundo párrafo se aplica pena privativa de libertad no menor de quince ni mayor de veinte años." (*)

(*) De conformidad con la Quinta Disposición Complementaria Modificatoria del Decreto de Urgencia N° 019-2020, publicado el 24 enero 2020, se modifica el numeral 1 del segundo párrafo del presente artículo, quedando redactado el artículo 121 de la siguiente manera:

“Artículo 121.- Lesiones graves

El que causa a otro daño grave en el cuerpo o en la salud física o mental, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años.

Se consideran lesiones graves:

1. Las que ponen en peligro inminente la vida de la víctima.

2. Las que mutilan un miembro u órgano principal del cuerpo o lo hacen impropio para su función, causan a una persona incapacidad para el trabajo, invalidez o anomalía psíquica permanente o la desfiguran de manera grave y permanente.

3. Las que infieren cualquier otro daño a la integridad corporal, o a la salud física o mental de una persona que requiera veinte o más días de asistencia o descanso según prescripción facultativa, o se determina un nivel grave o muy grave de daño psíquico.

4. La afectación psicológica generada como consecuencia de que el agente obligue a otro a presenciar cualquier modalidad de homicidio doloso, lesión dolosa o violación sexual, o pudiendo evitar esta situación no lo hubiera hecho.

Cuando la víctima muere a consecuencia de la lesión y el agente pudo prever este resultado, la pena será no menor de seis ni mayor de doce años.

En los supuestos 1, 2 y 3 del primer párrafo, la pena privativa de libertad será no menor de seis años ni mayor de doce años cuando concurra cualquiera de las siguientes circunstancias agravantes:

1. La víctima es miembro de la Policía Nacional del Perú o de las Fuerzas Armadas, magistrado del Poder Judicial o del Ministerio Público, magistrado del Tribunal Constitucional, autoridad elegida por mandato popular, servidor civil o autoridad administrativa relacionada con el transporte, tránsito terrestre o los servicios complementarios relacionados con dichas materias y es lesionada en ejercicio de sus funciones o como consecuencia de ellas.

2. La víctima es menor de edad, adulta mayor o tiene discapacidad y el agente se aprovecha de dicha condición.

3. Para cometer el delito se hubiera utilizado cualquier tipo de arma, objeto contundente o instrumento que ponga en riesgo la vida de la víctima.

4. El delito se hubiera realizado con ensañamiento o alevosía.

En este caso, si la muerte se produce como consecuencia de cualquiera de las agravantes del segundo párrafo se aplica pena privativa de libertad no menor de quince ni mayor de veinte años.”

CONCORDANCIAS:     D.S. Nº 016-2009-MTC, Décima Primera Disp. Complem. y Trans. (Grado de lesiones)

R.M.N° 552-2017-IN, Art. 8 (De las lesiones graves y leves)
JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Formas agravadas. El menor como víctima

"Artículo 121- A.- En los casos previstos en la primera parte del artículo anterior, cuando la víctima sea menor de catorce años y el agente sea el padre, madre, tutor, guardador o responsable de aquel, la pena será privativa de libertad no menor de cinco ni mayor de diez años, suspensión de la patria potestad según el literal b) del Artículo 83 del Código de los Niños y Adolescentes e inhabilitación a que se refiere el Artículo 36 inciso 5.

Igual pena se aplicará cuando el agente sea el cónyuge, conviviente, ascendiente, descendiente natural o adoptivo, o pariente colateral de la víctima.

Cuando la víctima muera a consecuencia de la lesión y el agente pudo prever este resultado, la pena será no menor de seis ni mayor de quince años." (1)(2)

CONCORDANCIAS:     Ley  Nº 27337, Art. 75 (Suspensión de la Patria Potestad)

(1) Artículo incorporado por el Artículo 1 de la Ley N° 26788, publicada el 16 mayo 1997.

(2) Artículo modificado por el Artículo 9 de la Ley N° 29282, publicada el 27 noviembre 2008, cuyo texto es el siguiente:

Artículo 121 “Formas agravadas. El menor como víctima
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- En los casos previstos en la primera parte del artículo 121, cuando la víctima sea menor de catorce años y el agente sea el tutor, guardador o responsable de aquel, la pena será privativa de libertad no menor de cinco ni mayor de ocho años, remoción del cargo según el numeral 2 del artículo 554 del Código Civil e inhabilitación a que se refiere el artículo 36 inciso 5.

Cuando la víctima muere a consecuencia de la lesión y el agente pudo prever este resultado, la pena será no menor de seis ni mayor de doce años.”(*)

(*) Artículo modificado por el Artículo Único de la Ley Nº 29699, publicada el 04 junio 2011, cuyo texto es el siguiente:

“Artículo 121-A.- Formas agravadas. Lesiones graves cuando la víctima es un menor

En los casos previstos en la primera parte del artículo 121, cuando la víctima sea menor de catorce años, la pena es privativa de libertad no menor de cinco ni mayor de diez años.

Cuando el agente sea el tutor o responsable del menor, procede además su remoción del cargo según el numeral 2 del artículo 554 del Código Civil e inhabilitación conforme a lo dispuesto en el inciso 5 del artículo 36 del presente Código.

Cuando la víctima muere a consecuencia de la lesión y el agente pudo prever ese resultado, la pena será no menor de seis ni mayor de doce años." (*)

(*) Artículo modificado por la Primera Disposición Complementaria Modificatoria de la Ley N° 30364, publicada el 23 noviembre 2015, cuyo texto es el siguiente:

"Artículo 121-A. Formas agravadas. Lesiones graves cuando la víctima es menor de edad, de la tercera edad o persona con discapacidad

En los casos previstos en la primera parte del artículo 121, cuando la víctima sea menor de edad, mayor de sesenta y cinco años o sufre discapacidad física o mental y el agente se aprovecha de dicha condición se aplica pena privativa de libertad no menor de seis ni mayor de doce años.

Cuando la víctima muere a consecuencia de la lesión y el agente pudo prever ese resultado, la pena será no menor de doce ni mayor de quince años." (*)

(*) Artículo derogado por la Única Disposición Complementaria Derogatoria del Decreto Legislativo N° 1323, publicado el 06 enero 2017.

Artículo 121 “Formas agravadas. Lesiones graves por violencia familiar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- El que causa a otro daño grave en el cuerpo o en la salud por violencia familiar será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años y suspensión de la patria potestad según el literal e) del artículo 75 del Código de los Niños y Adolescentes.

Cuando la víctima muere a consecuencia de la lesión y el agente pudo prever este resultado, la pena será no menor de seis ni mayor de quince años.” (1)(2)

(1) Artículo incorporado por el Artículo 10 de la Ley N° 29282, publicada el 27 noviembre 2008.

(2) Artículo modificado por la Primera Disposición Complementaria Modificatoria de la Ley N° 30364, publicada el 23 noviembre 2015, cuyo texto es el siguiente:

"Artículo 121-B.- Formas agravadas. Lesiones graves por violencia contra la mujer y su entorno familiar

En los casos previstos en la primera parte del artículo 121 se aplica pena privativa de libertad no menor de seis ni mayor de doce años cuando la víctima:

1. Es mujer y es lesionada por su condición de tal en cualquiera de los contextos previstos en el primer párrafo del artículo 108-B.

2. Es ascendiente, descendiente, natural o adoptivo, cónyuge o conviviente del agente.

3. Depende o está subordinado.

Cuando la víctima muere a consecuencia de la lesión y el agente pudo prever ese resultado, la pena será no menor de doce ni mayor de quince años."(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1323, publicado el 06 enero 2017, cuyo texto es el siguiente:

“Artículo 121-B.- Lesiones graves por violencia contra las mujeres e integrantes del grupo familiar

En los supuestos previstos en el primer párrafo del artículo 121 se aplica pena privativa de libertad no menor de seis ni mayor de doce años e inhabilitación conforme al artículo 36, cuando:

1. La víctima es mujer y es lesionada por su condición de tal en cualquiera de los contextos previstos en el primer párrafo del artículo 108-B.

2. La víctima se encuentra en estado de gestación;

3. La víctima es el padrastro; madrastra; ascendiente o descendiente por consanguinidad, adopción o por afinidad; pariente colateral hasta el cuarto grado de consanguinidad o adopción, o segundo grado de afinidad; habita en el mismo hogar, siempre que no medien relaciones contractuales o laborales, o la violencia se da en cualquiera de los contextos de los numeral 1, 2 y 3 del primer párrafo del artículo 108-B.

4. La víctima mantiene cualquier tipo de relación de dependencia o subordinación sea de autoridad, económica, laboral o contractual y el agente se hubiera aprovechado de esta situación.

5. Para cometer el delito se hubiera utilizado cualquier tipo de arma, objeto contundente o instrumento que ponga en riesgo la vida de la víctima.

6. El delito se hubiera realizado con ensañamiento o alevosía.

7. Cuando la afectación psicológica a la que se hace referencia en el numeral 4 del primer párrafo del artículo 121, se causa a los hijos, hijas, niñas, niños o adolescentes bajo el cuidado de la víctima de feminicidio, de lesiones en contextos de violencia familiar o de violación sexual.

Cuando la víctima muere a consecuencia de la lesión y el agente pudo prever ese resultado, la pena será no menor de quince ni mayor de veinte años." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 30819, publicada el 13 julio 2018, cuyo texto es el siguiente:

"Artículo 121-B.- Lesiones graves por violencia contra las mujeres e integrantes del grupo familiar

En los supuestos previstos en el primer párrafo del artículo 121 se aplica pena privativa de libertad no menor de seis ni mayor de doce años e inhabilitación conforme a los numerales 5 y 11 del artículo 36 del presente Código y los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda, cuando:

1. La víctima es mujer y es lesionada por su condición de tal en cualquiera de los contextos previstos en el primer párrafo del artículo 108-B.

2. La víctima se encuentra en estado de gestación.

3. La víctima es cónyuge; excónyuge; conviviente; exconviviente; padrastro; madrastra; ascendiente o descendiente por consanguinidad, adopción o afinidad; pariente colateral del cónyuge y conviviente hasta el cuarto grado de consanguinidad y segundo de afinidad; habita en el mismo hogar, siempre que no medien relaciones contractuales o laborales; o es con quien se ha procreado hijos en común, independientemente de que se conviva o no al momento de producirse los actos de violencia, o la violencia se da en cualquiera de los contextos de los numerales 1, 2 y 3 del primer párrafo del artículo 108-B.

4. La víctima mantiene cualquier tipo de relación de dependencia o subordinación sea de autoridad, económica, cuidado, laboral o contractual y el agente se hubiera aprovechado de esta situación.

5. Para cometer el delito se hubiera utilizado cualquier tipo de arma, objeto contundente o instrumento que ponga en riesgo la vida de la víctima.

6. El delito se hubiera realizado en cualquiera de las circunstancias del artículo 108.

7. La afectación psicológica a la que se hace referencia en el numeral 4 del primer párrafo del artículo 121, se causa a cualquier niña, niño o adolescente en contextos de violencia familiar o de violación sexual.

8. Si el agente actúa en estado de ebriedad, con presencia de alcohol en la sangre en proporción mayor de 0.25 gramos-litro, o bajo efecto de drogas tóxicas, estupefacientes, sustancias psicotrópicas o sintéticas.

La pena será no menor de doce ni mayor de quince años cuando concurran dos o más circunstancias agravantes.

Cuando la víctima muere a consecuencia de cualquiera de las agravantes y el agente pudo prever ese resultado, la pena será no menor de quince ni mayor de veinte años." (*)

(*) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

Artículo 122 Lesiones leves
~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que causa a otro un daño en el cuerpo o en la salud que requiera más de diez y menos de treinta días de asistencia o descanso, según prescripción facultativa, será reprimido con pena privativa de libertad no mayor de dos años y con sesenta a ciento cincuenta días-multa.

Cuando la víctima muere a consecuencia de la lesión y el agente pudo prever este resultado, la pena será no menor de tres ni mayor de seis años.(*)

(*) Artículo modificado por la Primera Disposición Complementaria Modificatoria de la Ley N° 30364, publicada el 23 noviembre 2015, cuyo texto es el siguiente:

"Artículo 122. Lesiones leves

1. El que causa a otro lesiones en el cuerpo o en la salud que requiera más de diez y menos de treinta días de asistencia o descanso, o nivel moderado de daño psíquico, según prescripción facultativa, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años.

2. La pena será privativa de libertad no menor de seis ni mayor de doce años si la víctima muere como consecuencia de la lesión prevista en el párrafo 1 y el agente pudo prever ese resultado.

3. La pena será privativa de libertad no menor de tres ni mayor de seis años si la víctima:

a. Es miembro de la Policía Nacional del Perú o de las Fuerzas Armadas, magistrado del Poder Judicial, del Ministerio Público o del Tribunal Constitucional o autoridad elegida por mandato popular o funcionario o servidor público y es lesionada en el ejercicio de sus funciones oficiales o como consecuencia de ellas.

b. Es menor de edad, mayor de sesenta y cinco años o sufre de discapacidad física o mental y el agente se aprovecha de dicha condición.

c. Es mujer y es lesionada por su condición de tal, en cualquiera de los contextos previstos en el primer párrafo del artículo 108-B.

d. Es ascendiente, descendiente, natural o adoptivo, cónyuge o conviviente del agente.

e. Depende o está subordinada de cualquier forma al agente.

4. La pena privativa de libertad será no menor de ocho ni mayor de catorce años si la víctima muere como consecuencia de la lesión a que se refiere el párrafo 3 y el agente pudo prever ese resultado.

5. El juez impone la inhabilitación correspondiente a los supuestos previstos en el párrafo 3."(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1323, publicado el 06 enero 2017, cuyo texto es el siguiente:

“Artículo 122. Lesiones leves

1. El que causa a otro lesiones en el cuerpo o en la salud que requiera más de diez y menos de treinta días de asistencia o descanso, según prescripción facultativa, o nivel moderado de daño psíquico, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años.

2. La pena será privativa de libertad no menor de seis ni mayor de doce años si la víctima muere como consecuencia de la lesión prevista en el párrafo precedente y el agente pudo prever ese resultado.

3. La pena será privativa de libertad no menor de tres ni mayor de seis años e inhabilitación de acuerdo al artículo 36, cuando:

a. La víctima es miembro de la Policía Nacional del Perú o de las Fuerzas Armadas, magistrado del Poder Judicial, del Ministerio Público o del Tribunal Constitucional o autoridad elegida por mandato popular o servidor civil y es lesionada en el ejercicio de sus funciones oficiales o como consecuencia de ellas.

b. La víctima es menor de edad, adulta mayor o tiene discapacidad y el agente se aprovecha de dicha condición.

c. La víctima es mujer y es lesionada por su condición de tal, en cualquiera de los contextos previstos en el primer párrafo del artículo 108-B.

d. La víctima se encontraba en estado de gestación;

e. La víctima es el padrastro; madrastra; ascendiente o descendiente por consanguinidad, adopción o por afinidad; pariente colateral hasta el cuarto grado de consanguinidad o adopción, o segundo grado de afinidad; habita en el mismo hogar, siempre que no medien relaciones contractuales o laborales, y la violencia se da en cualquiera de los contextos de los numerales 1, 2 y 3 del primer párrafo del artículo 108-B.

f. La víctima mantiene cualquier tipo de relación de dependencia o subordinación sea de autoridad, económica, laboral o contractual y el agente se hubiera aprovechado de esta situación.

g. Para cometer el delito se hubiera utilizado cualquier tipo de arma, objeto contundente o instrumento que ponga en riesgo la vida de la víctima.

h. El delito se hubiera realizado con ensañamiento o alevosía.

4. La pena privativa de libertad será no menor de ocho ni mayor de catorce años si la víctima muere como consecuencia de la lesión a que se refiere el párrafo 3 y el agente pudo prever ese resultado.” (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 30819, publicada el 13 julio 2018, cuyo texto es el siguiente:

"Artículo 122.- Lesiones leves

1. El que causa a otro lesiones en el cuerpo o en la salud física o mental que requiera más de diez y menos de veinte días de asistencia o descanso, según prescripción facultativa, o nivel moderado de daño psíquico, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años.

2. La pena privativa de libertad será no menor de seis ni mayor de doce años si la víctima muere como consecuencia de la lesión prevista en el párrafo precedente y el agente pudo prever ese resultado.

3. La pena privativa de libertad será no menor de tres ni mayor de seis años e inhabilitación conforme a los numerales 5 y 11 del artículo 36 del presente Código y los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda, cuando:

a. La víctima es miembro de la Policía Nacional del Perú o de las Fuerzas Armadas, magistrado del Poder Judicial, del Ministerio Público o del Tribunal Constitucional o autoridad elegida por mandato popular o servidor civil y es lesionada en el ejercicio de sus funciones oficiales o como consecuencia de ellas.

b. La víctima es menor de edad, adulta mayor o tiene discapacidad y el agente se aprovecha de dicha condición.

c. La víctima es mujer y es lesionada por su condición de tal, en cualquiera de los contextos previstos en el primer párrafo del artículo 108-B.

d. La víctima se encontraba en estado de gestación.

e. La víctima es el cónyuge; excónyuge; conviviente; exconviviente; padrastro; madrastra; ascendiente o descendiente por consanguinidad, adopción o afinidad; pariente colateral del cónyuge y conviviente hasta el cuarto grado de consanguinidad y segundo de afinidad; habita en el mismo hogar, siempre que no medien relaciones contractuales o laborales; o es con quien se ha procreado hijos en común, independientemente de que se conviva o no al momento de producirse los actos de violencia, o la violencia se da en cualquiera de los contextos de los numerales 1, 2 y 3 del primer párrafo del artículo 108-B.

f. La víctima mantiene cualquier tipo de relación de dependencia o subordinación sea de autoridad, económica, cuidado, laboral o contractual y el agente se hubiera aprovechado de esta situación.

g. Para cometer el delito se hubiera utilizado cualquier tipo de arma, objeto contundente o instrumento que ponga en riesgo la vida de la víctima.

h. El delito se hubiera realizado con ensañamiento o alevosía.

i. Si el agente actúa en estado de ebriedad, con presencia de alcohol en la sangre en proporción mayor de 0.25 gramos-litro, o bajo efecto de drogas tóxicas, estupefacientes, sustancias psicotrópicas o sintéticas.

4. La pena privativa de libertad será no menor de ocho ni mayor de catorce años si la víctima muere como consecuencia de la lesión a que se refiere el párrafo 3 y el agente pudo prever ese resultado." (*)

(*) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

CONCORDANCIAS:     D.S. Nº 016-2009-MTC, Décima Primera Disp. Complem. y Trans. (Grado de lesiones)

Formas agravadas. El menor como víctima

"Artículo 122 A.- En el caso previsto en la primera parte del artículo anterior, cuando la víctima sea menor de catorce años y el agente sea el padre, madre, tutor, guardador o responsable de aquel, la pena será privativa de libertad no menor de tres ni mayor de seis años, suspensión de la patria potestad según el literal b) del Artículo 83 del Código de los Niños y Adolescentes e inhabilitación a que se refiere el Artículo 36 inciso 5.

Igual pena se aplicará cuando el agente sea el cónyuge, conviviente, ascendiente, descendiente natural o adoptivo, o pariente colateral de la víctima.

Cuando la víctima muera a consecuencia de la lesión y el agente pudo prever este resultado, la pena será no menor de cuatro ni mayor de ocho años." (1)(2)

CONCORDANCIAS:     Ley  Nº 27337, Art. 75 (Suspensión de la Patria Potestad)

(1) Artículo incorporado por el Artículo 1 de la Ley N° 26788, publicada el 16 mayo 1997.

(2) Artículo modificado por el Artículo 11 de la Ley N° 29282, publicada el 27 noviembre 2008, cuyo texto es el siguiente:

Artículo 122 “Formas agravadas. El menor como víctima
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- En el caso previsto en la primera parte del artículo 122, cuando la víctima sea menor de catorce años y el agente sea el tutor, guardador o responsable de aquel, la pena será privativa de libertad no menor de tres ni mayor de seis años, remoción del cargo según el numeral 2 del artículo 554 del Código Civil e inhabilitación a que se refiere el artículo 36 inciso 5.

Cuando la víctima muere a consecuencia de la lesión y el agente pudo prever este resultado, la pena será no menor de cinco ni mayor de nueve años.”(*)

(*) Artículo modificado por el Artículo Único de la Ley Nº 29699, publicada el 04 junio 2011, cuyo texto es el siguiente:

"Artículo 122-A.- Formas agravadas. Lesiones leves cuando la víctima es un menor

En el caso previsto en la primera parte del artículo 122, cuando la víctima sea menor de catorce años, la pena es privativa de libertad no menor de tres ni mayor de seis años.

Cuando el agente sea el tutor o responsable del menor, procede además su remoción del cargo según el numeral 2 del artículo 554 del Código Civil e inhabilitación conforme a lo dispuesto en el inciso 5 del artículo 36 del presente Código.

Cuando la víctima muere a consecuencia de la lesión y el agente pudo prever ese resultado, la pena será no menor de cinco ni mayor de nueve años.”(*)

(*) Artículo derogado por la Primera Disposición Complementaria Derogatoria de la Ley N° 30364, publicada el 23 noviembre 2015.

Artículo 122 “Formas agravadas. Lesiones leves por violencia familiar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- El que causa a otro daño en el cuerpo o en la salud por violencia familiar que requiera más de diez y menos de treinta días de asistencia o descanso, según prescripción facultativa, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años y suspensión de la patria potestad según el literal e) del artículo 75 del Código de los Niños y Adolescentes.

Cuando la víctima muere a consecuencia de la lesión y el agente pudo prever este resultado, la pena será no menor de seis ni mayor de doce años.” (1)(2)

(1) Artículo incorporado por el Artículo 12 de la Ley N° 29282, publicada el 27 noviembre 2008.

(2) Artículo derogado por la Primera Disposición Complementaria Derogatoria de la Ley N° 30364, publicada el 23 noviembre 2015.

“Artículo 122-B.- Agresiones en contra de las mujeres o integrantes del grupo familiar

El que de cualquier modo cause lesiones corporales a una mujer por su condición de tal o a integrantes del grupo familiar que requieran menos de diez días de asistencia o descanso, o algún tipo de afectación psicológica, cognitiva o conductual en cualquiera de los contextos previstos en el primer párrafo del artículo 108-B, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años e inhabilitación conforme al artículo 36.

La pena será no menor de dos ni mayor de tres años, cuando en los supuestos del primer párrafo se presenten las siguientes agravantes:

1. Se utiliza cualquier tipo de arma, objeto contundente o instrumento que ponga en riesgo la vida de la víctima.

2. El hecho se comete con ensañamiento o alevosía.

3. La víctima se encuentra en estado de gestación.

4. La víctima es menor de edad, adulta mayor o tiene discapacidad y el agente se aprovecha de dicha condición.”(*)(**)

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1323, publicado el 06 enero 2017.

(**) Artículo modificado por el Artículo 1 de la Ley N° 30819, publicada el 13 julio 2018, cuyo texto es el siguiente:

"Artículo 122-B.- Agresiones en contra de las mujeres o integrantes del grupo familiar

El que de cualquier modo cause lesiones corporales que requieran menos de diez días de asistencia o descanso según prescripción facultativa, o algún tipo de afectación psicológica, cognitiva o conductual que no califique como daño psíquico a una mujer por su condición de tal o a integrantes del grupo familiar en cualquiera de los contextos previstos en el primer párrafo del artículo 108-B, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años e inhabilitación conforme a los numerales 5 y 11 del artículo 36 del presente Código y los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda.

La pena será no menor de dos ni mayor de tres años, cuando en los supuestos del primer párrafo se presenten las siguientes agravantes:

1. Se utiliza cualquier tipo de arma, objeto contundente o instrumento que ponga en riesgo la vida de la víctima.

2. El hecho se comete con ensañamiento o alevosía.

3. La víctima se encuentra en estado de gestación.

4. La víctima es menor de edad, adulta mayor o tiene discapacidad o si padeciera de enfermedad en estado terminal y el agente se aprovecha de dicha condición.

5. Si en la agresión participan dos o más personas.

6. Si se contraviene una medida de protección emitida por la autoridad competente.

7. Si los actos se realizan en presencia de cualquier niña, niño o adolescente."(*)

(*) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

Artículo 123 Lesiones preterintencionales con resultado fortuito
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando el agente produzca un resultado grave que no quiso causar, ni pudo prever, la pena será disminuida prudencialmente hasta la que corresponda a la lesión que quiso inferir.

Artículo 124 Lesiones  culposas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, por culpa causa a otro un daño en el cuerpo o en la salud, será reprimido, por acción privada, con pena privativa de libertad no mayor de un año, o con sesenta a ciento veinte días-multa.

La acción penal se promoverá de oficio y la pena será privativa de libertad no menor de uno ni mayor de dos años y de sesenta a ciento veinte días-multa, si la lesión es grave.

El Juez podrá acumular la multa con la pena privativa de libertad. (*)

(*) Artículo modificado por el Artículo Unico de la Ley Nº 27054, publicada el 23 enero 1999, cuyo texto es el siguiente:

Lesiones  culposas

"Artículo 124.- El que por culpa causa a otro un daño en el cuerpo o en la salud, será reprimido, por acción privada, con pena privativa de libertad no mayor de un año y con sesenta a ciento veinte días-multa.

La acción penal se promoverá de oficio y la pena será privativa de libertad no menor de uno ni mayor de dos años y de sesenta a ciento veinte días-multa, si la lesión es grave.

Cuando son varias las víctimas del mismo hecho o el delito resulte de la inobservancia de reglas técnicas, de profesión, de ocupación o de industria, la pena privativa de libertad será no menor de dos ni mayor de cuatro años e inhabilitación conforme al Artículo 36 incisos 4), 6) y 7)." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 27753, publicada el 09 junio 2002, cuyo texto es el siguiente:

"Artículo 124.- Lesiones Culposas

El que por culpa causa a otro un daño en el cuerpo o en la salud, será reprimido, por acción privada, con pena privativa de libertad no mayor de un año y con sesenta a ciento veinte días-multa.

La acción penal se promoverá de oficio y la pena será privativa de libertad no menor de uno ni mayor de dos años y de sesenta a ciento veinte días-multa, si la lesión es grave. (*)

(*) Párrafo modificado por el Artículo 1 de la Ley N° 29439, publicada el 19 noviembre 2009, cuyo texto es el siguiente:

"La pena será privativa de libertad no menor de uno ni mayor de dos años y de sesenta a ciento veinte días-multa, si la lesión es grave, de conformidad a los presupuestos establecidos en el artículo 121. "

La pena privativa de la libertad será no menor de tres años ni mayor de cinco años e inhabilitación, según corresponda, conforme al Artículo 36 incisos 4), 6) y 7), cuando el agente haya estado conduciendo un vehículo motorizado bajo el efecto de estupefacientes o en estado de ebriedad, con presencia de alcohol en la sangre en proporción mayor de 0.5 gramos-litro, o cuando sean varias las víctimas del mismo hecho o el delito resulte de la inobservancia de reglas técnicas de tránsito.(*)

(*) Párrafo modificado por el Artículo 1 de la Ley N° 29439, publicada el 19 noviembre 2009, cuyo texto es el siguiente:

"La pena privativa de libertad será no menor de uno ni mayor de tres años si el delito resulta de la inobservancia de reglas de profesión, ocupación o industria y no menor de un año ni mayor de cuatro años cuando sean varias las víctimas del mismo hecho."

La pena será no mayor de tres años si el delito resulta de la inobservancia de reglas de profesión, de ocupación o industria y cuando sean varias las víctimas del mismo hecho, la pena será no mayor de cuatro años." (*)

(*) Párrafo modificado por el Artículo 1 de la Ley N° 29439, publicada el 19 noviembre 2009, cuyo texto es el siguiente:

"La pena privativa de la libertad será no menor de cuatro años ni mayor de seis años e inhabilitación, según corresponda, conforme al artículo 36 -incisos 4), 6) y 7)-, si la lesión se comete utilizando vehículo motorizado o arma de fuego, estando el agente bajo el efecto de drogas tóxicas, estupefacientes, sustancias psicotrópicas o sintéticas, o con presencia de alcohol en la sangre en proporción mayor de 0.5 gramos-litro, en el caso de transporte particular, o mayor de 0.25 gramoslitro en el caso de transporte público de pasajeros, mercancías o carga en general, o cuando el delito resulte de la inobservancia de reglas técnicas de tránsito."

CONCORDANCIAS:     Ley N° 27753, Art. 3 (Tasas de alcoholemia en aire espirado), 4 (Tabla de Alcoholemia)

Daños al Concebido

"Artículo 124-A.- El que causa daño en el cuerpo o en la salud del concebido, será reprimido con pena privativa de la libertad no menor de un año ni mayor de tres" (*)

(*) Artículo incorporado por el Artículo 1 de la Ley N° 27716, publicada el  08-05-2002.

"Artículo 124-B. Determinación de la lesión psicológica

El nivel de la lesión psicológica es determinado mediante valoración realizada de conformidad con el instrumento técnico oficial especializado que orienta la labor pericial, con la siguiente equivalencia:

a. Falta de lesiones leves: nivel leve de daño psíquico.

b. Lesiones leves: nivel moderado de daño psíquico.

c. Lesiones graves: nivel grave o muy grave de daño psíquico”.(*)

(*) Artículo incorporado por la Segunda Disposición Complementaria Modificatoria de la Ley N° 30364, publicada el 23 noviembre 2015.

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1323, publicado el 06 enero 2017, cuyo texto es el siguiente:

“Artículo 124-B. Del daño psíquico y la afectación psicológica, cognitiva o conductual

El nivel del daño psíquico es determinado a través de un examen pericial o cualquier otro medio idóneo, con la siguiente equivalencia:

a. Falta de lesiones leves: nivel leve de daño psíquico.

b. Lesiones leves: nivel moderado de daño psíquico.

c. Lesiones graves: nivel grave o muy grave de daño psíquico.

La afectación psicológica, cognitiva o conductual, puede ser determinada a través de un examen pericial o cualquier otro elemento probatorio objetivo similar al que sea emitido por entidades públicas o privadas especializadas en la materia, sin someterse a la equivalencia del daño psíquico.”

CAPITULO  IV EXPOSICION A PELIGRO O ABANDONO DE PERSONAS EN PELIGRO
###################################################################

Artículo 125 Exposición o abandono peligrosos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que expone a peligro de muerte o de grave e inminente daño a la salud o abandona en iguales circunstancias a un menor de edad o a una persona incapaz de valerse por sí misma que estén legalmente bajo su protección o que se hallen de hecho bajo su cuidado, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

Si resulta lesión grave o muerte y éstas pudieron ser previstas, la pena será no menor de tres ni mayor de cinco años en caso de lesión grave, y no menor de cuatro ni mayor de ocho años en caso de muerte. (*)

(*) Artículo modificado por el Artículo 2 de la Ley Nº 26926, publicada el 21 febrero 1998, cuyo texto es el siguiente:

Exposición o abandono peligrosos

"Artículo 125.- El que expone a peligro de muerte o de grave e inminente daño a la salud o abandona en iguales circunstancias a un menor de edad o a una persona incapaz de valerse por sí misma que estén legalmente bajo su protección o que se hallen de hecho bajo su cuidado, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años."

Artículo 126 Omisión de socorro y exposición a peligro
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que omite prestar socorro a una persona que ha herido o incapacitado, poniendo en peligro su vida o su salud, será reprimido con pena privativa de libertad no mayor de tres años.

Artículo 127 Omisión de auxilio o aviso a la autoridad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que encuentra a un herido o a cualquier otra persona en estado de grave e inminente peligro y omite prestarle auxilio inmediato pudiendo hacerlo sin riesgo propio o de tercero o se abstiene de dar aviso a la autoridad, será reprimido con pena privativa de libertad no mayor de un año o con treinta a ciento veinte días-multa.

Artículo 128 Exposición a peligro de persona dependiente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que expone a peligro la vida o la salud de una persona colocada bajo su autoridad, dependencia, tutela, curatela o vigilancia, sea privándola de alimentos o cuidados indispensables, sea sometiéndola a trabajos excesivos o inadecuados o abusando de los medios de corrección o disciplina, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

Si resulta lesión grave o muerte y éstas pudieran ser previstas, la pena será no menor de tres ni mayor de seis años en caso de lesión grave y no menor de cuatro ni mayor de ocho años en caso de muerte. (*)

(1) Artículo modificado por el Artículo 2 de la Ley Nº 26926, publicada el 21 febrero 1998, cuyo texto es el siguiente:

Artículo 128 Exposición a peligro de persona dependiente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que expone a peligro la vida o la salud de una persona colocada bajo su  autoridad, dependencia, tutela, curatela o vigilancia, sea privándola de alimentos o cuidados indispensables, sea sometiéndola a trabajos excesivos o inadecuados o abusando de los medios de corrección o disciplina, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años. (*)

(*) Artículo modificado por la Segunda Disposición Final de la Ley N° 28190, publicada el 18 marzo 2004, cuyo texto es el siguiente:

Exposición a peligro de persona dependiente

“Artículo 128.- El que expone a peligro la vida o la salud de una persona colocada bajo su autoridad, dependencia, tutela, curatela o vigilancia, sea privándola de alimentos o cuidados indispensables, sea sometiéndola a trabajos excesivos, inadecuados, sea abusando de los medios de corrección o disciplina, sea obligándola o induciéndola a mendigar en lugares públicos, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

En los casos en que el agente tenga vínculo de parentesco consanguíneo o la víctima fuere menor de doce años de edad, la pena será privativa de libertad no menor de dos ni mayor de cuatro años.

En los casos en que el agente obligue o induzca a mendigar a dos o más personas colocadas bajo su autoridad, dependencia, tutela, curatela o vigilancia, la pena privativa de libertad será no menor de dos ni mayor de cinco años.” (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1351, publicado el 07 enero 2017, cuyo texto es el siguiente:

“Artículo 128.- Exposición a peligro de persona dependiente

El que expone a peligro la vida o la salud de una persona colocada bajo su autoridad, dependencia, tutela, curatela o vigilancia, sea privándola de alimentos o cuidados indispensables, sea abusando de los medios de corrección o disciplina, o cualquier acto análogo, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

En los casos en que el agente tenga vínculo de parentesco consanguíneo o la víctima fuere menor de catorce años de edad, la pena será privativa de libertad no menor de dos ni mayor de cuatro años.

Si se produce lesión grave o muerte de la víctima, la pena será no menor de cuatro ni mayor de ocho años.”

CONCORDANCIAS:     Ley N° 28970 (Ley que crea el Registro de Deudores Alimentarios Morosos)
R.A. Nº 136-2007-CE-PJ (Crean el Registro de Deudores Alimentarios Morosos -REDAM y aprueban Directiva)

CAPITULO V GENOCIDIO (*)
########################

(*) Capítulo V derogado por el Artículo 6 de la Ley Nº 26926, publicada el 21 febrero 1998. Debe tenerse en cuenta que solo se deroga la mención al Capítulo V y su sumilla "Genocidio", mas no el texto del Artículo 129, el mismo que ha sido modificado por el Artículo 3 de la citada Ley. La figura del Genocidio ha sido considerada dentro de los alcances del Título XIV-A, Capítulo I incorporado por la misma norma.

Artículo 129 
~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no menor de veinte años el que, con la intención de destruir, total o parcialmente, a un grupo nacional, étnico, social o religioso, realiza cualquiera de los actos siguientes:

1.- Matanza de miembros del grupo.

2.- Lesión grave a la integridad física o mental de los miembros del grupo.

3.- Sometimiento del grupo a condiciones de existencia que hayan de acarrear su destrucción física de manera total o parcial.

4.- Medidas destinadas a impedir los nacimientos en el seno del grupo.

5.- Transferencia forzada de niños a otro grupo. (*)

(*) Artículo sustituido por el Artículo 3 de la Ley Nº 26926, publicada el 21 febrero 1998, cuyo texto es el siguiente:

Formas agravadas

"Artículo 129.- En los casos de los Artículos 125 y 128, si resulta lesión grave o muerte y éstas pudieron ser previstas, la pena será privativa de libertad no menor de tres ni mayor de seis años en caso de lesión grave y no menor de cuatro ni mayor de ocho en caso de muerte."

TITULO II DELITOS CONTRA EL HONOR
=================================

CAPITULO UNICO INJURIA, CALUMNIA Y DIFAMACION
#############################################

Artículo 130 Injuria
~~~~~~~~~~~~~~~~~~~~

El que ofende o ultraja a una persona con palabras, gestos o vías de hecho, será reprimido con prestación de servicio comunitario de diez a cuarenta jornadas o con sesenta a noventa días-multa.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 131 Calumnia
~~~~~~~~~~~~~~~~~~~~~

El que atribuye falsamente a otro un delito, será reprimido con noventa a ciento veinte días-multa.

CONCORDANCIAS:     D.S.N° 016-2013-JUS, Única Disp.Comp. Final (Responsabilidad por denuncia falsa)

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 132 Difamación
~~~~~~~~~~~~~~~~~~~~~~~

El que, ante varias personas, reunidas o separadas, pero de manera que pueda difundirse la noticia, atribuye a una persona, un hecho, una cualidad o una conducta que pueda perjudicar su honor o reputación, será reprimido con pena privativa de libertad no mayor de dos años y con treinta a ciento veinte días-multa.

Si la difamación se refiere al hecho previsto en el artículo 131, la pena será privativa de libertad no menor de uno ni mayor de dos años y con noventa a ciento veinte días-multa.

Si el delito se comete por medio del libro, la prensa u otro medio de comunicación social, la pena será privativa de libertad no menor de uno ni mayor de tres años y de ciento veinte a trescientos sesenticinco días-multa.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 133 Conductas atípicas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No se comete injuria ni difamación cuando se trata de:

1. Ofensas proferidas con ánimo de defensa por los litigantes, apoderados o abogados en sus intervenciones orales o escritas ante el Juez.

2. Críticas literarias, artísticas o científicas.

3. Apreciaciones o informaciones que contengan conceptos desfavorables cuando sean realizadas por un funcionario público en cumplimiento de sus obligaciones.

Artículo 134 Prueba de la verdad de las imputaciones
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El autor del delito previsto en el artículo 132 puede probar la veracidad de sus imputaciones sólo en los casos siguientes:

1. Cuando la persona ofendida es un funcionario público y los hechos, cualidades o conductas que se le hubieran atribuído se refieren al ejercicio de sus funciones.

2. Cuando por los hechos imputados está aún abierto un proceso penal contra la persona ofendida.

3. Cuando es evidente que el autor del delito ha actuado en interés de causa pública o en defensa propia.

4. Cuando el querellante pide formalmente que el proceso se siga hasta establecer la verdad o falsedad de los hechos o de la cualidad o conducta que se le haya atribuido.

Si la verdad de los hechos, cualidad o conducta resulta probada, el autor de la imputación estará exento de pena.

Artículo 135 Inadmisibilidad de la prueba
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No se admite en ningún caso la prueba:

1. Sobre imputación de cualquier hecho punible que hubiese sido materia de  absolución definitiva en el Perú o en el extranjero.

2) Sobre cualquier imputación que se refiera a la intimidad personal y familiar, o a un delito de violación de la libertad sexual que requiere acción privada.(*)

(*) Inciso modificado por el Artículo 1 de la Ley Nº 27480, publicada el 13 junio 2001, cuyo texto es el siguiente:

"2) Sobre cualquier imputación que se refiera a la intimidad personal y familiar, o a un delito de violación de la libertad sexual o proxenetismo comprendido en los Capítulos IX y X, del Título IV, Libro Segundo.”

Artículo 136 Difamación o injuria encubierta o equívoca
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El acusado de difamación o injuria encubierta o equívoca que rehusa dar en juicio explicaciones satisfactorias, será considerado como agente de difamación o injuria manifiesta.

Artículo 137 Injurias recíprocas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En el caso de injurias recíprocas proferidas en el calor de un altercado, el Juez podrá, según las circunstancias, declarar exentas de pena a las partes o a una de ellas.

No es punible la injuria verbal provocada por ofensas personales.

Artículo 138 Ejercicio privado de la acción penal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En los delitos previstos en este Título sólo se procederá por acción privada.

Si la injuria, difamación o calumnia ofende a la memoria de una persona fallecida, presuntamente muerta, o declarada judicialmente ausente o desaparecida, la acción penal podrá ser promovida o continuada por su cónyuge, ascendientes, descendientes o hermanos.

TITULO III DELITOS CONTRA LA FAMILIA
====================================

CAPITULO I MATRIMONIOS ILEGALES
###############################

Artículo 139 Bigamia
~~~~~~~~~~~~~~~~~~~~

El casado que contrae matrimonio será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

Si, respecto a su estado civil, induce a error a la persona con quien contrae el nuevo matrimonio la pena será privativa de libertad no menor de dos ni mayor de cinco años.

Artículo 140 Matrimonio con persona casada
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El no casado que, a sabiendas, contrae matrimonio con persona casada será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años.

Artículo 141 Autorización ilegal de matrimonio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario público que, a sabiendas, celebra un matrimonio ilegal será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años e inhabilitación de dos a tres años conforme al artículo 36, incisos 1, 2 y 3.

Si el funcionario público obra por culpa, la pena será de inhabilitación no mayor de un año, conforme al artículo 36, incisos 1, 2 y 3.

Artículo 142 Inobservancia de formalidades legales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario público, párroco u ordinario que procede a la celebración del matrimonio sin observar las formalidades exigidas por la ley, aunque el matrimonio no sea anulado, será reprimido con pena privativa de libertad no mayor de tres años e inhabilitación de uno a dos años, conforme al artículo 36, incisos 1, 2 y 3.

CAPITULO II DELITOS CONTRA EL ESTADO CIVIL
##########################################

Artículo 143 Alteración o supresión del estado civil
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, con perjuicio ajeno, altera o suprime el estado civil de otra persona será reprimido con pena privativa de libertad no mayor de dos años o con prestación de servicio comunitario de veinte a cincuentidós jornadas.

Artículo 144 Fingimiento de embarazo o parto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La mujer que finge embarazo o parto, para dar a un supuesto hijo derechos que no le corresponden, será reprimida con pena privativa de libertad no menor de uno ni mayor de cinco años.

La misma pena privativa de libertad y, además, inhabilitación de uno a tres años, conforme al Artículo 36 inciso 4, se aplicará al médico u obstetra que cooperen en la ejecución del delito.

Artículo 145 Alteración o supresión de la filiación de menor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que exponga u oculte a un menor, lo sustituya por otro, le atribuya falsa filiación o emplee cualquier otro medio para alterar o suprimir su filiación será reprimido con pena privativa de libertad no menor de uno ni mayor de cinco años.

Artículo 146 Móvil de honor
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el agente de alguno de los delitos previstos en este Capítulo comete el hecho por un móvil de honor la pena será de prestación de servicio comunitario de veinte a treinta jornadas.

CAPITULO III ATENTADOS CONTRA LA PATRIA POTESTAD
################################################

Artículo 147 Sustracción de menor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, mediando relación parental, sustrae a un menor de edad o rehusa entregarlo a quien ejerce la patria potestad será reprimido con pena privativa de libertad no mayor de dos años.(*)

(*) Artículo modificado por el Inc. a) del Artículo 1 de la Ley N° 28760, publicada el 14 junio 2006, cuyo texto es el siguiente:

“Artículo 147.- Sustracción de menor

El que, mediando relación parental, sustrae a un menor de edad o rehúsa entregarlo a quien ejerce la patria potestad, será reprimido con pena privativa de libertad no mayor de dos años.

La misma pena se aplicará al padre o la madre u otros ascendientes, aún cuando aquellos no hayan sido excluidos judicialmente de la patria potestad."

Artículo 148 Inducción a la fuga de menor
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que induce a un menor de edad a que se fugue de la casa de sus padres o de la de su tutor o persona encargada de su custodia será reprimido con pena privativa de libertad no mayor de dos años o con prestación de servicio comunitario de veinte a cincuentidós jornadas.

"Artículo 148 - A.- El que instiga o induce a menores de edad a participar en pandillas perniciosas, o actúa como su cabecilla, líder o jefe, para cometer las infracciones previstas en el Capítulo III-A del Título III del Libro Cuarto del Código de los Niños y Adolescentes, será reprimido con pena privativa de libertad no menor de diez (10) ni mayor de veinte (20) años." (1)(2)

(1) Artículo incorporado por la Primera Disposición Complementaria y Final del Decreto Legislativo Nº 899, publicado el 28 mayo 1998, expedido con arreglo a la Ley N° 26950, que otorga al Poder Ejecutivo facultades para legislar en materia de seguridad nacional.

(2) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 982, publicado el 22 julio 2007, cuyo texto es el siguiente:

“Artículo 148-A.- Instigación o participación en pandillaje pernicioso

El que participa en pandillas perniciosas , instiga o induce a menores de edad a participar en ellas, para cometer las infracciones previstas en el Capítulo IV del Título II de Libro IV del Código de los Niños y Adolescentes, así como para agredir a terceras personas, lesionar la integridad física o atentar contra la vida de las personas, dañar bienes públicos o privados, obstaculizar vías de comunicación u ocasionar cualquier tipo de desmanes que alteren el orden interno, será reprimido con pena privativa de libertad no menor de diez ni mayor de veinte años.

La pena será no menor de veinte años cuando el agente:

1. Actúa como cabecilla, líder, dirigente o jefe.

2. Es docente en un centro de educación privado o público.

3. Es funcionario o servidor público.

4. Induzca a los menores a actuar bajo los efectos de bebidas alcohólicas o drogas.

5. Suministre a los menores, armas de fuego, armas blancas, material inflamable, explosivos u objetos contundentes.”(*)

(*) Artículo modificado por la Segunda Disposición Complementaria Modificatoria del Decreto Legislativo N° 1204, publicado el 23 septiembre 2015, cuyo texto es el siguiente:

“Artículo 148 -A.- Participación en pandillaje pernicioso El que participa en pandillas perniciosas, instiga o induce a menores de edad a participar en ellas, para atentar contra la vida, integridad física, el patrimonio o la libertad sexual de las personas, dañar bienes públicos o privados u ocasionar desmanes que alteren el orden público, será reprimido con pena privativa de libertad no menor de diez ni mayor de veinte años.

La pena será no menor de veinte años cuando el agente:

1. Actúa como cabecilla, líder, dirigente o jefe.

2. Es docente en un centro de educación privado o público.

3. Es funcionario o servidor público.

4. Instigue, induzca o utilice a menores de edad a actuar bajo los efectos de bebidas alcohólicas o drogas.

5. Utilice armas de fuego, armas blancas, material inflamable, explosivos u objetos contundentes o los suministre a los menores."

CONCORDANCIAS:     Artículos 206 al 212 (Capítulo IV del Título II del Libro Cuarto) del T.U.O. del Código de los Niños y Adolescentes aprobado
por D.S. N° 004-99-JUS
Ley Nº 27337, Capítulo V - Pandillaje Pernicioso (Código de los Niños y Adolescentes)

CAPITULO IV OMISION DE ASISTENCIA FAMILIAR
##########################################

Artículo 149 Omisión de prestación de alimentos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que omite cumplir su obligación de prestar los alimentos que establece una resolución judicial será reprimido con pena privativa de libertad no mayor de tres años, o con prestación de servicio comunitario de veinte a cincuentidós jornadas, sin perjuicio de cumplir el mandato judicial.

Si el agente ha simulado otra obligación de alimentos en connivencia con otra persona o renuncia o abandona maliciosamente su trabajo la pena será no menor de uno ni mayor de cuatro años.

Si resulta lesión grave o muerte y éstas pudieron ser previstas, la pena será no menor de dos ni mayor de cuatro años en caso de lesión grave, y no menor de tres ni mayor de seis años en caso de muerte.

CONCORDANCIAS:     Ley N° 28970 (Ley que crea el Registro de Deudores Alimentarios Morosos)
R.A. Nº 136-2007-CE-PJ (Crean el Registro de Deudores Alimentarios Morosos -REDAM y aprueban Directiva)

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 150 Abandono de mujer gestante y en situación crítica
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que abandona a una mujer en gestación, a la que ha embarazado y que se halla en situación crítica, será reprimido con pena privativa de libertad no menor de seis meses ni mayor de cuatro años y con sesenta a noventa días- multa.

TITULO IV DELITOS CONTRA LA LIBERTAD
====================================

CAPITULO I VIOLACION DE LA LIBERTAD PERSONAL
############################################

Artículo 151 Coacción
~~~~~~~~~~~~~~~~~~~~~

El que, mediante amenaza o violencia, obliga a otro a hacer lo que la ley no manda o le impide hacer lo que ella no prohibe será reprimido con pena privativa de libertad no mayor de dos años.

“Artículo 151-A.- Acoso

El que, de forma reiterada, continua o habitual, y por cualquier medio, vigila, persigue, hostiga, asedia o busca establecer contacto o cercanía con una persona sin su consentimiento, de modo que pueda alterar el normal desarrollo de su vida cotidiana, será reprimido con pena privativa de la libertad no menor de uno ni mayor de cuatro años, inhabilitación, según corresponda, conforme a los incisos 10 y 11 del artículo 36, y con sesenta a ciento ochenta días-multa.

La misma pena se aplica al que, por cualquier medio, vigila, persigue, hostiga, asedia o busca establecer contacto o cercanía con una persona sin su consentimiento, de modo que altere el normal desarrollo de su vida cotidiana, aun cuando la conducta no hubiera sido reiterada, continua o habitual.

Igual pena se aplica a quien realiza las mismas conductas valiéndose del uso de cualquier tecnología de la información o de la comunicación.

La pena privativa de la libertad será no menor de cuatro ni mayor de siete años, inhabilitación, según corresponda, conforme a los incisos 10 y 11 del artículo 36, y de doscientos ochenta a trescientos sesenta y cinco días-multa, si concurre alguna de las circunstancias agravantes:

1. La víctima es menor de edad, es persona adulta mayor, se encuentra en estado de gestación o es persona con discapacidad.

2. La víctima y el agente tienen o han tenido una relación de pareja, son o han sido convivientes o cónyuges, tienen vínculo parental consanguíneo o por afinidad.

3. La víctima habita en el mismo domicilio que el agente o comparten espacios comunes de una misma propiedad.

4. La víctima se encuentre en condición de dependencia o subordinación con respecto al agente.

5. La conducta se lleva a cabo en el marco de una relación laboral, educativa o formativa de la víctima.”(*)(**)

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1410, publicado el 12 septiembre 2018.

(**) De conformidad con Literal e) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del delito de acoso, previsto en el presente artículo.

Artículo 152 
~~~~~~~~~~~~~

El que, sin derecho, priva a otro de su libertad personal, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.

La pena será no menor de diez ni mayor de veinte años cuando:

1. El agente abusa, corrompe, trata con crueldad o pone en peligro la vida o salud del agraviado.

2. El agente pretexta enfermedad mental inexistente en el agraviado.

3. El agraviado es funcionario, servidor público o representante diplomático.

4. El agraviado es pariente, dentro del tercer grado de consanguinidad o segundo de afinidad, con las personas referidas en el inciso precedente.

5. El agraviado es menor de edad.

6. Se realiza con fines publicitarios.

7. Tiene por objeto obligar a un funcionario o servidor público a poner en libertad a un detenido.

8. Se comete para obligar al agraviado a incorporarse a una organización criminal, o para obligar al agraviado o a un tercero a que preste a la organización ayuda económica o su concurso en cualquier otra forma.

9. Tiene por finalidad obligar a la autoridad pública a conceder exigencias ilegales.

"La pena será de cadena perpetua, cuando el agraviado resulte con graves daños en el cuerpo o en la salud física o mental, o muere durante el secuestro, o a consecuencia de dicho acto." (1)(2)

(1) Párrafo adicionado por el Artículo 1 de la Ley N° 26222, publicada el 21 agosto 1993.

(2) Artículo  modificado por el Artículo 1 de la Ley Nº 26630, publicada el 21 junio 1996, cuyo texto es el siguiente:

"Artículo 152.- El que, sin derecho, priva a otro de su libertad personal, será reprimido con pena privativa de libertad no menor de diez, ni mayor de quince años.

La pena será no menor de veinte ni mayor de veinticinco años cuando:

1. El agente abusa, corrompe, trata con crueldad o pone en peligro la vida o salud del agraviado.

2. El agente pretexta enfermedad mental inexistente en el agraviado.

3. El agraviado es funcionario, servidor público o representante diplomático.

4. El agraviado es pariente, dentro del tercer grado de consanguinidad o segundo de afinidad, con las personas referidas en el inciso precedente.

5. El agraviado es menor de edad.

6. Se realiza con fines publicitarios.

7. Tiene por objeto obligar a un funcionario o servidor público a poner en libertad a un detenido.

8. Se comete para obligar al agraviado a incorporarse a una organización criminal, o para obligar al agraviado o a un tercero a que preste a la organización ayuda económica o su concurso en cualquier otra forma.

9. Tiene por finalidad obligar a la autoridad pública a conceder exigencias ilegales.

10. El agente haya sido sentenciado por terrorismo.

La pena será de cadena perpetua cuando el agraviado resulte con graves daños en el cuerpo o en la salud física o mental, o muere durante el secuestro, o a consecuencia de dicho acto." (*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 896, publicado el 24 mayo 1998, expedido con arreglo a la Ley N° 26950, que otorga al Poder Ejecutivo facultades para legislar en materia de seguridad nacional, cuyo texto es el siguiente:

"Secuestro
Artículo 152.- Será reprimido con pena privativa de libertad no menor de veinte ni mayor de treinta años el que, sin derecho, motivo ni facultad justificada, priva a otro de su libertad personal, cualquiera sea el móvil, el propósito, la modalidad o circunstancia o tiempo que el agraviado sufra la privación o restricción de su libertad.

La pena será no menor de treinta años cuando:

1.- Se abusa, corrompe, trata con crueldad o pone en peligro la vida o salud del agraviado.

2.- Se pretexta enfermedad mental inexistente en el agraviado.

3.- El agraviado es funcionario, servidor público o representante diplomático.

4.- El agraviado es secuestrado por sus actividades en el sector privado.

5.- El agraviado es pariente, dentro del tercer grado de consanguinidad o segundo de afinidad con las personas referidas en los incisos 3 y 4 precedentes.

6.- El agraviado es menor de edad o anciano.

7.- Tiene por objeto obligar a un funcionario o servidor público a poner en libertad a un detenido o a una autoridad a conceder exigencias ilegales.

8.- Se comete para obligar al agraviado a incorporarse a una agrupación criminal, o a una tercera persona para que preste al agente del delito ayuda económica o su concurso bajo cualquier modalidad.

9.- El que con la finalidad de contribuir a la comisión del delito de secuestro, suministra información que haya conocido por razón o con ocasión de sus funciones cargo u oficio, o suministre deliberadamente los medios para la perpetración del delito.

La pena será de cadena perpetua cuando el agraviado resulte con graves daños en el cuerpo o en la salud física o  mental, o muere durante el secuestro, o a consecuencia de dicho acto." (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 27472, publicada el 05 junio 2001, cuyo texto es el siguiente:

"Artículo 152.- Secuestro

Será reprimido con pena privativa de libertad no menor de diez ni mayor de quince años el que, sin derecho, motivo ni facultad justificada, priva a otro de su libertad personal, cualquiera sea el móvil, el propósito, la modalidad o circunstancia o tiempo que el agraviado sufra la privación o restricción de su libertad.

La pena será no menor de veinte ni mayor de veinticinco años cuando:

1. Se abusa, corrompe, trata con crueldad o pone en peligro la vida o salud del agraviado.

2. Se pretexta enfermedad mental inexistente en el agraviado.

3. El agraviado es funcionario, servidor público o representante diplomático.

4. El agraviado es secuestrado por sus actividades en el sector privado.

5. El agraviado es pariente, dentro del tercer grado de consanguinidad o segundo de afinidad con las personas referidas en los incisos 3 y 4 precedentes.

6. El agraviado es menor de edad o anciano.

7. Tiene por objeto obligar a un funcionario o servidor público a poner en libertad a un detenido o a una autoridad a conceder exigencias ilegales.

8. Se comete para obligar al agraviado a incorporarse a una agrupación criminal o a una tercera persona para que preste al agente del delito ayuda económica o su concurso bajo cualquier modalidad.

9. El que con la finalidad de contribuir a la comisión del delito de secuestro, suministra información que haya conocido por razón o con ocasión de sus funciones, cargo u oficio, o suministre deliberadamente los medios para la perpetración del delito.

“10. Se comete para obtener tejidos somáticos de la víctima, sin grave daño físico o mental.” (*)

(*) Inciso incorporado por la Tercera Disposición Transitoria y Final de la Ley N° 28189, publicada el 18 marzo 2004.

La pena será de cadena perpetua cuando el agraviado resulte con graves daños en el cuerpo o en la salud física o mental, o muere durante el secuestro, o a consecuencia de dicho acto. (*)

(*) Artículo modificado por el Inc. a) del Artículo 1 de la Ley N° 28760, publicada el 14 junio 2006, cuyo texto es el siguiente:

"Artículo 152.- Secuestro

Será reprimido con pena privativa de libertad no menor de veinte ni mayor de treinta años el que, sin derecho, motivo ni facultad justificada, priva a otro de su libertad personal, cualquiera sea el móvil, el propósito, la modalidad o circunstancia o tiempo que el agraviado sufra la privación o restricción de su libertad.

La pena será no menor de treinta años cuando:

1. Se abusa, corrompe, trata con crueldad o pone en peligro la vida o salud del agraviado.

2. Se pretexta enfermedad mental inexistente en el agraviado.

3. El agraviado o el agente es funcionario, servidor público o representante diplomático.

4. El agraviado es secuestrado por sus actividades en el sector privado.

5. El agraviado es pariente, dentro del tercer grado de consanguinidad o segundo de afinidad con las personas referidas en los incisos 3 y 4 precedentes.

6. Tiene por objeto obligar a un funcionario o servidor público a poner en libertad a un detenido o a una autoridad a conceder exigencias ilegales.

7. Se comete para obligar al agraviado a incorporarse a una agrupación criminal o a una tercera persona para que preste al agente del delito ayuda económica o su concurso bajo cualquier modalidad.

8. Se comete para obtener tejidos somáticos de la víctima, sin grave daño físico o mental.

La misma pena se aplicará al que con la finalidad de contribuir a la comisión del delito de secuestro, suministra información que haya conocido por razón o con ocasión de sus funciones, cargo u oficio, o proporciona deliberadamente los medios para la perpetración del delito.

La pena será de cadena perpetua, cuando el agraviado es menor de edad, mayor de sesenta y cinco años o discapacitado; así como cuando la víctima resulte con daños en el cuerpo o en su salud física o mental, o muera durante el secuestro, o a consecuencia de dicho acto."(*)

(*) Artículo modificado por el Artículo 2 de la Decreto Legislativo N° 982, publicado el 22 julio 2007, cuyo texto es el siguiente:

“Artículo 152.- Secuestro

Será reprimido con pena privativa de la libertad no menor de veinte ni mayor de treinta años el que, sin derecho, motivo ni facultad justificada, priva a otro de su libertad personal, cualquiera sea el móvil, el propósito, la modalidad o circunstancia o tiempo que el agraviado sufra la privación o restricción de su libertad.

La pena será no menor de treinta años cuando:

1. Se abusa, corrompe, trata con crueldad o pone en peligro la vida o salud del agraviado.

2. Se pretexta enfermedad mental inexistente en el agraviado.

3. El agraviado o el agente es funcionario o servidor público.

4. El agraviado es representante diplomático de otro país.

5. El agraviado es secuestrado por sus actividades en el sector privado.

6. El agraviado es pariente, dentro del tercer grado de consanguinidad o segundo de afinidad con las personas referidas en los incisos 3, 4 y 5 precedentes.

7. Tiene por finalidad obligar a un funcionario o servidor público a poner en libertad a un detenido o a conceder exigencias ilegales.

8. Se comete para obligar al agraviado a incorporarse a una agrupación criminal. (*)

(*) Numeral modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 30077, publicada el 20 agosto 2013, la misma que entró en vigencia el 1 de julio de 2014, cuyo texto es el siguiente:

"8. Se comete para obligar al agraviado a incorporarse a una organización criminal."

9. Se comete para obtener tejidos somáticos del agraviado.

10. Se causa lesiones leves al agraviado.

11. Es cometido por dos o más personas o se utiliza para la comisión del delito a menores de edad u otra persona inimputable.

12. El agraviado adolece de enfermedad grave.

13. La víctima se encuentra en estado de gestación.

La misma pena se aplicará al que con la finalidad de contribuir a la comisión del delito de secuestro, suministra información que haya conocido por razón o con ocasión de sus funciones, cargo u oficio, o proporciona deliberadamente los medios para la perpetración del delito.

La pena será de cadena perpetua cuando:

1. El agraviado es menor de edad o mayor de setenta años.

2. El agraviado sufre discapacidad y el agente se aprovecha de ésta circunstancia.

3. Si se causa lesiones graves o muerte al agraviado durante el secuestro o como consecuencia de dicho acto.”(*)(**)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

(**) De conformidad con Literal g) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del delito de secuestro, previsto en el presente artículo.

CONCORDANCIAS:     R.Adm. Nº 185-2001-P-CSJLI-PJ
Ley N° 27765, Art. 6

Ley N° 9024, Art. 136

D.S. Nº 015-2003-JUS, Art. 210.5 (Aprueban el Reglamento del Código de Ejecución Penal)

Ley Nº 30077, Arts. 3 (Delitos comprendidos), y 24 (Prohibición de beneficios penitenciarios)

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 153 
~~~~~~~~~~~~~

El que promueve, favorece o ejecuta el tráfico de menores, será reprimido con pena privativa de libertad no menor de dos ni mayor de seis años.

La pena será privativa de libertad no menor de cuatro ni mayor de ocho años e inhabilitación, conforme al artículo 36, incisos 1, 2 y 4:

1.- Si el agente comete el hecho en agrupación o en calidad de afiliado a una agrupación destinada al tráfico de menores.

2.- Si el agente es funcionario o servidor público, que tiene vinculación especial o genérica con menores.(*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 26309, publicada el 20 mayo 1994, cuyo texto es el siguiente:

Retención o traslado de menor de edad o de persona incapaz

"Artículo 153.- El que retiene o traslada de un lugar a otro a un menor de edad o a una persona incapaz de valerse por si misma, empleando violencia, amenaza, engaño u otro acto fraudulento, con la  finalidad de obtener ventaja económica o explotar social o económicamente a la víctima, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de 10 años, e inhabilitación conforme al artículo 36, incisos 1, 2, 4 y 5.

Si el agente comete el hecho en agrupación o en calidad de afiliado a una banda, la pena será privativa de libertad no menor de cinco ni mayor de doce años, e inhabilitación conforme al Artículo 36, incisos 1, 2, 4 y 5." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28950, publicada el 16 enero 2007, cuyo texto es el siguiente:

"Artículo 153.- Trata de personas

El que promueve, favorece, financia o facilita la captación, transporte, traslado, acogida, recepción o retención de otro, en el territorio de la República o para su salida o entrada del país, recurriendo a: la violencia, la amenaza u otras formas de coacción, la privación de libertad, el fraude, el engaño, el abuso del poder o de una situación de vulnerabilidad, o la concesión o recepción de pagos o beneficios, con fines de explotación, venta de niños, para que ejerza la prostitución, someterlo a esclavitud sexual u otras formas de explotación sexual, obligarlo a mendigar, a realizar trabajos o servicios forzados, a la servidumbre, la esclavitud o prácticas análogas a la esclavitud u otras formas de explotación laboral, o extracción o tráfico de órganos o tejidos humanos, será reprimido con pena privativa de libertad no menor de ocho ni mayor de quince años.

La captación, transporte, traslado, acogida, recepción o retención de niño, niña o adolescente con fines de explotación se considerará trata de personas incluso cuando no se recurra a ninguno de los medios señalados en el párrafo anterior." (*)

(*) Artículo modificado por el Artículo Único de la Ley N° 30251, publicada el 21 octubre 2014, cuyo texto es el siguiente:

“Artículo 153.- Trata de personas

1. El que mediante violencia, amenaza u otras formas de coacción, privación de la libertad, fraude, engaño, abuso de poder o de una situación de vulnerabilidad, concesión o recepción de pagos o de cualquier beneficio, capta, transporta, traslada, acoge, recibe o retiene a otro, en el territorio de la República o para su salida o entrada del país con fines de explotación, es reprimido con pena privativa de libertad no menor de ocho ni mayor de quince años.

2. Para efectos del inciso 1, los fines de explotación de la trata de personas comprende, entre otros, la venta de niños, niñas o adolescentes, la prostitución y cualquier forma de explotación sexual, la esclavitud o prácticas análogas a la esclavitud, cualquier forma de explotación laboral, la mendicidad, los trabajos o servicios forzados, la servidumbre, la extracción o tráfico de órganos o tejidos somáticos o sus componentes humanos, así como cualquier otra forma análoga de explotación.

3. La captación, transporte, traslado, acogida, recepción o retención de niño, niña o adolescente con fines de explotación se considera trata de personas incluso cuando no se recurra a ninguno de los medios previstos en el inciso 1.

4. El consentimiento dado por la víctima mayor de edad a cualquier forma de explotación carece de efectos jurídicos cuando el agente haya recurrido a cualquiera de los medios enunciados en el inciso 1.

5. El agente que promueve, favorece, financia o facilita la comisión del delito de trata de personas, es reprimido con la misma pena prevista para el autor”. (*)(**)(***)(****)(*****)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

(**) De conformidad con el Numeral 3 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de trata de personas, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

( ** * ) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

(* ** * ) De conformidad con la Segunda Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el beneficio de reducción de pena por terminación anticipada ni la conclusión anticipada en los procesos por el delito señalado en el presente artículo.

(** ** * ) De conformidad con Literal h) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del delito de trata de personas, previsto en el presente artículo.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

CONCORDANCIAS:     Ley N° 28950, Art.8

R. M. N° 2570-2006-IN-0105 (Institucionalizan el “Sistema de Registro y Estadística del delito de Trata de Personas y Afines (RETA)”

R. M. N° 129-2007-IN-0105 (Directiva “Procedimientos para el ingreso, registro, consulta y reporte de datos del Sistema de Registro y
Estadística del delito de Trata de personas y Afines (RETA)”)
Ley N° 27378, Art. 1, num. 2)

Ley Nº 30077, Arts. 3 (Delitos comprendidos) y 24 (Prohibición de beneficios penitenciarios)

Forma agravada. Abuso de cargo de persona vinculada con menores o personas incapaces

"Artículo 153- A.- El funcionario o servidor público y los directivos de las entidades privadas, vinculados especial o genéricamente con menores o personas incapaces que, abusando de su cargo, los retiene o traslada arbitrariamente de un lugar a otro, será reprimido con pena privativa de libertad no menor de cinco ni mayor de doce años e inhabilitación conforme al Artículo 36, incisos 1, 2, 4 y 5.

Si comete el hecho con la finalidad de obtener ventaja económica o explotar social o económicamente a la víctima, será reprimido con pena privativa de libertad no menor de diez ni mayor de veinte años, e inhabilitación conforme al Artículo 36, incisos 1, 2, 4 y 5." (1)(2)

(1) Artículo adicionado por el Artículo 2 de la Ley Nº 26309, publicada el 20 mayo 1994.

(2) Artículo modificado por el Artículo 1 de la Ley N° 28950, publicada el 16 enero 2007, cuyo texto es el siguiente:

"Artículo 153-A.- Formas agravadas de la Trata de Personas

La pena será no menor de doce ni mayor de veinte años de pena privativa de libertad e inhabilitación conforme al artículo 36 incisos 1, 2, 3, 4 y 5 del Código Penal, cuando:

1. El agente comete el hecho abusando del ejercicio de la función pública;

2. El agente es promotor, integrante o representante de una organización social, tutelar o empresarial, que aprovecha de esta condición y actividades para perpetrar este delito;

3. Exista pluralidad de víctimas;

4. La víctima tiene entre catorce y menos de dieciocho años de edad o es incapaz;

5. El agente es cónyuge, conviviente, adoptante, tutor, curador, pariente hasta el cuarto grado de consanguinidad o segundo de afinidad, o tiene a la víctima a su cuidado por cualquier motivo o habitan en el mismo hogar.

6. El hecho es cometido por dos o más personas.

La pena será privativa de libertad no menor de 25 años, cuando:

1. Se produzca la muerte, lesión grave o se ponga en inminente peligro la vida y la seguridad de la víctima.

2. La víctima es menor de catorce años de edad o padece, temporal o permanentemente, de alguna discapacidad física o mental.

3. El agente es parte de una organización criminal.” (*)(**)(***)(****)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

(**) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

( ** * ) De conformidad con la Segunda Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el beneficio de reducción de pena por terminación anticipada ni la conclusión anticipada en los procesos por el delito señalado en el presente artículo.

(* ** * ) De conformidad con Literal h) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del delito de trata de personas, previsto en el presente artículo.

CONCORDANCIAS:     Ley Nº 28950, Art. 8 último párrafo

R. M. N° 2570-2006-IN-0105 (Institucionalizan el “Sistema de Registro y Estadística del delito de Trata de Personas y Afines (RETA)”

R. M. N° 129-2007-IN-0105 (Directiva “Procedimientos para el ingreso, registro, consulta y reporte de datos del Sistema de Registro y
Estadística del delito de Trata de personas y Afines (RETA)”)
Ley N° 27378, Art. 1, num. 2)

“Artículo 153-B.- Explotación sexual

El que obliga a una persona a ejercer actos de connotación sexual con la finalidad de obtener un aprovechamiento económico o de otra índole, es reprimido con pena privativa de libertad no menor de diez ni mayor de quince años.

Si el agente comete el delito mediante engaño, manipulación u otro condicionamiento se aplicará la misma pena del primer párrafo.

El consentimiento brindado por el niño, niña o adolescente carece de efectos jurídicos.

La pena privativa de libertad es no menor de quince ni mayor de veinte años, cuando:

1. El agente tiene a la víctima bajo su cuidado o vigilancia por cualquier motivo, o mantiene con ella un vínculo de superioridad, autoridad, poder u otro que la impulse a depositar su confianza en él.

2. La víctima tiene entre catorce y menos de dieciocho años de edad.

3. El agente comete el delito en el ámbito del turismo, en el marco de la actividad de una persona jurídica o en el contexto de cualquier actividad económica.

La pena privativa de libertad es no menor de veinte ni mayor de veinticinco años, cuando:

1. El agente es ascendiente o descendiente por consanguinidad, adopción o por afinidad; pariente colateral hasta el cuarto grado de consanguinidad o adopción, o segundo grado de afinidad.

2. La explotación es un medio de subsistencia del agente.

3. Existe pluralidad de víctimas.

4. La víctima tiene discapacidad, es menor de catorce años de edad, adulta mayor, padece de una enfermedad grave, pertenece a un pueblo indígena o presenta cualquier situación de vulnerabilidad.

5. Se produzca una lesión grave o se ponga en peligro inminente la vida o la salud de la víctima.

6. Se derive de una situación de trata de personas.

Si se produce la muerte de la víctima, la pena privativa de libertad es no menor de veinticinco ni mayor de treinta años.

En todos los casos se impondrá además la pena de inhabilitación conforme al artículo 36 incisos 1, 2, 3, 4, 5, 6, 8, 10 y 11.”(*)(**)

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1323, publicado el 06 enero 2017.

(**) Artículo modificado por el Artículo 1 de la Ley N° 30963, publicada el 18 junio 2019, cuyo texto es el siguiente:

“Artículo 153-B. Explotación sexual

El que, mediante violencia, amenaza u otro medio, obliga a una persona a ejercer actos de connotación sexual con la finalidad de obtener un aprovechamiento económico o de otra índole, será reprimido con pena privativa de libertad no menor de diez ni mayor de quince años.

Si el agente comete el delito mediante engaño, manipulación u otro condicionamiento, se aplicará la misma pena del primer párrafo.

La pena privativa de libertad será no menor de quince ni mayor de veinte años, cuando:

1. El agente tiene a la víctima bajo su cuidado o vigilancia por cualquier motivo, o mantiene con ella un vínculo de superioridad, autoridad, poder o cualquier otra circunstancia que la impulse a depositar su confianza en él.

2. El agente comete el delito en el ámbito del turismo, en el marco de la actividad de una persona jurídica o en el contexto de cualquier actividad económica.

La pena privativa de libertad será no menor de veinte ni mayor de veinticinco años, cuando:

1. El agente es ascendiente o descendiente por consanguinidad, adopción o por afinidad; pariente colateral hasta el cuarto grado de consanguinidad o adopción, o segundo grado de afinidad; cónyuge, excónyuge, conviviente, exconviviente o tenga hijos en común con la víctima; o habite en el mismo domicilio de la víctima, siempre que no medien relaciones contractuales o laborales.

2. La explotación sexual es un medio de subsistencia del agente.

3. Existe pluralidad de víctimas.

4. La víctima tiene discapacidad, es adulta mayor, padece de una enfermedad grave, pertenece a un pueblo indígena u originario, o presenta cualquier situación de vulnerabilidad.

5. Se produzca una lesión grave o se ponga en peligro inminente la vida o la salud de la víctima.

6. Se derive de una situación de trata de personas.

7. El agente actúe como integrante de una banda o una organización criminal.

8. La víctima está en situación de abandono o de extrema necesidad económica.

Si se produce la muerte de la víctima, la pena privativa de libertad será no menor de veinticinco ni mayor de treinta años.

En todos los casos se impondrá, además, la pena de inhabilitación conforme al artículo 36 incisos 1, 2, 3, 4, 5, 6, 8, 9, 10 y 11." (*)(**)( ** * )

(*) De conformidad con la Primera Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el indulto ni la conmutación de pena ni el derecho de gracia a los sentenciados por el delito tipificado en el presente artículo, modificado e incorporado por la citada ley.

(**) De conformidad con la Segunda Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el beneficio de reducción de pena por terminación anticipada ni la conclusión anticipada en los procesos por el delito señalado en el presente artículo.

(**) De conformidad con Literal i) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del delito de explotación sexual, previsto en el presente artículo.

“Artículo 153-C.- Esclavitud y otras formas de explotación

El que obliga a una persona a trabajar en condiciones de esclavitud o servidumbre, o la reduce o mantiene en dichas condiciones, con excepción de los supuestos del delito de explotación sexual, será reprimido con pena privativa de libertad no menor de diez ni mayor de quince años.

Si el agente comete el delito mediante engaño, manipulación u otro condicionamiento, se aplicará la misma pena del primer párrafo.

El consentimiento brindado por el niño, niña o adolescente carece de efectos jurídicos.

La pena privativa de libertad es no menor de quince años ni mayor de veinte años, cuando:

1. La víctima tiene entre catorce y menos de dieciocho años de edad.

2. El agente comete el delito en el marco de las actividades de una persona jurídica o en el contexto de cualquier actividad económica.

3. Si el agente tiene a la víctima bajo su cuidado o vigilancia por cualquier motivo, o mantiene con ella un vínculo de superioridad, autoridad, poder u otro que la impulse a depositar su confianza en él.

La pena privativa de libertad es no menor de veinte ni mayor de veinticinco años, cuando:

1. El agente es familiar de la víctima hasta el cuarto grado de consanguinidad o segundo de afinidad.

2. La explotación es un medio de subsistencia del agente.

3. Existe pluralidad de víctimas.

4. La víctima tiene discapacidad, es menor de catorce años de edad, adulta mayor, padece de enfermedad grave, pertenece a un pueblo indígena, es trabajador migrante o presenta cualquier situación de vulnerabilidad.

5. Se produzca lesión grave o se ponga en peligro inminente la vida o la salud de la víctima.

6. Se derive de una situación de trata de personas.

Si se produce la muerte de la víctima, la pena privativa de libertad es no menor de veinticinco ni mayor de treinta años.

En todos los casos se impondrá además la pena de inhabilitación conforme al artículo 36 incisos 1, 2, 3, 4, 5, 6, 8, 10 y 11.”(*)(**)( ** * )

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1323, publicado el 06 enero 2017.

(**) De conformidad con la Segunda Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el beneficio de reducción de pena por terminación anticipada ni la conclusión anticipada en los procesos por el delito señalado en el presente artículo.

( ** * ) De conformidad con Literal j) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del delito de esclavitud y otras formas de explotación y delitos relacionados, previsto en el presente artículo.

“Artículo 153-D. Promoción o favorecimiento de la explotación sexual

El que promueve, favorece o facilita la explotación sexual de otra persona, será reprimido con pena privativa de libertad no menor de diez ni mayor de quince años.

La pena privativa de libertad será no menor de quince ni mayor de veinte años, cuando:

1. El agente se aproveche de su calidad de curador o tenga a la víctima bajo su cuidado o vigilancia por cualquier motivo, o tenga con ella un vínculo de superioridad, autoridad, poder o cualquier otra circunstancia que la impulse a depositar su confianza en él.

2. El agente cometa el delito en el ámbito del turismo, en el marco de la actividad de una persona jurídica o en el contexto de cualquier actividad económica.

La pena privativa de libertad será no menor de veinte ni mayor de veinticinco años, cuando:

1. El agente sea ascendiente o descendiente por consanguinidad, adopción o por afinidad; pariente colateral hasta el cuarto grado de consanguinidad o adopción, o segundo grado de afinidad; cónyuge, excónyuge, conviviente, exconviviente o tenga hijos en común con la víctima; o habite en el mismo domicilio de la víctima, siempre que no medien relaciones contractuales o laborales.

2. Es un medio de subsistencia del agente.

3. Exista pluralidad de víctimas.

4. La víctima tenga discapacidad, sea adulta mayor, padezca de una enfermedad grave, pertenezca a un pueblo indígena u originario, o presente cualquier situación de vulnerabilidad.

5. Cuando el agente, a sabiendas, favorezca o promueva actos de explotación sexual violentos que produzcan lesiones o ponga en peligro grave la integridad o la vida de quien realice la prostitución.

6. Se produzca una lesión grave o se ponga en peligro inminente la vida o la salud de la víctima.

7. Se derive de una situación de trata de personas.

8. El agente actúe como integrante de una banda o una organización criminal.

9. La víctima esté en situación de abandono o de extrema necesidad económica.

Si se produce la muerte de la víctima, la pena privativa de libertad será no menor de veinticinco ni mayor de treinta años.

En todos los casos se impone, además, la pena de inhabilitación conforme al artículo 36, incisos 1, 2, 3, 4, 5, 6, 8, 9, 10 y 11."(*)(**)(***)(****)

(*) Artículo incorporado por el Artículo 2 de la Ley N° 30963, publicada el 18 junio 2019.

(**) De conformidad con la Primera Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el indulto ni la conmutación de pena ni el derecho de gracia a los sentenciados por el delito tipificado en el presente artículo, modificado e incorporado por la citada ley.

( ** * ) De conformidad con la Segunda Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el beneficio de reducción de pena por terminación anticipada ni la conclusión anticipada en los procesos por el delito señalado en el presente artículo.

(* ** * ) De conformidad con Literal j) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del delito de esclavitud y otras formas de explotación y delitos relacionados, previsto en el presente artículo.

"Artículo 153-E. Cliente de la explotación sexual

El que, mediante una prestación económica o ventaja de cualquier naturaleza, tiene acceso carnal por vía vaginal, anal o bucal o realiza otros actos análogos introduciendo objetos o partes del cuerpo por alguna de esas vías con una víctima de explotación sexual será reprimido con pena privativa de libertad no menor de nueve ni mayor de doce años”.(*)(**)(***)(****)

(*) Artículo incorporado por el Artículo 2 de la Ley N° 30963, publicada el 18 junio 2019.

(**) De conformidad con la Primera Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el indulto ni la conmutación de pena ni el derecho de gracia a los sentenciados por el delito tipificado en el presente artículo, modificado e incorporado por la citada ley.

( ** * ) De conformidad con la Segunda Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el beneficio de reducción de pena por terminación anticipada ni la conclusión anticipada en los procesos por el delito señalado en el presente artículo.

(* ** * ) De conformidad con Literal j) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del delito de esclavitud y otras formas de explotación y delitos relacionados, previsto en el presente artículo.

"Artículo 153-F. Beneficio por explotación sexual

El que, sin participar de los actos de explotación sexual de una persona, recibe un beneficio económico o de otra índole derivado de dichos actos, será reprimido con pena privativa de libertad no menor de seis ni mayor de ocho años.

La pena privativa de libertad será no menor de ocho ni mayor de catorce años cuando:

1. El agente sea ascendiente o descendiente por consanguinidad, adopción o por afinidad, pariente colateral hasta el cuarto grado de consanguinidad o adopción, o segundo grado de afinidad; cónyuge, excónyuge, conviviente, exconviviente o tenga hijos en común con la víctima; o habite en el mismo domicilio de la víctima, siempre que no medien relaciones contractuales o laborales.

2. El agente se aproveche de su calidad de curador; o tenga a la víctima bajo su cuidado o vigilancia por cualquier motivo; o mantenga con la víctima un vínculo de superioridad, autoridad, poder u otro que le genere confianza en él.

3. Es un medio de subsistencia del agente.

4. Exista pluralidad de víctimas

6. La víctima pertenezca a un pueblo indígena u originario.

7. El agente actúe como integrante de una banda u organización criminal.

8. La víctima está en situación de abandono o de extrema necesidad económica.

En todos los casos se impone, además, la pena de inhabilitación conforme al artículo 36, incisos 1, 2, 3, 4, 5, 6, 8, 9, 10 y 11”.(*) (**)(***)(****)

(*) Artículo incorporado por el Artículo 2 de la Ley N° 30963, publicada el 18 junio 2019.

(**) De conformidad con la Primera Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el indulto ni la conmutación de pena ni el derecho de gracia a los sentenciados por el delito tipificado en el presente artículo, modificado e incorporado por la citada ley.

( ** * ) De conformidad con la Segunda Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el beneficio de reducción de pena por terminación anticipada ni la conclusión anticipada en los procesos por el delito señalado en el presente artículo.

(* ** * ) De conformidad con Literal j) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del delito de esclavitud y otras formas de explotación y delitos relacionados, previsto en el presente artículo.

"Artículo 153-G. Gestión de la explotación sexual

El que dirige o gestiona la explotación sexual de otra persona con el objeto de tener acceso carnal será reprimido con pena privativa de libertad no menor de diez ni mayor de quince años.

La pena privativa de libertad será no menor de quince ni mayor de veinte años, cuando:

1. El agente tenga a la víctima bajo su cuidado o vigilancia por cualquier motivo, o tenga con ella un vínculo de superioridad, autoridad, poder o cualquier otra circunstancia que la impulse a depositar su confianza en él.

2. El agente cometa el delito en el ámbito del turismo, en el marco de la actividad de una persona jurídica o en el contexto de cualquier actividad económica.

La pena privativa de libertad será no menor de veinte ni mayor de veinticinco años, cuando:

1. El agente sea ascendiente o descendiente por consanguinidad, adopción o por afinidad; pariente colateral hasta el cuarto grado de consanguinidad o adopción, o segundo grado de afinidad; cónyuge, excónyuge, conviviente, exconviviente o tenga hijos en común con la víctima; o habite en el mismo domicilio de la víctima, siempre que no medien relaciones contractuales o laborales.

2. Es un medio de subsistencia del agente.

3. Exista pluralidad de víctimas.

4. La víctima tenga discapacidad, sea adulta mayor, padezca de una enfermedad grave, pertenezca a un pueblo indígena u originario, o presente cualquier situación de vulnerabilidad.

5. Se produzca una lesión grave o se ponga en peligro inminente la vida o la salud de la víctima.

6. Se derive de una situación de trata de personas.

7. El agente actúe como integrante de una banda o una organización criminal.

8. La víctima esté en situación de abandono o de extrema necesidad económica.

Si se produce la muerte de la víctima, la pena privativa de libertad será no menor de veinticinco ni mayor de treinta años.

En todos los casos se impone, además, la pena de inhabilitación conforme al artículo 36, incisos 1, 2, 3, 4, 5, 6, 8, 9, 10 y 11”.(*)(**)(***)(****)

(*) Artículo incorporado por el Artículo 2 de la Ley N° 30963, publicada el 18 junio 2019.

(**) De conformidad con la Primera Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el indulto ni la conmutación de pena ni el derecho de gracia a los sentenciados por el delito tipificado en el presente artículo, modificado e incorporado por la citada ley.

( ** * ) De conformidad con la Segunda Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el beneficio de reducción de pena por terminación anticipada ni la conclusión anticipada en los procesos por el delito señalado en el presente artículo.

(* ** * ) De conformidad con Literal j) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del delito de esclavitud y otras formas de explotación y delitos relacionados, previsto en el presente artículo.

"Artículo 153-H. Explotación sexual de niñas, niños y adolescentes

El que hace ejercer a niña, niño o adolescente actos de connotación sexual con la finalidad de obtener un aprovechamiento económico o de otra índole será reprimido con pena privativa de libertad no menor de quince ni mayor de veinte años.

El consentimiento brindado por el adolescente carece de efectos jurídicos.

La pena privativa de libertad será no menor de veinte ni mayor de treinta años si el agente:

1. Es promotor, integrante o representante de una organización social, tutelar o empresarial que aprovecha esta condición y realiza actividades para perpetrar este delito.

2. Tiene a la víctima bajo su cuidado o vigilancia, por cualquier motivo, o mantiene con ella un vínculo de superioridad, autoridad, poder o cualquier otra circunstancia que impulse a depositar la confianza en él.

La pena privativa de libertad será no menor de treinta ni mayor de treinta y cinco años cuando:

1. El agente sea ascendiente por consanguinidad, adopción o por afinidad, pariente colateral hasta el cuarto grado de consanguinidad o adopción, o segundo grado de afinidad, tutor, cónyuge, excónyuge, conviviente, exconviviente o tenga hijos en común con la víctima; o habite en el mismo domicilio de la víctima, siempre que no medien relaciones contractuales o laborales.

2. Es un medio de subsistencia del agente.

3. Exista pluralidad de víctimas.

4. La víctima tenga discapacidad, padezca de una enfermedad grave, o presente cualquier situación de vulnerabilidad.

5. La víctima pertenezca a un pueblo indígena u originario.

6. El agente ponga en inminente y grave peligro la vida o la salud física o mental de la víctima.

7. Se derive de una situación de trata de personas.

8. El agente actúe como integrante de una banda o una organización criminal.

9. La víctima esté en situación de abandono o de extrema necesidad económica.

10. La víctima sea menor de catorce años.

La pena será de cadena perpetua:

1. Si se causa la muerte de la víctima.

2. Si se lesiona gravemente su salud física o mental.

3. Si, a consecuencia de la explotación sexual, la víctima menor de 14 años tiene acceso carnal por vía vaginal, anal o bucal o realiza cualquier otro acto análogo con la introducción de un objeto o parte del cuerpo por alguna de las dos primeras vías.

En todos los casos se impone, además, la pena de inhabilitación conforme al artículo 36, incisos 1, 2, 3, 4, 5, 6, 8, 9, 10 y 11”.(*)(**)(***)(****)

(*) Artículo incorporado por el Artículo 2 de la Ley N° 30963, publicada el 18 junio 2019.

(**) De conformidad con la Primera Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el indulto ni la conmutación de pena ni el derecho de gracia a los sentenciados por el delito tipificado en el presente artículo, modificado e incorporado por la citada ley.

( ** * ) De conformidad con la Segunda Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el beneficio de reducción de pena por terminación anticipada ni la conclusión anticipada en los procesos por el delito señalado en el presente artículo.

(* ** * ) De conformidad con Literal j) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del delito de esclavitud y otras formas de explotación y delitos relacionados, previsto en el presente artículo.

"Artículo 153-I. Beneficio de la explotación sexual de niñas, niños y adolescentes

El que, sin participar de los actos de explotación sexual de niña, niño o adolescente, recibe un beneficio económico o de otra índole derivado de dichos actos será reprimido con pena privativa de libertad no menor de diez ni mayor de quince años.

El consentimiento brindado por el adolescente carece de efectos jurídicos.

La pena privativa de libertad será no menor de quince ni mayor de veinte años si el agente:

1. Es promotor, integrante o representante de una organización social tutelar o empresarial que aprovecha esta condición y realiza actividades para perpetrar este delito.

2. Tiene a la víctima bajo su cuidado o vigilancia, por cualquier motivo, o mantiene con ella un vínculo de superioridad, autoridad, poder o cualquier otra circunstancia que impulse a depositar la confianza en él.

La pena privativa de libertad será no menor de veinte ni mayor de treinta años cuando:

1. El agente sea ascendiente por consanguinidad, adopción o por afinidad, pariente colateral hasta el cuarto grado de consanguinidad o adopción, o segundo grado de afinidad, tutor, cónyuge, excónyuge, conviviente, exconviviente o tenga hijos en común con la víctima; o habite en el mismo domicilio de la víctima, siempre que no medien relaciones contractuales o laborales.

2. Es un medio de subsistencia del agente.

3. Exista pluralidad de víctimas.

4. La víctima tenga discapacidad, padezca de una enfermedad grave o presenta cualquier situación de vulnerabilidad.

5. La víctima pertenezca a un pueblo indígena u originario.

6. Se derive de una situación de trata de personas.

7. El agente actúe como integrante de una banda o una organización criminal.

8. La víctima esté en situación de abandono o de extrema necesidad económica.

9. La víctima sea menor de catorce años.

La pena privativa de libertad será no menor de treinta y cinco años si se causa la muerte de la víctima o se lesiona gravemente su salud física o mental.

En todos los casos se impone, además, la pena de inhabilitación conforme al artículo 36, incisos 1, 2, 3, 4, 5, 6, 8, 9,10 y 11”.(*)(**)(***)(****)

(*) Artículo incorporado por el Artículo 2 de la Ley N° 30963, publicada el 18 junio 2019.

(**) De conformidad con la Primera Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el indulto ni la conmutación de pena ni el derecho de gracia a los sentenciados por el delito tipificado en el presente artículo, modificado e incorporado por la citada ley.

( ** * ) De conformidad con la Segunda Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el beneficio de reducción de pena por terminación anticipada ni la conclusión anticipada en los procesos por el delito señalado en el presente artículo.

(* ** * ) De conformidad con Literal j) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del delito de esclavitud y otras formas de explotación y delitos relacionados, previsto en el presente artículo.

"Artículo 153-J. Gestión de la explotación sexual de niñas, niños y adolescentes

El que dirige o gestiona la explotación sexual de niña, niño o adolescente con el objeto de tener acceso carnal será reprimido con pena privativa de libertad no menor de quince ni mayor de veinte años.

El consentimiento brindado por el adolescente carece de efectos jurídicos.

La pena privativa de libertad será no menor de veinte ni mayor de treinta años si el agente:

1. Es promotor, integrante o representante de una organización social, tutelar o empresarial que aprovecha esta condición y realiza actividades para perpetrar este delito.

2. Tiene a la víctima bajo su cuidado o vigilancia, por cualquier motivo, o mantiene con ella un vínculo de superioridad, autoridad, poder o cualquier otra circunstancia que impulse a depositar la confianza en él.

La pena privativa de libertad será no menor de treinta ni mayor de treinta y cinco años cuando:

1. El agente sea ascendiente por consanguinidad, adopción o por afinidad, pariente colateral hasta el cuarto grado de consanguinidad o adopción, o segundo grado de afinidad, tutor, cónyuge, excónyuge, conviviente, exconviviente o tenga hijos en común con la víctima; o habite en el mismo domicilio de la víctima, siempre que no medien relaciones contractuales o laborales.

2. Es un medio de subsistencia del agente.

3. Exista pluralidad de víctimas.

4. La víctima tenga discapacidad, padezca de una enfermedad grave, o presente cualquier situación de vulnerabilidad.

5. La víctima pertenezca a un pueblo indígena u originario.

6. El agente ponga en inminente y grave peligro la vida o la salud física o mental de la víctima.

7. Se derive de una situación de trata de personas.

8. El agente actúe como integrante de una banda o una organización criminal.

9. La víctima esté en situación de abandono o de extrema necesidad económica.

10. La víctima sea menor de catorce años.

La pena será de cadena perpetua:

1. Si se causa la muerte de la víctima.

2. Si se lesiona gravemente su salud física o mental.

3. Si, a consecuencia de la explotación sexual, la víctima menor de 14 años tiene acceso carnal por vía vaginal, anal o bucal o realiza cualquier otro acto análogo con la introducción de un objeto o parte del cuerpo por alguna de las dos primeras vías.

En todos los casos se impone, además, la pena de inhabilitación conforme al artículo 36, incisos 1, 2, 3, 4, 5, 6, 8, 9, 10 y 11”.(*)(**)(***)(****)

(*) Artículo incorporado por el Artículo 2 de la Ley N° 30963, publicada el 18 junio 2019.

(**) De conformidad con la Primera Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el indulto ni la conmutación de pena ni el derecho de gracia a los sentenciados por el delito tipificado en el presente artículo, modificado e incorporado por la citada ley.

( ** * ) De conformidad con la Segunda Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el beneficio de reducción de pena por terminación anticipada ni la conclusión anticipada en los procesos por el delito señalado en el presente artículo.

(* ** * ) De conformidad con Literal j) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del delito de esclavitud y otras formas de explotación y delitos relacionados, previsto en el presente artículo.

CAPITULO II VIOLACION DE LA INTIMIDAD
#####################################

Artículo 154 Violación de la intimidad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que viola la intimidad de la vida personal o familiar ya sea observando, escuchando o registrando un hecho, palabra, escrito o imagen, valiéndose de instrumentos, procesos técnicos u otros medios, será reprimido con pena privativa de libertad no mayor de dos años.

La pena será no menor de uno ni mayor de tres años y de treinta a ciento veinte días-multa, cuando el agente revela la intimidad conocida de la manera antes prevista.

Si utiliza algún medio de comunicación social, la pena privativa de libertad será no menor de dos ni mayor de cuatro años y de sesenta a ciento ochenta días-multa.

“Artículo 154-A. Tráfico ilegal de datos personales

El que ilegítimamente comercializa o vende información no pública relativa a cualquier ámbito de la esfera personal, familiar, patrimonial, laboral, financiera u otro de naturaleza análoga sobre una persona natural, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años.

Si el agente comete el delito como integrante de una organización criminal, la pena se incrementa hasta en un tercio por encima del máximo legal previsto en el párrafo anterior.” (*)

(*) Artículo incorporado por el Artículo 5 de la Ley N° 30171, publicada el 10 marzo 2014.

“Artículo 154-B.- Difusión de imágenes, materiales audiovisuales o audios con contenido sexual

El que, sin autorización, difunde, revela, publica, cede o comercializa imágenes, materiales audiovisuales o audios con contenido sexual de cualquier persona, que obtuvo con su anuencia, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años y con treinta a ciento veinte días-multa.

La pena privativa de libertad será no menor de tres ni mayor de seis años y de ciento ochenta a trescientos sesenta y cinco días-multa, cuando concurra cualquiera de las siguientes circunstancias:

1. Cuando la víctima mantenga o haya mantenido una relación de pareja con el agente, son o han sido convivientes o cónyuges.

2. Cuando para materializar el hecho utilice redes sociales o cualquier otro medio que genere una difusión masiva.”(*)

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1410, publicado el 12 septiembre 2018.

CONCORDANCIAS:     D.S.N° 039-2015-SA, Art. 72 (Denegación de acceso de forma temporal de un usuario del RENHICE)

D.S.N° 008-2016-SA, Art. 72 (Denegación de acceso de forma temporal de un usuario del RENHICE)

Artículo 155 Agravante por razón de la función
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si el agente es funcionario o servidor público y, en ejercicio del cargo, comete el hecho previsto en el artículo 154, la pena será no menor de tres ni mayor de seis años e inhabilitación conforme al artículo 36 incisos 1, 2 y 4.(*)

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 155.- Agravante por razón de la función

Si el agente es funcionario o servidor público y, en ejercicio del cargo, comete el hecho previsto en los artículos 154 y 154-A, la pena será no menor de tres ni mayor de seis años e inhabilitación conforme al artículo 36 incisos 1, 2 y 4.

Si el agente es funcionario o servidor público y, en ejercicio del cargo, comete el hecho previsto en los artículos 154 y 154-A y la información tenga su origen a partir de la aplicación de la medida de la localización o geolocalización, la pena será no menor de seis ni mayor de ocho años e inhabilitación conforme al artículo 36 incisos 1, 2 y 4."

PROCESOS CONSTITUCIONALES

Artículo 156 Revelación de la intimidad personal y familiar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que revela aspectos de la intimidad personal o familiar que conociera con motivo del trabajo que prestó al agraviado o a la persona a quien éste se lo confió, será reprimido con pena privativa de libertad no mayor de un año.

Artículo 157 Uso indebido de archivos computarizados
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, indebidamente, organiza, proporciona o emplea cualquier archivo que tenga datos referentes a las convicciones políticas o religiosas y otros aspectos de la vida íntima de una o más personas, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

Si el agente es funcionario o servidor público y comete el delito en ejercicio del cargo, la pena será no menor de tres ni mayor de seis años e inhabilitación conforme al artículo 36, incisos 1, 2 y 4.

CONCORDANCIAS:     D.S.N° 039-2015-SA, Art. 72 (Denegación de acceso de forma temporal de un usuario del RENHICE)

D.S.N° 008-2016-SA, Art. 72 (Denegación de acceso de forma temporal de un usuario del RENHICE)

Artículo 158 Acción privada
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los delitos previstos en este Capítulo son perseguibles por acción privada. (*)

(*) Artículo modificado por el Artículo 4 de la Ley N° 30171, publicada el 10 marzo 2014, cuyo texto es el siguiente:

“Artículo 158. Ejercicio de la acción penal

Los delitos previstos en este Capítulo son perseguibles por acción privada, salvo en el caso del delito previsto en el artículo 154-A.” (*)

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 158.- Ejercicio de la acción penal

Los delitos previstos en este Capítulo son perseguibles por acción privada, salvo en el caso del delito previsto en los artículos 154-A y 155."

CAPITULO III VIOLACION DE DOMICILIO
###################################

Artículo 159 Violación de domicilio 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, sin derecho, penetra en morada o casa de negocio ajena, en su dependencia o en el recinto habitado por otro o el que permanece allí rehusando la intimación que le haga quien tenga derecho a formularla, será reprimido con pena privativa de libertad no mayor de dos años y con treinta a noventa días-multa.

Artículo 160 Allanamiento ilegal de domicilio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario o servidor público que allana un domicilio, sin las formalidades prescritas por la ley o fuera de los casos que ella determina, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años e inhabilitación de uno a dos años conforme al artículo 36, incisos 1, 2 y 3.

CAPITULO IV VIOLACION DEL SECRETO DE LAS COMUNICACIONES
#######################################################

Artículo 161 Violación de correspondencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que abre, indebidamente, una carta, un pliego, telegrama, radiograma, despacho telefónico u otro documento de naturaleza análoga, que no le esté dirigido, o se apodera indebidamente de alguno de estos documentos, aunque no esté cerrado, será reprimido con pena privativa de libertad no mayor de dos años y con sesenta a noventa días-multa.

Artículo 162 Interferencia telefónica
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, indebidamente, interfiere o escucha una conversación telefónica o similar será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años.

Si el agente es funcionario público, la pena privativa de libertad será no menor de tres ni mayor de cinco años e inhabilitación conforme al artículo 36, incisos 1, 2 y 4. (*)

(*) Artículo modificado por la Cuarta Disposición Complementaria Modificatoria de la Ley N° 30096, publicada el 22 octubre 2013, cuyo texto es el siguiente:

“Artículo 162. Interferencia telefónica

El que, indebidamente, interfiere o escucha una conversación telefónica o similar será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años.

Si el agente es funcionario público, la pena privativa de libertad será no menor de cuatro ni mayor de ocho años e inhabilitación conforme al artículo 36, incisos 1, 2 y 4.

La pena privativa de libertad será no menor de cinco ni mayor de ocho años cuando el delito recaiga sobre información clasificada como secreta, reservada o confidencial de conformidad con las normas de la materia.

La pena privativa de libertad será no menor de ocho ni mayor de diez años cuando el delito comprometa la defensa, la seguridad o la soberanía nacionales." (*)

(*) Artículo modificado por el Artículo 4 de la Ley N° 30171, publicada el 10 marzo 2014, cuyo texto es el siguiente:

“Artículo 162. Interferencia telefónica

El que, indebidamente, interfiere o escucha una conversación telefónica o similar, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años.

Si el agente es funcionario público, la pena privativa de libertad será no menor de cuatro ni mayor de ocho años e inhabilitación conforme al artículo 36, incisos 1, 2 y 4.

La pena privativa de libertad será no menor de cinco ni mayor de ocho años cuando el delito recaiga sobre información clasificada como secreta, reservada o confidencial de conformidad con la Ley 27806, Ley de Transparencia y Acceso a la Información Pública.

La pena privativa de libertad será no menor de ocho ni mayor de diez años, cuando el delito comprometa la defensa, seguridad o soberanía nacionales.

Si el agente comete el delito como integrante de una organización criminal, la pena se incrementa hasta en un tercio por encima del máximo legal previsto en los supuestos anteriores.” (*)

(*) Artículo modificado por la Primera Disposición Complementaria Modificatoria del Decreto Legislativo N° 1182, publicado el 27 julio 2015, cuyo texto es el siguiente:

“Artículo 162. Interferencia telefónica

El que, indebidamente, interviene o interfiere o escucha una conversación telefónica o similar, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años.

La pena privativa de libertad será no menor de diez ni mayor de quince años:

1. Cuando el agente tenga la condición de funcionario o servidor público, y se impondrá además la inhabilitación conforme al artículo 36, incisos 1, 2 y 4.

2. Cuando el delito recaiga sobre información clasificada como secreta, reservada o confidencial de conformidad con la Ley 27806, Ley de Transparencia y Acceso a la Información Pública.

3. Cuando el delito comprometa la defensa, seguridad o soberanía nacionales.

Si el agente comete el delito como integrante de una organización criminal, la pena se incrementa hasta en un tercio por encima del máximo legal previsto en los supuestos anteriores."

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

“Artículo 162-A. Posesión o comercialización de equipos destinados a la interceptación telefónica o similar

El que fabrica, adquiere, introduce al territorio nacional, posee o comercializa equipos o softwares destinados a interceptar ilegalmente las comunicaciones o similares, será reprimido con pena privativa de la libertad no menor de diez ni mayor de quince años.”(*)

(*) Artículo incorporado por la Segunda Disposición Complementaria Modificatoria del Decreto Legislativo N° 1182, publicado el 27 julio 2015.

“Artículo 162-B. Interferencia de comunicaciones electrónicas, de mensajería instantánea y similares

El que, indebidamente, interviene o interfiere comunicaciones electrónicas o de mensajería instantánea o similares, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años.

La pena privativa de libertad será no menor de diez ni mayor de quince años, cuando:

1. El agente tenga la condición de funcionario o servidor público, y se impondrá además la inhabilitación conforme al artículo 36, incisos 1, 2 y 4.

2. El delito recaiga sobre información clasificada como secreta, reservada o confidencial de conformidad con la Ley Nº 27806, Ley de Transparencia y Acceso a la Información Pública.

3. El delito comprometa la defensa, seguridad o soberanía nacionales.

Si el agente comete el delito como integrante de una organización criminal, la pena se incrementa hasta en un tercio por encima del máximo legal previsto en los supuestos anteriores.”(*)

(*) Artículo incorporado por el Artículo Único del Decreto Legislativo N° 1234, publicado el 26 septiembre 2015.

Artículo 163 Supresión o extravío indebido de correspondencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, indebidamente, suprime o extravía de su destino una correspondencia epistolar o telegráfica, aunque no la haya violado, será reprimido con prestación de servicio comunitario de veinte a cincuentidós jornadas.

Artículo 164 Publicación indebida de correspondencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que publica, indebidamente, una correspondencia epistolar o telegráfica, no destinada a la publicidad, aunque le haya sido dirigida, será reprimido, si el hecho causa algún perjuicio a otro, con limitación de días libres de veinte a cincuentidós jornadas.

CAPITULO V VIOLACION DEL SECRETO PROFESIONAL
############################################

Artículo 165 Violación del secreto profesional
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, teniendo información por razón de su estado, oficio, empleo, profesión o ministerio, de secretos cuya publicación pueda causar daño, los revela sin consentimiento del interesado, será reprimido con pena privativa de libertad no mayor de dos años y con sesenta a ciento veinte días-multa.

CAPITULO VI VIOLACION DE LA LIBERTAD DE REUNION
###############################################

Artículo 166 Perturbación de reunión pública
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, con violencia o amenaza, impide o perturba una reunión pública lícita, será reprimido con pena privativa de libertad no mayor de un año y con sesenta a noventa días-multa.

Artículo 167 Prohibición de reunión pública lícita por funcionario público
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario público que abusando de su cargo no autoriza, no garantiza, prohíbe o impide una reunión pública, lícitamente convocada, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años e inhabilitación de uno a dos años conforme el artículo 36, incisos 1, 2 y 3.

CAPITULO VII VIOLACION DE LA LIBERTAD DE TRABAJO
################################################

Artículo 168 
~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no mayor de dos años el que obliga a otro, mediante violencia o amenaza, a realizar cualquiera de las conductas siguientes:

1. Integrar o no un sindicato.

2. Prestar trabajo personal sin la debida retribución.

3. Trabajar sin las condiciones de seguridad e higiene industriales determinadas por la autoridad.

4.- Celebrar contrato de trabajo o adquirir materias primas o productos industriales o agrícolas.

La misma pena se aplicará al que retiene las remuneraciones o indemnizaciones de los trabajadores o no entrega al destinatario las efectuadas por mandato legal o judicial; al que incumple las resoluciones consentidas o ejecutoriadas dictadas por la autoridad competente; y al que disminuye o distorsiona la producción, simula causales para el cierre del centro de trabajo o abandona éste para extinguir las relaciones laborales.(*)

(*) Artículo modificado por la Tercera Disposición Derogatoria y Final del Decreto Legislativo N° 857, publicado el 04 octubre 1996, cuyo texto es el siguiente:

"Artículo 168.- Será reprimido con pena privativa de libertad no mayor de dos años el que obliga a otro, mediante violencia o amenaza, a realizar cualquiera de los actos siguientes:

1. Integrar o no un sindicato.

2. Prestar trabajo personal sin la correspondiente retribución.

3. Trabajar sin las condiciones de seguridad e higiene industriales determinadas por la autoridad.

La misma pena se aplicará al que incumple las resoluciones consentidas o ejecutoriadas dictadas por la autoridad competente; y al que disminuye o distorsiona la producción, simula causales para el cierre del centro de trabajo o abandona éste para extinguir las relaciones laborales."(*)(**)

(*) Numeral 3 derogado por la Sexta Disposición Complementaria Modificatoria de la Ley Nº 29783, publicada el 20 agosto 2011.

(**) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1323, publicado el 06 enero 2017, cuyo texto es el siguiente:

“Artículo 168.- Atentado contra la libertad de trabajo y asociación

El que, mediante violencia o amenaza, obliga o impide a otro a integrar un sindicato, es reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años.

La misma pena se aplicará al que incumple las resoluciones consentidas o ejecutoriadas dictadas por la autoridad competente; y al que disminuye o distorsiona la producción, simula causales para el cierre del centro de trabajo o abandona éste para extinguir las relaciones laborales.”

CONCORDANCIAS:     D.S. N° 009-2007-TR, num. 3.2 inc. c)

“Artículo 168-A. Atentado contra las condiciones de seguridad e higiene industriales

El que, infringiendo las normas de seguridad y salud en el trabajo y estando legalmente obligado, no adopte las medidas preventivas necesarias para que los trabajadores desempeñen su actividad, poniendo en riesgo su vida, salud o integridad física, será reprimido con pena privativa de libertad no menor de dos años ni mayor de cinco años.

Si, como consecuencia de una inobservancia de las normas de seguridad y salud en el trabajo, ocurre un accidente de trabajo con consecuencias de muerte o lesiones graves, para los trabajadores o terceros, la pena privativa de libertad será no menor de cinco años ni mayor de diez años.” (*)

(*) Artículo incorporado por la Cuarta Disposición Complementaria Modificatoria de la Ley Nº 29783, publicada el 20 agosto 2011. Posteriormente la citada Disposición fue modificada por el Artículo 2 de la Ley N° 30222, publicada el 11 julio 2014, quedando el presente artículo redactado de la siguiente manera:

"Artículo 168-A. Atentado contra las condiciones de seguridad y salud en el trabajo

El que, deliberadamente, infringiendo las normas de seguridad y salud en el trabajo y estando legalmente obligado, y habiendo sido notificado previamente por la autoridad competente por no adoptar las medidas previstas en éstas y como consecuencia directa de dicha inobservancia, ponga en peligro inminente la vida, salud o integridad física de sus trabajadores, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

Si, como consecuencia de la inobservancia deliberada de las normas de seguridad y salud en el trabajo, se causa la muerte del trabajador o terceros o le producen lesión grave, y el agente pudo prever este resultado, la pena privativa de libertad será no menor de cuatro ni mayor de ocho años en caso de muerte y, no menor de tres ni mayor de seis años en caso de lesión grave.

Se excluye la responsabilidad penal cuando la muerte o lesiones graves son producto de la inobservancia de las normas de seguridad y salud en el trabajo por parte del trabajador."(*)

(*) Artículo modificado por la Primera Disposición Complementaria Modificatoria del Decreto de Urgencia N° 044-2019, publicado el 30 diciembre 2019, cuyo texto es el siguiente:

“Artículo 168-A.- Atentado contra las condiciones de seguridad y salud en el trabajo

El que, deliberadamente, infringiendo las normas de seguridad y salud en el trabajo y estando legalmente obligado, ponga en peligro inminente la vida, salud o integridad física de sus trabajadores de forma grave, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

Si, como consecuencia de la inobservancia deliberada de las normas de seguridad y salud en el trabajo, se causa la muerte del trabajador o terceros o le producen lesión grave, y el agente pudo prever este resultado, la pena privativa de libertad será no menor de cuatro ni mayor de ocho años en caso de muerte y, no menor de tres ni mayor de seis años en caso de lesión grave”.

“Artículo 168-B.- Trabajo forzoso

El que somete u obliga a otra persona, a través de cualquier medio o contra su voluntad, a realizar un trabajo o prestar un servicio, sea retribuido o no, será reprimido con pena privativa de libertad no menor de seis ni mayor de doce años.

La pena será privativa de libertad no menor de doce años ni mayor de quince años, si concurre alguna de las siguientes circunstancias:

1. El agente tiene a la víctima bajo su cuidado o vigilancia por cualquier motivo, o mantiene con ella un vínculo de superioridad, autoridad, poder u otro que la impulse a depositar su confianza en él.

2. La víctima tiene entre catorce y menos de dieciocho años de edad, y la actividad que desarrolla está prohibida por la ley en razón a su edad.

3. El agente comete el delito en el marco de la actividad de una persona jurídica o en el contexto de cualquier actividad económica.

La pena será privativa de libertad no menor de quince ni mayor de veinte años, en los siguientes casos:

1. El agente es familiar de la víctima hasta el cuarto grado de consanguinidad o segundo de afinidad.

2. Existe pluralidad de víctimas.

3. La víctima tiene menos de catorce años de edad, es adulta mayor, tiene discapacidad, padece de enfermedad grave, pertenece a un pueblo indígena, es trabajador migrante o presenta cualquier situación de vulnerabilidad.

4. Se produzca lesión grave o se ponga en peligro inminente la vida o la salud de la víctima.

5. Se derive de una situación de trata de personas.

Si se produce la muerte de la víctima, la pena privativa de libertad es no menor de veinte ni mayor de veinticinco años.

En todos los casos se impondrá además la pena de inhabilitación conforme al artículo 36 incisos 1, 2, 3, 4, 5, 6, 8, 10 y 11.”(*)(**)

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1323, publicado el 06 enero 2017.

(**) Artículo modificado por el Artículo Único de la Ley N° 30924, publicada el 29 marzo 2019, cuyo texto es el siguiente:

“Artículo 168-B. Trabajo forzoso

El que somete u obliga a otra persona, a través de cualquier medio o contra su voluntad, a realizar un trabajo o prestar un servicio, sea retribuido o no, será reprimido con pena privativa de libertad no menor de seis ni mayor de doce años y multa de cien a doscientos días-multa.

La pena será privativa de libertad no menor de doce años ni mayor de quince años y multa de doscientos a trescientos días-multa si concurre alguna de las siguientes circunstancias:

1. El agente tiene a la víctima bajo su cuidado o vigilancia por cualquier motivo, o mantiene con ella un vínculo de superioridad, autoridad, poder u otro que la impulse a depositar su confianza en él.

2. La víctima tiene entre catorce y menos de dieciocho años de edad, y la actividad que desarrolla está prohibida por la ley en razón a su edad.

3. El agente comete el delito en el marco de la actividad de una persona jurídica o en el contexto de cualquier actividad económica.

La pena será privativa de libertad no menor de quince ni mayor de veinte años y multa de trescientos a trescientos sesenta y cinco días-multa, en los siguientes casos:

1. El agente es familiar de la víctima hasta el cuarto grado de consanguinidad o segundo de afinidad.

2. Existe pluralidad de víctimas.

3. La víctima tiene menos de catorce años de edad, es adulta mayor, tiene discapacidad, padece de enfermedad grave, pertenece a un pueblo indígena, es trabajador migrante o presenta cualquier situación de vulnerabilidad.

4. Se produzca lesión grave o se ponga en peligro inminente la vida o la salud de la víctima.

5. Se derive de una situación de trata de personas.

Si se produce la muerte de la víctima, la pena privativa de libertad es no menor de veinte ni mayor de veinticinco años. Se aplica la misma multa si se dan los agravantes precedentes. En todos los casos se impondrá además la pena de inhabilitación conforme al artículo 36 incisos 1, 2, 3, 4, 5, 6, 8, 10 y 11."

CONCORDANCIAS:     D.S.N° 015-2019-TR (Decreto Supremo que aprueba el III Plan Nacional para la Lucha contra el Trabajo Forzoso 2019 - 2022)

R.M.N° 020-2020-TR (Declaran el día 1 de febrero de cada año como el “Día de la Lucha contra el Trabajo Forzoso”)

CAPITULO VIII VIOLACION DE LA LIBERTAD DE EXPRESION
###################################################

Artículo 169 Violación de la libertad de expresión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario público que, abusando de su cargo, suspende o clausura algún medio de comunicación social o impide su circulación o difusión, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años e inhabilitación conforme al artículo 36, incisos 1 y 2.

CAPITULO IX VIOLACION DE LA LIBERTAD SEXUAL
###########################################

(*) De conformidad con Literal c) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del delito de violación sexual, previsto en los artículos 170, 171, 172, 173, 174, 175, y sus formas agravadas comprendidas en el artículo 177 del presente Código Penal, cuando la víctima es una mujer agredida por su condición de tal, o niños, niñas o adolescentes.

(*) De conformidad con el Artículo 5 de la Ley N° 30838, publicada el 04 agosto 2018, no procede la terminación anticipada ni la conclusión anticipada en los procesos por cualquiera de los delitos previstos en el presente Capítulo.

(*) De conformidad con el Literal c) del Artículo 3 del Decreto Legislativo N° 1368, publicado el 29 julio 2018, el sistema  es competente para conocer las medidas de protección y las medidas cautelares que se dicten en el marco de la Ley Nº 30364, así como los procesos penales que se siguen por la comisión del delito de Violación sexual, previstos en los artículos 170, 171, 172, 173, 173-A y 174, y sus formas agravadas comprendidas en el artículo 177 del Código Penal cuando la víctima es una mujer agredida por su condición de tal, o niños, niñas o adolescentes.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

CONCORDANCIAS:     R.M. Nº 0405-2007-ED (Aprueban Lineamientos de acción en caso de maltrato físico y/o psicológico, hostigamiento sexual y violación de la
libertad sexual a estudiantes de Instituciones Educativas)
R.M.Nº 0519-2012-ED (Aprueban Directiva “Lineamientos para la prevención y protección de las y los estudiantes contra la violencia ejercida por personal de las Instituciones Educativas”)

Artículo 170 Violación sexual
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, con violencia o grave amenaza, obliga a una persona a practicar el acto sexual u otro análogo, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años.

Si la violación se realiza a mano armada y por dos o más sujetos, la pena será no menor de cuatro ni mayor de doce años. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 26293, publicado el 14 febrero 1994, cuyo texto es el siguiente:

Violación sexual

Artículo 170.- El que con violencia o grave amenaza, obliga a una persona a practicar el acto sexual u otro análogo, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años.

Si la violación se realiza a mano armada y por dos o más sujetos, la pena será no menor de ocho ni mayor de quince años. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28251, publicada el 08 junio 2004, cuyo texto es el siguiente:

“Artículo 170.- Violación sexual

El que con violencia o grave amenaza, obliga a una persona a tener acceso carnal por vía vaginal, anal o bucal o realiza otros actos análogos introduciendo objetos o partes del cuerpo por alguna de las dos primeras vías, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años.

La pena será no menor de ocho ni mayor de quince años e inhabilitación conforme corresponda:

1. Si la violación se realiza a mano armada y por dos o más sujetos.

2. Si para la ejecución del delito se haya prevalido de cualquier posición o cargo que le dé particular autoridad sobre la víctima, o de una relación de parentesco por ser ascendiente, descendiente o hermano, por naturaleza o adopción o afines de la víctima.

3. Si fuere cometido por personal perteneciente a las Fuerzas Armadas, Policía Nacional del Perú, Serenazgo, Policía Municipal o vigilancia privada, en ejercicio de su función pública.

4. Si la víctima tiene entre catorce y menos de dieciocho años.

5. Si el autor tuviere conocimiento de ser portador de una enfermedad de transmisión sexual grave." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28704, publicada el 05 abril 2006, cuyo texto es el siguiente:

“Artículo 170.- Violación sexual

El que con violencia o grave amenaza, obliga a una persona a tener acceso carnal por vía vaginal, anal o bucal o realiza otros actos análogos introduciendo objetos o partes del cuerpo por alguna de las dos primeras vías, será reprimido con pena privativa de libertad no menor de seis ni mayor de ocho años.

La pena será no menor de doce ni mayor de dieciocho años e inhabilitación conforme corresponda:

1. Si la violación se realiza a mano armada o por dos o más sujetos.

2. Si para la ejecución del delito se haya prevalido de cualquier posición o cargo que le dé particular autoridad sobre la víctima, o de una relación de parentesco por ser ascendiente, cónyuge de éste, descendiente o hermano, por naturaleza o adopción o afines de la víctima. (*)

(*) Numeral modificado por el Artículo Único de la Ley N° 28963, publicada el 24 enero 2007, cuyo texto es el siguiente:

"2. Si para la ejecución del delito se haya prevalido de cualquier posición o cargo que le dé particular autoridad sobre la víctima, o de una relación de parentesco por ser ascendente, cónyuge, conviviente de éste, descendiente o hermano, por naturaleza o adopción o afines de la víctima, de una relación proveniente de un contrato de locación de servicios, de una relación laboral o si la víctima le presta servicios como trabajador del hogar."

3. Si fuere cometido por personal perteneciente a las Fuerzas Armadas, Policía Nacional del Perú, Serenazgo, Policía Municipal o vigilancia privada, en ejercicio de su función pública.

4. Si el autor tuviere conocimiento de ser portador de una enfermedad de transmisión sexual grave.

5. Si el autor es docente o auxiliar de educación del centro educativo donde estudia la víctima. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 30076, publicada el 19 agosto 2013, cuyo texto es el siguiente:

"Artículo 170. Violación sexual

El que con violencia o grave amenaza, obliga a una persona a tener acceso carnal por vía vaginal, anal o bucal o realiza otros actos análogos introduciendo objetos o partes del cuerpo por alguna de las dos primeras vías, será reprimido con pena privativa de libertad no menor de seis ni mayor de ocho años.

La pena será no menor de doce ni mayor de dieciocho años e inhabilitación conforme corresponda:

1. Si la violación se realiza a mano armada o por dos o más sujetos. (*) RECTIFICADO POR FE DE ERRATAS

2. Si para la ejecución del delito se haya prevalido de cualquier posición o cargo que le dé particular autoridad sobre la víctima, o de una relación de parentesco por ser ascendente, cónyuge, conviviente de este, descendiente o hermano, por naturaleza o adopción o afines de la víctima, de una relación proveniente de un contrato de locación de servicios, de una relación laboral o si la víctima le presta servicios como trabajador del hogar.

3. Si fuere cometido por personal perteneciente a las Fuerzas Armadas, Policía Nacional del Perú, Serenazgo, Policía Municipal o vigilancia privada, en ejercicio de su función pública.

4. Si el autor tuviere conocimiento de ser portador de una enfermedad de transmisión sexual grave.

5. Si el autor es docente o auxiliar de educación del centro educativo donde estudia la víctima.

6. Si la víctima tiene entre catorce y menos de dieciocho años de edad." (1)(2)(3)(4)(5)

(1) De conformidad con el Artículo 3 de la Ley N° 28704, publicada el 05 abril 2006, en el caso del delito previsto en el presente Artículo, el interno redime la pena mediante el trabajo o la educación a razón de un día de pena por cinco días de labor efectiva o de estudio, en su caso.

(2) De conformidad con el Numeral 5 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de violación de la libertad sexual, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

(3) De conformidad con el Artículo 5 de la Ley N° 30813, publicada el 08 julio 2018, no se aplicará lo establecido en el artículo 3 de la citada Ley (Descarga procesal en el sistema fiscal y judicial) cuando los procesos judiciales se refieran a delitos de terrorismo, tráfico ilícito de drogas, traición a la patria, lavado de activos, lesa humanidad, robo agravado, asesinato, violación sexual de menores de edad, violación sexual de menor de edad seguida de muerte o lesión grave, formas agravadas del presente artículo sobre violación de la libertad sexual, organizaciones criminales, corrupción y demás delitos contra la Administración Pública que afecten al patrimonio del Estado. Estas disposiciones no afectan las acciones judiciales civiles que puedan derivarse del caso penal.

(4) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

(5) Artículo modificado por el Artículo 1 de la Ley N° 30838, publicada el 04 agosto 2018, cuyo texto es el siguiente:

"Artículo 170.- Violación sexual

El que con violencia, física o psicológica, grave amenaza o aprovechándose de un entorno de coacción o de cualquier otro entorno que impida a la persona dar su libre consentimiento, obliga a esta a tener acceso carnal por vía vaginal, anal o bucal o realiza cualquier otro acto análogo con la introducción de un objeto o parte del cuerpo por alguna de las dos primeras vías, será reprimido con pena privativa de libertad no menor de catorce ni mayor de veinte años. La pena privativa de libertad será no menor de veinte ni mayor de veintiséis años, en cualquiera de los casos siguientes:

1. Si la violación se realiza con el empleo de arma o por dos o más sujetos.

2. Si el agente abusa de su profesión, ciencia u oficio o se aprovecha de cualquier posición, cargo o responsabilidad legal que le confiera el deber de vigilancia, custodia o particular autoridad sobre la víctima o la impulsa a depositar su confianza en él.

3. Si el agente aprovecha su calidad de ascendiente o descendiente, por consanguinidad, adopción o afinidad; o de cónyuge, excónyuge, conviviente o exconviviente o con la víctima esté sosteniendo o haya sostenido una relación análoga; o tiene hijos en común con la víctima; o habita en el mismo hogar de la víctima siempre que no medien relaciones contractuales o laborales; o es pariente colateral hasta el cuarto grado, por consanguinidad o adopción o segundo grado de afinidad.

4. Si es cometido por pastor, sacerdote o líder de una organización religiosa o espiritual que tenga particular ascendencia sobre la víctima.

5. Si el agente tiene cargo directivo, es docente, auxiliar o personal administrativo en el centro educativo donde estudia la víctima.

6. Si mantiene una relación proveniente de un contrato de locación de servicios, o de una relación laboral con la víctima, o si esta le presta servicios como trabajador del hogar.

7. Si fuera cometido por personal perteneciente a las Fuerzas Armadas, Policía Nacional del Perú, Serenazgo, Policía Municipal o vigilancia privada, o cualquier funcionario o servidor público, valiéndose del ejercicio de sus funciones o como consecuencia de ellas.

8. Si el agente tiene conocimiento de ser portador de una enfermedad de transmisión sexual grave.

9. Si el agente, a sabiendas, comete la violación sexual en presencia de cualquier niña, niño o adolescente.

10. Si la víctima se encuentra en estado de gestación.

11. Si la víctima tiene entre catorce y menos de dieciocho años de edad, es adulto mayor o sufre de discapacidad, física o sensorial, y el agente se aprovecha de dicha condición.

12. Si la víctima es mujer y es agraviada por su condición de tal en cualquiera de los contextos previstos en el primer párrafo del artículo 108-B.

13. Si el agente actúa en estado de ebriedad, con presencia de alcohol en la sangre en proporción mayor de 0.5 gramos-litro, o bajo el efecto de drogas tóxicas, estupefacientes, sustancias psicotrópicas o sintéticas que pudiera alterar su conciencia."

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 171 Violación de persona en estado de inconsciencia o en la imposibilidad de resistir
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que practica el acto sexual u otro análogo con una persona, después de haberla puesto con este objeto en estado de inconsciencia o en la imposibilidad de resistir, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 26293, publicada el 14 febrero 1994, cuyo texto es el siguiente:

Violación de persona en estado de inconsciencia o en la imposibilidad de resistir

"Artículo 171.- El que practica el acto sexual u otro análogo con una persona, después de haberla puesto con ese objeto en estado de inconsciencia o en la imposibilidad de resistir, será reprimido con pena privativa de libertad no menor de 5 ni mayor de 10 años." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28251, publicada el 08 junio 2004, cuyo texto es el siguiente:

"Artículo 171.- Violación de persona en estado de inconsciencia o en la imposibilidad de resistir

El que tiene acceso carnal con una persona por vía vaginal, anal o bucal, o realiza otros actos análogos introduciendo objetos o partes del cuerpo por alguna de las dos primeras vías, después de haberla puesto en estado de inconsciencia o en la imposibilidad de resistir, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años.

Cuando el autor comete este delito abusando de su profesión, ciencia u oficio, la pena será privativa de la libertad no menor de ocho ni mayor a doce años." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28704, publicada el 05 abril 2006, cuyo texto es el siguiente:

"Artículo 171.- Violación de persona en estado de inconsciencia o en la imposibilidad de resistir

El que tiene acceso carnal con una persona por vía vaginal, anal o bucal, o realiza otros actos análogos introduciendo objetos o partes del cuerpo por alguna de las dos primeras vías, después de haberla puesto en estado de inconsciencia o en la imposibilidad de resistir, será reprimido con pena privativa de libertad no menor de diez ni mayor de quince años.

Cuando el autor comete este delito abusando de su profesión, ciencia u oficio, la pena será privativa de la libertad no menor de doce ni mayor a dieciocho años." (1)(2)(3)(4)(5)

(1) De conformidad con el Artículo 3 de la Ley N° 28704, publicada el 05 abril 2006, en el caso del delito previsto en el presente Artículo, el interno redime la pena mediante el trabajo o la educación a razón de un día de pena por cinco días de labor efectiva o de estudio, en su caso.

(2) De conformidad con el Numeral 5 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de violación de la libertad sexual, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

(3) De conformidad con el Artículo 5 de la Ley N° 30813, publicada el 08 julio 2018, no se aplicará lo establecido en el artículo 3 de la citada Ley (Descarga procesal en el sistema fiscal y judicial) cuando los procesos judiciales se refieran a delitos de terrorismo, tráfico ilícito de drogas, traición a la patria, lavado de activos, lesa humanidad, robo agravado, asesinato, violación sexual de menores de edad, violación sexual de menor de edad seguida de muerte o lesión grave, formas agravadas del presente artículo sobre violación de la libertad sexual, organizaciones criminales, corrupción y demás delitos contra la Administración Pública que afecten al patrimonio del Estado. Estas disposiciones no afectan las acciones judiciales civiles que puedan derivarse del caso penal.

(4) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

(5) Artículo modificado por el Artículo 1 de la Ley N° 30838, publicada el 04 agosto 2018, cuyo texto es el siguiente:

"Artículo 171.- Violación de persona en estado de inconsciencia o en la imposibilidad de resistir

El que tiene acceso carnal con una persona por vía vaginal, anal o bucal, o realiza cualquier otro acto análogo con la introducción de un objeto o parte del cuerpo por alguna de las dos primeras vías, después de haberla puesto en estado de inconsciencia o en la imposibilidad de resistir, será reprimido con pena privativa de libertad no menor de veinte ni mayor de veintiséis años."

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 172 Violación de persona en incapacidad de resistencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, conociendo el estado de su víctima, practica el acto sexual u otro análogo con una persona que sufre anomalía psíquica, grave alteración de la conciencia, retardo mental o que se encuentra en incapacidad de resistir, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 26293, publicada el 14 febrero 1994, cuyo texto es el siguiente:

Artículo 172 Violación de persona en incapacidad de resistencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, conociendo el estado de su víctima, practica el acto sexual u otro análogo con una persona que sufre anomalía psíquica, grave alteración de la conciencia, retardo mental o que se encuentra en incapacidad de resistir, será reprimido con pena privativa de libertad no menor de 5 ni mayor de 10 años (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28251, publicada el 08 junio 2004, cuyo texto es el siguiente:

"Artículo 172.- Violación de persona en incapacidad de resistencia

El que tiene acceso carnal con una persona por vía vaginal, anal o bucal o realiza otros actos análogos introduciendo objetos o partes del cuerpo por alguna de las dos primeras vías, conociendo que sufre anomalía psíquica, grave alteración de la conciencia, retardo mental o que se encuentra en incapacidad de resistir, será reprimido con pena privativa de libertad no menor de veinte ni mayor de veinticinco años.

Cuando el autor comete el delito abusando de su profesión, ciencia u oficio, la pena será privativa de la libertad no menor de ocho ni mayor a doce años." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28704, publicada el 05 abril 2006, cuyo texto es el siguiente:

"Artículo 172.- Violación de persona en incapacidad de resistencia

El que tiene acceso carnal con una persona por vía vaginal, anal o bucal o realiza otros actos análogos introduciendo objetos o partes del cuerpo por alguna de las dos primeras vías, conociendo que sufre anomalía psíquica, grave alteración de la conciencia, retardo mental o que se encuentra en incapacidad de resistir, será reprimido con pena privativa de libertad no menor de veinte ni mayor de veinticinco años.

Cuando el autor comete el delito abusando de su profesión, ciencia u oficio, la pena será privativa de libertad no menor de veinticinco ni mayor de treinta años." (1)(2)(3)(4)(5)

(1) De conformidad con el Artículo 3 de la Ley N° 28704, publicada el 05 abril 2006, en el caso del delito previsto en el presente Artículo, el interno redime la pena mediante el trabajo o la educación a razón de un día de pena por cinco días de labor efectiva o de estudio, en su caso.

(2) De conformidad con el Numeral 5 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de violación de la libertad sexual, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

(3) De conformidad con el Artículo 5 de la Ley N° 30813, publicada el 08 julio 2018, no se aplicará lo establecido en el artículo 3 de la citada Ley (Descarga procesal en el sistema fiscal y judicial) cuando los procesos judiciales se refieran a delitos de terrorismo, tráfico ilícito de drogas, traición a la patria, lavado de activos, lesa humanidad, robo agravado, asesinato, violación sexual de menores de edad, violación sexual de menor de edad seguida de muerte o lesión grave, formas agravadas del presente artículo sobre violación de la libertad sexual, organizaciones criminales, corrupción y demás delitos contra la Administración Pública que afecten al patrimonio del Estado. Estas disposiciones no afectan las acciones judiciales civiles que puedan derivarse del caso penal.

(4) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

(5) Artículo modificado por el Artículo 1 de la Ley N° 30838, publicada el 04 agosto 2018, cuyo texto es el siguiente:

"Artículo 172.- Violación de persona en incapacidad de dar su libre consentimiento

El que tiene acceso carnal con una persona por vía vaginal, anal o bucal o realiza cualquier otro acto análogo con la introducción de un objeto o parte del cuerpo por alguna de las dos primeras vías, conociendo que está impedida de dar su libre consentimiento por sufrir de anomalía psíquica, grave alteración de la conciencia, retardo mental o que se encuentra en incapacidad de resistir, será reprimido con pena privativa de libertad no menor de veinte ni mayor de veintiséis años."

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 173 
~~~~~~~~~~~~~

El que practica el acto sexual u otro análogo con un menor de catorce años, será reprimido con las siguientes penas privativas de libertad:

1. Si la víctima tiene menos de siete años, la pena será no menor de quince años.

2. Si la víctima tiene de siete años a menos de diez, la pena será no menor de ocho años.

3. Si la víctima tiene de diez años a menos de catorce, la pena será no menor cinco años.

Si el menor es un discípulo, aprendiz o doméstico del agente o su descendiente, hijo adoptivo, hijo de su cónyuge o de su concubina, o un menor confiado a su cuidado, la pena privativa de libertad será, respectivamente, no menor de veinte, doce y ocho años, para cada uno de los casos previstos en los tres incisos anteriores. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 26293, publicada el 14 febrero 1994, cuyo texto es el siguiente:

"Artículo 173.- El que practica el acto sexual u otro análogo con un menor de catorce años, será reprimido con las siguientes penas privativas de libertad:

1. Si la víctima tiene menos de siete años, la pena será no menor de 20 años ni mayor de 25 años.

2. Si la víctima tiene de siete años a menos de diez, la pena será no menor de 15 ni mayor de 20 años.

3. Si la víctima tiene de diez años a menos de catorce, la pena será no menor de 10 ni mayor de 15 años.

Si el agente tuviere cualquier posición, cargo o vínculo familiar que le dé particular autoridad sobre la víctima o le impulse a depositar en él su confianza, la pena será respectivamente no menor de 25 ni mayor de 30 años, no menor de 20 ni mayor de 25 años y no menor de 15 ni mayor de 20 años para cada uno de los supuestos previstos en los incisos 1, 2 y 3 del párrafo anterior." (*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 896, publicado el 24 mayo 1998, expedido con arreglo a la Ley N° 26950, que otorga al Poder Ejecutivo facultades para legislar en materia de seguridad nacional, cuyo texto es el siguiente:

"Violación Sexual de Menor de Catorce Años de Edad
Artículo 173.- El que practica el acto sexual u otro análogo con un menor de catorce años de edad, será reprimido con las siguientes penas privativas de libertad:

1.- Si la víctima tiene menos de siete años, la pena será de cadena perpetua.

2.- Si la víctima tiene de siete años a menos de diez, la pena será no menor de veinticinco ni mayor de treinta años.

3. - Si la víctima tiene de diez años a menos de catorce, la pena será no menor de veinte ni mayor de veinticinco años.

Si el agente tuviere cualquier posición, cargo o vínculo familiar que le dé particular autoridad sobre la víctima o le impulse a depositar en él su confianza, la pena será no menor de treinta años para los supuestos previstos en los incisos 2 y 3." (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 27472, publicada el 05 junio 2001, cuyo texto es el siguiente:

"Artículo 173.- Violación sexual de menor de catorce años de edad

El que practica el acto sexual u otro análogo con un menor de catorce años de edad, será reprimido con las siguientes penas privativas de libertad:

1. Si la víctima tiene menos de siete años, la pena será no menor de veinte ni mayor de veinticinco años.

2. Si la víctima tiene de siete años a menos de diez, la pena será no menor de quince ni mayor de veinte años.

3. Si la víctima tiene de diez años a menos de catorce, la pena será no menor de diez ni mayor de quince años.

Si el agente tuviere cualquier posición, cargo o vínculo familiar que le dé particular autoridad sobre la víctima o le impulse a depositar en él su confianza, la pena será no menor de veinticinco años para los supuestos previstos en los incisos 2 y 3." (*)

(*) Texto del Artículo 173 restablecido por el Artículo 1 de la Ley Nº 27507, publicada el 13 julio 2001, cuyo texto es el siguiente:

"Artículo 173.- Violación de menor de catorce años de edad

El que practica el acto sexual u otro análogo con un menor de catorce años de edad, será reprimido con las siguientes penas privativas de libertad:

1. Si la víctima tiene menos de siete años, la pena será de cadena perpetua.

2. Si la víctima tiene de siete años a menos de diez, la pena será no menor de veinticinco ni mayor de treinta años.

3. Si la víctima tiene de diez años a menos de catorce, la pena será no menor de veinte ni mayor de veinticinco años.

Si el agente tuviere cualquier posición, cargo o vínculo familiar que le dé particular autoridad sobre la víctima o le impulse a depositar en él su confianza, la pena será no menor de treinta años para los supuestos previstos en los incisos 2 y 3." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28251, publicada el 08 junio 2004, cuyo texto es el siguiente:

"Artículo 173.- Violación sexual de menor de catorce años de edad

El que tiene acceso carnal por vía vaginal, anal o bucal o realiza otros actos análogos introduciendo objetos o partes del cuerpo por alguna de las dos primeras vías, con un menor de edad, será reprimido con las siguientes penas privativas de la libertad:

1. Si la víctima tiene menos de siete años, la pena será cadena perpetua.

2. Si la víctima tiene de siete años a menos de diez, la pena será no menor de veinticinco ni mayor de treinta años.

3. Si la víctima tiene de diez años a menos de catorce, la pena será no menor de veinte ni mayor de veinticinco años.

Si el agente tuviere cualquier posición, cargo o vínculo familiar que le dé particular autoridad sobre la víctima o le impulse a depositar en él su confianza, la pena será no menor de treinta años para los supuestos previstos en los incisos 2 y 3." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28704, publicada el 05 abril 2006, cuyo texto es el siguiente:

"Artículo 173.- Violación sexual de menor de edad

El que tiene acceso carnal por vía vaginal, anal o bucal o realiza otros actos análogos introduciendo objetos o partes del cuerpo por alguna de las dos primeras vías, con un menor de edad, será reprimido con las siguientes penas privativas de libertad:

1. Si la víctima tiene menos de diez años de edad, la pena será de cadena perpetua.

2. Si la víctima tiene entre diez años de edad, y menos de catorce, la pena será no menor de treinta años, ni mayor de treinta y cinco.

3. Si la víctima tiene entre catorce años de edad y menos de dieciocho, la pena será no menor de veinticinco ni mayor de treinta años. (*)

(*) Inciso 3) declarado inconstitucional por el Resolutivo 1 de la Sentencia del Tribunal Constitucional, recaída en el Expediente N° 00008-2012-PI-TC, publicada el 24 enero 2013.

Si el agente tuviere cualquier posición, cargo o vínculo familiar que le dé particular autoridad sobre la víctima o le impulse a depositar en él su confianza, la pena para los sucesos previstos en los incisos 2 y 3, será de cadena perpetua." (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 30076, publicada el 19 agosto 2013, cuyo texto es el siguiente:

"Artículo 173. Violación sexual de menor de edad

El que tiene acceso carnal por vía vaginal, anal o bucal o realiza otros actos análogos introduciendo objetos o partes del cuerpo por alguna de las dos primeras vías, con un menor de edad, será reprimido con las siguientes penas privativas de libertad:

1. Si la víctima tiene menos de diez años de edad la pena será de cadena perpetua.

2. Si la víctima tiene entre diez años de edad, y menos de catorce, la pena será no menor de treinta, ni mayor de treinta y cinco años. (*) RECTIFICADO POR FE DE ERRATAS

En el caso del numeral 2, la pena será de cadena perpetua si el agente tiene cualquier posición, cargo o vínculo familiar que le dé particular autoridad sobre la víctima o le impulse a depositar en él su confianza." (1)(2)(3)(4)(5)

(1) De conformidad con el Artículo 2 de la Ley N° 28704, publicada el 05 abril 2006, no procede el indulto, ni la conmutación de pena ni el derecho de gracia a los sentenciados por los delitos previstos en el presente Artículo.

(2) De conformidad con el Artículo 3 de la Ley N° 28704, publicada el 05 abril 2006, los beneficios penitenciarios de redención de la pena por el trabajo y la educación, semi-libertad y liberación condicional no son aplicables a los sentenciados por el delito previsto en el presente Artículo.

(3) De conformidad con el Numeral 5 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de violación de la libertad sexual, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

(4) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

(5) Artículo modificado por el Artículo 1 de la Ley N° 30838, publicada el 04 agosto 2018, cuyo texto es el siguiente:

"Artículo 173.- Violación sexual de menor de edad

El que tiene acceso carnal por vía vaginal, anal o bucal o realiza cualquier otro acto análogo con la introducción de un objeto o parte del cuerpo por alguna de las dos primeras vías, con un menor de catorce años, será reprimido con pena de cadena perpetua."

CONCORDANCIAS:     R.M. N° 193-2007-JUS, Art. 25
R.M. N° 0162-2010-JUS, Art. 27

Sentencia Plena Casatoria N° 1-2018-CIJ-433 (I Pleno Casatorio Penal de la Corte Suprema de Justicia de la República)

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA
PROCESOS CONSTITUCIONALES

"Artículo 173-A.- Si los actos previstos en los incisos 1, 2 y 3 del artículo anterior causan la muerte de la víctima o le producen lesión grave, y el agente pudo prever este resultado o si procedió con crueldad, la pena será respectivamente de cadena perpetua y no menor de 25 ni mayor de 30 años." (1)(2)

(1) Artículo incorporado por el Artículo 2 de la Ley Nº 26293, publicado el 14 febrero 1994.

(2) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 896, publicado el 24 mayo 1998, expedido con arreglo a la Ley N° 26950, que otorga al Poder Ejecutivo facultades para legislar en materia de seguridad nacional, cuyo texto es el siguiente:

Artículo 173 "Violación de menor de catorce años seguida de muerte o lesión grave
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Si los actos previstos en los incisos 2 y 3 del artículo anterior causan la muerte de la víctima o le producen lesión grave, y el agente pudo prever este resultado o si procedió con crueldad, la pena será de cadena perpetua." (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 27472, publicada el 05 junio 2001, cuyo texto es el siguiente:

"Artículo 173-A.- Violación de menor de catorce años seguida de muerte o lesión grave

Si los actos previstos en los incisos 1, 2 y 3 del artículo anterior causan la muerte de la víctima o le producen lesión grave, y el agente pudo prever este resultado o si procedió con crueldad, la pena será de cadena perpetua; y, si le producen lesión grave la pena será no menor de veinticinco ni mayor de treinta años." (*)

(*) Texto del Artículo 173-A restablecido por el Artículo 1 de la Ley Nº 27507 publicada el 13 julio 2001, cuyo texto es el siguiente:

"Artículo 173 A.- Violación de menor de catorce años seguida de muerte o lesión grave

Si los actos previstos en los incisos 2 y 3 del artículo anterior causan la muerte de la víctima o le producen lesión grave, y el agente pudo prever este resultado o si procedió con crueldad, la pena será de cadena perpetua.” (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28704, publicada el 05 abril 2006, cuyo texto es el siguiente:

"Artículo 173-A.- Violación sexual de menor de edad seguida de muerte o lesión grave

Si los actos previstos en los incisos 2 y 3 del artículo anterior causan la muerte de la víctima o le producen lesión grave, y el agente pudo prever este resultado o si procedió con crueldad, la pena será de cadena perpetua." (1)(2)(3)(4)(5)

(1) De conformidad con el Artículo 2 de la Ley N° 28704, publicada el 05 abril 2006, no procede el indulto, ni la conmutación de pena ni el derecho de gracia a los sentenciados por los delitos previstos en el presente Artículo.

(2) De conformidad con el Artículo 3 de la Ley N° 28704, publicada el 05 abril 2006, los beneficios penitenciarios de redención de la pena por el trabajo y la educación, semi-libertad y liberación condicional no son aplicables a los sentenciados por el delito previsto en el presente Artículo.

(3) De conformidad con el Numeral 5 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de violación de la libertad sexual, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

(4) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

(5) Artículo derogado por la Única Disposición Complementaria Derogatoria de la Ley N° 30838, publicada el 04 agosto 2018.

CONCORDANCIAS:     R.M. N° 193-2007-JUS, Art. 25
R.M. N° 0162-2010-JUS, Art. 27

PROCESOS CONSTITUCIONALES

Artículo 174 
~~~~~~~~~~~~~

El que, aprovechando la situación de dependencia, autoridad o vigilancia practica el acto sexual u otro análogo con una persona colocada en un hospital, asilo u otro establecimiento similar o que se halla detenida, recluída o interna, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años e inhabilitación de dos a cuatro años, conforme al artículo 36, incisos 1, 2 y 3. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 26293, publicada el  14 febrero 1994, cuyo texto es el siguiente:

Violación de persona bajo autoridad o vigilancia

"Artículo 174.- El que, aprovechando la situación de dependencia, autoridad o vigilancia practica el acto sexual u otro análogo con una persona colocada en un hospital, asilo u otro establecimiento similar o que se halla detenida, recluída o interna, será reprimido con pena privativa de libertad no menor de 5 ni mayor de 8 años e inhabilitación de dos a cuatro años, conforme al artículo 36, incisos 1, 2 y 3." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28251, publicada el 08 junio 2004, cuyo texto es el siguiente:

"Artículo 174.- Violación de persona bajo autoridad o vigilancia

El que, aprovechando la situación de dependencia, autoridad o vigilancia tiene acceso carnal por vía vaginal, anal o bucal o introduce objetos o partes del cuerpo por alguna de las dos primeras vías a una persona colocada en un hospital, asilo u otro establecimiento similar o que se halle detenida o recluida o interna, será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años e inhabilitación de dos a cuatro años, conforme al artículo 36, incisos 1, 2 y 3." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28704, publicada el 05 abril 2006, cuyo texto es el siguiente:

"Artículo 174.- Violación de persona bajo autoridad o vigilancia

El que, aprovechando la situación de dependencia, autoridad o vigilancia tiene acceso carnal por vía vaginal, anal o bucal o introduce objetos o partes del cuerpo por alguna de las dos primeras vías a una persona colocada en un hospital, asilo u otro establecimiento similar o que se halle detenida o recluida o interna, será reprimido con pena privativa de libertad no menor de siete ni mayor de diez años e inhabilitación de dos a cuatro años, conforme al artículo 36, incisos 1, 2 y 3." (1)(2)(3)(4)(5)

(1) De conformidad con el Artículo 3 de la Ley N° 28704, publicada el 05 abril 2006, en el caso del delito previsto en el presente Artículo, el interno redime la pena mediante el trabajo o la educación a razón de un día de pena por cinco días de labor efectiva o de estudio, en su caso.

(2) De conformidad con el Numeral 5 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de violación de la libertad sexual, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

(3) De conformidad con el Artículo 5 de la Ley N° 30813, publicada el 08 julio 2018, no se aplicará lo establecido en el artículo 3 de la citada Ley (Descarga procesal en el sistema fiscal y judicial) cuando los procesos judiciales se refieran a delitos de terrorismo, tráfico ilícito de drogas, traición a la patria, lavado de activos, lesa humanidad, robo agravado, asesinato, violación sexual de menores de edad, violación sexual de menor de edad seguida de muerte o lesión grave, formas agravadas del presente artículo sobre violación de la libertad sexual, organizaciones criminales, corrupción y demás delitos contra la Administración Pública que afecten al patrimonio del Estado. Estas disposiciones no afectan las acciones judiciales civiles que puedan derivarse del caso penal.

(4) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

(5) Artículo modificado por el Artículo 1 de la Ley N° 30838, publicada el 04 agosto 2018, cuyo texto es el siguiente:

"Artículo 174.- Violación de persona bajo autoridad o vigilancia

El que, aprovechando la situación de dependencia, autoridad o vigilancia tiene acceso carnal por vía vaginal, anal o bucal o introduce objetos o partes del cuerpo por alguna de las dos primeras vías a una persona colocada en un hospital, asilo u otro establecimiento similar o que se halle detenida o recluida o interna, será reprimido con pena privativa de libertad no menor de veinte ni mayor de veintiséis años."

Artículo 175 
~~~~~~~~~~~~~

El que, mediante engaño, practica el acto sexual con una persona de catorce años y menor de dieciocho, será reprimido con pena privativa de libertad no mayor de dos años o con prestación de servicio comunitario de veinte a cincuentidós jornadas. (*)

(*) Artículo modificado por el Artículo 1 de la  Ley Nº 26357, publicada el 28 septiembre 1994, cuyo texto es el siguiente:

Seducción

"Artículo 175.- El que, mediante engaño, practica el acto sexual u otro análogo, con una persona de catorce años y menor de dieciocho, será reprimido con pena privativa de libertad no mayor de tres años o con prestación de servicio comunitario de treinta a setentiocho jornadas." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28251, publicada el 08 junio 2004, cuyo texto es el siguiente:

"Artículo 175.- Seducción

El que, mediante engaño tiene acceso carnal por vía vaginal, anal o bucal o introduce objetos o partes del cuerpo por alguna de las dos primeras vías, a una persona de catorce años y menos de dieciocho años será reprimido con pena privativa de libertad no menor de tres ni mayor de cinco años." (1)(2)(3)(4)(5)(6)

(1) De conformidad al acta de sesion plenaria del pleno jurisdiccional distrital penal de la Corte Superior de Justicia de Arequipa, setiembre 2006, se ha producido la derogatoria tácita del art. 175 del Código Penal modificado por la Ley 28704, referido al delito de seducción o estupro por engaño al haberse modificado el articulo 173 del mismo Código Sustantivo al Incluir en su inciso tercero como víctima de delito de violación presunta a menores entre catorce y menos de dieciocho años de edad.

(2) De conformidad al Resolutivo 1 de Sentencia recaída en el Expediente N° 00008-2012-PI-TC, publicada el 24 enero 2013, se declaró inconstitucional el artículo 173 inciso 3) del Código Penal, modificado por la Ley Nº 28704, normativa que sustentó el acuerdo sobre la derogatoria tácita del presente artículo, por parte del Pleno Jurisdiccional Distrital Penal de la Corte Superior de Justicia de Arequipa, setiembre 2006.

(3) De conformidad con el Numeral 5 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de violación de la libertad sexual, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

(4) De conformidad con el Artículo 5 de la Ley N° 30813, publicada el 08 julio 2018, no se aplicará lo establecido en el artículo 3 de la citada Ley (Descarga procesal en el sistema fiscal y judicial) cuando los procesos judiciales se refieran a delitos de terrorismo, tráfico ilícito de drogas, traición a la patria, lavado de activos, lesa humanidad, robo agravado, asesinato, violación sexual de menores de edad, violación sexual de menor de edad seguida de muerte o lesión grave, formas agravadas del presente artículo sobre violación de la libertad sexual, organizaciones criminales, corrupción y demás delitos contra la Administración Pública que afecten al patrimonio del Estado. Estas disposiciones no afectan las acciones judiciales civiles que puedan derivarse del caso penal.

(5) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

(6) Artículo modificado por el Artículo 1 de la Ley N° 30838, publicada el 04 agosto 2018, cuyo texto es el siguiente:

"Artículo 175.- Violación sexual mediante engaño

El que, mediante engaño tiene acceso carnal por vía vaginal, anal o bucal o realiza cualquier otro acto análogo con la introducción de un objeto o parte del cuerpo por alguna de las dos primeras vías, a una persona de catorce años y menos de dieciocho años será reprimido con pena privativa de libertad no menor de seis ni mayor de nueve años."

Artículo 176 
~~~~~~~~~~~~~

El que, sin propósito de practicar el acto sexual u otro análogo, comete un acto contrario al pudor en una persona menor de catorce años será reprimido con pena privativa de libertad no mayor de tres años.

Si la víctima está en una de las condiciones previstas por el último párrafo del artículo 173, la pena será no menor de tres ni mayor de seis años. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 26293, publicada el 14 febrero 1994, cuyo texto es el siguiente:

Actos contra el pudor

"Artículo 176.- El que sin propósito de practicar el acto sexual u otro análogo, con violencia o grave amenaza comete un acto contrario al pudor en una persona, será reprimido con pena privativa de libertad no mayor de tres años.

Si el agente se encuentra en las circunstancias previstas en el Artículo 174 la pena será no mayor de cinco años.

Si la víctima se hallare en los supuestos de los Artículos 171 y 172 la pena será no mayor de seis años." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28251, publicada el 08 junio 2004, cuyo texto es el siguiente:

"Artículo 176.- Actos contra el pudor

El que sin propósito de tener acceso carnal regulado por el artículo 170, con violencia o grave amenaza, realiza sobre una persona u obliga a ésta a efectuar sobre sí misma o sobre tercero tocamientos indebidos en sus partes íntimas o actos libidinosos contrarios al pudor será reprimido con pena privativa de libertad no menor de tres ni mayor de cinco años.

La pena será no menor de cuatro ni mayor de seis años:

1. Si el agente se encuentra en las agravantes previstas en el artículo 170 incisos 2, 3 y 4.
2. Si la víctima se hallare en los supuestos de los artículos 171 y 172." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28704, publicada el 05 abril 2006, cuyo texto es el siguiente:

"Artículo 176.- Actos contra el pudor

El que sin propósito de tener acceso carnal regulado por el artículo 170, con violencia o grave amenaza, realiza sobre una persona u obliga a ésta a efectuar sobre sí misma o sobre tercero, tocamientos indebidos en sus partes íntimas o actos libidinosos contrarios al pudor, será reprimido con pena privativa de libertad no menor de tres ni mayor de cinco años. La pena será no menor de cinco ni mayor de siete:

1. Si el agente se encuentra en las agravantes previstas en el artículo 170 incisos 2, 3 y 4.

2. Si la víctima se hallare en los supuestos de los artículos 171 y 172.

3. Si el agente tuviere la condición de docente, auxiliar u otra vinculación académica que le confiera autoridad sobre la víctima."(1)(2)(3)(4)

(1) De conformidad con el Numeral 5 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de violación de la libertad sexual, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

(2) De conformidad con el Artículo 5 de la Ley N° 30813, publicada el 08 julio 2018, no se aplicará lo establecido en el artículo 3 de la citada Ley (Descarga procesal en el sistema fiscal y judicial) cuando los procesos judiciales se refieran a delitos de terrorismo, tráfico ilícito de drogas, traición a la patria, lavado de activos, lesa humanidad, robo agravado, asesinato, violación sexual de menores de edad, violación sexual de menor de edad seguida de muerte o lesión grave, formas agravadas del presente artículo sobre violación de la libertad sexual, organizaciones criminales, corrupción y demás delitos contra la Administración Pública que afecten al patrimonio del Estado. Estas disposiciones no afectan las acciones judiciales civiles que puedan derivarse del caso penal.

(3) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

(4) Artículo modificado por el Artículo 1 de la Ley N° 30838, publicada el 04 agosto 2018, cuyo texto es el siguiente:

"Artículo 176.- Tocamientos, actos de connotación sexual o actos libidinosos sin consentimiento

El que sin propósito de tener acceso carnal regulado por el artículo 170, realiza sobre una persona, sin su libre consentimiento, tocamientos, actos de connotación sexual o actos libidinosos, en sus partes íntimas o en cualquier parte de su cuerpo será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años.

Si el agente realiza la conducta descrita en el primer párrafo, mediante amenaza, violencia, o aprovechándose de un entorno de coacción o de cualquier otro que impida a la víctima dar su libre consentimiento, o valiéndose de cualquiera de estos medios obliga a la víctima a realizarlos sobre el agente, sobre sí misma o sobre tercero, la pena privativa de libertad será no menor de seis ni mayor de nueve años.

En cualquiera de los casos previstos en el primer y segundo párrafos, la pena privativa de libertad se incrementa en cinco años en los extremos mínimo y máximo, si la víctima es mayor de catorce y menor de dieciocho años."

"Artículo 176-A.- El que sin propósito de practicar el acto sexual u otro análogo, comete un acto contrario al pudor en una persona menor de catorce años, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años.

Si la víctima está en algunas de las condiciones previstas en el último párrafo del Artículo 173, la pena será no menor de cinco ni mayor de ocho años."(1)(2)

(1) Artículo  incorporado por el Artículo 2 de la Ley Nº 26293, publicada el 14 febrero 1994.

(2) Artículo modificado por el Artículo 1 de la Ley Nº 27459, publicada el 26 mayo 2001, cuyo texto es el siguiente:

"Artículo 176- A.- Actos contra el pudor en menores

El que sin propósito de practicar el acto sexual u otro análogo, comete un acto contrario al pudor en una persona menor de catorce años, será reprimido con las siguientes penas privativas de libertad:

1. Si la víctima tiene menos de siete años, con pena no menor de siete ni mayor de diez años.
2. Si la víctima tiene de siete a menos de diez años, con pena no menor de cinco ni mayor de ocho años.
3. Si la víctima tiene de diez a menos de catorce años, con pena no menor de cuatro ni mayor de seis años.

Si la víctima se encuentra en alguna de las condiciones previstas en el último párrafo del Artículo 173 o el acto tiene un carácter particularmente degradante o produce un grave daño en la salud, física o mental de la víctima que el agente pudo prever, la pena será no menor de ocho ni mayor de doce años de pena privativa de libertad." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28251, publicada el 08 junio 2004, cuyo texto es el siguiente:

"Artículo 176-A.- Actos contra el pudor en menores

El que sin propósito de tener acceso carnal regulado en el artículo 170 realiza sobre un menor de catorce años u obliga a éste a efectuar sobre sí mismo o tercero, tocamientos indebidos en sus partes íntimas o actos libidinosos contrarios al pudor, será reprimido con las siguientes penas privativas de la libertad:

1. Si la víctima tiene menos de siete años, con pena no menor de siete ni mayor de diez años.

2. Si la víctima tiene de siete a menos de diez años, con pena no menor de cinco ni mayor de ocho años.

3. Si la víctima tiene de diez a menos de catorce años, con pena no menor de cuatro ni mayor de seis años.

Si la víctima se encuentra en alguna de las condiciones previstas en el último párrafo del artículo 173 o el acto tiene un carácter particularmente degradante o produce grave daño en la salud, física o mental de la víctima que el agente pudo prever, la pena será no menor de diez ni mayor de doce años de pena privativa de libertad." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28704, publicada el 05 abril 2006, cuyo texto es el siguiente:

"Artículo 176-A.- Actos contra el pudor en menores

El que sin propósito de tener acceso carnal regulado en el artículo 170, realiza sobre un menor de catorce años u obliga a éste a efectuar sobre sí mismo o tercero, tocamientos indebidos en sus partes íntimas o actos libidinosos contrarios al pudor, será reprimido con las siguientes penas privativas de la libertad:

1. Si la víctima tiene menos de siete años, con pena no menor de siete ni mayor de diez años.

2. Si la víctima tiene de siete a menos de diez años, con pena no menor de seis ni mayor de nueve años.

3. Si la víctima tiene de diez a menos de catorce años, con pena no menor de cinco ni mayor de ocho años.

Si la víctima se encuentra en alguna de las condiciones previstas en el último párrafo del artículo 173 o el acto tiene un carácter degradante o produce grave daño en la salud física o mental de la víctima que el agente pudo prever, la pena será no menor de diez ni mayor de doce años de pena privativa de libertad." (1)(2)(3)(4)(5)

(1) De conformidad con el Numeral 5 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de violación de la libertad sexual, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

(2) De conformidad con el Artículo 5 de la Ley N° 30813, publicada el 08 julio 2018, no se aplicará lo establecido en el artículo 3 de la citada Ley (Descarga procesal en el sistema fiscal y judicial) cuando los procesos judiciales se refieran a delitos de terrorismo, tráfico ilícito de drogas, traición a la patria, lavado de activos, lesa humanidad, robo agravado, asesinato, violación sexual de menores de edad, violación sexual de menor de edad seguida de muerte o lesión grave, formas agravadas del presente artículo sobre violación de la libertad sexual, organizaciones criminales, corrupción y demás delitos contra la Administración Pública que afecten al patrimonio del Estado. Estas disposiciones no afectan las acciones judiciales civiles que puedan derivarse del caso penal.

(3) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

(4) De conformidad con el Literal d) del Artículo 3 del Decreto Legislativo N° 1368, publicado el 29 julio 2018, el sistema  es competente para conocer las medidas de protección y las medidas cautelares que se dicten en el marco de la Ley Nº 30364, así como los procesos penales que se siguen por la comisión del delito de Actos contra el pudor en menores, previsto en el presente artículo.

(5) Artículo modificado por el Artículo 1 de la Ley N° 30838, publicada el 04 agosto 2018, cuyo texto es el siguiente:

"Artículo 176-A.- Tocamientos, actos de connotación sexual o actos libidinosos en agravio de menores

El que sin propósito de tener acceso carnal regulado en el artículo 170, realiza sobre un menor de catorce años u obliga a este a efectuar sobre sí mismo, sobre el agente o tercero, tocamientos indebidos en sus partes íntimas, actos de connotación sexual en cualquier parte de su cuerpo o actos libidinosos, será reprimido con pena privativa de libertad no menor de nueve ni mayor de quince años." (*)

(*) De conformidad con Literal d) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del delito de tocamientos, actos de connotación sexual o actos libidinosos en agravio de menores, previsto en el presente artículo.

“Artículo 176-B.- Acoso sexual

El que, de cualquier forma, vigila, persigue, hostiga, asedia o busca establecer contacto o cercanía con una persona, sin el consentimiento de esta, para llevar a cabo actos de connotación sexual, será reprimido con pena privativa de la libertad no menor de tres ni mayor de cinco años e inhabilitación, según corresponda, conforme a los incisos 5, 9, 10 y 11 del artículo 36.

Igual pena se aplica a quien realiza la misma conducta valiéndose del uso de cualquier tecnología de la información o de la comunicación.

La pena privativa de la libertad será no menor de cuatro ni mayor de ocho años e inhabilitación, según corresponda, conforme a los incisos 5, 9, 10 y 11 del artículo 36, si concurre alguna de las circunstancias agravantes:

1. La víctima es persona adulta mayor, se encuentra en estado de gestación o es persona con discapacidad.

2. La víctima y el agente tienen o han tenido una relación de pareja, son o han sido convivientes o cónyuges, tienen vínculo parental hasta el cuarto grado de consanguinidad o segundo de afinidad.

3. La víctima habita en el mismo domicilio que el agente o comparten espacios comunes de una misma propiedad.

4. La víctima se encuentra en condición de dependencia o subordinación con respecto al agente.

5. La conducta se lleva a cabo en el marco de una relación laboral, educativa o formativa de la víctima.

6. La víctima tiene entre catorce y menos de dieciocho años.”(*)(**)

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1410, publicado el 12 septiembre 2018.

(**) De conformidad con Literal f) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del delito de acoso sexual, previsto en el presente artículo.

“Artículo 176-C.- Chantaje sexual

El que amenaza o intimida a una persona, por cualquier medio, incluyendo el uso de tecnologías de la información o comunicación, para obtener de ella una conducta o acto de connotación sexual, será reprimido con pena privativa de la libertad no menor de dos ni mayor de cuatro años e inhabilitación, según corresponda, conforme a los incisos 5, 9, 10 y 11 del artículo 36.

La pena privativa de libertad será no menor de tres ni mayor de cinco años e inhabilitación, según corresponda, conforme a los incisos 5, 9, 10 y 11 del artículo 36, si para la ejecución del delito el agente amenaza a la víctima con la difusión de imágenes, materiales audiovisuales o audios con contenido sexual en los que esta aparece o participa.”(*)

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1410, publicado el 12 septiembre 2018.

Artículo 177 
~~~~~~~~~~~~~

En los casos de los artículos 170 al 176, la pena será privativa de libertad no menor de cinco años cuando los actos cometidos causan la muerte de la víctima y el agente pudo prever este resultado.

La pena será no menor de cuatro ni mayor de diez años si los actos cometidos producen lesión grave a la víctima y si el agente pudo prever este resultado o si procedió con crueldad. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 26293, publicada el 14 febrero 1994, cuyo texto es el siguiente:

Formas agravadas

"Artículo 177.- En los casos de los artículos 170, 171,172,174,175 y 176, si los actos cometidos causan la muerte de la víctima o le producen lesión grave, y el agente pudo prever este resultado o si procedió con crueldad, la pena privativa de libertad será respectivamente no menor de veinte ni mayor de 25 años, ni menor 10 ni mayor de veinte años." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28704, publicada el 05 abril 2006, cuyo texto es el siguiente:

"Artículo 177.- Formas agravadas

En los casos de los artículos 170, 171, 174, 175, 176 y 176-A, si los actos cometidos causan la muerte de la víctima o le producen lesión grave, y el agente pudo prever este resultado o si procedió con crueldad, la pena privativa de libertad será respectivamente no menor de veinte ni mayor de veinticinco años, ni menor de diez ni mayor de veinte años. De presentarse las mencionadas circunstancias agravantes en el caso del artículo 172, la pena privativa de la libertad será respectivamente no menor de treinta años, ni menor de veinticinco ni mayor de treinta años para el supuesto contemplado en su primer párrafo; y de cadena perpetua y no menor de treinta años, para el supuesto contemplado en su segundo párrafo."

"En los casos de los delitos previstos en los artículos 173, 173-A y 176-A, cuando el agente sea el padre o la madre, tutor o curador, en la sentencia se impondrá, además de la pena privativa de libertad que corresponda, la pena accesoria de inhabilitación a que se refiere el numeral 5) del artículo 36."(1)(2)(3)(4)

(1) Párrafo adicionado por el Artículo 1 de la Ley N° 29194, publicada el 25 enero 2008.

(2) De conformidad con el Numeral 5 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de violación de la libertad sexual, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

(3) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

(4) Artículo modificado por el Artículo 1 de la Ley N° 30838, publicada el 04 agosto 2018, cuyo texto es el siguiente:

"Artículo 177.- Formas agravadas

En cualquiera de los casos de los artículos 170, 171, 172, 174, 175, 176 y 176-A:

1. Si el agente procedió con crueldad, alevosía o para degradar a la víctima, la pena privativa de libertad se incrementa en cinco años en los extremos mínimo y máximo en el respectivo delito.

2. Si los actos producen lesión grave en la víctima y el agente pudo prever ese resultado, la pena privativa de libertad será no menor de treinta ni mayor de treinta y cinco años.

3. Si los actos causan la muerte de la víctima y el agente pudo prever ese resultado, la pena será de cadena perpetua.

En los casos de los delitos previstos en los artículos 171, 172, 174, 176 y 176-A la pena se incrementa en cinco años en sus extremos mínimo y máximo si concurre cualquiera de las circunstancias establecidas en el artículo 170, segundo párrafo.

Si el agente registra cualquiera de las conductas previstas en los artículos 170, 171, 172, 174, 175, 176 y 176-A mediante cualquier medio visual, auditivo o audiovisual o la transmite mediante cualquier tecnología de la información o comunicación, la pena se incrementa en cinco años en los extremos mínimo y máximo aplicable al delito registrado o transmitido."

Artículo 178 
~~~~~~~~~~~~~

En los casos de los delitos comprendidos en este Capítulo, el agente será sentenciado, además, a mantener a la prole que resulte, aplicándose las normas respectivas del Código Civil.

El ejercicio de la acción es privada en los casos de los Artículos 170 primer párrafo (*) RECTIFICADO POR FE DE ERRATAS, 171, 174 y 175.

El agente quedará exento de pena si contrae matrimonio con la ofendida, prestando ella su libre consentimiento, después de restituida al poder de sus padres o tutor, o a un lugar seguro. La exención de pena a que se alude se extiende a los coautores. (*)

(*) Artículo modificado por el Artículo 2 de la Ley Nº 26770, publicada el 15 abril 1997, cuyo texto es el siguiente:

"Artículo 178.- En los delitos comprendidos en este Capítulo, el agente será sentenciado, además, a prestar alimentos a la prole que resulte, conforme a las normas del Código Civil. El ejercicio de la acción es privada en los casos de los Artículos 170 primer párrafo, 171, 174 y 175. En el caso del Artículo 175 el agente quedará exento de la pena si contrae matrimonio con la víctima siempre que ésta preste su libre consentimiento, con arreglo a ley." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 27115, publicada el 17 mayo 1999, cuyo texto es el siguiente:

Responsabilidad civil especial

"Artículo 178.- En los casos comprendidos en este capítulo el agente será sentenciado, además, a prestar alimentos a la prole que resulte, aplicándose las normas respectivas del Código Civil." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 30838, publicada el 04 agosto 2018, cuyo texto es el siguiente:

"Artículo 178.- Responsabilidad especial

En los casos comprendidos en este capítulo, el juez penal debe resolver, de oficio o a petición de parte, sobre la obligación alimentaria a la prole que resulte, aplicando las normas respectivas.

La obligación alimentaria a que se hace referencia en el primer párrafo comprende lo necesario para el sustento, habitación, vestido, educación, instrucción y capacitación para el trabajo, asistencia médica y psicológica, y recreación del niño o del adolescente y, del mismo modo, los gastos del embarazo de la madre desde la concepción hasta la etapa de postparto.

La decisión del juez respecto de la obligación alimentaria comprende la asignación anticipada de alimentos durante la investigación fiscal, así como la fijación de la obligación de prestar alimentos inclusive antes de la sentencia atendiendo al material probatorio disponible."

Tratamiento terapéutico

"Artículo 178-A.- El condenado a pena privativa de libertad efectiva por los delitos comprendidos en este capítulo, previo examen médico o psicológico que determine su aplicación será sometido a un tratamiento terapéutico a fin de facilitar su readaptación social.

En los casos de suspensión de la ejecución de la pena y reserva del fallo condenatorio, el juez dispondrá la realización de un examen médico y psicológico al condenado, para los efectos a que se refiere el párrafo anterior. El sometimiento al tratamiento terapéutico será considerado como regla de conducta.

Los beneficios penitenciarios de semilibertad, liberación condicional y redención de la pena por el trabajo y la educación, y el derecho de gracia del indulto y de la conmutación de la pena, no pueden ser concedidos sin el correspondiente informe médico y psicológico que se pronuncie sobre la evolución del tratamiento terapéutico." (1)(2)

(1) Artículo incorporado por  el Artículo 2 de la Ley Nº 26293, publicada el 14 febrero 1994.

(2) Artículo modificado por el Artículo 1 de la Ley N° 30838, publicada el 04 agosto 2018, cuyo texto es el siguiente:

"Artículo 178-A.- Tratamiento terapéutico

El condenado a pena privativa de libertad efectiva por los delitos comprendidos en este capítulo, previo examen médico y psicológico que determine su aplicación, será sometido a un tratamiento terapéutico a fin de facilitar su readaptación social."

CONCORDANCIAS:     R.M. N° 193-2007-JUS, Art. 25
R.M. N° 0162-2010-JUS, Art. 27

R.A. N° 297-2011-P-PJ, (Circular sobre la debida interpretación y aplicación de los beneficios penitenciarios)

R. N° 1809-2011-MP-FN (Aprueban Circular “Criterios para el debido otorgamiento de Beneficios Penitenciarios”

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

CAPITULO X PROXENETISMO
#######################

(*) De conformidad con el Artículo 5 de la Ley N° 30838, publicada el 04 agosto 2018, no procede la terminación anticipada ni la conclusión anticipada en los procesos por cualquiera de los delitos previstos en el presente Capítulo.

CONCORDANCIAS:     Ley N° 27765, Art. 6

Artículo 179 Favorecimiento a la prostitución
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


El que promueve o favorece la prostitución de otra persona, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años.

La pena será no menor de cuatro ni mayor de doce años cuando:

1. La víctima es menor de catorce años.

2. El autor emplea violencia, engaño, abuso de autoridad, o cualquier medio de intimidación.

3. La víctima se encuentra privada de discernimiento por cualquier causa.

4. El autor es pariente dentro del cuarto grado de consanguinidad o segundo de afinidad, o es cónyuge, concubino, adoptante, tutor o curador o tiene al agraviado a su cuidado por cualquier motivo.

5. La víctima está en situación de abandono o de extrema necesidad económica.

6. El autor haya hecho del proxenetismo su oficio o modo de vida. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28251, publicada el 08 junio 2004, cuyo texto es el siguiente:

"Artículo 179.- Favorecimiento a la prostitución

El que promueve o favorece la prostitución de otra persona, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años.

La pena será no menor de cinco ni mayor de doce años cuando:

1. La víctima es menor de dieciocho años.

2. El autor emplea violencia, engaño, abuso de autoridad, o cualquier medio de intimidación.

3. La víctima se encuentra privada de discernimiento por cualquier causa.

4. El autor es pariente dentro del cuarto grado de consanguinidad o segundo de afinidad, o es cónyuge, concubino, adoptante, tutor o curador o tiene al agraviado a su cuidado por cualquier motivo.

5. La víctima ha sido desarraigada de su domicilio habitual con la finalidad de prostituirla o está en situación de abandono o de extrema necesidad económica.

6. El autor haya hecho del proxenetismo su oficio o modo de vida.

7. Si el agente actúa como integrante de una organización delictiva o banda." (*)

(*) Numeral modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 30077, publicada el 20 agosto 2013, la misma que entró en vigencia el 1 de julio de 2014, cuyo texto es el siguiente:

"7. El agente actúa como integrante de una organización criminal." (**)(***)(****)

(**) De conformidad con el Numeral 4 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de proxenetismo, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

( ** * ) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

(* ** * ) Artículo modificado por el Artículo 1 de la Ley N° 30963, publicada el 18 junio 2019, cuyo texto es el siguiente:

"Artículo 179. Favorecimiento a la prostitución

El que promueve o favorece la prostitución de otra persona, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años.

La pena será no menor de seis ni mayor de doce años cuando:

1. El agente cometa el delito en el ámbito del turismo, en el marco de la actividad de una persona jurídica o en el contexto de cualquier actividad económica.

2. El agente es ascendiente o descendiente por consanguinidad, adopción o por afinidad, pariente colateral hasta el cuarto grado de consanguinidad o adopción o segundo grado de afinidad; cónyuge, excónyuge, conviviente, exconviviente o tenga hijos en común con la víctima; o habite en el mismo domicilio de la víctima, siempre que no medien relaciones contractuales o laborales.

3. Es un medio de subsistencia del agente.

4. La víctima esté en situación de abandono o extrema necesidad económica.

5. Se realice respecto a una pluralidad de personas.

6. La persona en prostitución tenga discapacidad, sea adulta mayor, padezca de una enfermedad grave, pertenezca a un pueblo indígena u originario o presente cualquier situación de vulnerabilidad.

7. Cuando el agente, a sabiendas, favorezca o promueva actos de prostitución violentos que produzcan lesiones o ponga en peligro grave la integridad o la vida de quien realice la prostitución.

8. El agente actúe como integrante de una banda u organización criminal." (*)

(*) De conformidad con Literal k) del Numeral 2.3 del Artículo 2 del Decreto de Urgencia N° 023-2020, publicado el 24 enero 2020, la información sobre los antecedentes policiales puede ser solicitada respecto del delito de favorecimiento de la prostitución, previsto en el presente artículo.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

“Artículo 179-A.- Usuario-cliente

El que, mediante una prestación económica o ventaja de cualquier naturaleza tiene acceso carnal por vía vaginal, anal o bucal o realiza otros actos análogos introduciendo objetos o partes del cuerpo por alguna de las dos primeras vías con una persona de catorce y menor de dieciocho años, será reprimido con pena privativa de la libertad no menor de cuatro ni mayor de seis años."(*)(**)(***)(****)

(*) Artículo incorporado por el Artículo 2 de la Ley N° 28251, publicada el 08 junio 2004.

(**) De conformidad con el Numeral 4 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de proxenetismo, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

( ** * ) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

(* ** * ) Artículo modificado por el Artículo 1 de la Ley N° 30963, publicada el 18 junio 2019, cuyo texto es el siguiente:

"Artículo 179-A. Cliente del adolescente

El que, mediante una prestación económica o ventaja de cualquier naturaleza tiene acceso carnal por vía vaginal, anal o bucal o realiza otros actos análogos introduciendo objetos o partes del cuerpo por alguna de las dos primeras vías con una persona de catorce y menor de dieciocho años, será reprimido con pena privativa de libertad no menor de quince ni mayor de veinte años.

El consentimiento brindado por el adolescente carece de efectos jurídicos." (*)

(*) De conformidad con la Primera Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el indulto ni la conmutación de pena ni el derecho de gracia a los sentenciados por el delito tipificado en el presente artículo, modificado e incorporado por la citada ley.

Artículo 180 Rufianismo
~~~~~~~~~~~~~~~~~~~~~~~

El que explota la ganancia deshonesta obtenida por una persona que ejerce la prostitución será reprimido con pena privativa de libertad no menor de tres ni mayor de ocho años.

Si la víctima es menor de catorce años, o cónyuge, conviviente, descendiente, hijo adoptivo, hijo de su cónyuge o de su conviviente o si está a su cuidado, la pena será no menor de cuatro ni mayor de doce años. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28251, publicada el 08 junio 2004, cuyo texto es el siguiente:

"Artículo 180.- Rufianismo

El que explota la ganancia obtenida por una persona que ejerce la prostitución será reprimido con pena privativa de libertad no menor de tres ni mayor de ocho años.

Si la víctima tiene entre catorce y menos de dieciocho años, la pena será no menor de seis ni mayor de diez años.

Si la víctima tiene menos de catorce años, o es cónyuge, conviviente, descendiente, hijo adoptivo, hijo de su cónyuge o de su conviviente o si está a su cuidado, la pena será no menor de ocho ni mayor de doce años." (*)(**)( ** * )

(*) De conformidad con el Numeral 4 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de proxenetismo, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

(**) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

(* ** * ) Artículo modificado por el Artículo 1 de la Ley N° 30963, publicada el 18 junio 2019, cuyo texto es el siguiente:

"Artículo 180. Rufianismo

El que gestiona el beneficio económico o de otra índole de la prostitución de otra persona será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años.

La pena privativa de libertad será no menor de seis ni mayor de doce años cuando:

1. El agente cometa el delito en el ámbito del turismo, en el marco de la actividad de una persona jurídica o en el contexto de cualquier actividad económica.

2. El agente es ascendiente o descendiente por consanguinidad, adopción o por afinidad, pariente colateral hasta el cuarto grado de consanguinidad, o adopción o segundo grado de afinidad; cónyuge, excónyuge, conviviente, exconviviente o tenga hijos en común con la víctima; o habite en el mismo domicilio de la víctima, siempre que no medien relaciones contractuales o laborales.

3. Es un medio de subsistencia del agente.

4. La víctima esté en situación de abandono o extrema necesidad económica.

5. Exista pluralidad de personas en prostitución.

6. La persona en prostitución tenga discapacidad, sea adulta mayor, padezca de una enfermedad grave, pertenezca a un pueblo indígena u originario o presente cualquier situación de vulnerabilidad.

7. El agente actúe como integrante de una banda u organización criminal."

Artículo 181 Proxenetismo
~~~~~~~~~~~~~~~~~~~~~~~~~

El que compromete, seduce, o sustrae a una persona para entregarla a otro con el objeto de practicar relaciones sexuales, o el que la entrega con este fin, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años.

La pena será no menor de cinco ni mayor de doce años, cuando:

1.   La víctima tiene menos de dieciocho años de edad.

2.   El agente emplea violencia, amenaza, abuso de autoridad u otro medio de coerción.

3.  La víctima es cónyuge, concubina, descendiente, hijo adoptivo, hijo de su cónyuge o de su concubina, o si está a su cuidado.

4.   La víctima es entregada a un proxeneta. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28251, publicada el 08 junio 2004, cuyo texto es el siguiente:

"Artículo 181.- Proxenetismo

El que compromete, seduce, o sustrae a una persona para entregarla a otro con el objeto de tener acceso carnal, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años.

La pena será no menor de seis ni mayor de doce años, cuando:

1. La víctima tiene menos de dieciocho años.

2. El agente emplea violencia, amenaza, abuso de autoridad u otro medio de coerción.

3. La víctima es cónyuge, concubina, descendiente, hijo adoptivo, hijo de su cónyuge o de su concubina, o si está a su cuidado.

4. Si el agente actúa como integrante de una organización delictiva o banda. (*)

(*) Numeral modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 30077, publicada el 20 agosto 2013, la misma que entró en vigencia el 1 de julio de 2014, cuyo texto es el siguiente:

"4. El agente actúa como integrante de una organización criminal."

5. La víctima es entregada a un proxeneta."  (*)(**)( ** * )

(*) De conformidad con el Numeral 4 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de proxenetismo, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

(**) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

( ** * ) Artículo modificado por el Artículo 1 de la Ley N° 30963, publicada el 18 junio 2019, cuyo texto es el siguiente:

"Artículo 181. Proxenetismo

El que dirige o gestiona la prostitución de otra persona será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años.

La pena será no menor de seis ni mayor de doce años, cuando:

1. El agente cometa el delito en el ámbito del turismo, en el marco de la actividad de una persona jurídica o en el contexto de cualquier actividad económica.

2. El agente sea ascendiente o descendiente por consanguinidad, adopción o por afinidad, pariente colateral hasta el cuarto grado de consanguinidad o adopción o segundo grado de afinidad; cónyuge, excónyuge, conviviente, exconviviente o tenga hijos en común con la víctima o habite en el mismo domicilio de la víctima, siempre que no medien relaciones contractuales o laborales.

3. El proxenetismo sea un medio de subsistencia del agente.

4. La víctima esté en situación de abandono o extrema necesidad económica.

5. Exista pluralidad de personas en prostitución.

6. La persona en prostitución tenga discapacidad, sea adulta mayor, padezca de una enfermedad grave, pertenezca a un pueblo indígena u originario o presente cualquier situación de vulnerabilidad.

7. Se produzca una lesión grave o se ponga en peligro inminente la vida o la salud de la persona en prostitución.

8. El agente actúe como integrante de una banda u organización criminal."

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

"Artículo 181-A.- Turismo sexual infantil

El que promueve, publicita, favorece o facilita el turismo sexual, a través de cualquier medio escrito, folleto, impreso, visual, audible, electrónico, magnético o a través de internet, con el objeto de ofrecer relaciones sexuales de carácter comercial de personas de catorce y menos de dieciocho años de edad será reprimido con pena privativa de libertad no menor de dos ni mayor de seis años.

Si la víctima es menor de catorce años, el agente, será reprimido con pena privativa de la libertad no menor de seis ni mayor de ocho años.

El agente también será sancionado con inhabilitación conforme al artículo 36 incisos 1, 2, 4 y 5.

Será no menor de ocho ni mayor de diez años de pena privativa de la libertad cuando ha sido cometido por autoridad pública, sus ascendientes, maestro o persona que ha tenido a su cuidado por cualquier título a la víctima." (1)(2)

(1) Artículo incorporado por el Artículo 2 de la Ley N° 28251, publicada el 08 junio 2004.

(2) Artículo modificado por la Segunda Disposición Final de la Ley Nº 29408, publicada el 18 septiembre 2009, cuyo texto es el siguiente:

“Artículo 181-A.- Explotación sexual comercial infantil y adolescente en ámbito del turismo

El que promueve, publicita, favorece o facilita la explotación sexual comercial en el ámbito del turismo, a través de cualquier medio escrito, folleto, impreso, visual, audible, electrónico, magnético o a través de Internet, con el objeto de ofrecer relaciones sexuales de carácter comercial de personas de catorce (14) y menos de dieciocho (18) años de edad será reprimido con pena privativa de libertad no menor de cuatro (4) ni mayor de ocho (8) años.

Si la víctima es menor de catorce años, el agente, será reprimido con pena privativa de la libertad no menor de seis (6) ni mayor de ocho (8) años.

El agente también será sancionado con inhabilitación conforme al artículo 36 incisos 1, 2, 4 y 5.

Será no menor de ocho (8) ni mayor de diez (10) años de pena privativa de la libertad cuando ha sido cometido por autoridad pública, sus ascendientes, maestro o persona que ha tenido a su cuidado por cualquier título a la víctima.”(*)(**)( ** * )

(*) De conformidad con el Numeral 4 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de proxenetismo, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

(**) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

( ** * ) Artículo modificado por el Artículo 1 de la Ley N° 30963, publicada el 18 junio 2019, cuyo texto es el siguiente:

"Artículo 181-A. Promoción y favorecimiento de la explotación sexual de niñas, niños y adolescentes

El que promueve, favorece o facilita la explotación sexual de niña, niño o adolescente será reprimido con pena privativa de libertad no menor de quince ni mayor de veinte años.

El consentimiento brindado por el adolescente carece de efectos jurídicos.

Si quien favorece, directamente o a través de un tercero, utiliza como medio una retribución o promesa de retribución, económica o de otra índole, al menor de edad, será reprimido con pena privativa de libertad no menor de veinte ni mayor de veinticinco años.

La pena privativa de libertad será no menor de veinte ni mayor de treinta años si el agente:

1. Es promotor, integrante o representante de una organización social, tutelar o empresarial que aprovecha esta condición y realiza actividades para perpetrar este delito.

2. Tiene a la víctima bajo su cuidado o vigilancia, por cualquier motivo, o mantiene con ella un vínculo de superioridad, autoridad, poder o cualquier otra circunstancia que impulse a depositar la confianza en él.

La pena privativa de libertad será no menor de treinta ni mayor de treinta y cinco años cuando:

1. El agente sea ascendiente por consanguinidad, adopción o por afinidad, pariente colateral hasta el cuarto grado de consanguinidad o adopción, o segundo grado de afinidad, tutor, cónyuge, excónyuge, conviviente, exconviviente o tenga hijos en común con la víctima; o habite en el mismo domicilio de la víctima, siempre que no medien relaciones contractuales o laborales.

2. Es un medio de subsistencia del agente.

3. Exista pluralidad de víctimas.

4. La víctima tenga discapacidad, padezca de una enfermedad grave, o presente cualquier situación de vulnerabilidad.

5. La víctima pertenezca a pueblo indígena u originario.

6. Cuando el agente, a sabiendas, favorezca o promueva actos de explotación sexual violentos que produzcan lesiones o ponga en peligro grave la integridad o la vida de quien realice la explotación sexual.

7. Se derive de una situación de trata de personas.

8. El agente actúe como integrante de una banda o una organización criminal.

9. La víctima esté en situación de abandono o de extrema necesidad económica.

10. La víctima tiene menos de catorce años.

La pena será de cadena perpetua:

1. Si se causa la muerte de la víctima.

2. Si se lesiona gravemente su salud física o mental.

3. Si, a consecuencia de la explotación sexual, la víctima menor de 14 años tiene acceso carnal por vía vaginal, anal o bucal o realiza cualquier otro acto análogo con la introducción de un objeto o parte del cuerpo por alguna de las dos primeras vías.

En todos los casos se impone, además, la pena de inhabilitación conforme al artículo 36, incisos 1, 2, 3, 4, 5, 6, 8, 9,10 y 11." (*)

(*) De conformidad con la Primera Disposición Complementaria Final de la Ley N° 30963, publicada el 18 junio 2019, no procede el indulto ni la conmutación de pena ni el derecho de gracia a los sentenciados por el delito tipificado en el presente artículo, modificado e incorporado por la citada ley.

"Artículo 181-B.- Formas agravadas

En los casos de los delitos previstos en los artículos 179, 181 y 181-A, cuando el agente sea el padre o la madre, el tutor o curador, en la sentencia se impondrá, además de la pena privativa de libertad que corresponda, la pena accesoria de inhabilitación a que se refiere el numeral 5) del artículo 36.”(*)

(*) Artículo incorporado por el Artículo 1 de la Ley N° 29194, publicada el 25 enero 2008.

Artículo 182 Trata de personas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que promueve o facilita la entrada o salida del país o el traslado dentro del territorio de la República de una persona para que ejerza la prostitución, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años.

La pena será no menor de ocho ni mayor de doce años, si media alguna de las circunstancias agravantes enumeradas en el artículo anterior.(*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28251, publicada el 08 junio 2004, cuyo texto es el siguiente:

"Artículo 182.- Trata de personas

El que promueve o facilita la captación para la salida o entrada del país o el traslado dentro del territorio de la República de una persona para que ejerza la prostitución, someterla a esclavitud sexual, pornografía u otras formas de explotación sexual, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años.

La pena será no menor de diez ni mayor de doce años, si media alguna de las circunstancias agravantes enumeradas en el artículo anterior."(*)

(*) Artículo derogado por la Quinta Disposición Complementaria, Transitoria y Final de la Ley N° 28950, publicada el 16 enero 2007.

CONCORDANCIAS:     R.M. N° 2570-2006-IN-0105 (Institucionalizan el "Sistema de Registro y Estadística del delito de Trata de Personas y Afines (RETA)")

CAPITULO  XI OFENSAS  AL  PUDOR  PUBLICO
########################################

(*) De conformidad con el Artículo 5 de la Ley N° 30838, publicada el 04 agosto 2018, no procede la terminación anticipada ni la conclusión anticipada en los procesos por cualquiera de los delitos previstos en el presente Capítulo.

"Artículo 182 
~~~~~~~~~~~~~~

- Publicación en los medios de comunicación sobre delitos de libertad sexual a menores

Los gerentes o responsables de las publicaciones o ediciones a transmitirse a través de los medios de comunicación masivos que publiciten la prostitución infantil, el turismo sexual infantil o la trata de menores de dieciocho años de edad serán reprimidos con pena privativa de la libertad no menor de dos ni mayor de seis años.

El agente también será sancionado con inhabilitación conforme al inciso 4 del artículo 36 y con trescientos sesenta días multa.”(*)(**)

(*) Artículo incorporado por el Artículo 2 de la Ley N° 28251, publicada el 08 junio 2004.

(**) Artículo modificado por el Artículo 1 de la Ley N° 30963, publicada el 18 junio 2019, cuyo texto es el siguiente:

"Artículo 182-A. Publicación en los medios de comunicación sobre delitos de libertad sexual contra niñas, niños y adolescentes

El gerente o responsable u otro con poder de decisión sobre las publicaciones o ediciones que autorice o disponga que se difunda pornografía infantil o se publiciten actos que conlleven a la trata o la explotación sexual de niñas, niños y adolescentes será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años, así como la pena de inhabilitación conforme al artículo 36, incisos 1, 2, 3, 4, 6, 8, 9, 10 y 11."

Artículo 183 
~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no mayor de dos años:

1. El que expone, vende o entrega a un menor de catorce años, objetos, libros, escritos, imágenes visuales o auditivas que, por su carácter obsceno, pueden afectar gravemente el pudor del agraviado o excitar prematuramente o pervertir su instinto sexual.

2. El que, en lugar público, realiza exhibiciones, gestos, tocamientos u observa cualquier otra conducta de índole obscena.

3. El que incita a un menor de catorce años a la ebriedad o a la práctica de un acto obsceno o le facilita la entrada a los prostíbulos u otros lugares de corrupción.

4. El administrador, vigilante o persona autorizada para el control de un cine u otro espectáculo de índole obsceno, que permite ingresar a un menor de catorce años. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 27459, publicada el 26 mayo 2001, cuyo texto es el siguiente:

"Artículo 183.- Exhibiciones y publicaciones obscenas

Será reprimido con pena privativa de libertad no menor de dos años el que, en lugar público, realiza exhibiciones, gestos, tocamientos u otra conducta de índole obscena.

Será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años:

1. El que muestra, vende o entrega a un menor de catorce años, objetos, libros, escritos, imágenes sonoras o auditivas que, por su carácter obsceno, pueden afectar gravemente el pudor, excitar prematuramente o pervertir su instinto sexual.

2. El que incita a un menor de catorce años a la ebriedad o a la práctica de un acto obsceno o le facilita la entrada a los prostíbulos u otros lugares de corrupción.

3. El administrador, vigilante o persona autorizada para controlar un cine u otro espectáculo donde se exhiban representaciones obscenas, que permita ingresar a un menor de catorce años." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28251, publicada el 08 junio 2004, cuyo texto es el siguiente:

"Artículo 183.- Exhibiciones y publicaciones obscenas

Será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años el que, en lugar público, realiza exhibiciones, gestos, tocamientos u otra conducta de índole obscena.

Será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años:

1. El que muestra, vende o entrega a un menor de dieciocho años, por cualquier medio, objetos, libros, escritos, imágenes, visuales o auditivas, que por su carácter obsceno, pueden afectar gravemente el pudor, excitar prematuramente o pervertir su instinto sexual.

2. El que incita a un menor de dieciocho años a la práctica de un acto obsceno o le facilita la entrada a los prostíbulos u otros lugares de corrupción.

3. El administrador, vigilante o persona autorizada para controlar un cine u otro espectáculo donde se exhiban representaciones obscenas, que permita ingresar a un menor de dieciocho años." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 30963, publicada el 18 junio 2019, cuyo texto es el siguiente:

"Artículo 183. Exhibiciones y publicaciones obscenas

Será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años el que, en lugar público, realiza exhibiciones, gestos, tocamientos u otra conducta de índole obscena.

Será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años:

1. El que muestra, vende o entrega a un menor de dieciocho años, por cualquier medio, objetos, libros, escritos, imágenes, visuales o auditivas, que por su carácter pueden afectar su desarrollo sexual.

2. El que incita a un menor de dieciocho años a la práctica de un acto de índole sexual o le facilita la entrada a lugares con dicho propósito.

3. El administrador, vigilante o persona autorizada para controlar un cine u otro espectáculo donde se exhiban representaciones de índole sexual que permita ingresar a un menor de dieciocho años.

En todos los casos se impone, además, la pena de inhabilitación conforme al artículo 36, incisos 1, 2, 3, 4, 6, 8, 9, 10 y 11."

Artículo 183 
~~~~~~~~~~~~~

- Pornografía infantil

El que posee, promueve, fabrica, distribuye, exhibe, ofrece, comercializa o publica, importa o exporta objetos, libros, escritos, imágenes visuales o auditivas, o realiza espectáculos en vivo de carácter pornográfico, en los cuales se utilice a menores de catorce a dieciocho años de edad, será sancionado con pena privativa de libertad no menor de cuatro ni mayor de seis años y con ciento veinte a trescientos sesenta y cinco días multa.

Cuando el menor tenga menos de catorce años de edad la pena será no menor de cuatro ni mayor de ocho años y con ciento cincuenta a trescientos sesenta y cinco días multa.

Si la víctima se encuentra en alguna de las condiciones previstas en el último párrafo del Artículo 173, la pena privativa de libertad será no menor de ocho ni mayor de doce años.

De ser el caso, el agente será inhabilitado conforme al Artículo 36, incisos 1), 2), 4) y 5).” (1)(2)

(1) Artículo incorporado por el Artículo 2 de la Ley Nº 27459, publicada el 26 mayo 2001.

(2) Artículo modificado por el Artículo 1 de la Ley N° 28251, publicada el 08 junio 2004, cuyo texto es el siguiente:

"Artículo 183-A.- Pornografía infantil

El que posee, promueve, fabrica, distribuye, exhibe, ofrece, comercializa o publica, importa o exporta por cualquier medio incluido la internet, objetos, libros, escritos, imágenes visuales o auditivas, o realiza espectáculos en vivo de carácter pornográfico, en los cuales se utilice a personas de catorce y menos de dieciocho años de edad, será sancionado con pena privativa de libertad no menor de cuatro ni mayor de seis años y con ciento veinte a trescientos sesenta y cinco días multa.

Cuando el menor tenga menos de catorce años de edad la pena será no menor de seis ni mayor de ocho años y con ciento cincuenta a trescientos sesenta y cinco días multa.

Si la víctima se encuentra en alguna de las condiciones previstas en el último párrafo del artículo 173, o si el agente actúa en calidad de integrante de una organización dedicada a la pornografía infantil la pena privativa de libertad será no menor de ocho ni mayor de doce años.

De ser el caso, el agente será inhabilitado conforme al artículo 36, incisos 1, 2, 4 y 5.”(*)

(*) Artículo modificado por la Cuarta Disposición Complementaria Modificatoria de la Ley N° 30096, publicada el 22 octubre 2013, cuyo texto es el siguiente:

"Artículo 183-A. Pornografía infantil

El que posee, promueve, fabrica, distribuye, exhibe, ofrece, comercializa o publica, importa o exporta por cualquier medio objetos, libros, escritos, imágenes, videos o audios, o realiza espectáculos en vivo de carácter pornográfico, en los cuales se utilice a personas de catorce y menos de dieciocho años de edad, será sancionado con pena privativa de libertad no menor de seis ni mayor de diez años y con ciento veinte a trescientos sesenta y cinco días multa.

La pena privativa de libertad será no menor de diez ni mayor de doce años y de cincuenta a trescientos sesenta y cinco días multa cuando:

1. El menor tenga menos de catorce años de edad.

2. El material pornográfico se difunda a través de las tecnologías de la información o de la comunicación.

Si la víctima se encuentra en alguna de las condiciones previstas en el último párrafo del artículo 173 o si el agente actúa en calidad de integrante de una organización dedicada a la pornografía infantil, la pena privativa de libertad será no menor de doce ni mayor de quince años. De ser el caso, el agente será inhabilitado conforme a los numerales 1, 2 y 4 del artículo 36." (*)(**)

(*) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

(**) Artículo modificado por el Artículo 1 de la Ley N° 30963, publicada el 18 junio 2019, cuyo texto es el siguiente:

"Artículo 183-A. Pornografía infantil

El que posee, promueve, fabrica, distribuye, exhibe, ofrece, comercializa, publicita, publica, importa o exporta por cualquier medio objetos, libros, escritos, imágenes, videos o audios, o realiza espectáculos en vivo de carácter sexual, en los cuales participen menores de dieciocho años de edad, será sancionado con pena privativa de libertad no menor de seis ni mayor de diez años y con ciento veinte a trescientos sesenta y cinco días multa.

La pena privativa de libertad será no menor de diez ni mayor de quince años y de cincuenta a trescientos sesenta y cinco días multa cuando:

1. La víctima tenga menos de catorce años de edad.

2. El material se difunda a través de cualquier tecnología de la información o de la comunicación o cualquier otro medio que genere difusión masiva.

3. El agente actúe como miembro o integrante de una banda u organización criminal.

En todos los casos se impone, además, la pena de inhabilitación conforme al artículo 36, incisos 1, 2, 3, 4, 5, 6, 8, 9,10 y 11."

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

“Artículo 183-B. Proposiciones sexuales a niños, niñas y adolescentes

El que contacta con un menor de catorce años para solicitar u obtener de él material pornográfico, o para llevar a cabo actividades sexuales con él, será reprimido con una pena privativa de libertad no menor de cuatro ni mayor de ocho años e inhabilitación conforme a los numerales 1, 2 y 4 del artículo 36.

Cuando la víctima tiene entre catorce y menos de dieciocho años de edad y medie engaño, la pena será no menor de tres ni mayor de seis años e inhabilitación conforme a los numerales 1, 2 y 4 del artículo 36.” (1)(2)(3)

(1) Artículo incorporado por el Artículo 5 de la Ley N° 30171, publicada el 10 marzo 2014.

(2) De conformidad con el Artículo 3 de la Ley N° 30819, publicada el 13 julio 2018, en el delito previsto en el presente artículo el juez penal aplica la suspensión y extinción de la Patria Potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal. Está prohibido, bajo responsabilidad, disponer que dicha materia sea resuelta por justicia especializada de familia o su equivalente.

(3) Artículo modificado por el Artículo 1 de la Ley N° 30838, publicada el 04 agosto 2018, cuyo texto es el siguiente:

"Artículo 183-B.- Proposiciones a niños, niñas y adolescentes con fines sexuales

El que contacta con un menor de catorce años para solicitar u obtener de él material pornográfico, o para proponerle llevar a cabo cualquier acto de connotación sexual con él o con tercero, será reprimido con pena privativa de libertad no menor de seis ni mayor de nueve años e inhabilitación conforme a los numerales 1, 2, 4 y 9 del artículo 36.

Cuando la víctima tiene entre catorce y menos de dieciocho años, y medie engaño, la pena será no menor de tres ni mayor de seis años e inhabilitación conforme a los numerales 1, 2, 4 y 9 del artículo 36”. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 30963, publicada el 18 junio 2019, cuyo texto es el siguiente:

"Artículo 183-B. Proposiciones a niños, niñas y adolescentes con fines sexuales

El que contacta con un menor de catorce años para solicitar u obtener de él material pornográfico, o para proponerle llevar a cabo cualquier acto de connotación sexual con él o con tercero, será reprimido con pena privativa de libertad no menor de seis ni mayor de nueve años.

Cuando la víctima tiene entre catorce y menos de dieciocho años, y medie engaño, la pena será no menor de tres ni mayor de seis años.

En todos los casos se impone, además, la pena de inhabilitación conforme al artículo 36, incisos 1, 2, 3, 4, 5, 6, 8, 9, 10 y 11”.

CAPITULO  XII DISPOSICION COMUN
###############################

Artículo 184 Castigo a cómplices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los ascendientes, descendientes, afines en línea recta, hermanos y cualquier persona que, con abuso de autoridad, encargo o confianza, cooperen a la perpetración de los delitos comprendidos en los Capítulos IX, X y XI de este Título actuando en la forma señalada por el artículo 25 primer párrafo, serán reprimidos con la pena de los autores.

"Artículo 184-A.- Inhabilitación

El juez impone como pena principal la inhabilitación prevista en el artículo 36 del presente Código, según corresponda.

En los delitos comprendidos en los capítulos IX, X y XI del presente título, el juzgado penal aplica, de oficio o a pedido de parte, la suspensión y extinción de la patria potestad conforme con los artículos 75 y 77 del Código de los Niños y Adolescentes, según corresponda al momento procesal”.(*)

(*) Artículo incorporado por el Artículo 2 de la Ley N° 30838, publicada el 04 agosto 2018.

TITULO V DELITOS  CONTRA  EL  PATRIMONIO
========================================

CAPITULO I HURTO
################

Artículo 185 Hurto Simple
~~~~~~~~~~~~~~~~~~~~~~~~~

El que, para obtener provecho, se apodera ilegítimamente de un bien mueble, total o parcialmente ajeno, sustrayéndolo del lugar donde se encuentra, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años.

Se equiparan a bien mueble la energía eléctrica, el gas, el agua y cualquier otra energía o elemento que tenga valor económico, así como el espectro electromagnético. (*)

(*) Artículo modificado por el numeral 1 del Artículo 29 del Decreto Legislativo Nº 1084, publicado el 28 junio 2008, cuyo texto es el siguiente:

Hurto Simple

Artículo 185.- “El que, para obtener provecho, se apodera ilegítimamente de un bien mueble, total o parcialmente ajeno, sustrayéndolo del lugar donde se encuentra, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años. Se equiparan a bien mueble la energía eléctrica, el gas, el agua y cualquier otra energía o elemento que tenga valor económico, así como el espectro electromagnético y también los recursos pesqueros objeto de un mecanismo de asignación de Límites Máximos de Captura por Embarcación.”(*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1245, publicado el 06 noviembre 2016, cuyo texto es el siguiente:

“Artículo 185.- Hurto simple

El que, para obtener provecho, se apodera ilegítimamente de un bien mueble, total o parcialmente ajeno, sustrayéndolo del lugar donde se encuentra, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años. Se equiparan a bien mueble la energía eléctrica, el gas, los hidrocarburos o sus productos derivados, el agua y cualquier otra energía o elemento que tenga valor económico, así como el espectro electromagnético y también los recursos pesqueros objeto de un mecanismo de asignación de Límites Máximos de Captura por Embarcación.”

Artículo 186 
~~~~~~~~~~~~~

El agente será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años, si el hurto es cometido:

1. En casa habitada.

2. Durante la noche.

3. Mediante destreza, escalamiento, destrucción o rotura de obstáculos.

4. Con ocasión de incendio, inundación, naufragio, calamidad pública o desgracia particular del agraviado.

5. Sobre los bienes muebles que forman el equipaje de viajero.

6. Mediante el concurso de dos o más personas.

Si el agente usa sistemas de transferencia electrónica de fondos, de la telemática en general, o viola el empleo de claves secretas, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años y con ciento ochenta a trescientos sesenticinco días-multa. (*)

(*) Artículo modificado por Artículo 1 de la Ley  26319, publicada el 01 junio 1994, cuyo texto es el siguiente:

Hurto agravado

"Artículo 186.- El agente será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años si el hurto es cometido:

1. En casa habitada.

2. Durante la noche.

3. Mediante destreza, escalamiento, destrucción o rotura de obstáculos.

4. Con ocasión de incendio, inundación, naufragio, calamidad pública o desgracia particular del agraviado.

5. Sobre los bienes muebles que forman el equipaje de viajero.

6. Mediante el concurso de dos o más personas.

La pena será no menor de cuatro ni mayor de ocho años si el hurto es cometido:

1. Por un agente que actúa en calidad de integrante de una organización destinada a perpetrar estos delitos.

2. Sobre bienes de valor científico o que integren el patrimonio cultural de  la Nación.

3. Mediante la utilización de sistemas de transferencia  electrónica de  fondos, de la telemática en general, o la violación del empleo de claves secretas.

4. Colocando a la víctima o a su familia en grave situación económica.

5. Con empleo de materiales o artefactos explosivos para la destrucción o rotura de obstáculos.

La pena será no menor de ocho ni mayor de quince años cuando el agente actúa en calidad de jefe, cabecilla o dirigente de una organización destinada a perpretar estos delitos." (*)

(*) De conformidad con el Artículo Único de la Ley N° 28848, publicada el 27 julio 2006, se incorpora el inciso 6 a la segunda parte del presente artículo, quedando redactado en su integridad en los siguientes términos:

“HURTO AGRAVADO
Artículo 186.- El agente será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años si el hurto es cometido:

1. En casa habitada.

2. Durante la noche.

3. Mediante destreza, escalamiento, destrucción o rotura de obstáculos.

4. Con ocasión de incendio, inundación, naufragio, calamidad pública o desgracia particular del agraviado.

5. Sobre los bienes muebles que forma el equipaje del viajero.

6. Mediante el concurso de dos o más personas.

La pena será no menor de cuatro ni mayor de ocho años si el hurto es cometido:

1. Por un agente que actúa en calidad de integrante de una organización destinada a perpetrar estos delitos.

2. Sobre bienes de valor científico o que integren el patrimonio cultural de la Nación.

Mediante la utilización de sistemas de transferencia electrónica de fondos, de la telemática en general, o la violación del empleo de claves secretas.

4. Colocando a la víctima o a su familia en grave situación económica.

5. Con empleo de materiales o artefactos explosivos para la destrucción o rotura de obstáculos.

6. Utilizando el espectro radioeléctrico para la transmisión de señales de telecomunicación ilegales.

La pena será no menor de ocho ni mayor de quince años cuando el agente actúa en calidad de jefe, cabecilla o dirigente de una organización destinada a perpetrar estos delitos." (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 29407, publicada el 18 septiembre 2009, cuyo texto es el siguiente:

“Artículo 186.- Hurto agravado

El agente será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años si el hurto es cometido:

1. En casa habitada.

2. Durante la noche.

3. Mediante destreza, escalamiento, destrucción o rotura de obstáculos.

4. Con ocasión de incendio, inundación, naufragio, calamidad pública o desgracia particular del agraviado.

5. Sobre los bienes muebles que forman el equipaje de viajero.

6. Mediante el concurso de dos o más personas.

La pena será no menor de cuatro ni mayor de ocho años si el hurto es cometido:

1. Por un agente que actúa en calidad de integrante de una organización destinada a perpetrar estos delitos.

2. Sobre bienes de valor científico o que integren el patrimonio cultural de la Nación.

3. Mediante la utilización de sistemas de transferencia electrónica de fondos, de la telemática en general o la violación del empleo de claves secretas.

4. Colocando a la víctima o a su familia en grave situación económica.

5. Con empleo de materiales o artefactos explosivos para la destrucción o rotura de obstáculos.

6. Utilizando el espectro radioeléctrico para la transmisión de señales de telecomunicación ilegales.

7. Sobre bien que constituya único medio de subsistencia o herramienta de trabajo de la víctima.

8. Sobre vehículo automotor.

"9. Sobre bienes que forman parte de la infraestructura o instalaciones de transporte de uso público, de sus equipos o elementos de seguridad, o de prestación de servicios públicos de saneamiento, electricidad, gas o telecomunicaciones." (*)

(*) Inciso incorporado por el Artículo Único de la Ley N° 29583, publicada el 18 septiembre 2010.

La pena será no menor de ocho ni mayor de quince años cuando el agente actúa en calidad de jefe, cabecilla o dirigente de una organización destinada a perpetrar estos delitos.” (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 30076, publicada el 19 agosto 2013, cuyo texto es el siguiente:

"Artículo 186. Hurto agravado

El agente será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años si el hurto es cometido:

1. Durante la noche.

2. Mediante destreza, escalamiento, destrucción o rotura de obstáculos.

3. Con ocasión de incendio, inundación, naufragio, calamidad pública o desgracia particular del agraviado.

4. Sobre los bienes muebles que forman el equipaje del viajero.

5. Mediante el concurso de dos o más personas.

La pena será no menor de cuatro ni mayor de ocho años si el hurto es cometido:

1. En inmueble habitado.

2. Por un agente que actúa en calidad de integrante de una organización destinada a perpetrar estos delitos.

3. Sobre bienes de valor científico o que integren el patrimonio cultural de la Nación.

4. Mediante la utilización de sistemas de transferencia electrónica de fondos, de la telemática en general o la violación del empleo de claves secretas.(*)

(*) Numeral 4) derogado por la Única Disposición Complementaria Derogatoria de la Ley N° 30096, publicada el 22 octubre 2013. (*) RECTIFICADO POR FE DE ERRATAS

5. Colocando a la víctima o a su familia en grave situación económica.

6. Con empleo de materiales o artefactos explosivos para la destrucción o rotura de obstáculos.

7. Utilizando el espectro radioeléctrico para la transmisión de señales de telecomunicación ilegales.

8. Sobre bien que constituya único medio de subsistencia o herramienta de trabajo de la víctima.

9. Sobre vehículo automotor, sus autopartes o accesorios.

10. Sobre bienes que forman parte de la infraestructura o instalaciones de transportes de uso público, de sus equipos o elementos de seguridad, o de prestación de servicios públicos de saneamiento, electricidad, gas o telecomunicaciones. (*)

(*) Numeral modificado por el Artículo 2 del Decreto Legislativo N° 1245, publicado el 06 noviembre 2016, cuyo texto es el siguiente:

"10. Sobre bienes que forman parte de la infraestructura o instalaciones de transportes de uso público, de sus equipos o elementos de seguridad, o de prestación de servicios públicos de saneamiento, electricidad o telecomunicaciones."

11. En agravio de menores de edad, personas con discapacidad, mujeres en estado de gravidez o adulto mayor.

"12. Sobre bienes que forman parte de la infraestructura o instalaciones públicas o privadas para la exploración, explotación, procesamiento, refinación, almacenamiento, transporte, distribución, comercialización o abastecimiento de gas, de hidrocarburos o de sus productos derivados, conforme a la legislación de la materia."(*)

(*) Numeral incorporado por el Artículo 2 del Decreto Legislativo N° 1245, publicado el 06 noviembre 2016.

La pena será no menor de ocho ni mayor de quince años cuando el agente actúa en calidad de jefe, cabecilla o dirigente de una organización destinada a perpetrar estos delitos."(*)

(*) Extremo modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 30077, publicada el 20 agosto 2013, la misma que entró en vigencia el 1 de julio de 2014, cuyo texto es el siguiente:

"La pena será no menor de ocho ni mayor de quince años cuando el agente actúa en calidad de jefe, cabecilla o dirigente de una organización criminal destinada a perpetrar estos delitos."

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA
PROCESOS CONSTITUCIONALES

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

“Artículo 186-A.- Dispositivos para asistir a la decodificación de señales de satélite portadoras de programas

El que fabrique, ensamble, modifique, importe, exporte, venda, alquile o distribuya por otro medio un dispositivo o sistema tangible o intangible, cuya función principal sea asistir en la decodificación de una señal de satélite codificada portadora de programas, sin la autorización del distribuidor legal de dicha señal, será reprimido con pena privativa de la libertad no menor de cuatro años ni mayor de ocho años y con noventa a ciento ochenta días multa. (*)

(*) Artículo incorporado por el Artículo 1 de la Ley N° 29316, publicada el 14 enero 2009.

CONCORDANCIAS:     Ley N° 29316, Tercera Disposición Complementaria Final

Artículo 187 Hurto de uso
~~~~~~~~~~~~~~~~~~~~~~~~~

El que sustrae un bien mueble ajeno con el fin de hacer uso momentáneo y lo devuelve será reprimido con pena privativa de libertad no mayor de un año.

CAPITULO  II ROBO
#################

Artículo 188 
~~~~~~~~~~~~~

El que se apodera ilegítimamente de un bien mueble, total o parcialmente ajeno, para aprovecharse de él, sustrayéndolo del lugar en que se encuentra, empleando violencia contra la persona o amenazándola con un peligro inminente para su vida o integridad física será reprimido con pena privativa de libertad no menor de dos ni mayor de seis años. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 26319, publicada el 01 junio 1994, cuyo texto es el siguiente:

"Artículo 188.- El que se apodera ilegítimamente de un mueble, total o parcialmente ajeno, para aprovecharse de él, sustrayéndolo del lugar en que se encuentra, empleando violencia contra la persona o amenazándola con un peligro inminente para su vida o integridad física será reprimido  con pena privativa de libertad no menor de tres ni mayor de ocho años." (*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 896, publicado el 24 mayo 1998, expedido con arreglo a la Ley N° 26950, que otorga al Poder Ejecutivo facultades para legislar en materia de seguridad ciudadana, cuyo texto es el siguiente:

"Robo
Artículo 188.- El que se apodera ilegítimamente de un bien mueble total o parcialmente ajeno, para aprovecharse de él, sustrayéndolo del lugar en que se encuentra, empleando violencia contra la persona o amenazándola con un peligro inminente para su vida o integridad física, será reprimido con pena privativa de libertad no menor de seis ni mayor de quince años." (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 27472, publicada el 05 junio 2001, cuyo texto es el siguiente:

"Artículo 188.- Robo

El que se apodera ilegítimamente de un bien mueble total o parcialmente ajeno, para aprovecharse de él, sustrayéndolo del lugar en que se encuentra, empleando violencia contra la persona o amenazándola con un peligro inminente para su vida o integridad física será reprimido con pena privativa de libertad no menor de tres ni mayor de ocho años."

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 189 
~~~~~~~~~~~~~

La pena será no menor de tres ni mayor de ocho años, si el robo se comete:

1. Con crueldad.

2.  En casa habitada.

3. Durante la noche o en lugar desolado.

4. A mano armada.

5. Con el concurso de dos o más personas.

6. En vehículo de transporte público de pasajeros que esté prestando servicio.

7. Fingiendo ser agente de la policía, autoridad o servidor público o mostrando orden o mandamiento falso de autoridad.

En los casos de concurso con delitos contra la vida, el cuerpo, y la salud, la pena se aplica sin perjuicio de otra más grave que pudiera corresponder en cada caso. (*) (*) RECTIFICADO POR FE DE ERRATAS

(*) Artículo modificado por el Artículo  1 de la Ley Nº 26319, publicada el 01 junio 1994, cuyo texto es el siguiente:

"Artículo 189.- La pena será no menor de cinco ni mayor de quince años si el robo es cometido:

1. En casa habitada.

2. Durante la noche o en lugar desolado.

3. A mano armada.

4. Con el concurso de dos o más personas.

5. En vehículo de transporte público de pasajeros que esté prestando servicio.

6. Fingiendo ser agente de policía, autoridad o servidor público o mostrando orden o mandamiento falso de autoridad.

Si la violencia o amenaza fuesen insignificantes, la pena podrá ser disminuida en un tercio.

La pena será no menor de diez ni mayor de veinte años si el robo es cometido:

1. Con crueldad.

2. Con empleo de armamentos, materiales o artefactos explosivos.

3. Con abuso de la incapacidad física o mental de la víctima o mediante el empleo de drogas contra la víctima.

4. Por un agente que actúa en calidad de integrante de una organización destinada a perpetrar estos delitos.

5. Colocando a la víctima o a su familia en grave situación económica.

6. Sobre bienes de valor científico o que integren el patrimonio cultural de la Nación.

La pena será no menor de quince ni mayor de veinticinco años cuando el agente actúa en calidad de jefe, cabecilla o dirigente de una organización destinada a perpetrar estos delitos.

En los casos de concurso con delitos contra la vida, el cuerpo y la salud, la pena se aplica sin perjuicio de otra más grave que pudiera corresponder en cada caso." (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 26630, publicada el 21 junio 1996, cuyo texto es el siguiente:

"Artículo 189.- La pena será no menor de diez, ni mayor de veinte años, si el robo es cometido:

1. En casa habitada.

2. Durante la noche o en lugar desolado.

3. A mano armada.

4. Con el concurso de dos o más personas.

5. En vehículo de transporte público de pasajeros que esté prestando servicio.

6. Fingiendo ser agente de policía, autoridad o servidor público o mostrando orden o mandamiento falso de autoridad.

Si la violencia o amenaza fuesen insignificantes, la pena podrá ser disminuida en un tercio.

La pena será no menor de veinte ni mayor de veinticinco años, si el robo es cometido:

1. Con abuso de la incapacidad física o mental de la víctima o mediante el empleo de drogas contra la víctima.

2. Colocando a la víctima o a su familia en grave situación económica.

3. Sobre bienes de valor científico o que integren el patrimonio cultural de la nación.

4. Por un agente que haya sido sentenciado por terrorismo.

La pena será de cadena perpetua cuando el agente actúa en calidad de integrante de una organización destinada a perpetrar estos delitos o con empleo de armamentos, materiales o artefactos explosivos o con crueldad." (*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 896, publicado el 24 mayo 1998, expedido con arreglo a la Ley N° 26950, que otorga al Poder Ejecutivo facultades para legislar en materia de seguridad ciudadana, cuyo texto es el siguiente:

"Robo Agravado
Artículo 189.- La pena será no menor de quince ni mayor de veinticinco años, si el robo es cometido:

1 - En casa habitada.

2 - Durante la noche o en lugar desolado.

3.- A mano armada.

4.- Con el concurso de dos o más personas.

5.- En cualquier medio de locomoción de transporte público o privado de pasajeros o de carga.

6.- Fingiendo ser autoridad o servidor público o trabajador del sector privado o mostrando mandamiento falso de autoridad.

7.- En agravio de menores de edad o ancianos.

8.- Cuando se cause lesiones a la integridad física o mental de la víctima.

9.- Con abuso de la incapacidad física o mental de la víctima o mediante el empleo de drogas y/o insumos químicos o fármacos contra la víctima.

10.- Colocando a la víctima o a su familia en grave situación económica.

11.- Sobre bienes de valor científico o que integren el patrimonio cultural de la nación.

La pena será de cadena perpetua cuando el agente actúe en calidad de integrante de una organización delictiva o banda, o si como consecuencia del hecho se produce la muerte de la víctima o se le causa lesiones graves a su integridad física o mental." (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 27472, publicada el 05 junio 2001, cuyo texto es el siguiente:

"Artículo 189.- Robo agravado

La pena será no menor de diez ni mayor de veinte años, si el robo es cometido:

1. En casa habitada.

2. Durante la noche o en lugar desolado.

3. A mano armada.

4. Con el concurso de dos o más personas.

5. En cualquier medio de locomoción de transporte público o privado de pasajeros o de carga.

6. Fingiendo ser autoridad o servidor público o trabajador del sector privado o mostrando mandamiento falso de autoridad.

7. En agravio de menores de edad o ancianos.

La pena será no menor de veinte ni mayor de veinticinco años, si el robo es cometido:

1. Cuando se cause lesiones a la integridad física o mental de la víctima.

2. Con abuso de la incapacidad física o mental de la víctima o mediante el empleo de drogas y/o insumos químicos o fármacos contra la víctima.

3. Colocando a la víctima o a su familia en grave situación económica.

4. Sobre bienes de valor científico o que integren el patrimonio cultural de la nación.

La pena será de cadena perpetua cuando el agente actúe en calidad de integrante de una organización delictiva o banda, o si como consecuencia del hecho se produce la muerte de la víctima o se le causa lesiones graves a su integridad física o mental." (*)

(*) De conformidad con el Artículo 2 de la Ley N° 28982, publicada el 03 marzo 2007, se modifica inciso 5 del primer párrafo del presente Artículo, quedando redactado en su integridad en los siguientes términos:

“Artículo 189.- Robo agravado

La pena será no menor de diez ni mayor de veinte años, si el robo es cometido:

1. En casa habitada.

2. Durante la noche o en lugar desolado.

3. A mano armada.

4. Con el concurso de dos o más personas.

5. En cualquier medio de locomoción de transporte público o privado de pasajeros o de carga, terminales terrestres, ferroviarios, lacustres y fluviales, puertos, aeropuertos, restaurantes y afines, establecimientos de hospedaje y lugares de alojamiento, áreas naturales protegidas, fuentes de agua minero-medicinales con fines turísticos, bienes inmuebles integrantes del patrimonio cultural de la Nación y museos.

6. Fingiendo ser autoridad o servidor público o trabajador del sector privado o mostrando mandamiento falso de autoridad.

7. En agravio de menores de edad o ancianos.

La pena será no menor de veinte ni mayor de veinticinco años, si el robo es cometido:

1. Cuando se cause lesiones a la integridad física o mental de la víctima.

2. Con abuso de la incapacidad física o mental de la víctima o mediante el empleo de drogas y/o insumos químicos o fármacos contra la víctima.

3. Colocando a la víctima o a su familia en grave situación económica.

4. Sobre bienes de valor científico o que integren el patrimonio cultural de la Nación.

La pena será de cadena perpetua cuando el agente actúe en calidad de integrante de una organización delictiva o banda, o si como consecuencia del hecho se produce la muerte de la víctima o se le causa lesiones graves a su integridad física o mental.” (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 29407, publicada el 18 septiembre 2009, cuyo texto es el siguiente:

“Artículo 189.- Robo agravado

La pena será no menor de doce ni mayor de veinte años si el robo es cometido:

1. En casa habitada.

2. Durante la noche o en lugar desolado.

3. A mano armada.

4. Con el concurso de dos o más personas.

5. En cualquier medio de locomoción de transporte público o privado de pasajeros o de carga, terminales terrestres, ferroviarios, lacustres y fluviales, puertos, aeropuertos, restaurantes y afines, establecimientos de hospedaje y lugares de alojamiento, áreas naturales protegidas, fuentes de agua minero-medicinales con fines turísticos, bienes inmuebles integrantes del patrimonio cultural de la Nación y museos.

6. Fingiendo ser autoridad o servidor público o trabajador del sector privado o mostrando mandamiento falso de autoridad.

7. En agravio de menores de edad, discapacitados, mujeres en estado de gravidez o ancianos.

8. Sobre vehículo automotor.

La pena será no menor de veinte ni mayor de treinta años si el robo es cometido:

1. Cuando se cause lesiones a la integridad física o mental de la víctima.

2. Con abuso de la incapacidad física o mental de la víctima o mediante el empleo de drogas, insumos químicos o fármacos contra la víctima.

3. Colocando a la víctima o a su familia en grave situación económica.

4. Sobre bienes de valor científico o que integren el patrimonio cultural de la Nación.

La pena será de cadena perpetua cuando el agente actúe en calidad de integrante de una organización delictiva o banda, o si, como consecuencia del hecho, se produce la muerte de la víctima o se le causa lesiones graves a su integridad física o mental.” (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 30076, publicada el 19 agosto 2013, cuyo texto es el siguiente:

"Artículo 189. Robo agravado

La pena será no menor de doce ni mayor de veinte años si el robo es cometido:

1. En inmueble habitado.

2. Durante la noche o en lugar desolado.

3. A mano armada.

4. Con el concurso de dos o más personas.

5. En cualquier medio de locomoción de transporte público o privado de pasajeros o de carga, terminales terrestres, ferroviarios, lacustres y fluviales, puertos, aeropuertos, restaurantes y afines, establecimientos de hospedaje y lugares de alojamiento, áreas naturales protegidas, fuentes de agua minero-medicinales con fines turísticos, bienes inmuebles integrantes del patrimonio cultural de la Nación y museos.

6. Fingiendo ser autoridad o servidor público o trabajador del sector privado o mostrando mandamiento falso de autoridad.

7. En agravio de menores de edad, personas con discapacidad, mujeres en estado de gravidez o adulto mayor.

8. Sobre vehículo automotor, sus autopartes o accesorios.

La pena será no menor de veinte ni mayor de treinta años si el robo es cometido:

1. Cuando se cause lesiones a la integridad física o mental de la víctima.

2. Con abuso de la incapacidad física o mental de la víctima o mediante el empleo de drogas, insumos químicos o fármacos contra la víctima.

3. Colocando a la víctima o a su familia en grave situación económica.

4. Sobre bienes de valor científico o que integren el patrimonio cultural de la Nación.

La pena será de cadena perpetua cuando el agente en calidad de integrante de una organización criminal, como consecuencia del hecho, produce la muerte de la víctima o le causa lesiones graves a su integridad física o mental." (*)

(*) Extremo modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 30077, publicada el 20 agosto 2013, la misma que entró en vigencia el 1 de julio de 2014, cuyo texto es el siguiente:

"La pena será de cadena perpetua cuando el agente actúe en calidad de integrante de una organización criminal, o si, como consecuencia del hecho, se produce la muerte de la víctima o se le causa lesiones graves a su integridad física o mental."

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

CONCORDANCIAS:     Ley Nº 30077, Arts. 3 (Delitos comprendidos) y 24 (Prohibición de beneficios penitenciarios)

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA
PROCESOS CONSTITUCIONALES

"CAPITULO II "A"
ABIGEATO

Artículo 189-A Hurto de ganado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, para obtener provecho, se apodera ilegítimamente de ganado vacuno, ovino, equino, caprino, porcino o auquénido, total o parcialmente ajeno, aunque se trate de un solo animal, sustrayéndolo del lugar donde se encuentra, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años.

Si concurre alguna de las circunstancias previstas en los incisos 1, 2, 3, 4 y 5 del primer párrafo del Artículo 186, la pena será privativa de libertad no menor de tres ni mayor de seis años.

Si el delito es cometido conforme a los incisos 2, 4 y 5 del segundo párrafo del Artículo 186, la pena será no menor de cuatro ni mayor de diez años.

La pena será no menor de 8 ni mayor de 15 años cuando el agente actúa en calidad de jefe, cabecilla o dirigente de una organización destinada a perpetrar estos delitos.

Artículo 189-B Hurto de uso de ganado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que sustrae ganado ajeno, con el fin de hacer uso momentáneo y lo devuelve, directa o indirectamente en un plazo no superior a setentidós horas, (*) RECTIFICADO POR FE DE ERRATAS será reprimido con pena privativa de libertad no mayor de un año o de prestación de servicios a la comunidad no mayor de cincuenta jornadas. Si la devolución del animal se produce luego de transcurrido dicho plazo, será aplicable el artículo anterior.

Artículo 189-C Robo de ganado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que se apodera ilegítimamente de ganado vacuno, ovino, equino, caprino, porcino o auquénido, total o parcialmente ajeno, aunque se trate de un solo animal, sustrayéndolo del lugar donde se encuentra, empleando violencia contra la persona o amenazándola con un peligro inminente para su vida o integridad física, será reprimido con pena privativa de libertad no menor de tres ni mayor de ocho años.

La pena será privativa de libertad no menor de cinco ni mayor de quince años si el delito se comete con el concurso de dos o más personas, o el agente hubiere inferido lesión grave a otro o portando cualquier clase de arma o de instrumento que pudiere servir como tal.

Si la violencia o amenaza fuesen insignificantes, la pena será disminuida en un tercio.

La pena será no menor de diez ni mayor de veinte años si el delito cometido  conforme a los incisos 1, 2, 3, 4 y 5  del segundo párrafo del artículo 189.

La pena será no menor de quince ni mayor de venticinco años si el agente actúa en calidad de jefe, cabecilla o dirigente de una organización destinada a perpetrar estos delitos.

En los casos de concurso con delitos contra la vida, el cuerpo y la salud, la pena se aplica sin perjuicio de otra más grave que pudiera corresponder en cada caso." (*)

(*) Capítulo II "A" incorporado por el Artículo 1 de la Ley  Nº 26326, publicada el 04 junio 1994, disposición que entró en vigencia a los 60 días siguientes de su publicación, conforme al Artículo 3 de la citada Ley.

CAPITULO  III APROPIACION ILICITA
#################################

Artículo 190 Apropiación ilícita común
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, en su provecho o de un tercero, se apropia indebidamente de un bien mueble, una suma de dinero o un valor que ha recibido en depósito, comisión, administración u otro título semejante que produzca obligación de entregar, devolver, o hacer un uso determinado, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.

Si el agente obra en calidad de curador, tutor, albacea, síndico, depositario judicial o en el ejercicio de una profesión o industria para la cual tenga título o autorización oficial, la pena será privativa de libertad no menor de tres ni mayor de seis años.

Cuando el agente se apropia de bienes destinados al auxilio de poblaciones que sufren las consecuencias de desastres naturales u otros similares la pena será privativa de libertad no menor de cuatro ni mayor de diez años.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA
PROCESOS CONSTITUCIONALES

Artículo 191 Sustracción de bien propio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El propietario de un bien mueble que lo sustrae de quien lo tenga legítimamente en su poder, con perjuicio de éste o de un tercero, será reprimido con pena privativa de libertad no mayor de cuatro años.

Artículo 192 Apropiación irregular
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no mayor de dos años o con limitación de días libres de diez a veinte jornadas, quien realiza cualquiera de las acciones siguientes:

1. Se apropia de un bien que encuentra perdido o de un tesoro, o de la parte del tesoro correspondiente al propietario del suelo, sin observar las normas del Código Civil.

2. Se apropia de un bien ajeno en cuya tenencia haya entrado a consecuencia de un error, caso fortuito o por cualquier otro motivo independiente de su voluntad.

Artículo 193 Apropiación de prenda
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que vende la prenda constituída en su favor o se apropia o dispone de ella sin observar las formalidades legales, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

CAPITULO  IV RECEPTACION
########################

Artículo 194 Receptación
~~~~~~~~~~~~~~~~~~~~~~~~

El que adquiere, recibe en donación o en prenda o guarda, esconde, vende o ayuda a negociar un bien de cuya procedencia delictuosa tenía conocimiento o debía presumir que provenía de un delito, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años y, con treinta a noventa días- multa. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 30076, publicada el 19 agosto 2013, cuyo texto es el siguiente:

"Artículo 194. Receptación

El que adquiere, recibe en donación o en prenda o guarda, esconde, vende o ayuda a negociar un bien de cuya procedencia delictuosa tenía conocimiento o debía presumir que provenía de un delito, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años y con treinta a noventa días-multa."

CONCORDANCIAS:     Ley Nº 29407, Art. 4

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

"Artículo 194-A.- Distribución de señales de satélite portadoras de programas

El que distribuya una señal de satélite portadora de programas, originariamente codificada, a sabiendas que fue decodificada sin la autorización del distribuidor legal de dicha señal, será reprimido con pena privativa de la libertad no menor de dos años ni mayor de seis años y con treinta a noventa días multa." (*)

(*) Artículo incorporado por el Artículo 1 de la Ley N° 29316, publicada el 14 enero 2009.

CONCORDANCIAS:     Ley N° 29316, Tercera Disposición Complementaria Final

Artículo 195 Formas agravadas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La pena será privativa de libertad no menor de dos ni mayor de seis años y de treinta a noventa días-multa, cuando:

1.El agente se dedica al comercio de objetos provenientes de acciones delictuosas.

2.Se trata de bienes de propiedad del Estado destinados al servicio público. (*)

(*) Artículo sustituido por el Artículo Unico de la Ley Nº 25404, publicada el 26 febrero 1992, cuyo texto es el siguiente:

"Artículo 195.- La pena privativa de la libertad será:

1. No menor de 02 ni mayor de 06 años y treinta a noventa días multa, cuando se trata de bienes de propiedad del Estado destinados al servicio público o cuando el agente se dedica al comercio de objetos provenientes de acciones delictuosas no comprendidas en el inciso 2).

2. No menor de 06 ni mayor de 15 años y de 180 a 365 días multa, e inhabilitación, conforme al Artículo 36, incisos 1), 2) y 4) cuando se trate de bienes provenientes de delitos de tráfico ilícito de drogas o terrorismo". (*)

(*) De conformidad con el Artículo 2 del Decreto Ley N° 25428, publicado el 11 abril 1992, se deroga la Ley Nº 25404, la misma que sustituia el Artículo 195.

“Artículo 195.- Formas agravadas

La pena será privativa de la libertad no menor de seis ni mayor de diez años si se trata de bienes provenientes de la comisión de los delitos de secuestro, extorsión y trata de personas.” (1)(2) (*)RECTIFICADO POR FE DE ERRATAS

(1) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 982, publicado el 22 julio 2007.

(2) Artículo modificado por el Artículo 1 de la Ley Nº 29407, publicada el 18 septiembre 2009, cuyo texto es el siguiente:

“Artículo 195.- Formas agravadas

La pena será privativa de la libertad no menor de dos ni mayor de cinco años si se trata de vehículos automotores o sus partes importantes.

La pena será privativa de la libertad no menor de seis ni mayor de diez años si se trata de bienes provenientes de la comisión de los delitos de secuestro, extorsión y trata de personas.” (*)

(*) Artículo modificado por el Artículo Único de la Ley N° 29583, publicada el  18 septiembre 2010, cuyo texto es el siguiente:

"Artículo 195.- Formas agravadas

La pena será privativa de la libertad no menor de dos ni mayor de cinco años y de sesenta a ciento cincuenta días multa si se trata de vehículos automotores o sus partes importantes, o si la conducta recae sobre bienes que forman parte de la infraestructura o instalaciones de transporte de uso público, de sus equipos o elementos de seguridad, o de prestación de servicios públicos de saneamiento, electricidad, gas o telecomunicaciones.

La pena será privativa de la libertad no menor de seis ni mayor de diez años si se trata de bienes provenientes de la comisión de los delitos de secuestro, extorsión y trata de personas." (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 30076, publicada el 19 agosto 2013, cuyo texto es el siguiente:

"Artículo 195. Receptación agravada

La pena será privativa de libertad no menor de cuatro ni mayor de seis años y de sesenta a ciento cincuenta días-multa si se trata de vehículos automotores, sus autopartes o accesorios, o si la conducta recae sobre bienes que forman parte de la infraestructura o instalaciones de transporte de uso público, de sus equipos o elementos de seguridad, o de prestación de servicios públicos de saneamiento, electricidad, gas o telecomunicaciones.

La pena será privativa de libertad no menor de seis ni mayor de doce años si se trata de bienes provenientes de la comisión de los delitos de robo agravado, secuestro, extorsión y trata de personas."(*)

(*) Artículo modificado por la Única Disposición Complementaria Modificatoria del Decreto Legislativo N° 1215, publicado el 24 septiembre 2015, cuyo texto es el siguiente:

«Artículo 195 - Formas agravadas.

La pena privativa de libertad será no menor de cuatro ni mayor de seis años y de sesenta a ciento cincuenta días-multa:

1. Si se trata de vehículos automotores, sus autopartes o accesorios.

2. Si se trata de equipos de informática, equipos de telecomunicación, sus componentes y periféricos.

3. Si la conducta recae sobre bienes que forman parte de la infraestructura o instalaciones de transporte de uso público, de sus equipos o elementos de seguridad, o de prestación de servicios públicos de saneamiento, electricidad, gas o telecomunicaciones. (*)

(*) Numeral modificado por el Artículo 2 del Decreto Legislativo N° 1245, publicado el 06 noviembre 2016, cuyo texto es el siguiente:

"3. Si la conducta recae sobre bienes que forman parte de la infraestructura o instalaciones de transporte de uso público, de sus equipos o elementos de seguridad, o de prestación de servicios públicos de saneamiento, electricidad o telecomunicaciones."

4. Si se trata de bienes de propiedad del Estado destinado al uso público, fines asistenciales o a programas de apoyo social.

5. Si se realiza en el comercio de bienes muebles al público.

"6. Si se trata de gas, de hidrocarburos o de sus productos derivados."(*)

(*) Numeral incorporado por el Artículo 2 del Decreto Legislativo N° 1245, publicado el 06 noviembre 2016.

"7. Si la conducta recae sobre bienes que forman parte de la infraestructura o instalaciones públicas o privadas para la exploración, explotación, procesamiento, refinación, almacenamiento, transporte, distribución, comercialización o abastecimiento de gas, de hidrocarburos o de sus productos derivados, conforme a la legislación de la materia."(*)

(*) Numeral incorporado por el Artículo 2 del Decreto Legislativo N° 1245, publicado el 06 noviembre 2016.

La pena será privativa de libertad no menor de seis ni mayor de doce años si se trata de bienes provenientes de la comisión de los delitos de robo agravado, secuestro, extorsión y trata de personas». (*)

(*) Párrafo modificado por el Artículo Único de la Ley N° 30924, publicada el 29 marzo 2019, cuyo texto es el siguiente:

"La pena será privativa de libertad no menor de seis ni mayor de doce años si se trata de bienes provenientes de la comisión de los delitos de robo agravado, secuestro, extorsión, trata de personas y trabajo forzoso”.

CONCORDANCIAS:     Ley Nº 29407, Art. 4

Ley Nº 30077, Art. 3 (Delitos comprendidos)

D.S.N° 015-2019-TR (Decreto Supremo que aprueba el III Plan Nacional para la Lucha contra el Trabajo Forzoso 2019 - 2022)

CAPITULO  V ESTAFA Y OTRAS DEFRAUDACIONES
#########################################

Artículo 196 Estafa
~~~~~~~~~~~~~~~~~~~

El que procura para sí o para otro un provecho ilícito en perjuicio de tercero, induciendo o manteniendo en error al agraviado mediante engaño, astucia, ardid u otra forma fraudulenta, será reprimido con pena privativa de libertad no menor de uno ni mayor de seis años.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA
PROCESOS CONSTITUCIONALES

"Artículo 196-A. Estafa agravada

La pena será privativa de libertad no menor de cuatro ni mayor de ocho años y con noventa a doscientos días-multa, cuando la estafa:

1. Se cometa en agravio de menores de edad, personas con discapacidad, mujeres en estado de gravidez o adulto mayor.

2. Se realice con la participación de dos o más personas.

3. Se cometa en agravio de pluralidad de víctimas.

4. Se realice con ocasión de compra-venta de vehículos motorizados o bienes inmuebles.

5. Se realice para sustraer o acceder a los datos de tarjetas de ahorro o de crédito, emitidos por el sistema financiero o bancario."(*)

(*) Artículo incorporado por el Artículo 2 de la Ley Nº 30076, publicada el 19 agosto 2013.

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1351, publicado el 07 enero 2017, cuyo texto es el siguiente:

“Artículo 196-A.- Estafa agravada

La pena será privativa de libertad no menor de cuatro ni mayor de ocho años y con noventa a doscientos días-multa, cuando la estafa:

1. Se cometa en agravio de menores de edad, personas con discapacidad, mujeres en estado de gravidez o adulto mayor.

2. Se realice con la participación de dos o más personas.

3. Se cometa en agravio de pluralidad de víctimas.

4. Se realice con ocasión de compra-venta de vehículos motorizados o bienes inmuebles.

5. Se realice para sustraer o acceder a los datos de tarjetas de ahorro o de crédito, emitidos por el sistema financiero o bancario.

6. Se realice con aprovechamiento de la situación de vulnerabilidad de la víctima."

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

Artículo 197 Casos de defraudación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La defraudación será reprimida con pena privativa de libertad no menor de uno ni mayor de cuatro años y con sesenta a ciento veinte días-multa cuando:

1. Se realiza con simulación de juicio o empleo de otro fraude procesal.

2. Se abusa de firma en blanco, extendiendo algún documento en perjuicio del firmante o de tercero.

3. Si el comisionista o cualquier otro mandatario, altera en sus cuentas los  precios o condiciones de los contratos, suponiendo gastos o exagerando los que hubiera hecho.

4. Se vende o grava, como bienes libres, los que son litigiosos o están embargados o gravados y cuando se vende, grava o arrienda como propios los bienes ajenos.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

CAPITULO  VI FRAUDE EN LA ADMINISTRACION DE PERSONAS JURIDICAS
##############################################################

Artículo 198 Administración fraudulenta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años el que, en su condición de fundador, miembro del directorio o del consejo de administración o del consejo de vigilancia, gerente, administrador o liquidador de una persona jurídica, realiza, en perjuicio de ella o de terceros, cualquiera de los actos siguientes:

1. Ocultar a los accionistas, socios, asociados o terceros interesados, la  verdadera situación de la persona jurídica, falseando los balances, reflejando u omitiendo en los mismos beneficios o pérdidas o usando cualquier artificio que suponga aumento o disminución de las partidas contables.

2. Proporcionar datos falsos relativos a la situación de una persona  jurídica.

3. Promover, por cualquier medio fraudulento, falsas cotizaciones de acciones, títulos o participaciones.

4. Aceptar, estando prohibido hacerlo, acciones o títulos de la misma persona jurídica como garantía de crédito.

5. Fraguar balances para reflejar y distribuir utilidades inexistentes.

6. Omitir comunicar al directorio, consejo de administración, consejo directivo u otro órgano similar, acerca de la existencia de intereses  propios que son incompatibles con los de la persona jurídica.

7. Asumir préstamos para la persona jurídica.

8. Usar en provecho propio, o de otro, el patrimonio de la persona.(*)

(*) Artículo modificado por el Artículo 3 de la Ley N° 28755, publicada el 06 junio 2006, cuyo texto es el siguiente:

“Artículo 198.- Administración fraudulenta

Será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años el que, en su condición de fundador, miembro del directorio o del consejo de administración o del consejo de vigilancia, gerente, administrador, auditor interno, auditor externo o liquidador de una persona jurídica, realiza, en perjuicio de ella o de terceros, cualquiera de los actos siguientes:

1. Ocultar a los accionistas, socios, asociados, auditor interno, auditor externo, según sea el caso o a terceros interesados, la verdadera situación de la persona jurídica, falseando los balances, reflejando u omitiendo en los mismos beneficios o pérdidas o usando cualquier artificio que suponga aumento o disminución de las partidas contables.

2. Proporcionar datos falsos relativos a la situación de una persona jurídica.

3. Promover, por cualquier medio fraudulento, falsas cotizaciones de acciones, títulos o participaciones.

4. Aceptar, estando prohibido hacerlo, acciones o títulos de la misma persona jurídica como garantía de crédito.

5. Fraguar balances para reflejar y distribuir utilidades inexistentes.

6. Omitir comunicar al directorio, consejo de administración, consejo directivo u otro órgano similar, o al auditor interno o externo, acerca de la existencia de intereses propios que son incompatibles con los de la persona jurídica.

7. Asumir préstamos para la persona jurídica.

8. Usar en provecho propio, o de otro, el patrimonio de la persona jurídica.

9. Emitir informes o dictámenes que omitan revelar, o revelen en forma distorsionada, situaciones de falta de solvencia o insuficiencia patrimonial de la persona jurídica, o que no revelen actos u omisiones que violen alguna disposición que la persona jurídica está obligada a cumplir y qué esté relacionada con alguna de las conductas tipificadas en el presente artículo." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 29307, publicada el 31 diciembre 2008, cuyo texto es el siguiente:

“Artículo 198.- Administración fraudulenta

Será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años el que ejerciendo funciones de administración o representación de una persona jurídica, realiza, en perjuicio de ella o de terceros, cualquiera de los actos siguientes:

1. Ocultar a los accionistas, socios, asociados, auditor interno, auditor externo, según sea el caso o a terceros interesados, la verdadera situación de la persona jurídica, falseando los balances, reflejando u omitiendo en los mismos beneficios o pérdidas o usando cualquier artificio que suponga aumento o disminución de las partidas contables.

2. Proporcionar datos falsos relativos a la situación de una persona jurídica.

3. Promover, por cualquier medio fraudulento, falsas cotizaciones de acciones, títulos o participaciones.

4. Aceptar, estando prohibido hacerlo, acciones o títulos de la misma persona jurídica como garantía de crédito.

5. Fraguar balances para reflejar y distribuir utilidades inexistentes.

6. Omitir comunicar al directorio, consejo de administración, consejo directivo u otro órgano similar o al auditor interno o externo, acerca de la existencia de intereses propios que son incompatibles con los de la persona jurídica.

7. Asumir indebidamente préstamos para la persona jurídica.

8. Usar en provecho propio, o de otro, el patrimonio de la persona jurídica."

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

“Artículo 198-A.- Informes de auditoría distorsionados

Será reprimido con la pena señalada en el artículo anterior el auditor interno o externo que a sabiendas de la existencia de distorsiones o tergiversaciones significativas en la información contable-financiera de la persona jurídica no las revele en su informe o dictamen.” (*)

(*) Artículo incorporado por el Artículo 2 de la Ley N° 29307, publicada el 31 diciembre 2008.

Artículo 199 Contabilidad paralela
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, con la finalidad de obtener ventaja indebida, mantiene contabilidad paralela distinta a la exigida por la ley, será reprimido con pena privativa de libertad no mayor de un año y con sesenta a noventa días-multa (*) RECTIFICADO POR FE DE ERRATAS.

CAPITULO  VII EXTORSION
#######################

Artículo 200 
~~~~~~~~~~~~~

El que, mediante violencia, amenaza o manteniendo en rehén a una persona, obliga a ésta o a otra a otorgar al agente o a un tercero una ventaja económica indebida, será reprimido con pena privativa de libertad no menor de seis ni mayor de doce años.

La pena será privativa de libertad no menor de doce ni mayor de veinte años cuando:

1. El rehén es menor de edad.
2. El secuestro dura más de cinco días.
3. Se emplea crueldad contra el rehén.
4. El secuestrado ejerce función pública.
5. El rehén es inválido o adolece de enfermedad.
6. Es cometido por dos o más personas.(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 896, publicado el 24 mayo 1998, expedido con arreglo a la Ley N° 26950, que otorga al Poder Ejecutivo facultades para legislar en materia de seguridad nacional, cuyo texto es el siguiente:

"Extorsión
Artículo 200.- El que mediante violencia, amenaza o manteniendo en rehén a una persona, obliga a ésta o a otra a otorgar al agente o a un tercero una ventaja económica indebida o de cualquier otra índole, será reprimido con pena privativa de libertad no menor de diez ni mayor de veinte años.

La pena será privativa de libertad no menor de veinte años cuando:

1.- El rehén es menor de edad.
2.- El secuestro dura más de cinco días.
3.- Se emplea crueldad contra el rehén.
4.- El rehén ejerce función pública o privada o es representante diplomático.
5.- El rehén es inválido o adolece de enfermedad.
6.- Es cometido por dos o más personas.

La pena será de cadena perpetua si el rehén muere o sufre lesiones graves a su integridad física o mental." (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 27472, publicada el 05 junio 2001, cuyo texto es el siguiente:

"Artículo 200.- Extorsión

El que mediante violencia, amenaza o manteniendo en rehén a una persona, obliga a ésta o a otra a otorgar al agente o a un tercero una ventaja económica indebida o de cualquier otra índole, será reprimido con pena privativa de libertad no menor de seis ni mayor de doce años.

La pena será privativa de libertad no menor de veinte años cuando:

1. El rehén es menor de edad.

2. El secuestro dura más de cinco días.

3. Se emplea crueldad contra el rehén.

4. El rehén ejerce función pública o privada o es representante diplomático.

5. El rehén es inválido o adolece de enfermedad.

6. Es cometido por dos o más personas.

La pena será no menor de veinticinco años si el rehén muere y no menor de doce ni mayor de quince años si el rehén sufre lesiones graves a su integridad física o mental.” (*)

(*) Artículo modificado por el Artículo Único de la Ley N° 28353, publicada el 06 octubre 2004, cuyo texto es el siguiente:

“Artículo 200.- Extorsión

El que mediante violencia, amenaza o manteniendo en rehén a una persona, obliga a ésta o a otra a otorgar al agente o a un tercero una ventaja económica indebida o de cualquier otra índole, será reprimido con pena privativa de libertad no menor de seis ni mayor de doce años.

La pena será privativa de libertad no menor de veinte años, cuando el secuestro:

1. Dura más de cinco días.

2. Se emplea crueldad contra el rehén.

3. El rehén ejerce función pública o privada o es representante diplomático.

4. El rehén es inválido o adolece de enfermedad.

5. Es cometido por dos o más personas.

La pena será no menor de veinticinco años si el rehén es menor de edad o sufre lesiones graves en su integridad física o mental.

La pena será privativa de libertad no menor de veinticinco ni mayor de treinta y cinco años si el rehén fallece durante el delito o a consecuencia de dicho acto.” (*)

(*) Artículo modificado por el Inc. a) del Artículo 1 de la Ley N° 28760, publicada el 14 junio 2006, cuyo texto es el siguiente:

"Artículo 200.- Extorsión

El que mediante violencia, amenaza o manteniendo en rehén a una persona, obliga a ésta o a otra a otorgar al agente o a un tercero una ventaja económica indebida o de cualquier otra índole, será reprimido con pena privativa de libertad no menor de veinte ni mayor de treinta años.

La pena será privativa de libertad no menor de treinta años, cuando el secuestro:

1. Dura más de cinco días.
2. Se emplea crueldad contra el rehén.
3. El agraviado o el agente ejerce función pública o privada o es representante diplomático.
4. El rehén adolece de enfermedad.
5. Es cometido por dos o más personas.

La pena será de cadena perpetua si el rehén es menor de edad, mayor de sesenta y cinco años o discapacitado o si la víctima sufre lesiones en su integridad física o mental o si fallece a consecuencia de dicho acto.”(*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 982, publicado el 22 julio 2007, cuyo texto es el siguiente:

“Artículo 200.- Extorsión

El que mediante violencia o amenaza obliga a una persona o a una institución pública o privada a otorgar al agente o a un tercero una ventaja económica indebida u otra ventaja de cualquier otra índole, será reprimido con pena privativa de libertad no menor de diez ni mayor de quince años.

La misma pena se aplicará al que, con la finalidad de contribuir a la comisión del delito de extorsión, suministra información que haya conocido por razón o con ocasión de sus funciones, cargo u oficio o proporciona deliberadamente los medios para la perpetración del delito.

El que mediante violencia o amenaza, toma locales, obstaculiza vías de comunicación o impide el libre tránsito de la ciudadanía o perturba el normal funcionamiento de los servicios públicos o la ejecución de obras legalmente autorizadas, con el objeto de obtener de las autoridades cualquier beneficio o ventaja económica indebida u otra ventaja de cualquier otra índole, será sancionado con pena privativa de libertad no menor de cinco ni mayor de diez años.

El funcionario público con poder de decisión o el que desempeña cargo de confianza o de dirección que, contraviniendo lo establecido en el artículo 42 de la Constitución Política del Perú, participe en una huelga con el objeto de obtener para sí o para terceros cualquier beneficio o ventaja económica indebida u otra ventaja de cualquier otra índole, será sancionado con inhabilitación conforme a los incisos 1) y 2) del artículo 36 del Código Penal.

La pena será no menor de quince ni mayor de veinticinco años si la violencia o amenaza es cometida:

a) A mano armada;

b) Participando dos o más personas; o,

c) Valiéndose de menores de edad.

Si el agente con la finalidad de obtener una ventaja económica indebida o de cualquier otra índole, mantiene en rehén a una persona, la pena será no menor de veinte ni mayor de treinta años.

La pena será privativa de libertad no menor de treinta años, cuando en el supuesto previsto en el párrafo anterior:

a) Dura más de veinticuatro horas.

b) Se emplea crueldad contra el rehén.

c) El agraviado ejerce función pública o privada o es representante diplomático.

d) El rehén adolece de enfermedad grave.

e) Es cometido por dos o más personas.

f) Se causa lesiones leves a la víctima.

La pena será de cadena perpetua cuando:

a) El rehén es menor de edad o mayor de setenta años.

b) El rehén es persona con discapacidad y el agente se aprovecha de esta circunstancia.

c) Si la víctima resulta con lesiones graves o muere durante o como consecuencia de dicho acto.” (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 30076, publicada el 19 agosto 2013, cuyo texto es el siguiente:

"Artículo 200. Extorsión

El que mediante violencia o amenaza obliga a una persona o a una institución pública o privada a otorgar al agente o a un tercero una ventaja económica indebida u otra ventaja de cualquier otra índole, será reprimido con pena privativa de libertad no menor de diez ni mayor de quince años.

La misma pena se aplicará al que, con la finalidad de contribuir a la comisión del delito de extorsión, suministra información que haya conocido por razón o con ocasión de sus funciones, cargo u oficio o proporciona deliberadamente los medios para la perpetración del delito.

El que mediante violencia o amenaza, toma locales, obstaculiza vías de comunicación o impide el libre tránsito de la ciudadanía o perturba el normal funcionamiento de los servicios públicos o la ejecución de obras legalmente autorizadas, con el objeto de obtener de las autoridades cualquier beneficio o ventaja económica indebida u otra ventaja de cualquier otra índole, será sancionado con pena privativa de libertad no menor de cinco ni mayor de diez años.

El funcionario público con poder de decisión o el que desempeña cargo de confianza o de dirección que, contraviniendo lo establecido en el artículo 42 de la Constitución Política del Perú, participe en una huelga con el objeto de obtener para sí o para terceros cualquier beneficio o ventaja económica indebida u otra ventaja de cualquier otra índole, será sancionado con inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal.

La pena será no menor de quince ni mayor de veinticinco años e inhabilitación conforme a los numerales 4 y 6 del artículo 36, si la violencia o amenaza es cometida:

a) A mano armada;

b) Participando dos o más personas; o,

c) Contra el propietario, responsable o contratista de la ejecución de una obra de construcción civil pública o privada, o de cualquier modo, impidiendo, perturbando, atentando o afectando la ejecución de la misma.

Si el agente con la finalidad de obtener una ventaja económica indebida o de cualquier otra índole, mantiene en rehén a una persona, la pena será no menor de veinte ni mayor de treinta años.

La pena será privativa de libertad no menor de treinta años, cuando en el supuesto previsto en el párrafo anterior:

a) Dura más de veinticuatro horas.

b) Se emplea crueldad contra el rehén.

c) El agraviado ejerce función pública o privada o es representante diplomático.

d) El rehén adolece de enfermedad grave.

e) Es cometido por dos o más personas.

f) Se causa lesiones leves a la víctima.

La pena prevista en el párrafo anterior se impone al agente que, para conseguir sus cometidos extorsivos, usa armas de fuego o artefactos explosivos.

La pena será de cadena perpetua cuando:

a) El rehén es menor de edad o mayor de setenta años.

b) El rehén es persona con discapacidad y el agente se aprovecha de esta circunstancia.

c) Si la víctima resulta con lesiones graves o muere durante o como consecuencia de dicho acto.

d) El agente se vale de menores de edad."(*)

(*) Artículo modificado por la Primera Disposición Complementaria Modificatoria del Decreto Legislativo N° 1187, publicado el 16 agosto 2015, cuyo texto es el siguiente:

“Artículo 200. Extorsión

El que mediante violencia o amenaza obliga a una persona o a una institución pública o privada a otorgar al agente o a un tercero una ventaja económica indebida u otra ventaja de cualquier otra índole, será reprimido con pena privativa de libertad no menor de diez ni mayor de quince años.

La misma pena se aplicará al que, con la finalidad de contribuir a la comisión del delito de extorsión, suministra información que haya conocido por razón o con ocasión de sus funciones, cargo u oficio o proporciona deliberadamente los medios para la perpetración del delito.

El que mediante violencia o amenaza, toma locales, obstaculiza vías de comunicación o impide el libre tránsito de la ciudadanía o perturba el normal funcionamiento de los servicios públicos o la ejecución de obras legalmente autorizadas, con el objeto de obtener de las autoridades cualquier beneficio o ventaja económica indebida u otra ventaja de cualquier otra índole, será sancionado con pena privativa de libertad no menor de cinco ni mayor de diez años.

El funcionario público con poder de decisión o el que desempeña cargo de confianza o de dirección que, contraviniendo lo establecido en el artículo 42 de la Constitución Política del Perú, participe en una huelga con el objeto de obtener para sí o para terceros cualquier beneficio o ventaja económica indebida u otra ventaja de cualquier otra índole, será sancionado con inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal.

La pena será no menor de quince ni mayor de veinticinco años e inhabilitación conforme a los numerales 4 y 6 del artículo 36, si la violencia o amenaza es cometida:

a) A mano armada;

b) Participando dos o más personas; o,

c) Contra el propietario, responsable o contratista de la ejecución de una obra de construcción civil pública o privada, o de cualquier modo, impidiendo, perturbando, atentando o afectando la ejecución de la misma.

d) Aprovechando su condición de integrante de un sindicato de construcción civil.

e) Simulando ser trabajador de construcción civil.

Si el agente con la finalidad de obtener una ventaja económica indebida o de cualquier otra índole, mantiene en rehén a una persona, la pena será no menor de veinte ni mayor de treinta años.

La pena será privativa de libertad no menor de treinta años, cuando en el supuesto previsto en el párrafo anterior:

a) Dura más de veinticuatro horas.

b) Se emplea crueldad contra el rehén.

c) El agraviado ejerce función pública o privada o es representante diplomático.

d) El rehén adolece de enfermedad grave.

e) Es cometido por dos o más personas.

f) Se causa lesiones leves a la víctima.

La pena prevista en el párrafo anterior se impone al agente que, para conseguir sus cometidos extorsivos, usa armas de fuego o artefactos explosivos.

La pena será de cadena perpetua cuando:

a) El rehén es menor de edad o mayor de setenta años.

b) El rehén es persona con discapacidad y el agente se aprovecha de esta circunstancia.

c) Si la víctima resulta con lesiones graves o muere durante o como consecuencia de dicho acto.

d) El agente se vale de menores de edad.” (*)

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 200.- Extorsión

El que mediante violencia o amenaza obliga a una persona o a una institución pública o privada a otorgar al agente o a un tercero una ventaja económica indebida u otra ventaja de cualquier otra índole, será reprimido con pena privativa de libertad no menor de diez ni mayor de quince años.

La misma pena se aplicará al que, con la finalidad de contribuir a la comisión del delito de extorsión, suministra información que haya conocido por razón o con ocasión de sus funciones, cargo u oficio o proporciona deliberadamente los medios para la perpetración del delito.

El que mediante violencia o amenaza, toma locales, obstaculiza vías de comunicación o impide el libre tránsito de la ciudadanía o perturba el normal funcionamiento de los servicios públicos o la ejecución de obras legalmente autorizadas, con el objeto de obtener de las autoridades cualquier beneficio o ventaja económica indebida u otra ventaja de cualquier otra índole, será sancionado con pena privativa de libertad no menor de cinco ni mayor de diez años.

El funcionario público con poder de decisión o el que desempeña cargo de confianza o de dirección que, contraviniendo lo establecido en el artículo 42 de la Constitución Política del Perú, participe en una huelga con el objeto de obtener para sí o para terceros cualquier beneficio o ventaja económica indebida u otra ventaja de cualquier otra índole, será sancionado con inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal.

La pena será no menor de quince ni mayor de veinticinco años e inhabilitación conforme a los numerales 4 y 6 del artículo 36, si la violencia o amenaza es cometida:

a) A mano armada, o utilizando artefactos explosivos o incendiarios.

b) Participando dos o más personas; o,

c) Contra el propietario, responsable o contratista de la ejecución de una obra de construcción civil pública o privada, o de cualquier modo, impidiendo, perturbando, atentando o afectando la ejecución de la misma.

d) Aprovechando su condición de integrante de un sindicato de construcción civil.

e) Simulando ser trabajador de construcción civil.

Si el agente con la finalidad de obtener una ventaja económica indebida o de cualquier otra índole, mantiene en rehén a una persona, la pena será no menor de veinte ni mayor de treinta años.

La pena será privativa de libertad no menor de treinta años, cuando en el supuesto previsto en el párrafo anterior:

a) Dura más de veinticuatro horas.

b) Se emplea crueldad contra el rehén.

c) El agraviado ejerce función pública o privada o es representante diplomático.

d) El rehén adolece de enfermedad grave.

e) Es cometido por dos o más personas.

f) Se causa lesiones leves a la víctima.

La pena prevista en el párrafo anterior se impone al agente que, para conseguir sus cometidos extorsivos, usa armas de fuego o artefactos explosivos.

La pena será de cadena perpetua cuando:

a) El rehén es menor de edad o mayor de setenta años.

b) El rehén es persona con discapacidad y el agente se aprovecha de esta circunstancia.

c) Si la víctima resulta con lesiones graves o muere durante o como consecuencia de dicho acto.

d) El agente se vale de menores de edad." (*)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

CONCORDANCIAS:     R.Adm. Nº 185-2001-P-CSJLI-PJ

Ley N° 9024, Art. 136
D.S. Nº 015-2003-JUS, Art. 210.5 (Aprueban el Reglamento del Código de Ejecución Penal)

Ley Nº 30077, Arts. 3 (Delitos comprendidos) y 24 (Prohibición de beneficios penitenciarios)

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA
PROCESOS CONSTITUCIONALES

Artículo 201 Chantaje
~~~~~~~~~~~~~~~~~~~~~

El que, haciendo saber a otro que se dispone a publicar, denunciar o revelar un hecho o conducta cuya divulgación puede perjudicarlo personalmente o a un tercero con quien esté estrechamente vinculado, trata de determinarlo o lo determina a comprar su silencio, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años y con ciento ochenta a trescientos sesenticinco días-multa.

CAPITULO  VIII USURPACION
#########################

Artículo 202 Usurpación
~~~~~~~~~~~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años:

1. El que, para apropiarse de todo o parte de un inmueble, destruye o altera los linderos del mismo.

2. El que, por violencia, amenaza, engaño o abuso de confianza, despoja a otro, total o parcialmente, de la posesión o tenencia de un inmueble o del ejercicio de un derecho real.

3. El que, con violencia o amenaza, turba la posesión de un inmueble.(*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 30076, publicada el 19 agosto 2013, cuyo texto es el siguiente:

"Artículo 202. Usurpación

Será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años:

1. El que, para apropiarse de todo o en parte de un inmueble, destruye o altera los linderos del mismo.

2. El que, con violencia, amenaza, engaño o abuso de confianza, despoja a otro, total o parcialmente, de la posesión o tenencia de un inmueble o del ejercicio de un derecho real.

3. El que, con violencia o amenaza, turba la posesión de un inmueble.

4. El que, ilegítimamente, ingresa a un inmueble, mediante actos ocultos, en ausencia del poseedor o con precauciones para asegurarse el desconocimiento de quienes tengan derecho a oponerse.

La violencia a la que se hace referencia en los numerales 2 y 3 se ejerce tanto sobre las personas como sobre los bienes."

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 203 Desvío ilegal del curso de las aguas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, con el fin de obtener para sí o para otro un provecho ilícito con perjuicio de tercero, desvía el curso de las aguas públicas o privadas, impide que corran por su cauce o las utiliza en una cantidad mayor de la debida, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años  (*) RECTIFICADO POR FE DE ERRATAS.

Artículo 204 Formas agravadas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La pena será privativa de libertad no menor de dos ni mayor de seis años cuando:

1. La usurpación se realiza usando armas de fuego, explosivos o cualquier otro instrumento o sustancia peligrosos.

2. Intervienen dos o más personas.

3. El inmueble está reservado para fines habitacionales.

4. Se trata de bienes del Estado o destinados a servicios públicos o de  comunidades campesinas o nativas(*) (*) RECTIFICADO POR FE DE ERRATAS.

(*) Artículo modificado por el Artículo 1 de la Ley Nº 30076, publicada el 19 agosto 2013, cuyo texto es el siguiente:

"Artículo 204. Formas agravadas de usurpación

La pena privativa de libertad será no menor de cuatro ni mayor de ocho años e inhabilitación, según corresponda, cuando la usurpación se comete:

1. Usando armas de fuego, explosivos o cualquier otro instrumento o sustancia peligrosos.

2. Con la intervención de dos o más personas.

3. Sobre inmueble reservado para fines habitacionales.

4. Sobre bienes del Estado o de comunidades campesinas o nativas, o sobre bienes destinados a servicios públicos o inmuebles que integran el patrimonio cultural de la Nación declarados por la entidad competente.

5. Afectando la libre circulación en vías de comunicación.

6. Colocando hitos, cercos perimétricos, cercos vivos, paneles o anuncios, demarcaciones para lotizado, instalación de esteras, plásticos u otros materiales.

7. Abusando de su condición o cargo de funcionario o servidor público.

Será reprimido con la misma pena el que organice, financie, facilite, fomente, dirija, provoque o promueva la realización de usurpaciones de inmuebles de propiedad pública o privada." (*)

(*) Artículo modificado por la Cuarta Disposición Complementaria Transitoria de la Ley N° 30327, publicada el 21 mayo 2015, cuyo texto es el siguiente:

“Artículo 204.- Formas agravadas de usurpación

La pena privativa de libertad será no menor de cuatro ni mayor de ocho años e inhabilitación, según corresponda, cuando la usurpación se comete:

1. Usando armas de fuego, explosivos o cualquier otro instrumento o sustancia peligrosos.

2. Con la intervención de dos o más personas.

3. Sobre inmueble reservado para fines habitacionales.

4. Sobre bienes del Estado o de comunidades campesinas o nativas, o sobre bienes destinados a servicios públicos o inmuebles que integran el patrimonio cultural de la Nación, declarados por la entidad competente.

5. Afectando la libre circulación en vías de comunicación.

6. Colocando hitos, cercos perimétricos, cercos vivos, paneles o anuncios, demarcaciones para lotizado, instalación de esteras, plásticos u otros materiales.

7. Abusando de su condición o cargo de funcionario o servidor público.

8. Sobre derechos de vía o localización de área otorgados para proyectos de inversión.

Será reprimido con la misma pena el que organice, financie, facilite, fomente, dirija, provoque o promueva la realización de usurpaciones de inmuebles de propiedad pública o privada”. (*)

(*) Artículo modificado por la Primera Disposición Complementaria Modificatoria del Decreto Legislativo N° 1187, publicado el 16 agosto 2015, cuyo texto es el siguiente:

“Artículo 204. Formas agravadas de usurpación

La pena privativa de libertad será no menor de cinco ni mayor de doce años e inhabilitación según corresponda, cuando la usurpación se comete:

1. Usando armas de fuego, explosivos o cualquier otro instrumento o sustancia peligrosos.

2. Con la intervención de dos o más personas.

3. Sobre inmueble reservado para fines habitacionales.

4. Sobre bienes del Estado o de comunidades campesinas o nativas, o sobre bienes destinados a servicios públicos o inmuebles, que integran el patrimonio cultural de la Nación declarados por la entidad competente, o sobre las Áreas Naturales Protegidas por el Estado.

5. Afectando la libre circulación en vías de comunicación.

6. Colocando hitos, cercos perimétricos, cercos vivos, paneles o anuncios, demarcaciones para lotizado, instalación de esteras, plásticos u otros materiales.

7. Abusando de su condición o cargo de funcionario, servidor público, de la función notarial o arbitral.

8. Sobre derechos de vía o localización de área otorgados para proyectos de inversión.

9. Utilizando documentos privados falsos o adulterados.

10. En su condición de representante de una asociación u otro tipo de organización, representante de persona jurídica o cualquier persona natural, que entregue o acredite indebidamente documentos o valide actos de posesión de terrenos del Estado o de particulares.

Será reprimido con la misma pena el que organice, financie, facilite, fomente, dirija, provoque o promueva la realización de usurpaciones de inmuebles de propiedad pública o privada." (*)

(*) Artículo modificado por la Tercera Disposición Complementaria Modificatoria de la Ley N° 30556, publicada el 29 abril 2017, cuyo texto es el siguiente:

“Artículo 204. Formas agravadas de usurpación

La pena privativa de libertad será no menor de cinco ni mayor de doce años e inhabilitación según corresponda, cuando la usurpación se comete:

1. Usando armas de fuego, explosivos o cualquier otro instrumento o sustancia peligrosos.

2. Con la intervención de dos o más personas.

3. Sobre inmueble reservado para fines habitacionales.

4. Sobre bienes del Estado o de comunidades campesinas o nativas, o sobre bienes destinados a servicios públicos o inmuebles, que integran el patrimonio cultural de la nación declarados por la entidad competente, o sobre las áreas naturales protegidas por el Estado.

5. Afectando la libre circulación en vías de comunicación.

6. Colocando hitos, cercos perimétricos, cercos vivos, paneles o anuncios, demarcaciones para lotizado, instalación de esteras, plásticos u otros materiales.

7. Abusando de su condición o cargo de funcionario, servidor público, de la función notarial o arbitral.

PROCESOS CONSTITUCIONALES

8. Sobre derechos de vía o localización de área otorgados para proyectos de inversión.

9. Utilizando documentos privados falsos o adulterados.

10. En su condición de representante de una asociación u otro tipo de organización, representante de persona jurídica o cualquier persona natural, que entregue o acredite indebidamente documentos o valide actos de posesión de terrenos del Estado o de particulares.

11. Sobre inmuebles en zonas declaradas de riesgo no mitigable.

Será reprimido con la misma pena el que organice, financie, facilite, fomente, dirija, provoque o promueva la realización de usurpaciones de inmuebles de propiedad pública o privada”.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

CAPITULO  IX DAÑOS
##################

Artículo 205 Daño simple 
~~~~~~~~~~~~~~~~~~~~~~~~~

El que daña, destruye o inutiliza un bien, mueble o inmueble, total o parcialmente ajeno, será reprimido con pena privativa de libertad no mayor de dos años y con treinta a sesenta días-multa.(*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 30076, publicada el 19 agosto 2013, cuyo texto es el siguiente:

"Artículo 205. Daño simple

El que daña, destruye o inutiliza un bien, mueble o inmueble, total o parcialmente ajeno, será reprimido con pena privativa de libertad no mayor de tres años y con treinta a sesenta días-multa."

Artículo 206 Formas agravadas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La pena para el delito previsto en el artículo 205 será privativa de libertad no menor de uno ni mayor de seis años cuando:

1. Es ejecutado en bienes de valor científico, artístico, histórico o cultural, siempre que por el lugar en que se encuentren estén librados a la confianza pública o destinados al servicio, a la utilidad o a la reverencia de un número indeterminado de personas.

2. Recae sobre medios o vías de comunicación, diques o canales o instalaciones destinadas al servicio público.

3. La acción es ejecutada empleando violencia o amenaza contra las personas.

4. Causa destrucción de plantaciones o muerte de animales.

5. Es efectuado en bienes cuya entrega haya sido ordenada judicialmente.

"6. Recae sobre infraestructura o instalaciones de transporte de uso público, de sus equipos o elementos de seguridad, o de prestación de servicios públicos de saneamiento, electricidad, gas o telecomunicaciones." (*)

(*) Inciso incorporado por el Artículo Único de la Ley N° 29583, publicada el 18 septiembre 2010.

(*) Numeral modificado por el Artículo 2 del Decreto Legislativo N° 1245, publicado el 06 noviembre 2016, cuyo texto es el siguiente:

"6. Recae sobre infraestructura o instalaciones de transporte de uso público, de sus equipos o elementos de seguridad, o de prestación de servicios públicos de saneamiento, electricidad o telecomunicaciones."

"7. Si la conducta recae sobre la infraestructura o instalaciones públicas o privadas para la exploración, explotación, procesamiento, refinación, almacenamiento, transporte, distribución, comercialización o abastecimiento de gas, de hidrocarburos o de sus productos derivados conforme a la legislación de la materia.”(*)

(*) Numeral incorporado por el Artículo 2 del Decreto Legislativo N° 1245, publicado el 06 noviembre 2016.

“Artículo 206-A. Abandono y actos de crueldad contra animales domésticos y silvestres

El que comete actos de crueldad contra un animal doméstico o un animal silvestre, o los abandona, es reprimido con pena privativa de libertad no mayor de tres años, con cien a ciento ochenta días-multa y con inhabilitación de conformidad con el numeral 13 del artículo 36.

Si como consecuencia de estos actos de crueldad o del abandono el animal doméstico o silvestre muere, la pena es privativa de libertad no menor de tres ni mayor de cinco años, con ciento cincuenta a trescientos sesenta díasmulta y con inhabilitación de conformidad con el numeral 13 del artículo 36”.(*)

(*) Artículo incorporado por la Segunda Disposición Complementaria Modificatoria de la Ley N° 30407, publicada el 08 enero 2016.

Artículo 207 Producción o venta de alimentos en mal estado para los animales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que produce o vende alimentos, preservantes, aditivos y mezclas para consumo animal, falsificados, corrompidos o dañados, cuyo consumo genere peligro para la vida, la salud o la integridad física de los animales, será reprimido con pena privativa de libertad no mayor de un año y con treinta a cien días-multa.

"CAPÍTULO X
DELITOS INFORMÁTICOS

Artículo 207-A Delito Informático
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que utiliza o ingresa indebidamente a una base de datos, sistema o red de computadoras o cualquier parte de la misma, para diseñar, ejecutar o alterar un esquema u otro similar, o para interferir, interceptar, acceder o copiar información en tránsito o contenida en una base de datos, será reprimido con pena privativa de libertad no mayor de dos años o con prestación de servicios comunitarios de cincuentidós a ciento cuatro jornadas.

Si el agente actuó con el fin de obtener un beneficio económico, será reprimido con pena privativa de libertad no mayor de tres años o con prestación de servicios comunitarios no menor de ciento cuatro jornadas.(*)

(*) Artículo derogado por la Única Disposición Complementaria Derogatoria de la Ley N° 30096, publicada el 22 octubre 2013.

Artículo 207-B Alteración, daño y destrucción de base de datos, sistema, red o programa de computadoras
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que utiliza, ingresa o interfiere indebidamente una base de datos, sistema, red o programa de computadoras o cualquier parte de la misma con el fin de alterarlos, dañarlos o destruirlos, será reprimido con pena privativa de libertad no menor de tres ni mayor de cinco años y con setenta a noventa días multa.(*)

(*) Artículo derogado por la Única Disposición Complementaria Derogatoria de la Ley N° 30096, publicada el 22 octubre 2013.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

Artículo 207-C Delito informático agravado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En los casos de los Artículos 207-A y 207-B, la pena será privativa de libertad no menor de cinco ni mayor de siete años, cuando:

1. El agente accede a una base de datos, sistema o red de computadora, haciendo uso de información privilegiada, obtenida en función a su cargo.

2. El agente pone en peligro la seguridad nacional." (*)(**)

(*) Capítulo X) incorporado por el Artículo Unico de la Ley Nº 27309, publicada el 17 julio 2000.

(**) Artículo 207-C) derogado por la Única Disposición Complementaria Derogatoria de la Ley N° 30096, publicada el 22 octubre 2013.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

"Artículo 207-D. Tráfico ilegal de datos

El que, crea, ingresa o utiliza indebidamente una base de datos sobre una persona natural o jurídica, identificada o identificable, para comercializar, traficar, vender, promover, favorecer o facilitar información relativa a cualquier ámbito de la esfera personal, familiar, patrimonial, laboral, financiera u otro de naturaleza análoga, creando o no perjuicio, será reprimido con pena privativa de libertad no menor de tres ni mayor de cinco años."(*)(**)

(*) Artículo incorporado por el Artículo 2 de la Ley Nº 30076, publicada el 19 agosto 2013.

(**) Artículo derogado por la Única Disposición Complementaria Derogatoria de la Ley N° 30096, publicada el 22 octubre 2013.

"CAPITULO  XI
DISPOSICION COMUN

(*) Capítulo incorporado por el Artículo Unico de la Ley Nº 27309, publicada el 17 julio 2000.

Artículo 208 Excusa absolutoria. Exención de Pena
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

No son reprimibles, sin perjuicio de la reparación civil, los hurtos, apropiaciones, defraudaciones o daños que se causen:

1. Los cónyuges, concubinos, ascendientes, descendientes y afines en línea recta.

2. El consorte viudo, respecto de los bienes de su difunto cónyuge, mientras no hayan pasado a poder de tercero.

3. Los hermanos y cuñados, si viviesen juntos."(*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1323, publicado el 06 enero 2017, cuyo texto es el siguiente:

“Artículo 208.- Excusa absolutoria. Exención de Pena

No son reprimibles, sin perjuicio de la reparación civil, los hurtos, apropiaciones, defraudaciones o daños que se causen:

1. Los cónyuges, concubinos, ascendientes, descendientes y afines en línea recta.

2. El consorte viudo, respecto de los bienes de su difunto cónyuge, mientras no hayan pasado a poder de tercero.

3. Los hermanos y cuñados, si viviesen juntos.

La excusa absolutoria no se aplica cuando el delito se comete en contextos de violencia contra las mujeres o integrantes del grupo familiar.”

(*) Capítulo incorporado por el Artículo Unico de la Ley Nº 27309, publicada el 17 julio 2000.

TITULO VI DELITOS CONTRA LA CONFIANZA Y LA BUENA FE EN LOS NEGOCIOS
===================================================================

CONCORDANCIAS:     R.A.N° 203-2016-CE-PJ (Precisan delitos de mercado a los que se refiere la Res. Adm. Nº 152-2016-CE-PJ que dispuso la creación de órganos jurisdiccionales especializados en delitos aduaneros, tributarios, de mercado y ambientales en
diversos distritos judiciales del país)

CAPITULO I QUIEBRA (*)
######################

(*) Denominación modificada por la Octava Disposición Final de la Ley Nº 27146, publicada el 24 junio 1999, cuyo texto es el siguiente:

"CAPITULO I
ATENTADOS CONTRA EL SISTEMA CREDITICIO"

Artículo 209 
~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años e inhabilitación de uno a tres años conforme al artículo 36, incisos 2 y 4, el comerciante declarado en quiebra que, en fraude de sus acreedores, incurre en alguno de los hechos siguientes:

1. Simula o supone deudas, enajenaciones, gastos o pérdidas.

2. Sustrae u oculta bienes que correspondan a la masa o no justifica su salida o existencia.

3. Concede ventajas indebidas a cualquier acreedor. (*)

(*) Artículo modificado por la Décimo Primera Disposición Final del Decreto Legislativo Nº 861, publicada el 22 octubre 1996, cuyo texto es el siguiente:

"Artículo 209.- Será reprimido con pena privativa de libertad no menor de tres (3) ni mayor de seis (6) años e inhabilitación de uno a tres años conforme al artículo 36, incisos 2 y 4, el comerciante declarado en quiebra que, en fraude a sus acreedores:

1. Simule, suponga o contraiga efectivamente deudas, enajenaciones, gastos o pérdidas.

2. Sustraiga u oculte bienes que correspondan a la masa o no justifique su salida o existencia.

3. Conceda ventajas indebidas a cualquier acreedor.

Si el acto hubiese sido cometido dentro de un proceso de titulización la pena será privativa de la libertad no menor de cinco (5) ni mayor de ocho (8) años e inhabilitación de tres (3) a cinco (5) años conforme al artículo 36, incisos 2 y 4." (*)

(*) Artículo modificado por la Octava Disposición Final de la Ley Nº 27146, publicada el 24 junio 1999, cuyo texto es el siguiente:

"Artículo 209.- Será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años e inhabilitación de tres a cinco años conforme al Artículo 36 incisos 2) y 4), el deudor, la persona que actúa en su nombre, el administrador o el liquidador, que en un procedimiento de insolvencia, procedimiento simplificado o concurso preventivo, realizara, en perjuicio de los acreedores, alguna de las siguientes conductas:

1. Ocultamiento de bienes.

2. Simulación, adquisición o realización de deudas, enajenaciones, gastos o pérdidas.

3. Realización de actos de disposición patrimonial o generador de obligaciones, destinados a pagar a uno o varios acreedores, preferentes o no, posponiendo el pago del resto de acreedores. Si ha existido connivencia con el acreedor beneficiado, éste o la persona que haya actuado en su nombre, será reprimido con la misma pena.

Si la Junta de Acreedores hubiere aprobado la reprogramación de obligaciones en un proceso de insolvencia, concurso preventivo o procedimiento simplificado, según el caso o, el convenio de liquidación o convenio concursal, las conductas tipificadas en el inciso 3) sólo serán sancionadas si contravienen dicha reprogramación o convenio. Asimismo, si fuera el caso de una liquidación declarada por la Comisión, conforme a lo señalado en la ley de la materia, las conductas tipificadas en el inciso 3) sólo serán sancionadas si contravienen el desarrollo de dicha liquidación.

Si el agente realiza alguna de las conductas descritas en los incisos 1), 2) y 3), cuando se encontrare suspendida la exigibilidad le las obligaciones del deudor, como consecuencia de una declaración de insolvencia, procedimiento simplificado o concurso preventivo, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años e inhabilitación de cuatro a cinco años conforme al Artículo 36 incisos 2) y 4)." (1)(2)

(1) De conformidad con la Novena Disposición Final de la Ley N° 27146, publicada el 24 junio 1999, antes de ejercer la acción penal en lo relacionado con la materia de reestructuración patrimonial, el Fiscal deberá solicitar el informe técnico del INDECOPI, el cual deberá emitirlo en el término de 5 (cinco) días hábiles. Dicho informe deberá ser valorado por los órganos competentes del Ministerio Público y del Poder Judicial en la fundamentación de los dictámenes o resoluciones respectivas; la misma que ha sido recogida por el Decreto Supremo N° 014-99-ITINCI, Texto Unico Ordenado de la Ley de Reestructuración Patrimonial, publicado el 01-11-99.

(2) Artículo modificado por la Primera Disposición Transitoria y Final de la Ley Nº 27295, publicada el 29 junio 2000, cuyo texto es el siguiente:

Actos Ilícitos

"Artículo 209.- Será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años e inhabilitación de tres a cinco años conforme al Artículo 36 incisos 2) y 4), el deudor, la persona que actúa en su nombre, el administrador o el liquidador, que en un procedimiento de insolvencia, procedimiento simplificado, concurso preventivo, procedimiento transitorio u otro procedimiento de reprogramación de obligaciones cualesquiera fuera su denominación, realizara, en perjuicio de los acreedores, alguna de las siguientes conductas:

1. Ocultamiento de bienes;

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

2. Simulación, adquisición o realización de deudas, enajenaciones, gastos o pérdidas; y,

3. Realización de actos de disposición patrimonial o generador de obligaciones, destinados a pagar a uno o varios acreedores, preferentes o no, posponiendo el pago del resto de acreedores. Si ha existido connivencia con el acreedor beneficiado, éste o la persona que haya actuado en su nombre, será reprimido con la misma pena.

Si la Junta de Acreedores hubiere aprobado la reprogramación de obligaciones en un procedimiento de insolvencia, procedimiento simplificado, concurso preventivo, procedimiento transitorio u otro procedimiento de reprogramación de obligaciones cualesquiera fuera su denominación, según el caso o, el convenio de liquidación o convenio concursal, las conductas tipificadas en el inciso 3) sólo serán sancionadas si contravienen dicha reprogramación o convenio. Asimismo, si fuera el caos de una liquidación declarada por la Comisión, conforme a lo señalado en la ley de la materia, las conductas tipificadas en el inciso 3) sólo serán sancionadas si contravienen el desarrollo de dicha liquidación.

Si el agente realiza alguna de las conductas descritas en los incisos 1), 2) ó 3) cuando se encontrare suspendida la exigibilidad de obligaciones del deudor, como consecuencia de un procedimiento de insolvencia, procedimiento simplificado, concurso preventivo, procedimiento transitorio u otro procedimiento de reprogramación de obligaciones cualesquiera fuera su denominación, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años e inhabilitación de cuatro a cinco años, conforme al Artículo 36 incisos 2) y 4)."

Artículo 210 
~~~~~~~~~~~~~

El comerciante que causa su propia quiebra perjudicando a sus acreedores por sus gastos excesivos en relación al capital o por cualquier otro acto de negligencia o imprudencia manifiesta, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años e inhabilitación de uno a dos años conforme el artículo 36, incisos 2 y 4.(*)

(*) Artículo modificado por la Décimo Primera Disposición Final del Decreto Legislativo Nº 861, publicada el 22 octubre 1996, cuyo texto es el siguiente:

"Artículo 210.- El comerciante que causa su propia quiebra perjudicando a sus acreedores por sus gastos excesivos e innecesarios en relación al capital o por cualquier acto de negligencia o imprudencia manifiesta, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres (3) años e inhabilitación de uno a dos (2) años conforme al artículo 36, incisos 2 y 4.

Si el acto hubiese sido cometido dentro de un proceso de titulización la pena  privativa de libertad será no menor de tres (3) ni mayor de cinco (5) años e inhabilitación de dos (2) a cuatro (4) años conforme al artículo 36, incisos 2 y 4." (*)

(*) Artículo modificado por la Octava Disposición Final de la Ley Nº 27146, publicada el 24 junio 1999, cuyo texto es el siguiente:

Comisión de delito por culpa del agente

"Artículo 210.- Si el agente realiza por culpa alguna de las conductas descritas en el Artículo 209, los límites máximo y mínimo de las penas privativas de libertad e inhabilitación se reducirán en una mitad." (*)

(*) De conformidad con la Novena Disposición Final de la Ley N° 27146, publicada el 24 junio 1999, antes de ejercer la acción penal en lo relacionado con la materia de reestructuración patrimonial, el Fiscal deberá solicitar el informe técnico del INDECOPI, el cual deberá emitirlo en el término de 5 (cinco) días hábiles. Dicho informe deberá ser valorado por los órganos competentes del Ministerio Público y del Poder Judicial en la fundamentación de los dictámenes o resoluciones respectivas; la misma que ha sido recogida por el Decreto Supremo N° 014-99-ITINCI, Texto Unico Ordenado de la Ley de Reestructuración Patrimonial, publicado el 01-11-99.

Artículo 211 
~~~~~~~~~~~~~

El director, administrador, fiscalizador, gerente o liquidador de persona jurídica declarada en quiebra o en estado de liquidación, según la ley que rige su funcionamiento, que comete alguno de los hechos previstos en los artículos 209 y 210, será reprimido con la pena indicada, según el delito de que se trate.(*)

(*) Artículo modificado por la Décimo Primera Disposición Final del Decreto Legislativo Nº 861, publicada el 22 octubre 1996, cuyo texto es el siguiente:

"Artículo 211.- El director, administrador, fiscalizador, gerente o liquidador de persona jurídica o patrimonio fideicometido declarado en quiebra o en estado de liquidación, según la ley que rige su funcionamiento, que comete alguno de los hechos previstos en los artículos 209 y 210, será reprimido con la pena indicada, según el delito de que se trate." (*)

(*) Artículo modificado por la Octava Disposición Final de la Ley Nº 27146, publicada el 24 junio 1999, cuyo texto es el siguiente:

"Artículo 211.- El que en un procedimiento de insolvencia, procedimiento simplificado o concurso preventivo, lograre la suspensión de la exigibilidad de las obligaciones del deudor, mediante el uso de información, documentación o contabilidad falsas o la simulación de obligaciones o pasivos, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años e inhabilitación de cuatro a cinco años conforme al Artículo 36 incisos 2) y 4)." (1)(2)

(1) De conformidad con la Novena Disposición Final de la Ley N° 27146, publicada el 24 junio 1999, antes de ejercer la acción penal en lo relacionado con la materia de reestructuración patrimonial, el Fiscal deberá solicitar el informe técnico del INDECOPI, el cual deberá emitirlo en el término de 5 (cinco) días hábiles.  Dicho informe deberá ser valorado por los órganos competentes del Ministerio Público y del Poder Judicial en la fundamentación de los dictámenes o resoluciones respectivas; la misma que ha sido recogida por el Decreto Supremo N° 014-99-ITINCI, Texto Unico Ordenado de la Ley de Reestructuración Patrimonial, publicado el 01-11-99.

(2) Artículo modificado por la Primera Disposición Transitoria y Final de la Ley Nº 27295, publicada el 29 junio 2000, cuyo texto es el siguiente:

Suspensión ilícita de la exigibilidad de las obligaciones del deudor

"Artículo 211.- El que en un procedimiento de insolvencia, procedimiento simplificado, concurso preventivo, procedimiento transitorio u otro procedimiento de reprogramación de obligaciones cualesquiera fuera su denominación, lograre la suspensión de la exigibilidad de las obligaciones del deudor, mediante el uso de información, documentación o contabilidad falsas o la simulación de obligaciones o pasivos, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años e inhabilitación de cuatro a cinco años, conforme al Artículo 36 incisos 2) y 4)."

Artículo 212 
~~~~~~~~~~~~~

El deudor no comerciante declarado en quiebra que, para defraudar a su acreedor, cometa alguno de los hechos mencionados en el artículo 209, será reprimido con pena privativa de libertad no menor de dos, ni mayor de cuatro años. (*)

(*) Artículo modificado por la Décimo Primera Disposición Final del Decreto Legislativo Nº 861, publicada el 22 octubre 1996, cuyo texto es el siguiente:

"Artículo 212.- El deudor no comerciante declarado en quiebra que, para defraudar a su acreedor, comete alguno de los hechos mencionados en el artículo 209, será reprimido con pena privativa de libertad no menor de dos (2), ni mayor de cuatro (4) años.

Si el acto hubiese sido cometido dentro de un proceso de titulización la pena privativa de libertad será no menor de cuatro (4) ni mayor a ocho (8) años." (*)

(*) Artículo modificado por la Octava Disposición Final de la Ley Nº 27146, publicada el 24 junio 1999, cuyo texto es el siguiente:

Beneficios por colaboración

"Artículo 212.- Podrá reducirse la pena hasta por debajo del mínimo legal en el caso de autores y eximirse de pena al partícipe que, encontrándose incurso en una investigación a cargo del Ministerio Público o en el desarrollo de un proceso penal por cualquiera de los delitos sancionados en este Capítulo, proporcione información eficaz que permita:

1. Evitar la continuidad o consumación del delito.

2. Conocer las circunstancias en las que se cometió el delito e identificar a los autores y partícipes.

3. Conocer el paradero o destino de los bienes objeto material del delito y su restitución al patrimonio del deudor. En tales casos los bienes serán destinados al pago de las obligaciones del deudor según la ley de la materia.

La pena del autor se reducirá en dos tercios respecto del máximo legal y el partícipe quedará exento de pena si, durante la investigación a cargo del Ministerio Público o en el desarrollo del proceso penal en el que estuvieran incursos, restituye voluntariamente los bienes o entrega una suma equivalente a su valor, los mismos que serán destinados al pago de sus obligaciones según la ley de la materia. La reducción o exención de pena sólo se aplicará a quien o quienes realicen la restitución o entrega del valor señalado."

Artículo 213 
~~~~~~~~~~~~~

El acreedor que, en convivencia con el deudor o un tercero, celebra convenio o transacción por el cual estipulan ventajas en perjuicio de otro acreedor, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años.

El representante de una persona jurídica que, en estado de quiebra consiente un convenio o transacción de este género, será reprimido con la misma pena. (*)

(*) Artículo modificado por la Octava Disposición Final de la Ley Nº 27146, publicada el 24 junio 1999, cuyo texto es el siguiente:

Ejercicio de la acción penal e intervención del INDECOPI

"Artículo 213.- En los delitos previstos en este Capitulo sólo se procederá por acción privada ante el Ministerio Público. El Instituto Nacional de Defensa de la Competencia y Protección de la Propiedad Intelectual (INDECOPI), a través de sus órganos correspondientes, podrá denunciar el hecho en defecto del ejercicio de la acción privada y en todo caso podrá intervenir como parte interesada en el proceso penal que se instaure."

Manejo ilegal de patrimonio de propósito exclusivo

"Artículo 213-A.- El factor fiduciario o quien ejerza el dominio fiduciario sobre un patrimonio fideicometido, o el director, gerente o quien ejerza la administración de una sociedad de propósito especial que, en beneficio propio o de terceros, efectúe actos de enajenación, gravamen, adquisición u otros en contravención del fin para el que fue constituído el patrimonio de propósito exclusivo, será reprimido con pena privativa de libertad no menor de dos (2), ni mayor de cuatro (4) años e inhabilitación de uno a dos (2) años conforme al Artículo 36, incisos 2) y 4)." (*)

(*) Artículo añadido por la Décimo Segunda Disposición Final del Decreto Legislativo Nº 861, publicado el 22 octubre 1996.

CAPITULO II USURA
#################

Artículo 214 Usura
~~~~~~~~~~~~~~~~~~

El que, con el fin de obtener una ventaja patrimonial, para sí o para otro, en la concesión de un crédito o en su otorgamiento, renovación, descuento o prórroga del plazo de pago, obliga o hace prometer pagar un interés superior al límite fijado por la ley, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años y con veinte a treinta días-multa.

Si el agraviado es persona incapaz o se halla en estado de necesidad, la pena privativa de libertad será no menor de dos ni mayor de cuatro años.

PROCESOS CONSTITUCIONALES

CAPITULO III LIBRAMIENTO Y COBRO INDEBIDO
#########################################

Artículo 215 
~~~~~~~~~~~~~

Será reprimido con pena privativa de la libertad no menor de uno ni (*) RECTIFICADO POR FE DE ERRATAS mayor de cuatro años, el que gira un cheque, cuando:

1) No tenga provisión de fondos o autorización para sobregirarse.

2) Frustra maliciosamente el pago.

3) Hace giro en talonario adjunto.

4) Gira a sabiendas que al tiempo de su presentación no podrá ser pagado legalmente.

El que endosa el documento a sabiendas que no tiene provisión de fondos, será reprimido con la misma pena.

En los casos de los incisos 1, 2 y 4 el agente debe ser informado de la falta de pago mediante protesto u otra forma documentada de requerimiento.

No procede la acción penal, si el agente abona el importe del documento dentro del tercer día hábil a la fecha de requerimiento. (*)

(*) Artículo modificado por la Cuarta Disposición Modificatoria de la Ley Nº 27287- Ley de Títulos Valores, publicada el 19 junio 2000, la misma que entró en vigencia a partir de los 120 días siguientes desde su publicación en el Diario Oficial El Peruano, cuyo texto es el siguiente:

Modalidades de libramientos indebidos

"Artículo 215.- Será reprimido con pena privativa de la libertad no menor de uno ni mayor de cinco años, el que gire, transfiera o cobre un Cheque, en los siguientes casos:

1) Cuando gire sin tener provisión de fondos suficientes o autorización para sobregirar la cuenta corriente;

2) Cuando frustre maliciosamente por cualquier medio su pago;

3) Cuando gire a sabiendas que al tiempo de su presentación no podrá ser pagado legalmente;

4) Cuando revoque el cheque durante su plazo legal de presentación a cobro, por causa falsa;

5) Cuando utilice cualquier medio para suplantar al beneficiario o al endosatario, sea en su identidad o firmas; o modifique sus cláusulas, líneas de cruzamiento, o cualquier otro requisito formal del Cheque;

6) Cuando lo endose a sabiendas que no tiene provisión de fondos.

En los casos de los incisos 1) y 6) se requiere del protesto o de la constancia expresa puesta por el banco girado en el mismo documento, señalando el motivo de la falta de pago.

Con excepción del incisos 4) y 5), no procederá la acción penal, si el agente abona el monto total del Cheque dentro del tercer día hábil de la fecha de requerimiento escrito y fehaciente, sea en forma directa, notarial, judicial o por cualquier otro medio  con entrega fehaciente que se curse al girador."

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

TITULO VII DELITOS  CONTRA  LOS DERECHOS  INTELECTUALES
=======================================================

CAPITULO I DELITOS CONTRA LOS DERECHOS DE AUTOR Y CONEXOS
#########################################################

Artículo 216 
~~~~~~~~~~~~~

El que copia, reproduce, exhibe, emite, ejecuta o difunde al público, en todo o en parte, por impresión, grabación, fonograma (*) RECTIFICADO POR FE DE ERRATAS, videograma, fijación u otro medio, una obra o producción literaria, artística, científica o técnica, sin la autorización escrita del autor o productor o titular de los derechos, en su caso, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años. (*)

(*) Artículo modificado por la Tercera Disposición Final del Decreto Legislativo Nº 822, publicado el 24 abril 1996, cuyo texto es el siguiente:

Copia o reproducción no autorizada

"Artículo 216.- Será reprimido con pena privativa  de la libertad de uno a tres años y de diez a sesenta días-multa, a quien estando autorizado para publicar una obra, lo hiciere en una de las formas siguientes:

a. Sin mencionar en los ejemplares el nombre del autor, traductor, adaptador, compilador o arreglador.

b. Estampe el nombre con adiciones o supresiones que afecte la reputación del autor como tal, o en su caso, del traductor, adaptador, compilador o arreglador.

c. Publique la obra con abreviaturas, adiciones, supresiones, o cualquier otra modificación, sin el consentimiento del titular del derecho.

d. Publique separadamente varias obras, cuando la autorización se haya conferido para publicarlas en conjunto; o las publique en conjunto, cuando solamente se le haya autorizado la publicación de ellas en forma separada." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 27729, publicada el 24 mayo 2002, cuyo texto es el siguiente:

Copia o reproducción no autorizada

“Artículo 216.- Será reprimido con pena privativa de la libertad no menor de dos ni mayor de cuatro años y de diez a sesenta días-multa, a quien estando autorizado para publicar una obra, lo hiciere en una de las formas siguientes:

a. Sin mencionar en los ejemplares el nombre del autor, traductor, adaptador, compilador o arreglador.

b. Estampe el nombre con adiciones o supresiones que afecte la reputación del autor como tal, o en su caso, del traductor, adaptador, compilador o arreglador.

c. Publique la obra con abreviaturas, adiciones, supresiones, o cualquier otra modificación, sin el consentimiento del titular del derecho.

d. Publique separadamente varias obras, cuando la autorización se haya conferido para publicarlas en conjunto; o las publique en conjunto, cuando solamente se le haya autorizado la publicación de ellas en forma separada.”

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 217 
~~~~~~~~~~~~~

La pena privativa de libertad no menor de dos ni mayor de cuatro años, cuando:

1. La copia, reproducción, exhibición, emisión, difusión o utilización de la obra o producción intelectual se hace con fines de comercialización.

2. La copia, reproducción, exhibición, emisión, difusión de la obra o producción intelectual se hace suprimiendo o alterando el nombre o seudónimo del autor o productor o titular de los derechos, en su caso, nombre, denominación, sello o distintivo de autenticidad de la obra o producción intelectual. (*)

(*) Artículo modificado por la Tercera Disposición Final del Decreto Legislativo Nº 822, publicado el 24 abril 1996, cuyo texto es el siguiente:

Difusión, distribución y circulación de la obra sin la autorización del autor

"Artículo 217.- Será reprimido con pena privativa de libertad no menor de dos ni mayor de seis años y con treinta a noventa días-multa, el que con respecto a una obra, una interpretación o ejecución artística, un fonograma, o una emisión o transmisión de radiodifusión, o una grabación audiovisual o una imagen fotográfica expresada en cualquier forma, realiza alguno de los siguientes actos, sin la autorización previa y escrita del autor o titular de los derechos:

a.  La modifique total  o parcialmente.

b.  La reproduzca total o parcialmente, por cualquier medio o procedimiento.

c.  La distribuya mediante venta, alquiler o préstamo público.

d.  La comunique o difunda públicamente por cualquiera de los medios o procedimientos reservados al titular del respectivo derecho.

e.  La reproduzca, distribuya o comunique en mayor número que el autorizado por escrito." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28289, publicada el 20 julio 2004, cuyo texto es el siguiente:

“Artículo 217.- Reproducción, difusión, distribución y circulación de la obra sin la autorización del autor

Será reprimido con pena privativa de libertad no menor de dos ni mayor de seis años y con treinta a noventa días-multa, el que con respecto a una obra, una interpretación o ejecución artística, un fonograma o una emisión o transmisión de radiodifusión, o una grabación audiovisual o una imagen fotográfica expresada en cualquier forma, realiza alguno de los siguientes actos sin la autorización previa y escrita del autor o titular de los derechos:

a. La modifique total o parcialmente.

b. La distribuya mediante venta, alquiler o préstamo público.

c. La comunique o difunda públicamente por cualquiera de los medios o procedimientos reservados al titular del respectivo derecho. (*)

(*) Inciso modificado por el Artículo 1 de la Ley N° 29263, publicada el 02 octubre 2008, cuyo texto es el siguiente:

"c. La comunique o difunda públicamente, transmita o retransmita por cualquiera de los medios o procedimientos reservados al titular del respectivo derecho."

d. La reproduzca, distribuya o comunique en mayor número que el autorizado por escrito.

La pena será no menor de cuatro años ni mayor de ocho y con sesenta a ciento veinte días multa, cuando el agente la reproduzca total o parcialmente, por cualquier medio o procedimiento y si la distribución se realiza mediante venta, alquiler o préstamo al público u otra forma de transferencia de la posesión del soporte que contiene la obra o producción que supere las dos (2) Unidades Impositivas Tributarias, en forma fraccionada, en un solo acto o en diferentes actos de inferior importe cada uno."

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

Artículo 218 
~~~~~~~~~~~~~

El que, falsamente, se atribuye o atribuye a otro la autoría o titularidad de una obra o producción intelectual, será reprimido con pena privativa de libertad no mayor de dos años y con veinte a treinta días-multa. (*)

(*) Artículo modificado por la Tercera Disposición Final del Decreto Legislativo Nº 822, publicado el 24 abril 1996, cuyo texto es el siguiente:

Artículo 218 Plagio y comercialización de obra
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La pena será privativa de libertad no menor de dos ni mayor de ocho años y sesenta a ciento veinte días-multa cuando:

a.  Se dé a conocer a cualquier persona una obra inédita o no divulgada, que haya recibido en confianza del titular del derecho de autor o de alguien en su nombre, sin el consentimiento del titular.

b.  La reproducción, distribución o comunicación pública, se realiza con fines de comercialización, o alterando o suprimiendo, el nombre o seudónimo del autor, productor o titular de los derechos.

c.  Conociendo el origen ilícito de la copia o reproducción, la distribuya al público, por cualquier medio, la almacene, oculte, introduzca en el país o la saca de éste.

d.  Se fabrique, ensamble, importe, modifique, venda, alquile, ofrezca para la venta o alquiler, o ponga de cualquier otra manera en circulación dispositivos, sistemas, esquemas o equipos capaces de soslayar otro dispositivo destinado a impedir o restringir la realización de copias de obras, o a menoscabar la calidad de las copias realizadas; o capaces de permitir o fomentar la recepción de un programa codificado, radiodifundido o comunicado en otra forma al público, por aquellos que no estén autorizados para ello.

e.  Se inscriba en el Registro del Derecho de Autor la obra, interpretación, producción o emisión ajenas, o cualquier otro tipo de bienes intelectuales, como si fueran propios, o como de persona distinta del verdadero titular de los derechos. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28289, publicada el 20 julio 2004, cuyo texto es el siguiente:

"Artículo 218.- Formas agravadas

La pena será privativa de libertad no menor de cuatro ni mayor de ocho años y con noventa a ciento ochenta días multa cuando:

a. Se dé a conocer al público una obra inédita o no divulgada, que haya recibido en confianza del titular del derecho de autor o de alguien en su nombre, sin el consentimiento del titular.

b. La reproducción, distribución o comunicación pública se realiza con fines de comercialización, o alterando o suprimiendo, el nombre o seudónimo del autor, productor o titular de los derechos. (*)

(*) Inciso modificado por el Artículo 1 de la Ley N° 29263, publicada el 02 octubre 2008, cuyo texto es el siguiente:

"b. La reproducción, distribución o comunicación pública se realiza con fines comerciales u otro tipo de ventaja económica, o alterando o suprimiendo el nombre o seudónimo del autor, productor o titular de los derechos."

c. Conociendo el origen ilícito de la copia o reproducción, la distribuya al público, por cualquier medio, la almacene, oculte, introduzca en el país o la saque de éste.

d. Se fabrique, ensamble, importe, modifique, venda, alquile, ofrezca para la venta o alquiler, o ponga de cualquier otra manera en circulación dispositivos, sistemas, esquemas, o equipos capaces de soslayar otro dispositivo destinado a impedir o restringir la realización de copias de obras o producciones protegidas, o a menoscabar la calidad de las copias realizadas; o capaces de permitir o fomentar la recepción de un programa codificado, radiodifundido o comunicado en otra forma al público, por aquellos que no estén autorizados para ello. (*)

(*) Inciso modificado por el Artículo 1 de la Ley N° 29263, publicada el 02 octubre 2008, cuyo texto es el siguiente:

"d. Se fabrique, ensamble, importe, exporte, modifique, venda, alquile, ofrezca para la venta o alquiler, o ponga de cualquier otra manera en circulación dispositivos, sistemas tangibles o intangibles, esquemas o equipos capaces de soslayar otro dispositivo destinado a impedir o restringir la realización de copias de obras, o a menoscabar la calidad de las copias realizadas, o capaces de permitir o fomentar la recepción de un programa codificado, radiodifundido o comunicado en otra forma al público, por aquellos que no están autorizados para ello."

e. Se inscriba en el Registro del Derecho de Autor la obra, interpretación, producción o emisión ajenas, o cualquier otro tipo de bienes intelectuales, como si fueran propios, o como de persona distinta del verdadero titular de los derechos."

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

Artículo 219 
~~~~~~~~~~~~~

El que edita, reproduce o pone en circulación una obra o producción intelectual en mayor número que el autorizado en forma escrita, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años y con veinte a treinta días-multa. (*)

(*) Artículo modificado por la Tercera Disposición Final del Decreto Legislativo Nº 822, publicado el 24 abril 1996, cuyo texto es el siguiente:

Falsa atribución de autoría de obra

"Artículo 219.- Será reprimido con pena privativa de libertad no menor de dos ni mayor de ocho años y sesenta a ciento ochenta días-multa, el que con respecto a una obra, la difunda como propia, en todo o en parte, copiándola o reproduciéndola textualmente, o tratando de disimular la copia mediante ciertas alteraciones, atribuyéndose o atribuyendo a otro, la autoría o titularidad ajena." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28289, publicada el 20 julio 2004, cuyo texto es el siguiente:

"Artículo 219.- Plagio

Será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años y noventa a ciento ochenta días multa, el que con respecto a una obra, la difunda como propia, en todo o en parte, copiándola o reproduciéndola textualmente, o tratando de disimular la copia mediante ciertas alteraciones, atribuyéndose o atribuyendo a otro, la autoría o titularidad ajena.”

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 220 
~~~~~~~~~~~~~

El que, conociendo el origen ilícito de la copia o reproducción de una obra o producción intelectual, la distribuye al público, oculta, vende, arrienda, o transmite su propiedad o posesión por cualquier otro medio, introduce en el país o la saca de éste, será reprimido con pena privativa de libertad no menor de dos no (*) RECTIFICADO POR FE DE ERRATAS mayor de cuatro años. (*)

(*) Artículo modificado por la Tercera Disposición Final del Decreto Legislativo Nº 822, publicado el 24 abril 1996, cuyo texto es el siguiente:

Formas agravadas

"Artículo 220.- Será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años y noventa  a trescientos sesenticinco días-multa:

a.  Quien se atribuya falsamente la calidad de titular originario o derivado, de cualquiera de los derechos protegidos en la legislación del derecho de autor y derechos conexos y, con esa indebida atribución, obtenga que la autoridad competente suspenda el acto de comunicación, reproducción o distribución de la obra, interpretación, producción, emisión o de cualquier otro de los bienes intelectuales protegidos.

b.  Quien realice actividades propias de una entidad de gestión colectiva de derecho de autor o derechos conexos, sin contar con la autorización debida de la autoridad administrativa competente.

c.  El que presente declaraciones falsas en cuanto certificaciones de ingresos; asistencia de público; repertorio utilizado; identificación de los autores; autorización supuestamente obtenida; número de ejemplares producidos, vendidos o distribuidos gratuitamente o toda otra adulteración de datos susceptible de causar perjuicio a cualquiera de lo titulares del derecho de autor o conexos.

d.  Si el agente que comete el delito integra una organización destinada a perpetrar los ilícitos previstos en el presente capítulo.

e.  Si el agente que comete cualquiera de los delitos previstos en el presente capítulo, posee la calidad de funcionario o servidor público."

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

“Artículo 220-A.- Elusión de medidas tecnológicas

El que, con fines de comercialización u otro tipo de ventaja económica, eluda cualquier medida tecnológica que utilicen los productores de fonogramas, artistas intérpretes o ejecutantes, así como los autores de cualquier obra protegida por derechos de propiedad intelectual, será reprimido con pena privativa de libertad no mayor de dos años y de diez a sesenta días-multa." (1)(2)

(1) Artículo incorporado por el Artículo 2 de la Ley N° 29263, publicada el 02 octubre 2008.

(2) Posteriormente de conformidad con el Artículo 2 de la Ley N° 29316, publicada el 14 enero 2009, se modifica el Artículo 220-A de la Ley N° 29263 mediante la cual se modificó el Código Penal, cuyo texto es el siguiente:

“Artículo 220-A.- Elusión de medida tecnológica efectiva

El que, con fines de comercialización u otro tipo de ventaja económica, eluda sin autorización cualquier medida tecnológica efectiva que utilicen los productores de fonogramas, artistas, intérpretes o ejecutantes, así como los autores de cualquier obra protegida por derechos de propiedad intelectual, será reprimido con pena privativa de libertad no mayor de dos años y de diez a sesenta días multa.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

"Artículo 220-B.- Productos destinados a la elusión de medidas tecnológicas

El que, con fines de comercialización u otro tipo de ventaja económica, fabrique, importe, distribuya, ofrezca al público, proporcione o de cualquier manera comercialice dispositivos, productos o componentes destinados principalmente a eludir una medida tecnológica que utilicen los productores de fonogramas, artistas intérpretes o ejecutantes, así como los autores de cualquier obra protegida por derechos de propiedad intelectual, será reprimido con pena privativa de libertad no mayor de dos años y de diez a sesenta días-multa." (*)

(*) Artículo incorporado por el Artículo 2 de la Ley N° 29263, publicada el 02 octubre 2008.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

"Artículo 220-C.- Servicios destinados a la elusión de medidas tecnológicas

El que, con fines de comercialización u otro tipo de ventaja económica, brinde u ofrezca servicios al público destinados principalmente a eludir una medida tecnológica efectiva que utilicen los productores de fonogramas, artistas intérpretes o ejecutantes, así como los autores de cualquier obra protegida por derechos de propiedad intelectual, será reprimido con pena privativa de libertad no mayor de dos años y de diez a sesenta días-multa." (*)

(*) Artículo incorporado por el Artículo 2 de la Ley N° 29263, publicada el 02 octubre 2008.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

"Artículo 220-D.- Delitos contra la información sobre gestión de derechos

El que, sin autorización y con fines de comercialización u otro tipo de ventaja económica, suprima o altere, por sí o por medio de otro, cualquier información sobre gestión de derechos, será reprimido con pena privativa de libertad no mayor de dos años y de diez a sesenta días-multa.

La misma pena será impuesta al que distribuya o importe para su distribución información sobre gestión de derechos, a sabiendas que esta ha sido suprimida o alterada sin autorización; o distribuya, importe para su distribución, transmita, comunique o ponga a disposición del público copias de las obras, interpretaciones o ejecuciones o fonogramas, a sabiendas que la información sobre gestión de derechos ha sido suprimida o alterada sin autorización." (*)

(*) Artículo incorporado por el Artículo 2 de la Ley N° 29263, publicada el 02 octubre 2008.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

"Artículo 220-E.- Etiquetas, carátulas y empaques

El que fabrique, comercialice, distribuya o almacene con fines comerciales etiquetas o carátulas no auténticas adheridas o diseñadas para ser adheridas a un fonograma, copia de un programa de ordenador, documentación o empaque de un programa de ordenador o a la copia de una obra cinematográfica o cualquier otra obra audiovisual, será reprimido con pena privativa de libertad no menor de tres años ni mayor de seis años y de sesenta a ciento veinte días-multa." (1)(2)

(1) Artículo incorporado por el Artículo 2 de la Ley N° 29263, publicada el 02 octubre 2008.

(2) Posteriormente de conformidad con el Artículo 2 de la Ley N° 29316, publicada el 14 enero 2009, se modifica el Artículo 220-E de la Ley N° 29263 mediante la cual se modificó el Código Penal, cuyo texto es el siguiente:

"Artículo 220-E.- Etiquetas, carátulas o empaques

El que fabrique, comercialice, distribuya, almacene, transporte, transfiera o de otra manera disponga con fines comerciales u otro tipo de ventaja económica etiquetas o carátulas no auténticas adheridas o diseñadas para ser adheridas a un fonograma, copia de un programa de ordenador, documentación o empaque de un programa de ordenador o a la copia de una obra cinematográfica o cualquier otra obra audiovisual, será reprimido con pena privativa de libertad no menor de tres años ni mayor de seis años y de y de sesenta a ciento veinte días multa."

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

"Artículo 220-F.- Manuales y licencias para programas de ordenador

El que elabore, comercialice, distribuya o almacene con fines comerciales manuales o licencias no auténticas para un programa de ordenador será reprimido con pena privativa de libertad no menor de cuatro años ni mayor de seis años y de sesenta a ciento veinte días-multa." (1)(2)

(1) Artículo incorporado por el Artículo 2 de la Ley N° 29263, publicada el 02 octubre 2008.

(2) Posteriormente de conformidad con el Artículo 2 de la Ley N° 29316, publicada el 14 enero 2009, se modifica el Artículo 220-F de la Ley N° 29263 mediante la cual se modificó el Código Penal, cuyo texto es el siguiente:

"Artículo 220-F.- Manuales, licencias u otra documentación, o empaques no auténticos relacionados a programas de ordenador

El que elabore, comercialice, distribuya, almacene, transporte, transfiera o de otra manera disponga con fines comerciales u otro tipo de ventaja económica manuales, licencias u otro tipo de documentación, o empaques no auténticos para un programa de ordenador, será reprimido con pena privativa de libertad no menor de cuatro años ni mayor de seis años y de sesenta a ciento veinte días multa”.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

Artículo 221 
~~~~~~~~~~~~~

En los delitos previstos en este Capítulo se procederá a la incautación previa de los ejemplares ilícitos y, en caso de condena, el decomiso en favor del titular del derecho vulnerado. (*)

(*) Artículo modificado por la Tercera Disposición Final del Decreto Legislativo Nº 822, publicado el 24 abril 1996, cuyo texto es el siguiente:

Incautación o decomiso

"Artículo 221.- En los delitos previstos en este capítulo, se procederá a la incautación previa de los ejemplares ilícitos y de los aparatos o medios utilizados para la comisión del ilícito. Asimismo, el Juez, a solicitud del Ministerio Público ordenará el allanamiento o descerraje del lugar donde se estuviere  cometiendo el ilícito  penal.

En caso  de emitirse sentencia condenatoria, los ejemplares ilícitos podrán ser entregados al titular del derecho vulnerado o a una institución adecuada y en caso de no corresponder, serán destruídos. La entrega no tendrá carácter indemnizatorio.

En ningún caso procederá la devolución de los ejemplares ilícitos al encausado." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 29263, publicada el 02 octubre 2008, cuyo texto es el siguiente:

"Artículo 221.- Incautación preventiva y comiso definitivo

En los delitos previstos en este capítulo se procederá a la incautación preventiva de los ejemplares y materiales, de los aparatos o medios utilizados para la comisión del ilícito y, de ser el caso, de los activos y cualquier evidencia documental, relacionados al ilícito penal.

De ser necesario, el Fiscal pedirá autorización al Juez para leer la documentación que se halle en el lugar de la intervención, en ejecución de cuya autorización se incautará la documentación vinculada con el hecho materia de investigación.

Para la incautación no se requerirá identificar individualmente la totalidad de los materiales, siempre que se tomen las medidas necesarias para que durante el proceso judicial se identifiquen la totalidad de los mismos. En este acto participará el representante del Ministerio Público.

Asimismo, el Juez, a solicitud del Ministerio Público, ordenará el allanamiento o descerraje del local donde se estuviere cometiendo el ilícito penal.

En caso de emitirse sentencia condenatoria, los ejemplares, materiales ilícitos, aparatos y medios utilizados para la comisión del ilícito serán comisados y destruidos, salvo casos excepcionales debidamente calificados por la autoridad judicial.

En ningún caso procederá la devolución de los ejemplares ilícitos al encausado."

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

CAPITULO II DELITOS CONTRA LA PROPIEDAD INDUSTRIAL
##################################################

Artículo 222 Fabricación o uso no autorizado de patente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que fabrica producto o usa un medio o proceso patentado de fabricación, sin estar autorizado por quien tiene derecho a hacerlo, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años, con sesenta a trescientos sesenticinco días-multa e inhabilitación conforme el artículo 36, inciso 4. (*)

(*) Artículo sustituido por el Artículo 2 de la Ley N° 27729, publicada el 24 mayo 2002, cuyo texto es el siguiente:

Fabricación o uso no autorizado de patente

“Artículo 222.- Será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años, con sesenta a trescientos sesenta y cinco días multa e inhabilitación conforme al Artículo 36 inciso 4) tomando en consideración la gravedad del delito y el valor de los perjuicios ocasionados, quien en violación de las normas y derechos de propiedad industrial, almacene, fabrique, utilice con fines comerciales, oferte, distribuya, venda, importe o exporte, en todo o en parte:

a. Un producto amparado por una patente de invención o un producto fabricado mediante la utilización de un procedimiento amparado por una patente de invención obtenidos en el país;

b. Un producto amparado por un modelo de utilidad obtenido en el país;

c. Un producto amparado por un diseño industrial registrado en el país;

d. Una obtención vegetal registrada en el país, así como su material de reproducción, propagación o multiplicación;

e. Un esquema de trazado (tipografía) registrado en el país, un circuito semiconductor que incorpore dicho esquema de trazado (topografía) o un artículo que incorpore tal circuito semiconductor;

f. Un producto o servicio que utilice una marca no registrada idéntica o similar a una marca registrada en el país."

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

Artículo 222-A "PENALIZACIÓN DE LA CLONACIÓN O ADULTERACIÓN DE TERMINALES DE TELEFONÍA CELULAR
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no menor de dos (2) ni mayor de cinco (5) años, con sesenta (60) a trescientos sesenta y cinco (365) días multa, el que altere, reemplace, duplique o de cualquier modo modifique un número de línea, o de serie electrónico, o de serie mecánico de un terminal celular, de modo tal que pueda ocasionar perjuicio al titular o usuario del mismo así como a terceros.”(*)

(*) Artículo adicionado por el Artículo 4 de la Ley N° 28774, publicada el 07 julio 2006.

(*) Artículo modificado por la Tercera Disposición Complementaria Modificatoria del Decreto Legislativo N° 1182, publicado el 27 julio 2015, cuyo texto es el siguiente:

“Artículo 222-A.- Penalización de la clonación o adulteración de terminales de telecomunicaciones

Será reprimido con pena privativa de libertad no menor de cuatro (4) ni mayor de seis (6) años, con sesenta (60) a trescientos sesenta y cinco (365) días multa, el que altere, reemplace, duplique o de cualquier modo modifique un número de línea, o de serie electrónico, o de serie mecánico de un terminal celular, o de IMEI electrónico o físico de modo tal que pueda ocasionar perjuicio al titular, al usuario del mismo, a terceros o para ocultar la identidad de los que realizan actos ilícitos.”

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

Artículo 223 Uso o venta no autorizada de diseño o modelo industrial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, sin autorización, reproduce por cualquier medio, en todo o en parte, diseño o modelo industrial registrado  por otro o vende o expone a la venta objeto que es imitación o copia del modelo legalmente registrado, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años, con sesenta a trescientos sesenticinco días-multa e inhabilitación (*) RECTIFICADO POR FE DE ERRATAS conforme al artículo 36, inciso 4 .(*)

(*) Artículo sustituido por el Artículo 2 de la Ley N° 27729, publicada el 24 mayo 2002, cuyo texto es el siguiente:

Uso o venta no autorizada de diseño o modelo industrial

“Artículo 223.- Serán reprimidos con pena privativa de la libertad no menor de dos ni mayor de cinco años, con sesenta a trescientos sesenta y cinco días-multa e inhabilitación conforme al Artículo 36 inciso 4) tomando en consideración la gravedad del delito y el valor de los perjuicios ocasionados, quienes en violación de las normas y derechos de propiedad industrial:

a. Fabriquen, comercialicen, distribuyan o almacenen etiquetas, sellos o envases que contengan marcas registradas;

b. Retiren o utilicen etiquetas, sellos o envases que contengan marcas originales para utilizarlos en productos de distinto origen; y

c. Envasen y/o comercialicen productos empleando envases identificados con marcas cuya titularidad corresponde a terceros."

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

Artículo 224 Uso ilícito de diseño o modelo industrial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que usa en modelo o diseño industrial una expresión que lo acredite falsamente como titular del derecho o menciona en anuncio o medio publicitario como registrado diseño o modelo que no lo estuviera (*) RECTIFICADO POR FE DE ERRATAS, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años, con sesenta a trescientos sesenticinco días-multa e inhabilitación conforme al artículo 36, inciso 4.(*)

(*) Artículo sustituido por el Artículo 2 de la Ley N° 27729, publicada el 24 mayo 2002, cuyo texto es el siguiente:

Uso ilícito de diseño o modelo industrial

“Artículo 224.- En los delitos previstos en este Título, el Juez a solicitud del Ministerio Público ordenará el allanamiento o descerraje del lugar donde se estuviere cometiendo el ilícito penal y se procederá a la incautación de los ejemplares de procedencia ilícita y de los aparatos o medios utilizados para la comisión del delito.

En caso de emitirse sentencia condenatoria, los ejemplares de procedencia ilícita podrán ser entregados al titular del derecho vulnerado o a una institución adecuada y en caso de no corresponder, serán destruidos. La entrega no tendrá carácter indemnizatorio.

En ningún caso procederá la devolución de los ejemplares de procedencia ilícita al encausado." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 29263, publicada el 02 octubre 2008, cuyo texto es el siguiente:

"Artículo 224.- Incautación preventiva y comiso definitivo

En los delitos previstos en este capítulo se procederá a la incautación preventiva de los ejemplares y materiales, de los aparatos o medios utilizados para la comisión del ilícito y, de ser el caso, de los activos y cualquier evidencia documental, relacionados al ilícito penal.

De ser necesario, el Fiscal pedirá autorización al Juez para leer la documentación que se halle en el lugar de la intervención, en ejecución de cuya autorización se incautará la documentación vinculada con el hecho materia de investigación.

Para la incautación no se requerirá identificar individualmente la totalidad de los materiales, siempre que se tomen las medidas necesarias para que durante el proceso judicial se identifiquen la totalidad de los mismos. En este acto participará el representante del Ministerio Público.

Asimismo, el Juez, a solicitud del Ministerio Público, ordenará el allanamiento o descerraje del local donde se estuviere cometiendo el ilícito penal.

En caso de emitirse sentencia condenatoria, los ejemplares, materiales ilícitos, aparatos y medios utilizados para la comisión del ilícito serán comisados y destruidos, salvo casos excepcionales debidamente calificados por la autoridad judicial.

En ningún caso procederá la devolución de los ejemplares ilícitos al encausado.”

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

Artículo 225 Uso indebido de marca
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que reproduce, indebidamente, en todo o en parte, artículo industrial con marca registrada por otro o lo imita de modo que pueda inducir a error o confusión o el que, a sabiendas, usa marca reproducida o imitada o vende, expone a la venta o tiene en depósito productos con marca imitada o reproducida, en todo o en parte o productos que tengan marca de otro y no hayan sido fabricados por el agente, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años, con sesenta a trescientos sesenticinco días-multa e inhabilitación conforme al artículo 36, inciso 4. (*)

(*) Artículo sustituido por el Artículo 2 de la Ley N° 27729, publicada el 24 mayo 2002, cuyo texto es el siguiente:

Uso indebido de marca

“Artículo 225.- Será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años y noventa a trescientos sesenta y cinco días multa e inhabilitación conforme al Artículo 36 inciso 4):

a. Si el agente que comete el delito integra una organización destinada a perpetrar los ilícitos previstos en el presente capítulo. (*)

(*) Extremo modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 30077, publicada el 20 agosto 2013, la misma que entró en vigencia el 1 de julio de 2014, cuyo texto es el siguiente:

"Artículo 225. - Condición y grado de participación del agente

Será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años y con noventa a trescientos sesenta y cinco días-multa e inhabilitación conforme al artículo 36, inciso 4:

a) Si el agente que comete el delito integra una organización criminal destinada a perpetrar los ilícitos previstos en el presente capítulo."

b. Si el agente que comete cualquiera de los delitos previstos en el presente capítulo, posee la calidad de funcionario o servidor público.”

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

TITULO VIII DELITOS CONTRA EL PATRIMONIO CULTURAL
=================================================

CAPITULO UNICO DELITOS CONTRA LOS BIENES CULTURALES
###################################################

Artículo 226 Atentados contra yacimientos arqueológicos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que depreda o el que, sin autorización, explora, excava o remueve yacimientos arqueológicos prehispánicos, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años y con ciento veinte a trescientos sesenticinco días-multa. (*)

(*) Artículo modificado por el Artículo Único de la Ley Nº 28567, publicada el 02 julio 2005, cuyo texto es el siguiente:

“Artículo 226.- Atentados contra monumentos arqueológicos

El que se asienta, depreda o el que, sin autorización, explora, excava o remueve monumentos arqueológicos prehispánicos, sin importar la relación de derecho real que ostente sobre el terreno donde aquél se ubique, siempre que conozca el carácter de patrimonio cultural del bien, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años y con ciento veinte a trescientos sesenta y cinco días multa."

Artículo 227 Inducción a la comisión de atentados contra yacimientos arqueológicos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que promueve, organiza, financia o dirige grupos de personas para la comisión de los delitos previstos en el artículo 226, será reprimido con pena privativa de libertad no menor de tres ni mayor de ocho años y con ciento ochenta a trescientos sesenticinco días- multa.

Artículo 228 
~~~~~~~~~~~~~

El que extrae del país bienes del patrimonio cultural prehispánico o no los retorna de conformidad con la autorización que le fue concedida, será reprimido con pena privativa de libertad no menor de tres ni mayor de ocho años y con ciento ochenta a trescientos sesenticinco días-multa. (*)

(*) Artículo modificado por el Artículo Unico de la Ley Nº 26690, publicada el 30 noviembre 1996, cuyo texto es el siguiente:

"Artículo 228.- El que destruye, altera o extrae del país bienes del Patrimonio Cultural prehispánico o no los retorna de conformidad con la autorización que le fue concedida, será reprimido con pena privativa de libertad no menor de tres ni mayor de ocho años y con ciento ochenta a trescientos sesenticinco días multa." (*)

(*) Artículo modificado por el Artículo Unico de la Ley Nº 27244, publicada el 26 diciembre 1999, cuyo texto es el siguiente:

Extracción ilegal de bienes culturales

"Artículo 228.- El que destruye, altera, extrae del país o comercializa bienes del patrimonio cultural prehispánico o no los retorna de conformidad con la autorización que le fue concedida, será reprimido con pena privativa de libertad no menor de tres ni mayor de ocho años y con ciento ochenta a trescientos sesenta y cinco días-multa." (*)

(*) Artículo modificado por el Artículo Único de la Ley Nº 28567, publicada el 02 julio 2005, cuyo texto es el siguiente:

"Artículo 228.- Extracción ilegal de bienes culturales

El que destruye, altera, extrae del país o comercializa bienes del patrimonio cultural prehispánico o no los retorna de conformidad con la autorización que le fue concedida, será reprimido con pena privativa de libertad no menor de tres ni mayor de ocho años y con ciento ochenta a trescientos sesenta y cinco días multa.

En el caso que el agente sea un funcionario o servidor público con deberes de custodia de los bienes, la pena será no menor de cinco ni mayor de diez años.”

Artículo 229 Omisión de deberes de funcionarios públicos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las autoridades políticas, administrativas, aduaneras, municipales y miembros de la Fuerzas Armadas o de la Policía Nacional que, omitiendo los deberes de sus cargos, intervengan o faciliten la comisión de los delitos mencionados en este Capítulo, serán reprimidos con pena privativa de libertad no menor de tres ni mayor de seis años, con treinta a noventa días-multa e inhabilitación no menor de un año, conforme al artículo 36, incisos 1, 2 y 3.

Si el agente obró por culpa, la pena será privativa de libertad no mayor de dos años.

Artículo 230 
~~~~~~~~~~~~~

El que destruye, altera o extrae del país bienes culturales previamente declarados como tales, distintos a los de la época prehispánica, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años y con noventa a ciento ochenta días-multa. (*)

(*) Artículo modificado por el Artículo Unico de la Ley Nº 27244, publicada el 26 diciembre 1999, cuyo texto es el siguiente:

Destrucción, alteración o extracción de bienes culturales

"Artículo 230.- El que destruye, altera, extrae del país o comercializa, sin autorización, bienes culturales previamente declarados como tales, distintos a los de la época prehispánica, o no los retorna al país de conformidad con la autorización que le fue concedida, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años y con noventa a ciento ochenta días-multa."

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 231 
~~~~~~~~~~~~~

Las penas previstas en este Capítulo, se imponen sin perjuicio del decomiso en favor del Estado, de los materiales, equipos y vehículos empleados en la comisión de los delitos contra el patrimonio cultural, así como de los bienes culturales obtenidos indebidamente. (*)

(*) Artículo modificado por el Artículo Unico de la Ley Nº 27244, publicada el 26 diciembre 1999, cuyo texto es el siguiente:

Decomiso

"Artículo 231.- Las penas previstas en este capítulo, se imponen sin perjuicio del decomiso en favor del Estado, de los materiales, equipos y vehículos empleados en la comisión de los delitos contra el patrimonio cultural, así como de los bienes culturales obtenidos indebidamente, sin perjuicio de la reparación civil a que hubiere lugar."

TITULO IX DELITOS CONTRA EL ORDEN ECONOMICO
===========================================

CAPITULO I ABUSO DEL PODER ECONOMICO
####################################

(*) De conformidad con el Artículo 19 del Decreto Legislativo Nº 701, publicado el 11 julio 1991, modificado por el Artículo 11 del Decreto Legislativo Nº 807, publicado el 18 abril 1996, las infracciones de índole administrativo, son reconocidas también como justiciables penalmente; y la iniciativa de la acción penal compete exclusivamente al Fiscal Provincial.

Artículo 232 Abuso de poder económico
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, infringiendo la ley de la materia, abusa de su posición monopólica u oligopólica en el mercado, o el que participa en prácticas y acuerdos restrictivos en la actividad productiva, mercantil o de servicios, con el objeto de impedir, restringir o distorsionar la libre competencia, será reprimido con pena privativa de libertad no menor de dos ni mayor de seis años, con ciento ochenta a trescientos sesenticinco días-multa e inhabilitación conforme al artículo 36º, incisos 2 y 4.(*)

(*) Artículo derogado por el inciso b) de la Segunda Disposición Complementaria y Derogatoria del Decreto Legislativo N° 1034, publicado el 25 junio 2008. La citada Ley entró en vigencia a los treinta (30) días de su publicación en el Diario Oficial El Peruano.

CAPITULO II ACAPARAMIENTO, ESPECULACION, ADULTERACION
#####################################################

Artículo 233 Acaparamiento
~~~~~~~~~~~~~~~~~~~~~~~~~~

El que acapara o de cualquier manera sustrae del comercio, bienes de consumo o producción, con el fin de alterar los precios, provocar escasez u obtener lucro indebido en perjuicio de la colectividad, será reprimido con pena privativa de libertad (*) RECTIFICADO POR FE DE ERRATAS no menor de dos ni mayor de cuatro años y con noventa a ciento ochenta días-multa.

Si se trata de bienes de primera necesidad, la pena será privativa de libertad no menor de tres ni mayor de cinco años y de ciento ochenta a trescientos sesenticinco días-multa.(*)

(*) Artículo derogado por el inciso b) de la Segunda Disposición Complementaria y Derogatoria del Decreto Legislativo N° 1034, publicado el 25 junio 2008. La citada Ley entró en vigencia a los treinta (30) días de su publicación en el Diario Oficial El Peruano.

Artículo 234 Especulación
~~~~~~~~~~~~~~~~~~~~~~~~~

El productor, fabricante o comerciante que pone en venta productos considerados oficialmente de primera necesidad a precios superiores a los fijados (*) RECTIFICADO POR FE DE ERRATAS por la autoridad competente, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años y con noventa a ciento ochenta días-multa.

El que, injustificadamente vende bienes, o presta servicios a precio superior al que consta en las etiquetas, rótulos, letreros o listas elaboradas por el propio vendedor o prestador de servicios, será reprimido con pena privativa de libertad no mayor de un año y con noventa a ciento ochenta días-multa.

El que vende bienes que, por unidades tiene cierto peso o medida, cuando dichos bienes sean inferiores a estos pesos o medidas, será reprimido con pena privativa de libertad no mayor de un año y con noventa a ciento ochenta días-multa.

El que vende bienes contenidos en embalajes o recipientes cuyas cantidades sean inferiores a los mencionados en ellos, será reprimido con pena privativa de libertad no mayor de un año y con noventa a ciento ochenta días-multa.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 235 Adulteración
~~~~~~~~~~~~~~~~~~~~~~~~~

El que altera o modifica la calidad, cantidad, peso o medida de artículos considerados oficialmente de primera necesidad, en perjuicio del consumidor, será reprimido con pena privativa de libertad no menor de uno ni (*) RECTIFICADO POR FE DE ERRATAS mayor de tres años y con noventa a ciento ochenta días-multa.

Artículo 236 Agravante común
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si los delitos previstos en este Capítulo se cometen en época de conmoción o calamidad públicas, la pena será privativa de libertad no menor de tres ni mayor de seis años y de ciento ochenta a trescientos sesenticinco días-multa.

CAPITULO III VENTA ILICITA DE MERCADERIAS
#########################################

Artículo 237 Venta ilegal de mercaderías
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que pone en venta o negocia de cualquier manera bienes recibidos para su distribución gratuita, será reprimido con pena privativa de libertad no menor de dos ni mayor de seis años.

Si el delito se comete en época de conmoción o calamidad públicas, o es realizado por funcionario o servidor público, la pena será no menor de tres ni mayor de ocho años.(*)

(*) Artículo modificado por el Artículo 3 de la Ley N° 27776, publicada el 09 julio 2002, cuyo texto es el siguiente:

Venta ilegal de mercaderías

“Artículo 237.- El que pone en venta o negocia de cualquier manera bienes recibidos para su distribución gratuita, será reprimido con pena privativa de libertad no menor de dos ni mayor de seis años.

La pena será no menor de tres años ni mayor de seis años e inhabilitación conforme a los incisos 1), 2) y 3) del Artículo 36, cuando el agente transporta o comercializa sin autorización bienes fuera del territorio en el que goza de beneficios provenientes de tratamiento tributario especial. Si el delito se comete en época de conmoción o calamidad pública, o es realizado por funcionario o servidor público, la pena será no menor de tres ni mayor de ocho años."

CAPITULO IV DE OTROS DELITOS ECONOMICOS
#######################################

Artículo 238 Informaciones falsas sobre calidad de productos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que hace, por cualquier medio publicitario, afirmaciones falsas sobre la naturaleza, composición, virtudes o cualidades sustanciales de los productos o servicios anunciados, capaces por sí mismas de inducir a grave error al consumidor, será reprimido con noventa a ciento ochenta días-multa.

Cuando se trate de publicidad de productos alimenticios, preservantes y aditivos alimentarios, medicamentos o artículos de primera necesidad o destinados al consumo infantil, la multa se aumentará en un cincuenta por ciento. (*)

(*) Artículo derogado por la Segunda Disposición Derogatoria del Decreto Legislativo N° 1044, publicado el 26 junio 2008. La citada Ley entró en vigencia luego de treinta (30) días calendario de la fecha de su publicación en el Diario Oficial El Peruano y será aplicable inmediatamente en todas sus disposiciones, salvo en las que ordenan el procedimiento administrativo, incluidas las que determinan la escala de sanciones, las que serán aplicables únicamente a los procedimientos iniciados con posterioridad a su vigencia.

Artículo 239 Venta de bienes o prestación de servicios diferentes a los anunciados
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que vende bienes o presta servicios, cuya calidad o cantidad son diferentes a los ofertados o a los consignados en los rótulos, etiquetas, letreros o listas elaboradas por la propia empresa vendedora o prestadora de servicios, será reprimido con pena privativa de libertad no mayor de tres años y con sesenta a ciento veinte días-multa.

El que vende bienes cuya fecha de vencimiento ha caducado, será reprimido con la misma pena. (*)

(*) Artículo derogado por la Segunda Disposición Derogatoria del Decreto Legislativo N° 1044, publicado el 26 junio 2008. La citada Ley entró en vigencia luego de treinta (30) días calendario de la fecha de su publicación en el Diario Oficial El Peruano y será aplicable inmediatamente en todas sus disposiciones, salvo en las que ordenan el procedimiento administrativo, incluidas las que determinan la escala de sanciones, las que serán aplicables únicamente a los procedimientos iniciados con posterioridad a su vigencia.

Artículo 240 Aprovechamiento indebido de ventajas de reputación industrial o comercial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no mayor de dos años o con ciento ochenta a trescientos sesenticinco días-multa, el que en beneficio propio o de terceros:

1. Se aprovecha indebidamente de las ventajas de una reputación industrial o comercial adquirida por el esfuerzo de otro.

2. Realiza actividades, revela o divulga informaciones que perjudiquen la reputación económica de una empresa, o que produzca descrédito injustificado de los productos o servicios ajenos.

En los delitos previstos en este artículo sólo se procederá por acción privada. (*)

(*) Artículo derogado por la Segunda Disposición Derogatoria del Decreto Legislativo N° 1044, publicado el 26 junio 2008. La citada Ley entró en vigencia luego de treinta (30) días calendario de la fecha de su publicación en el Diario Oficial El Peruano y será aplicable inmediatamente en todas sus disposiciones, salvo en las que ordenan el procedimiento administrativo, incluidas las que determinan la escala de sanciones, las que serán aplicables únicamente a los procedimientos iniciados con posterioridad a su vigencia.

Artículo 241 Fraude en remates, licitaciones y concursos públicos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Serán reprimidos con pena privativa de libertad no mayor de tres años o con ciento ochenta a trescientos sesenticinco días-multa quienes practiquen las siguientes acciones:

1. Solicitan o aceptan dádivas o promesas para no tomar parte en un remate público, en una licitación pública o en un concurso público de precios.

2. Intentan alejar a los postores por medio de amenazas, dádivas, promesas o cualquier otro artificio.

3. Conciertan entre sí con el objeto de alterar el precio. (*)

(*) Numeral derogado por el inciso b) de la Segunda Disposición Complementaria y Derogatoria del Decreto Legislativo N° 1034, publicado el 25 junio 2008. La citada Ley entró en vigencia a los treinta (30) días de su publicación en el Diario Oficial El Peruano.

Si se tratare de concurso público de precios o de licitación pública, se impondrá además al agente o a la empresa o persona por él representada, la suspensión del derecho a contratar con el Estado por un período no menor de tres ni mayor de cinco años.

«Artículo 241-A.- Corrupción en el ámbito privado

El socio, accionista, gerente, director, administrador, representante legal, apoderado, empleado o asesor de una persona jurídica de derecho privado, organización no gubernamental, asociación, fundación, comité, incluidos los entes no inscritos o sociedades irregulares, que directa o indirectamente acepta, reciba o solicita donativo, promesa o cualquier otra ventaja o beneficio indebido de cualquier naturaleza, para sí o para un tercero para realizar u omitir un acto que permita favorecer a otro en la adquisición o comercialización de bienes o mercancías, en la contratación de servicios comerciales o en las relaciones comerciales, será reprimido con pena privativa de libertad no mayor de cuatro años e inhabilitación conforme al inciso 4 del artículo 36 del Código Penal y con ciento ochenta a trescientos sesenta y cinco días-multa.

Será reprimido con las mismas penas previstas en el párrafo anterior quien, directa o indirectamente, prometa, ofrezca o conceda a accionistas, gerentes, directores, administradores, representantes legales, apoderados, empleados o asesores de una persona jurídica de derecho privado, organización no gubernamental, asociación, fundación, comité, incluidos los entes no inscritos o sociedades irregulares, una ventaja o beneficio indebido de cualquier naturaleza, para ellos o para un tercero, como contraprestación para realizar u omitir un acto que permita favorecer a éste u otro en la adquisición o comercialización de bienes o mercancías, en la contratación de servicios comerciales o en las relaciones comerciales».(*)

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1385, publicado el 04 septiembre 2018.

«Artículo 241-B.- Corrupción al interior de entes privados

El socio, accionista, gerente, director, administrador, representante legal, apoderado, empleado o asesor de una persona jurídica de derecho privado, organización no gubernamental, asociación, fundación, comité, incluidos los entes no inscritos o sociedades irregulares, que directa o indirectamente acepta, recibe o solicita donativo, promesa o cualquier otra ventaja o beneficio indebido de cualquier naturaleza para sí o para un tercero para realizar u omitir un acto en perjuicio de la persona jurídica, será reprimido con pena privativa de libertad no mayor de cuatro años e inhabilitación conforme al inciso 4 del artículo 36 del Código Penal y con ciento ochenta a trescientos sesenta y cinco días-multa.

Será reprimido con las mismas penas previstas en el párrafo anterior quien, directa o indirectamente, promete, ofrece o concede a accionistas, gerentes, directores, administradores, representantes legales, apoderados, empleados o asesores de una persona jurídica de derecho privado, organización no gubernamental, asociación, fundación, comité, incluidos los entes no inscritos o sociedades irregulares, una ventaja o beneficio indebido de cualquier naturaleza, para ellos o para un tercero, como contraprestación para realizar u omitir un acto en perjuicio de la persona jurídica.

En los supuestos previstos en este artículo solo se procederá mediante ejercicio privado de la acción penal».(*)

(*) Artículo incorporado por el Artículo 2 del Decreto Legislativo N° 1385, publicado el 04 septiembre 2018.

Artículo 242 Rehusamiento a prestar información económica, industrial o comercial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El director, administrador o gerente de una empresa que, indebidamente, rehusa suministrar a la autoridad competente la información económica, industrial o mercantil que se le requiera, o deliberamente presta la información de modo inexacto, será reprimido con pena privativa de libertad no mayor de dos años o con noventa a ciento ochenta días-multa.

Artículo 243 Subvaluación de mercaderías adquiridas con tipo de cambio preferencial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que recibe moneda extranjera con tipo de cambio preferencial para realizar importaciones de mercaderías y vende éstas a precios superiores a los autorizados, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años, con ciento veinte a trescientos sesenticinco días-multa e inhabilitación conforme al artículo 36, incisos 1, 2 y 4.

El que da a las mercaderías finalidad distinta a la que establece la norma que fija el tipo de cambio o el régimen especial tributario, será reprimido con la pena señalada en el párrafo anterior.(*)

(*) Mediante Oficio Nº 970-2013-MP-FN-OAJ de fecha 24 de octubre de 2013, enviado por la Oficina de Asesoría Jurídica del Ministerio Público, se indica que el presente artículo estaría derogado tácitamente en atención a lo establecido en el artículo 83 de la Ley N° 26123 - Ley Orgánica del Banco Central de Reserva del Perú, al establecer que está prohibido al Banco establecer regímenes de tipos de cambio múltiples; siendo la presente disposición, viejo rezago de la época en que se establecían regímenes de cambio preferenciales. (*)

Funcionamiento ilegal de casinos de juego

"Artículo 243-A.- Será reprimido con pena privativa de la libertad no menor de uno ni mayor de seis años y con trescientos sesenticinco días-multa, el que organiza o conduce Casinos de Juego sujetos a autorización sin haber cumplido los requisitos que exijan las leyes o reglamentos para su funcionamiento; sin perjuicio del decomiso de los efectos, dinero y bienes utilizados en la comisión del delito." (*)

(*) Artículo incorporado por el Artículo 10 del Decreto Ley Nº 25836, publicado el 11 noviembre 1992. Posteriormente el Decreto Ley N° 25836 fue derogado por la Tercera Disposición Complementaria y Final de la Ley Nº 27153, publicada el 09 julio 1999.

“CAPITULO V
DESEMPEÑO DE ACTIVIDADES NO AUTORIZADAS

"Artículo 243-B.- El que por cuenta propia o ajena realiza o desempeña actividades propias de los agentes de intermediación, sin contar con la autorización para ello, efectuando transacciones o induciendo a la compra o venta de valores, por medio de cualquier acto, práctica o mecanismo engañoso o fraudulento y siempre que los valores involucrados en tales actuaciones tengan en conjunto un valor de mercado superior a cuatro (4) UIT, será reprimido con pena privativa de libertad no menor de uno (1) ni mayor de cinco (5) años." (*)

(*) Capítulo V incorporado por la Décimo Primera Disposición Transitoria y Final de la Ley Nº 27649, publicada el 23 enero 2002.

“Artículo 243-C.- Funcionamiento ilegal de juegos de casino y máquinas tragamonedas

El que organiza, conduce o explota juegos de casino y máquinas tragamonedas, sin haber cumplido con los requisitos que exigen las leyes y sus reglamentos para su explotación, será reprimido con pena privativa de la libertad no menor de uno ni mayor de cuatro años, con trescientos sesenta y cinco días multa e inhabilitación para ejercer dicha actividad, de conformidad con el inciso 4) del artículo 36 del Código Penal." (*)

(*) Artículo incorporado por el Artículo 1 de la Ley N° 28842, publicada el 26 julio 2006.

TITULO X DELITOS CONTRA EL ORDEN FINANCIERO Y MONETARIO
=======================================================

CAPITULO I DELITOS FINANCIEROS
##############################

Artículo 244 Concentración crediticia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El director, gerente, administrador, representante legal o funcionario de una institución bancaria, financiera u otra que opere con fondos del público, que directa o indirectamente apruebe créditos u otros financiamientos por encima de los límites legales en favor de personas vinculadas a accionistas de la propia institución, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de diez años y con trescientos sesenticinco a setecientos treinta días-multa, si como consecuencia de ello la institución incurre en situación de insolvencia.

Serán reprimidos con la misma pena los beneficiarios del crédito que hayan participado en el delito.(*)

(*) Artículo modificado por el Artículo 3 de la Ley N° 28755, publicada el 06 junio 2006, cuyo texto es el siguiente:

"Artículo 244.- Concentración crediticia

El director, gerente, administrador, representante legal, miembro del consejo de administración, miembro de comité de crédito o funcionario de una institución bancaria, financiera u otra que opere con fondos del público que, directa o indirectamente, a sabiendas, apruebe créditos, descuentos u otros financiamientos por encima de los límites operativos establecidos en la ley de la materia, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de diez años y con trescientos sesenta y cinco a setecientos treinta días-multa.

En caso de que los créditos, descuentos u otros financiamientos a que se refiere el párrafo anterior sean otorgados a favor de directores o trabajadores de la institución, o de personas vinculadas a accionistas de la propia institución conforme a los criterios de vinculación normados por la Superintendencia de Banca, Seguros y Administradoras Privadas de Fondos de Pensiones, el autor será reprimido con pena privativa de libertad no menor de seis ni mayor de diez años y con trescientos sesenta y cinco a setecientos treinta días-multa.

Si como consecuencia de la aprobación de las operaciones señaladas en los párrafos anteriores, la Superintendencia de Banca, Seguros y Administradoras Privadas de Fondos de Pensiones resuelve la intervención o liquidación de la institución, el autor será reprimido con pena privativa de libertad no menor de ocho ni mayor de doce años y trescientos sesenta y cinco a setecientos treinta días-multa.

Los beneficiarios de las operaciones señaladas en el presente artículo, que hayan participado en el delito, serán reprimidos con la misma pena que corresponde al autor."

Artículo 245 Ocultamiento, omisión o falsedad de información
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


El director, gerente, administrador, representante legal o funcionario de una institución bancaria, financiera u otra que opere con fondos del público, que con el propósito de ocultar situaciones de iliquidez o insolvencia de la institución, omita o niegue proporcionar información o proporcione datos falsos a las autoridades de control y regulación, será reprimido con pena privativa de libertad no menor de dos ni mayor de seis años y con (*) RECTIFICADO POR FE DE ERRATAS ciento ochenta a trescientos sesenticinco días-multa.(*)

(*) Artículo modificado por el Artículo 3 de la Ley N° 28755, publicada el 06 junio 2006, cuyo texto es el siguiente:

"Artículo 245.- Ocultamiento, omisión o falsedad de información

El director, gerente, administrador, representante legal, miembro del consejo de administración, miembro del consejo de vigilancia, miembro del comité de crédito, auditor interno, auditor externo, liquidador o funcionario de una institución bancaria, financiera u otra que opere con fondos del público, que con el propósito de ocultar situaciones de liquidez o insolvencia de la institución, omita o niegue proporcionar información o proporcione datos falsos a las autoridades de control y regulación, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años y con ciento ochenta a trescientos sesenta y cinco días-multa.” (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 29307, publicada el 31 diciembre 2008, cuyo texto es el siguiente:

"Artículo 245.- Ocultamiento, omisión o falsedad de información
El que ejerce funciones de administración o representación de una institución bancaria, financiera u otra que opere con fondos del público, que con el propósito de ocultar situaciones de liquidez o insolvencia de la institución, omita o niegue proporcionar información o proporcione datos falsos a las autoridades de control y regulación, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años y con ciento ochenta a trescientos sesenta y cinco días-multa.”

“Artículo 245-A.- Falsedad de información presentada por un emisor en el mercado de valores

El que ejerce funciones de administración, de un emisor con valores inscritos en el Registro Público del Mercado de Valores, que deliberadamente proporcione o consigne información o documentación falsas de carácter económico-financiera, contable o societaria referida al emisor, a los valores que emita, a la oferta que se haga de estos, y que el emisor se encuentre obligado a presentar o revelar conforme a la normatividad del mercado de valores, para obtener un beneficio o evitar un perjuicio propio o de un tercero, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años y con ciento ochenta a trescientos sesenta y cinco días-multa.

Si como consecuencia de la conducta descrita en el párrafo anterior se produce un perjuicio económico para algún inversionista o adquirente de los valores o instrumentos financieros, el agente será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años y con ciento ochenta a trescientos sesenta y cinco días-multa.

Previamente a la formalización de la denuncia respectiva, el Ministerio Público deberá requerir un informe técnico a la Superintendencia del Mercado de Valores (SMV), que será emitido dentro del plazo de quince (15) días de solicitado, vencido el cual resolverá."(*)

(*) Artículo incorporado por el Artículo 14 de la Ley Nº 30050, publicada el 26 junio 2013.

Artículo 246 Instituciones financieras ilegales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, por cuenta propia o ajena, se dedica directa o indirectamente a la captación habitual de recursos del público, bajo la forma de depósito, mutuo o cualquier modalidad, sin contar con permiso de la autoridad competente, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años y con ciento ochenta a trescientos sesenticinco días-multa.

Si para dichos fines el agente hace uso de los medios de comunicación social, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años y con ciento ochenta a trescientos sesenticinco días-multa.

Artículo 247 Financiamiento por medio de información fraudulenta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El usuario de una institución bancaria, financiera u otra que opera con fondos del público que, proporcionando información o documentación falsas o mediante engaños obtiene créditos directos o indirectos u otro tipo de financiación, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años y con ciento ochenta a trescientos sesenticinco días-multa.

Si como consecuencia del crédito así obtenido, la Superintendencia de Banca y Seguros resuelve la intervención o liquidación de la institución financiera, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de diez años y con trescientos sesenticinco a setecientos treinta días-multa.

Los accionistas, asociados, directores, gerentes y funcionarios de la institución que cooperen en la ejecución del delito, serán reprimidos con la misma pena señalada en el párrafo anterior y, además, con inhabilitación conforme al artículo 36, incisos 1, 2 y 4. (*)

(*) Artículo modificado por el Artículo 13 de la Ley N° 30822, publicada el 19 julio 2018, la misma que entró en vigencia el 1 de enero de 2019, cuyo texto es el siguiente:

Artículo 247 “Financiamiento por medio de información fraudulenta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El usuario de una institución bancaria, financiera u otra que opera con fondos del público, o de una cooperativa de ahorro y crédito que solo opera con sus socios y que no está autorizada a captar recursos del público u operar con terceros, inscrita en el Registro Nacional de Cooperativas de Ahorro y Crédito No Autorizadas a Captar Recursos del Público, que, proporcionando información o documentación falsas o mediante engaños obtiene créditos directos o indirectos u otro tipo de financiación, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años y con ciento ochenta a trescientos sesenticinco días-multa.

Si como consecuencia del crédito así obtenido, la Superintendencia de Banca, Seguros y AFP resuelve la intervención o liquidación de la institución bancaria, financiera o de la cooperativa de ahorro y crédito que solo opera con sus socios y que no está autorizada a captar recursos del público u operar con terceros, inscrita en el Registro Nacional de Cooperativas de Ahorro y Crédito No Autorizadas a Captar Recursos del Público, es reprimido con pena privativa de libertad no menor de cuatro ni mayor de diez años y con trescientos sesenticinco a setecientos treinta días-multa.

Los accionistas, asociados, directores, gerentes y funcionarios de la institución que cooperen en la ejecución del delito, serán reprimidos con la misma pena señalada en el párrafo anterior y, además, con inhabilitación conforme al artículo 36, incisos 1, 2 y 4."

Artículo 248 Condicionamiento de créditos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los directores, gerentes, administradores o funcionarios de las instituciones bancarias, financieras y demás que operan con fondos del público que condicionan, en forma directa o indirecta, el otorgamiento de créditos a la entrega por parte del usuario de contraprestaciones indebidas, serán reprimidos con pena privativa de libertad no menor de uno ni mayor de tres años y con noventa a ciento ochenta días-multa.

Artículo 249 Pánico financiero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que produce alarma en la población mediante la propalación de noticias falsas, ocasionando retiros masivos de depósitos de cualquier institución bancaria, financiera u otras que operan con fondos del público, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años y con ciento ochenta a trescientos sesenticinco días-multa. (*)

(*) Artículo modificado por el Artículo Único de la Ley N° 27941, publicada el 26 febrero 2003, cuyo texto es el siguiente:

“Artículo 249.- Pánico Financiero
El que a sabiendas produce alarma en la población propalando noticias falsas atribuyendo a una empresa del sistema financiero, a una empresa del sistema de seguros, a una sociedad administradora de fondos mutuos de inversión en valores o de fondos de inversión, a una administradora privada de fondos de pensiones u otra que opere con fondos del público, cualidades o situaciones de riesgo que generen el peligro de retiros masivos de depósitos o el traslado o la redención de instrumentos financieros de ahorro o de inversión, será reprimido con pena privativa de la libertad no menor de tres ni mayor de seis años y con ciento ochenta a trescientos sesenta y cinco días-multa.

La pena será no menor de cuatro ni mayor de ocho años y de trescientos sesenta a setecientos veinte días-multa si el agente es miembro del directorio, gerente o funcionario de una empresa del sistema financiero, de una empresa del sistema de seguros, de una sociedad administradora de fondos mutuos de inversión en valores o de fondos de inversión, de una administradora privada de fondos de pensiones u otra que opere con fondos del público, o si es miembro del directorio o gerente de una empresa auditora, de una clasificadora de riesgo u otra que preste servicios a alguna de las empresas antes señaladas, o si es funcionario del Ministerio de Economía y Finanzas, el Banco Central de Reserva del Perú, la Superintendencia de Banca y Seguros o la Comisión Nacional Supervisora de Empresas y Valores.

La pena prevista en el párrafo anterior se aplica también a los ex funcionarios del Ministerio de Economía y Finanzas, el Banco Central de Reserva del Perú, la Superintendencia de Banca y Seguros o la Comisión Nacional Supervisora de Empresas y Valores, siempre que hayan cometido delito dentro de los seis años posteriores a la fecha de su cese.” (*)

(*) Artículo modificado por el Artículo 13 de la Ley N° 30822, publicada el 19 julio 2018, la misma que entró en vigencia el 1 de enero de 2019, cuyo texto es el siguiente:

Artículo 249 "Pánico Financiero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

l que a sabiendas produce alarma en la población propalando noticias falsas atribuyendo a una empresa del sistema financiero, a una empresa del sistema de seguros, a una sociedad administradora de fondos mutuos de inversión en valores o de fondos de inversión, a una administradora privada de fondos de pensiones u otra que opere con fondos del público, o a una cooperativa de ahorro y crédito que solo opera con sus socios y que no está autorizada a captar recursos del público u operar con terceros, inscrita en el Registro Nacional de Cooperativas de Ahorro y Crédito No Autorizadas a Captar Recursos del Público, cualidades o situaciones de riesgo que generen el peligro de retiros masivos de depósitos o el traslado o la redención de instrumentos financieros de ahorro o de inversión, es reprimido con pena privativa de la libertad no menor de tres ni mayor de seis años y con ciento ochenta a trescientos sesenta y cinco días-multa.

La pena es no menor de cuatro ni mayor de ocho años y de trescientos sesenta a setecientos veinte días-multa si el agente es miembro del directorio o consejo de administración, gerente o funcionario de una empresa del sistema financiero, de una empresa del sistema de seguros, de una sociedad administradora de fondos mutuos de inversión en valores o de fondos de inversión, de una administradora privada de fondos de pensiones u otra que opere con fondos del público, o de una cooperativa de ahorro y crédito que solo opera con sus socios y que no está autorizada a captar recursos del público u operar con terceros, inscrita en el Registro Nacional de Cooperativas de Ahorro y Crédito No Autorizadas a Captar Recursos del Público; o si es miembro del directorio o gerente de una empresa auditora, de una clasificadora de riesgo u otra que preste servicios a alguna de las empresas antes señaladas, o si es funcionario del Ministerio de Economía y Finanzas, el Banco Central de Reserva del Perú, la Superintendencia de Banca, Seguros y AFP o la Superintendencia del Mercado de Valores.

La pena prevista en el párrafo anterior se aplica también a los ex funcionarios del Ministerio de Economía y Finanzas, el Banco Central de Reserva del Perú, la Superintendencia de Banca, Seguros y AFP o la Superintendencia del Mercado de Valores, siempre que hayan cometido delito dentro de los seis años posteriores a la fecha de su cese”.

Artículo 250 Omisión de las provisiones específicas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los directores, administradores, gerentes y funcionarios, accionistas o asociados de las instituciones bancarias, financieras y demás que operan con fondos del público supervisada por la Superintendencia de Banca y Seguros u otra entidad de regulación y control que hayan omitido efectuar las provisiones específicas para créditos calificados como dudosos o pérdida u otros activos sujetos igualmente a provisión, inducen a la aprobación del órgano social pertinente, a repartir dividendos o distribuir utilidades bajo cualquier modalidad o capitalizar utilidades,  serán reprimidos con pena privativa de libertad no menor de uno ni mayor de tres años y con ciento ochenta a trescientos sesenticinco días-multa.

Artículo 251 Desvío fraudulento de crédito promocional
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que aplica o desvía fraudulentamente un crédito promocional hacia una finalidad distinta a la que motivó su otorgamiento, será reprimido con pena privativa de libertad no mayor de dos años.

Uso indebido de información privilegiada-Formas agravadas

"Artículo 251-A.- El que obtiene un beneficio o se evita un perjuicio de carácter económico en forma directa o a través de terceros, mediante el uso de información privilegiada, será reprimido con pena privativa de la libertad no menor de uno (1) ni mayor de cinco (5) años.

Si el delito a que se refiere el párrafo anterior es cometido por un director, funcionario o empleado de una Bolsa de Valores, de un agente de intermediación, de las entidades supervisoras de los emisores, de las clasificadoras de riesgo, de las administradoras de fondos mutuos de inversión en valores, de las administradoras de fondos de inversión, de las administradoras de fondos de pensiones, así como de las empresas bancarias, financieras o de seguros, la pena no será menor de cinco (5) ni mayor de siete (7) años." (1)

"Previamente a la formalización de la denuncia respectiva, el Ministerio Público deberá requerir un informe técnico a la Superintendencia del Mercado de Valores (SMV), que será emitido dentro del plazo de quince (15) días de solicitado, vencido del cual resolverá.” (2)

(1) Artículo  incorporado por la Novena Disposición Final del Decreto Legislativo Nº 861, publicado el 22 octubre 1996.

(2) Párrafo final incorporado por el Artículo 14 de la Ley Nº 30050, publicada el 26 junio 2013.

“Artículo 251-B.- Manipulación de precios en el mercado de valores

El que proporcione señales falsas o engañosas respecto de la oferta o demanda de un valor o instrumento financiero, en beneficio propio o ajeno, mediante transacciones que suban o bajen el precio, incrementen o reduzcan su liquidez, será reprimido con pena privativa de la libertad no menor de uno (1) ni mayor de cinco (5) años, siempre que el monto de dichas transacciones superen las trescientas (300) Unidades Impositivas Tributarias (UIT) vigentes al momento de la comisión del delito, o el beneficio, pérdida evitada o perjuicio causado supere dicho monto.

La misma pena se aplicará a directores, gerentes, miembros del comité de inversiones, funcionarios y personas vinculadas al proceso de inversión de un inversionista institucional que, en beneficio propio o ajeno, manipulen el precio de su cartera de valores o instrumentos financieros o la administrada por otro inversionista institucional, mediante transacciones, suban o bajen el precio, incrementen o reduzcan la liquidez de los valores o instrumentos financieros que integren dicha cartera.

Previamente a que el Ministerio Público formalice la denuncia respectiva, se deberá contar con un informe técnico emitido por la Comisión Nacional Supervisora de Empresas y Valores (Conasev).”(*)

(*) Artículo incorporado por el Artículo 4 de la Ley Nº 29660, publicado el 04 febrero 2011.

CAPITULO II DELITOS MONETARIOS
##############################

CONCORDANCIAS:      Ley Nº 27583 (Ley que Crea la Oficina Central de Lucha Contra la Falsificación de Numerario (Billetes y Monedas))

Artículo 252 
~~~~~~~~~~~~~

El que, indebidamente, fabrica moneda de curso legal en la República para ponerla en circulación como auténtica o la falsifica para ponerla en circulación por un valor superior, será reprimido con pena privativa de libertad no menor de tres ni mayor de diez años y con ciento ochenta a trescientos sesenticinco días-multa. (*)

(*)  Artículo modificado por el Artículo 1 de la Ley Nº 26714, publicada el 27 diciembre 1996, cuyo texto es el siguiente:

Fabricación y falsificación de moneda de curso legal

"Artículo 252.- El que falsifica billetes o monedas será reprimido con pena privativa de libertad no menor de cinco ni mayor de doce años y con ciento veinte a trescientos días-multa.

El que falsifica billetes o monedas separando el anverso y el reverso de los auténticos, superponiendo sus fragmentos, recurriendo al empleo de disolventes químicos, usando los fabricados por otros países, recurriendo a aleaciones distintas o valiéndose de cualquier otro medio que no fuere de producción masiva, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de diez años y con ciento veinte a trescientos días-multa." (*)

(*) De conformidad con el Artículo 3 de la Ley N° 26714, publicada el 27 diciembre 1996, modificada por el Artículo Único de la Ley Nº 26992 el 12 noviembre 1998, el Banco Central de Reserva será considerado agraviado en el delito que trata el presente Artículo.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

Artículo 253 
~~~~~~~~~~~~~

El que, de cualquier manera, altera la moneda de curso legal en la República, aminorando su valor intrínseco o dándole la apariencia de un valor superior, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años y con noventa a ciento veinte días-multa.(*)

(*)  Artículo modificado por el Artículo 1 de la Ley Nº 26714, publicada el 27 diciembre 1996, cuyo texto es el siguiente:

Alteración de la moneda de curso legal

"Artículo 253.- El que altera los billetes o monedas con el propósito de atribuirles un valor superior, o realiza tal alteración con billetes o monedas que se hallan fuera de circulación o corresponden a otros  países, para darles la apariencia de los que tienen poder cancelatorio, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de diez años y con ciento veinte a trescientos días-multa.

El que altera la moneda, aminorando su valor intrínseco, será reprimido con pena privativa de libertad no menor de seis meses ni mayor de dos años y con treinta a noventa días-multa." (*)

(*) De conformidad con el Artículo 3 de la Ley N° 26714, publicada el 27 diciembre 1996, modificada por el Artículo Único de la Ley Nº 26992 el 12 noviembre 1998, el Banco Central de Reserva será considerado agraviado en el delito que trata el presente Artículo.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

Artículo 254 
~~~~~~~~~~~~~

El que, habiendo recibido como auténtica moneda de curso legal ilícitamente fabricada, falsa o alterada, la expande o pone en circulación, a sabiendas de su ilicitud, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años y con treinta a sesenta días-multa. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 26714, publicada el 27 diciembre 1996, cuyo texto es el siguiente:

Artículo 254.- El que introduce en el territorio de la República o pone en circulación monedas o billetes falsificados o alterados por terceros, así como el que promueve tales actos, será reprimido con pena privativa de libertad o (*) NOTA SPIJ menor de cuatro ni mayor de diez años y con ciento ochenta a trescientos sesenta y cinco días-multa.

El que habiendo recibido como auténticos o intactos billetes o monedas falsificados o alterados, los pone en circulación, a sabiendas de su ilicitud, será reprimido con pena privativa de libertad no menor de seis ni mayor de dieciocho meses y con treinta a sesenta días-multa. (*)

(*)  Artículo modificado por el Artículo 1 de la Ley Nº 27593, publicada el 13 diciembre 2001, cuyo texto es el siguiente:

Tráfico de moneda falsa

"Artículo 254.- El que a sabiendas, introduce, transporta o retira del territorio de la República; comercializa, distribuye o pone en circulación monedas o billetes falsificados o alterados por terceros, cuyo valor nominal supere una remuneración mínima vital, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años y con ciento ochenta a trescientos sesenta y cinco días multa. La pena será de ciento ochenta a trescientos sesenta y cinco días-multa, si el valor nominal es menor a una remuneración mínima vital." (*)

(*) De conformidad con el Artículo 3 de la Ley N° 26714, publicada el 27 diciembre 1996, modificada por el Artículo Único de la Ley Nº 26992 el 12 noviembre 1998, el Banco Central de Reserva será considerado agraviado en el delito que trata el presente Artículo.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

Artículo 255 
~~~~~~~~~~~~~

Cuando la fabricación ilícita, la falsificación, la alteración de moneda de curso legal sea de muy poca gravedad, por su valor nominal o número, o por la facilidad con que pueda ser reconocida, la pena privativa de libertad no mayor de un año o prestación de servicio comunitario de veinte a treinta jornadas. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 26714, publicada el 27 diciembre 1996, cuyo texto es el siguiente:

Artículo 255.- El que fabrica o introduce en el territorio de la República, máquinas, cuños o cualquier otra clase de instrumentos o insumos destinados a la falsificación de billetes o monedas o que, a sabiendas, los conserva en su poder, será reprimido con pena privativa de libertad no menor de cinco ni mayor de doce años y con ciento ochenta a trescientos sesenta y cinco días-multa. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 27593, publicada el 13 diciembre 2001, cuyo texto es el siguiente:

Fabricación o introducción en el territorio de la República de instrumentos destinados a la falsificación de billetes o monedas

"Artículo 255.- El que fabrica, introduce en el territorio de la República o retira de él, máquinas, matrices, cuños o cualquier otra clase de instrumentos o insumos destinados a la falsificación de billetes o monedas o se encuentra en posesión de uno o más pliegos de billetes falsificados, o extrae de un billete auténtico medidas de seguridad, con el objeto de insertarlas en uno falso o alterado, o que, a sabiendas, los conserva en su poder será reprimido con pena privativa de libertad no menor de cinco ni mayor de doce años y con ciento ochenta a trescientos sesenta y cinco días multa." (*)

(*) De conformidad con el Artículo 3 de la Ley N° 26714, publicada el 27 diciembre 1996, modificada por el Artículo Único de la Ley Nº 26992 el 12 noviembre 1998, el Banco Central de Reserva será considerado agraviado en el delito que trata el presente Artículo.

Artículo 256 
~~~~~~~~~~~~~

El director, gerente o funcionario del Banco Central de Reserva que, facultado para emitir moneda de curso legal, lo haga con características distintas a las determinadas por ley o emita en cantidad superior a la autorizada y haga o deje circular el excedente, será reprimido con pena privativa de libertad no menor de dos ni mayor de seis años e inhabilitación de uno a cuatro años conforme al artículo 36, incisos 1 y 2.

Los funcionarios que autoricen la emisión de los documentos a que se refiere el artículo 261, en cantidad superior a la autorizada, serán reprimidos con la misma pena. (*)

(*)  Artículo modificado por el Artículo 1 de la Ley Nº 26714, publicada el 27 diciembre 1996, cuyo texto es el siguiente:

Alteración de billetes o monedas

"Artículo 256.- Será reprimido con pena de multa no menor de treinta ni mayor de ciento veinte días-multa:

1.- El que escribe sobre billetes, imprime sellos en ellos o de cualquier manera daña intencionalmente billetes o monedas.

2.- El que, con fines publicitarios o análogos, reproduce o distribuye billetes o monedas, o el anverso o reverso de ellos, de modo que pueda generar confusión o propiciar que las reproducciones sean utilizadas por terceros como si se tratase de billetes auténticos." (*)

(*) De conformidad con el Artículo 3 de la Ley N° 26714, publicada el 27 diciembre 1996, modificada por el Artículo Único de la Ley Nº 26992 el 12 noviembre 1998, el Banco Central de Reserva será considerado agraviado en el delito que trata el presente Artículo.

Artículo 257 
~~~~~~~~~~~~~

El que, a sabiendas, fabrica o introduce en el territorio de la República, o conserva en su poder máquinas, cuños o cualquier otra clase de útiles o instrumentos, destinados a la falsificación de moneda, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años y con ciento ochenta a trescientos sesenticinco días-multa. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 26714, publicada el 27 diciembre 1996, cuyo texto es el siguiente:

"Artículo 257.- Las disposiciones de los artículos precedentes son extensivas a los billetes, monedas, valores y títulos valores de otros países." (*)

(*)  Artículo modificado por el Artículo 1 de la Ley Nº 27593, publicada el 13 diciembre 2001, cuyo texto es el siguiente:

Aplicación extensiva

"Artículo 257.- Las disposiciones de los artículos de este Capítulo se hacen extensivas a los billetes, monedas, valores y títulos valores de otros países." (*)

(*) De conformidad con el Artículo 3 de la Ley N° 26714, publicada el 27 diciembre 1996, modificada por el Artículo Único de la Ley Nº 26992 el 12 noviembre 1998, el Banco Central de Reserva será considerado agraviado en el delito que trata el presente Artículo.

“Artículo 257-A.- Será reprimido con pena privativa de libertad no menor de seis años ni mayor de catorce años y con ciento ochenta a trescientos sesenta y cinco días-multa el que comete los delitos establecidos en los Artículos 252, 253, 254, 255 y 257 si concurriera cualquiera de las siguientes circunstancias agravantes:

1. Si el agente obra como miembro de una asociación delictiva o en calidad de integrante de una banda. (*)

(*) Extremo modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 30077, publicada el 20 agosto 2013, la misma que entró en vigencia el 1 de julio de 2014, cuyo texto es el siguiente:

"Artículo 257-A. - Formas agravadas

Será reprimido con pena privativa de libertad no menor de seis ni mayor de catorce años y con ciento ochenta a trescientos sesenta y cinco días-multa el que comete los delitos establecidos en los artículos 252, 253, 254, 255 y 257, si concurriera cualquiera de las siguientes circunstancias agravantes:

1. Si el agente actúa como integrante de una organización criminal."

2. Si el agente labora o ha laborado en imprentas o talleres gráficos o en la industria metalmecánica y se ha valido de su conocimiento para perpetrar el delito.

3. Si el agente labora o ha laborado en el Banco Central de Reserva del Perú y se ha valido de esa circunstancia para obtener información privilegiada, sobre los procesos de fabricación y las medidas de seguridad, claves o marcas secretas de las monedas o billetes.

4. Si para facilitar la circulación de monedas o billetes falsificados, el agente los mezcla con monedas o billetes genuinos.” (*)

(*) Artículo incorporado por el Artículo 2 de la Ley Nº 27593, publicada el 13 diciembre 2001.

CONCORDANCIAS:     Ley N° 26714, Art. 3

Artículo 258 
~~~~~~~~~~~~~

Las disposiciones de los artículos 252 a 257 son aplicables a la moneda de procedencia extranjera.(*)

(*)  Artículo modificado por el Artículo 1 de la Ley Nº 26714, publicada el 27 diciembre 1996, cuyo texto es el siguiente:

Emisión ilegal de billetes y otros

"Artículo 258.- El funcionario del Banco Central de Reserva del Perú que emita numerario en exceso de las cantidades autorizadas, será reprimido con pena privativa de libertad no menor de dos ni mayor de seis años e inhabilitación de uno a cuatro años conforme al Artículo 36, incisos 1) y 2)." (*)

(*) De conformidad con el Artículo 3 de la Ley N° 26714, publicada el 27 diciembre 1996, modificada por el Artículo Único de la Ley Nº 26992 el 12 noviembre 1998, el Banco Central de Reserva será considerado agraviado en el delito que trata el presente Artículo.

Artículo 259 Uso ilegal de divisas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que destina las divisas asignadas por el Banco Central de Reserva, a fin distinto del señalado y autorizado, será reprimido con pena privativa de libertad no menor de dos ni mayor de diez años, con ciento veinte a trescientos sesenticinco días-multa e inhabilitación conforme al artículo 36, incisos 1, 2 y 4.

Los directores, gerentes y funcionarios del Banco Central de Reserva o funcionarios públicos que faciliten la comisión del delito, serán reprimidos con la misma pena. (*)

(*) De conformidad con el Artículo 3 de la Ley N° 26714, publicada el 27 diciembre 1996, modificada por el Artículo Único de la Ley Nº 26992 el 12 noviembre 1998, el Banco Central de Reserva será considerado agraviado en el delito que trata el presente Artículo.

Artículo 260 Retención indebida de divisas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, teniendo obligación de hacerlo, no entrega, indebidamente, al Banco Central de Reserva las divisas generadas por exportaciones o las retiene, injustificadamente, luego de vencido el plazo establecido, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años, con ciento ochenta a trescientos sesenticinco días-multa e inhabilitación conforme al artículo 36, incisos 1, 2 y 4. (*)

(*) De conformidad con el Artículo 3 de la Ley N° 26714, publicada el 27 diciembre 1996, modificada por el Artículo Único de la Ley Nº 26992 el 12 noviembre1998, el Banco Central de Reserva será considerado agraviado en el delito que trata el presente Artículo.

Artículo 261 
~~~~~~~~~~~~~

Para los efectos de este Capítulo, quedan equiparados a la moneda los títulos de la deuda pública, los bonos, pagarés, cédulas, cupones, acciones o valores emitidos por el Estado o por instituciones o entidades en que éste tenga parte.(*)

(*)  Artículo modificado por el Artículo 1 de la Ley Nº 26714, publicada el 27 diciembre 1996, cuyo texto es el siguiente:

Valores equiparados a moneda

"Artículo 261.- Para los efectos de este Capítulo quedan equiparados a los billetes y monedas, los títulos de la deuda pública, bonos, cupones, cédulas, libramientos, acciones y otros valores o títulos-valores emitidos por el Estado o por personas de derecho público."

TITULO XI DELITOS TRIBUTARIOS
=============================

CAPITULO I CONTRABANDO (*)
##########################

(*) Capítulo I derogado por la Tercera Disposición Final de la Ley Nº 26461, publicada el 08 junio 1995.

Artículo 262 
~~~~~~~~~~~~~

El que, aludiendo el control fiscal, ingresa del extranjero o extrae mercancías del territorio nacional, cuyo valor sea mayor de diez remuneraciones (*) RECTIFICADO POR FE DE ERRATAS mínimas vitales, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años y con ciento ochenta a trescientos sesenticinco días-multa.

Artículo 263 
~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años y con ciento ochenta a trescientos sesenticinco días-multa, el que cometa delito de contrabando, bajo las siguientes modalidades:

1.- Interna mercancías extranjeras procedentes de las zonas geográficas nacionales de tributación menor hacia otras sin el previo pago de reintegro de ley o sin autorización oficial pendiente.

2.- Extrae mercancías de los recintos fiscales o fiscalizados sin que le hayan sido entregadas legalmente por las autoridades respectivas.

3.- Posee, a sabiendas, mercancías extranjeras de tráfico prohibido.

Artículo 264 
~~~~~~~~~~~~~

La pena privativa de libertad será no menor de tres ni mayor de ocho años cuando:

1.- Las mercancías objeto del delito sean armas de fuego, municiones y explosivos.

2.- Se utiliza violencia para perpetrar o facilitar el delito o para evitar su descubrimiento.

3.- Los hechos son (*) RECTIFICADO POR FE DE ERRATAS cometidos por dos o más personas, o el agente integra una organización destinada al contrabando. (*)

(*) Capítulo I derogado por la Tercera Disposición Final de la Ley N° 26461, publicada el 08 junio 1995.

CAPITULO  II DEFRAUDACION  FISCAL
#################################

SECCION  I  (*)
DEFRAUDACION  DE RENTAS  DE  ADUANAS

(*) Sección I derogada por la Tercera Disposición Final de la Ley N° 26461, publicada el 08 junio 1995.

Artículo 265 
~~~~~~~~~~~~~

El que, mediante trámite aduanero, elude el pago, total o parcial, de tributos a la importación o exportación de mercancías, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años y con ciento ochenta a trescientos sesenticinco días-multa.

Artículo 266 
~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años, el que comete el delito a que se refiere el artículo 265, bajo cualquiera de las modalidades siguientes:

1.- Importar o exportar mercancías con documentos adulterados o datos falsos con relación a su valor, calidad, cantidad, peso u otras características cuya información incida en la tributación.

2.- Simular importación o exportación de mercancías no trasladadas realmente a fin de obtener beneficios tributarios o de cualquier índole que otorgue el Estado.

3.- Subvaluación o sobrevaluación sobre el precio de las mercancías, o la aplicación de aranceles menores a los que corresponden para obtener beneficios indebidos.

4.- No retornar al lugar de origen mercancías, materia de importación o exportación temporal.

5.- Importar mercancías (*) RECTIFICADO POR FE DE ERRATAS distintas a las que fueran exceptuadas de tributación por ley.

6.- Consumir, utilizar o disponer de las mercancías trasladadas legalmente para su aforo fuera de los recintos aduaneros, sin el pago previo de los tributos.

7.- Comercializar, sin autorización, las mercancías ingresadas mediante el régimen de equipaje exento de tributos.

Artículo 267 
~~~~~~~~~~~~~

La pena privativa de libertad será no menor de tres ni mayor de ocho años cuando:

1.- Las mercancías objeto del delito sean armas de fuego, municiones o explosivos.

2.- Se utilice violencia para perpetrar o facilitar el delito, o para evitar el delito o su descubrimiento.

3.- Se destine a personas naturales o jurídicas inexistentes, en los documentos referentes al despacho de las mercancías importadas.

4.- Los hechos son cometidos por dos o más personas o el agente integra una organización destinada a la defraudación. (*)

(*) Sección I derogada por la Tercera Disposición Final de la Ley N° 26461, publicada el 08 junio 1995.

SECCION  II

DEFRAUDACION TRIBUTARIA

CONCORDANCIAS:     D.Leg. N° 813 (Ley Penal Tributaria)
Ley N° 27765, Art. 6

Artículo 268 
~~~~~~~~~~~~~

El que, en provecho propio o de un tercero, valiéndose de cualquier artificio, engaño, astucia, ardid u otra forma fraudulenta, deja de pagar en todo o en parte los tributos que establecen las leyes, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años o multa de sesenta a trescientos sesenticinco días-multa. (*)

(*) Artículo sustituido por el Artículo 2 del Decreto Ley Nº 25859, publicado el 24 noviembre 1992, cuyo texto es el siguiente:

Defraudación tributaria

"Artículo 268.- El que, en provecho propio o de un tercero, valiéndose de cualquier artificio, engaño, astucia, ardid u otra forma fraudulenta, deja de pagar en todo o en parte los tributos que establecen las leyes, será reprimido con pena privativa de libertad no menor de uno ni mayor de seis años y con ciento ochenta a trescientos sesenta y cinco días multa." (*)

(*) Artículo derogado por la Sétima Disposición Final y Transitoria del Decreto Legislativo 813, publicado el 20 abril 1996.

Artículo 269 Modalidades
~~~~~~~~~~~~~~~~~~~~~~~~

Son modalidades de defraudación tributaria y reprimidas con la pena del artículo anterior:

1.- Presentar declaraciones, comunicaciones o documentos falsos o falsificados que anulen o reduzcan la materia imponible.

2.- Ocultar, total o parcialmente, bienes, ingresos, rentas, frutos o productos o consignar pasivos total o parcialmente falsos, para anular o reducir el impuesto a pagar.

3.- Realizar actos fraudulentos en los libros de contabilidad; estados contables y declaraciones juradas, en perjuicio del acreedor tales como: alteración, raspadura o tacha de anotaciones, asientos o constancias hechas en los libros, así como la inscripción de asientos, cuentas, nombres, cantidades o datos falsos.

4.- Ordenar o consentir la comisión de los actos fraudulentos a que se contrae el inciso 3.

5.- Destruir total o parcialmente los libros de contabilidad y otros exigidos por las normas tributarias o los documentos relacionados con la tributación.

6.- No entregar al acreedor tributario el monto de las retenciones o percepciones de tributos que se hubieren efectuado, dentro de los tres meses siguientes al vencimiento del plazo que, para hacerlo, fijen las leyes y reglamentos pertinentes.

7.- No pagar los tributos a su cargo durante el ejercicio gravable, que en conjunto, excedan de cinco unidades impositivas tributarias vigentes al inicio de dicho ejercicio. (*)

(*) Inciso modificado por el Artículo 1 del Decreto Ley Nº 25495, publicado el 14 mayo1992, cuyo texto es el siguiente:

"7. No pagar intencionalmente los tributos a su cargo durante el ejercicio gravable que, en conjunto, excedan de 5 unidades de referencia tributarias vigentes al inicio de dicho ejercicio". (*)

(*) Inciso sustituido por el Artículo 3 del Decreto Ley Nº 25859, publicado el  24 noviembre 1992, cuyo texto es el siguiente:

"7. No pagar intencionalmente los tributos a su cargo durante el ejercicio gravable, que en conjunto, excedan de 5 Unidades Impositivas Tributarias vigentes al inicio de dicho ejercicio."

"8. Obtener exoneraciones tributarias, reintegros o devoluciones de impuestos de cualquier naturaleza simulando la existencia de hechos que permitan gozar de tales beneficios." (*)

(*) Inciso incorporado por el Artículo 3 del Decreto Ley Nº 25859, publicado el  24 noviembre 1992.

"9. Simular o provocar estados de insolvencia patrimonial que imposibiliten el cobro de tributos una vez iniciado el proceso administrativo o judicial." (1)(2)

(1) Inciso incorporado por el Artículo 3 del Decreto Ley Nº 25859, publicado el  24 noviembre 1992.

(2) Artículo derogado por la Sétima Disposición Final y Transitoria del Decreto Legislativo 813, publicado el 20 abril 1996.

Artículo 270 
~~~~~~~~~~~~~

No procede la acción penal si el agente paga el tributo dentro de los diez días siguientes a la fecha en que conoce el requerimiento de la autoridad administrativa correspondiente. (*)

(*) Artículo derogado por el Artículo Segundo del Decreto Ley Nº 25495, publicado el 14 mayo 1992.

CAPITULO III ELABORACION Y COMERCIO CLANDESTINO DE PRODUCTOS
############################################################

Artículo 271 Elaboración clandestina de productos 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años, sin perjuicio del decomiso cuando ello proceda, el que:

1. Elabora mercaderías gravadas cuya producción, sin autorización, esté prohibida.

2. Habiendo cumplido los requisitos establecidos, realiza la elaboración de dichas mercaderías con maquinarias, equipos o instalaciones ignoradas por la autoridad o modificados sin conocimiento de ésta.

3. Ocultar la producción o existencia de estas mercaderías.

Artículo 272 
~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no mayor de un año, el que: (*) RECTIFICADO POR FE DE ERRATAS

1. Se dedique a una actividad comercial sujeta a autorización sin haber cumplido los requisitos que exijan las leyes o reglamentos respecto de los productos que se refiere el artículo.

2. Emplee, expenda o haga circular mercaderías y productos sin el timbre o precinto correspondiente, cuando deban llevarlo o sin acreditar el pago del tributo.

3. Utilice mercaderías exoneradas de tributos en fines distintos de los previstos en la ley exonerativa respectiva. (*)

(*) Artículo sustituido por la Octava Disposición Final y Transitoria de la Ley Nº 27335, publicada el 31 julio 2000, cuyo texto es el siguiente:

Comercio clandestino

"Artículo 272.- Será reprimido con pena privativa de libertad no menor de 1 (un) año ni mayor de 3 (tres) años y con 170 (ciento setenta) a 340 (trescientos cuarenta) días-multa, el que:

1. Se dedique a una actividad comercial sujeta a autorización sin haber cumplido los requisitos que exijan las leyes o reglamentos.

2. Emplee, expenda o haga circular mercaderías y productos sin el timbre o precinto correspondiente, cuando deban llevarlo o sin acreditar el pago del tributo.

3. Utilice mercaderías exoneradas de tributos en fines distintos de los previstos en la ley exonerativa respectiva.

En el supuesto previsto en el inciso 3), constituirá circunstancia agravante sancionada con pena privativa de libertad no menor de 5 (cinco) ni mayor de 8 (ocho) años y con 365 (trescientos sesenta y cinco) a 730 (setecientos treinta) días-multa, cuando la conducta descrita se realice:

a) Por el Consumidor Directo de acuerdo con lo dispuesto en las normas tributarias;
b) Utilizando documento falso o falsificado; o
c) Por una organización delictiva.” (*)

(*) Artículo modificado por la Tercera Disposición Complementaria Final del Decreto Legislativo Nº 1103, publicado el 04 marzo 2012, cuyo texto es el siguiente:

“Comercio Clandestino

Artículo 272.- Será reprimido con pena privativa de libertad no menor de 1 (un) año ni mayor de 3 (tres) años y con 170 (ciento setenta) a 340 (trescientos cuarenta) días-multa, el que:

1. Se dedique a una actividad comercial sujeta a autorización sin haber cumplido los requisitos que exijan las leyes o reglamentos.

2. Emplee, expenda o haga circular mercaderías y productos sin el timbre o precinto correspondiente, cuando deban llevarlo o sin acreditar el pago del tributo.

3. Utilice mercaderías exoneradas de tributos en fines distintos de los previstos en la ley exonerativa respectiva.

4. Evada el control fiscal en la comercialización, transporte o traslado de bienes sujetos a control y fiscalización dispuesto por normas especiales.

5. Utilice rutas distintas a las rutas fiscales en el transporte o traslado de bienes, insumos o productos sujetos a control y fiscalización.

En los supuestos previstos en los incisos 3), 4) y 5), constituirán circunstancias agravantes sancionadas con pena privativa de libertad no menor de 5 (cinco) ni mayor de 8 (ocho) años y con 365 (trescientos sesenta y cinco) a 730 (setecientos treinta) días-multa, cuando cualquiera de las conductas descritas se realice: (*)

(*) Extremo modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 30077, publicada el 20 agosto 2013, la misma que entró en vigencia el 1 de julio de 2014, cuyo texto es el siguiente:

"En los supuestos previstos en los incisos 3), 4) y 5) constituyen circunstancias agravantes sancionadas con pena privativa de libertad no menor de cinco ni mayor de ocho años y con trescientos sesenta y cinco a setecientos treinta días-multa, cuando cualquiera de las conductas descritas se realice:"

a) Por el Consumidor Directo de acuerdo con lo dispuesto en las normas tributarias;

b) Utilizando documento falso o falsificado; o

c) Por una organización delictiva; o (*)

(*) Literal modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 30077, publicada el 20 agosto 2013, la misma que entró en vigencia el 1 de julio de 2014, cuyo texto es el siguiente:

"c) Por una organización criminal;"

d) En los supuestos 4) y 5), si la conducta se realiza en dos o más oportunidades dentro de un plazo de 10 años.”

CONCORDANCIAS:     D.S. N° 016-2014-EM, Art. 13 (Controles especiales del transporte de Hidrocarburos en las zonas geográficas sujetas al Régimen Complementario de Control de Insumos Químicos)

TITULO  XII DELITOS  CONTRA  LA  SEGURIDAD  PUBLICA
===================================================

CAPITULO  I DELITOS DE PELIGRO COMUN
####################################

Artículo 273 Peligro por medio de incendio o explosión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que crea un peligro común para las personas o los bienes mediante incendio, explosión o liberando cualquier clase de energía, será reprimido con pena privativa de libertad no menor de tres ni mayor de diez años.

“Artículo 273-A. Producción de peligro en el servicio público de transporte de pasajeros

El que presta el servicio público de transportes de pasajeros y/o el que conduce vehículos de dicho servicio, con o sin habilitación otorgada por la autoridad competente, que pueda generar un peligro para la vida, la salud o la integridad física de las personas al no cumplir con los requisitos de ley para circular y que, además, dicho vehículo no cuente con el correspondiente Seguro Obligatorio de Accidentes de Tránsito vigente o no haya pasado la última inspección técnica vehicular, será reprimido con pena privativa de libertad no menor de uno (1) ni mayor de tres (3) años e inhabilitación conforme al artículo 36, inciso 7.”(*)

(*) Artículo 273-A) incorporado por la Sexta Disposición Complementaria Modificatoria del Decreto de Urgencia N° 019-2020, publicado el 24 enero 2020.

Artículo 274 
~~~~~~~~~~~~~

El conduce vehículo motorizado en estado de ebriedad o drogadicción será reprimido con prestación de servicio comunitario no mayor de veinte jornadas e inhabilitación conforme al artículo 36, inciso 7), hasta por seis meses. (*)

(*) Artículo modificado por el Artículo Unico de la Ley Nº 27054, publicada el 23 enero 1999, cuyo texto es el siguiente:

Conducción en estado de ebriedad o drogadicción

"Artículo 274.- El que encontrándose en estado de ebriedad o drogadicción conduce, opera o maniobra vehículo motorizado, instrumento, herramienta, máquina u otro análogo, será reprimido con pena privativa de la libertad no mayor de un año e inhabilitación según el Artículo 36 incisos 6) y 7).

Cuando el agente presta servicios de transporte público de pasajeros o de transporte pesado, la pena privativa de libertad será no menor de uno ni mayor de dos años e inhabilitación conforme al Artículo 36 incisos 6) y 7)." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 27753, publicada el 09 junio 2002, cuyo texto es el siguiente:

"Artículo 274.- Conducción en estado de ebriedad o Drogadicción

El que encontrándose en estado de ebriedad, con presencia de alcohol en la sangre en proporción mayor de 0.5 gramos-litro, o bajo el efecto de estupefacientes, conduce, opera o maniobra vehículo motorizado, instrumento, herramienta, máquina u otro análogo, será reprimido con pena privativa de la libertad no mayor de un año o treinta días-multa como mínimo y cincuenta días-multa como máximo e inhabilitación, según corresponda, conforme al Artículo 36, incisos 6) y 7).

Cuando el agente presta servicios de transporte público de pasajeros o de transporte pesado, la pena privativa de libertad será no menor de uno ni mayor de dos años o cincuenta días-multa como mínimo y cien días-multa como máximo e inhabilitación conforme al Artículo 36 incisos 6) y 7).” (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 29439, publicada el 19 noviembre 2009, cuyo texto es el siguiente:

"Artículo 274.- Conducción en estado de ebriedad o drogadicción

El que encontrándose en estado de ebriedad, con presencia de alcohol en la sangre en proporción mayor de 0.5 gramos-litro, o bajo el efecto de drogas tóxicas, estupefacientes, sustancias psicotrópicas o sintéticas, conduce, opera o maniobra vehículo motorizado, será reprimido con pena privativa de la libertad no menor de seis meses ni mayor de dos años o con prestación de servicios comunitarios de cincuenta y dos a ciento cuatro jornadas e inhabilitación, conforme al artículo 36 inciso 7).

Cuando el agente presta servicios de transporte público de pasajeros, mercancías o carga en general, encontrándose en estado de ebriedad, con presencia de alcohol en la sangre en proporción superior de 0.25 gramos-litro, o bajo el efecto de drogas tóxicas, estupefacientes, sustancias psicotrópicas o sintéticas, la pena privativa de libertad será no menor de uno ni mayor de tres años o con prestación de servicios comunitarios de setenta a ciento cuarenta jornadas e inhabilitación conforme al artículo 36, inciso 7)."

PROCESOS CONSTITUCIONALES

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

“Artículo 274-A.- Manipulación en estado de ebriedad o drogadicción

El que encontrándose en estado de ebriedad, con presencia de alcohol en la sangre en proporción mayor de 0.5 gramos-litro, o bajo el efecto de estupefacientes, drogas tóxicas, sustancias psicotrópicas o sintéticas, opera o maniobra instrumento, herramienta, máquina u otro análogo que represente riesgo o peligro, será reprimido con pena privativa de la libertad no menor de seis meses ni mayor de un año o treinta días-multa como mínimo a cincuenta días-multa como máximo e inhabilitación, conforme al artículo 36, inciso 4)." (*)

(*) Artículo incorporado por el Artículo 2 de la Ley N° 29439, publicada el 19 noviembre 2009.

Artículo 275 Formas agravadas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La pena será privativa de libertad no menor de seis ni mayor de quince años cuando en la comisión del delito previsto en el artículo 273 concurre cualquiera de las siguientes circunstancias:

1. Si hay peligro de muerte para las personas.

2. Si el incendio provoca explosión o destruye bienes de valor científico, histórico, artístico, cultural, religioso, asistencial, militar o de importancia económica.

3. Si resultan lesiones graves o muerte y el agente pudo prever estos resultados.

Artículo 276 Estragos especiales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que causa estragos por medio de inundación, desmoronamiento, derrumbe o por cualquier otro medio análogo, será reprimido conforme a la pena señalada en los artículos 273 y 275, según el caso.

Artículo 277 Daños de obras para la defensa común
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que daña o inutiliza diques u obras destinadas a la defensa común contra desastres, perjudicando su función preventiva, o el que, para impedir o dificultar las tareas de defensa, sustrae, oculta, destruye o inutiliza materiales, instrumentos u otros medios destinados a la defensa común, será reprimido con pena privativa de libertad no menor de tres ni mayor de ocho años.

Artículo 278 Formas culposas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, por culpa, ocasiona un desastre de los previstos en los artículos 273, 275 y 276, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años.

Artículo 279 
~~~~~~~~~~~~~

El que, ilegítimamente, fabrica, almacena, suministra o tiene en su poder bombas, armas, municiones o materiales explosivos, inflamables, asfixiantes o tóxicos o sustancias o materiales destinados para su preparación, será reprimido con pena privativa de libertad no menor de tres ni mayor de diez años. (*)

(*) Artículo modificado por la Primera Disposición Complementaria del Decreto Legislativo Nº 898, publicado el 27 mayo 1998, expedido con arreglo a la Ley N° 26950, que otorga al Poder Ejecutivo facultades para legislar en materia de seguridad nacional, cuyo texto es el siguiente:

Fabricación, suministro o  tenencia de materiales peligrosos

"Artículo 279.- El que, ilegítimamente, fabrica, almacena, suministra o tiene en su poder bombas, armas, municiones o materiales explosivos, inflamables, asfixiantes o tóxicos o sustancias o materiales destinados para su preparación, será reprimido con pena privativa de libertad no menor de seis ni mayor de quince años." (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 30076, publicada el 19 agosto 2013, cuyo texto es el siguiente:

"Artículo 279. Fabricación, suministro o tenencia de materiales peligrosos

El que, sin estar debidamente autorizado, fabrica, almacena, suministra, comercializa, ofrece o tiene en su poder bombas, armas, armas de fuego artesanales, municiones o materiales explosivos, inflamables, asfixiantes o tóxicos o sustancias o materiales destinados para su preparación, será reprimido con pena privativa de libertad no menor de seis ni mayor de quince años."(*)

(*) El presente artículo quedó modificado por la Primera Disposición Complementaria Modificatoria de la Ley N° 30299, publicada el 22 enero 2015, a partir de la publicación de su reglamento, cuyo texto es el siguiente:

“Artículo 279. Fabricación, suministro o tenencia de materiales peligrosos

El que, sin estar debidamente autorizado, fabrica, ensambla, almacena, suministra, comercializa, ofrece o tiene en su poder bombas, armas, armas de fuego artesanales, municiones o materiales explosivos, inflamables, asfixiantes o tóxicos o sustancias o materiales destinados para su preparación, será reprimido con pena privativa de libertad no menor de seis ni mayor de quince años, e inhabilitación conforme al inciso 6 del artículo 36 del Código Penal”.(*)

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 279. Fabricación, suministro o tenencia de materiales peligrosos y residuos peligrosos

El que, sin estar debidamente autorizado, fabrica, ensambla, modifica, almacena, suministra, comercializa, ofrece o tiene en su poder bombas, armas, municiones o materiales explosivos, inflamables, asfixiantes o tóxicos o sustancias o materiales destinados para su preparación, será reprimido con pena privativa de libertad no menor de seis ni mayor de quince años, e inhabilitación conforme al inciso 6 del artículo 36 del Código Penal.

Será sancionado con la misma pena el que presta o alquila, sin la debida autorización, las armas a las que se hacen referencia en el primer párrafo.

El que trafica con armas de fuego, armas de fuego artesanales, bombas, municiones o materiales explosivos, inflamables, asfixiantes o tóxicos o sustancias o materiales destinados para su preparación, será reprimido con pena privativa de libertad no menor de seis ni mayor de quince años, e inhabilitación conforme al inciso 6 del artículo 36 del Código Penal.

El que, sin estar debidamente autorizado, transforma o transporta materiales y residuos peligrosos sólidos, líquidos, gaseosos u otros, que ponga en peligro la vida, salud, patrimonio público o privado y el medio ambiente, será sancionado con la misma pena que el párrafo anterior." (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1244, publicado el 29 octubre 2016, cuyo texto es el siguiente:

“Artículo 279. Fabricación, suministro o tenencia de materiales peligrosos y residuos peligrosos

El que, sin estar debidamente autorizado, fabrica, ensambla, modifica, almacena, suministra, comercializa, ofrece o tiene en su poder bombas, artefactos o materiales explosivos, inflamables, asfixiantes o tóxicos o sustancias o materiales destinados para su preparación, será reprimido con pena privativa de libertad no menor de seis ni mayor de quince años, e inhabilitación conforme al inciso 6 del artículo 36 del Código Penal.

Será sancionado con la misma pena el que presta o alquila, los bienes a los que se hacen referencia en el primer párrafo.

El que trafica con bombas, artefactos o materiales explosivos, inflamables, asfixiantes o tóxicos o sustancias o materiales destinados para su preparación, será reprimido con pena privativa de libertad no menor de seis ni mayor de quince años, e inhabilitación conforme al inciso 6 del artículo 36 del Código Penal.

El que, sin estar debidamente autorizado, transforma o transporta materiales y residuos peligrosos sólidos, líquidos, gaseosos u otros, que ponga en peligro la vida, salud, patrimonio público o privado y el medio ambiente, será sancionado con la misma pena que el párrafo anterior."

CONCORDANCIAS:     Ley N° 28397, Art. 4
Ley N° 27378, Art. 1, num. 2)

Ley Nº 30077, Art. 3 (Delitos comprendidos)

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Producción, desarrollo y comercialización ilegal de armas químicas

"Artículo 279-A.- El que produce,desarrolla, comercializa, almacena, vende, adquiere, usa o posee armas químicas, -contraviniendo las prohibiciones establecidas en la Convención sobre Armas Químicas adoptada por las Naciones Unidas en 1992- o las que transfiere a otro, o el que promueve, favorece o facilita que se realicen dichos actos será reprimido con pena privativa de libertad no menor de cinco ni mayor de veinte años." (*)

(*) Artículo incorporado por  el Artículo 5 de la Ley Nº 26672, publicada el 20 octubre 1996.

"El que ilegítimamente se dedique a la fabricación, importación, exportación, trasferencia, comercialización, intermediación, transporte, tenencia, ocultamiento, usurpación, porte y use ilícitamente armas, municiones, explosivos de guerra y otros materiales relacionados, será reprimido con pena privativa de libertad no menor de diez ni mayor de veinte años." (*)

(*) Párrafo incorporado por la Sexta Disposición Complementaria de la Ley N° 28627, publicada el 22 Noviembre 2005. 

"La pena será no menor de veinte ni mayor de treinta y cinco años si a consecuencia del empleo de las armas descritas en el párrafo precedente se causare la muerte o lesiones graves de la víctima o de terceras personas.” (*)

(*) Párrafo incorporado por la Sexta Disposición Complementaria de la Ley N° 28627, publicada el 22 Noviembre 2005. 

CONCORDANCIAS:     Ley N° 27378, Art. 1, num. 2)
Ley Nº 30077, Art. 3 (Delitos comprendidos)

"Artículo 279-B.- Sustracción o arrebato de armas de fuego

El que sustrae o arrebate armas de fuego en general, o municiones y granadas de guerra o explosivos a miembros de las Fuerzas Armadas o de la Policía Nacional o de Servicios de Seguridad, será reprimido con pena privativa de libertad no menor de diez ni mayor de veinte años.

La pena será de cadena perpetua si a consecuencia del arrebato o sustracción del arma o municiones a que se refiere el párrafo precedente, se causare la muerte o lesiones graves de la víctima o de terceras personas." (*)

(*) Artículo incorporado por la Segunda Disposición Complementaria del Decreto Legislativo Nº 898, publicado el 27-05-98, expedido con arreglo a la Ley N° 26950, que otorga al Poder Ejecutivo facultades para legislar en materia de seguridad nacional.

CONCORDANCIAS:     Ley N° 27378, Art. 1, num. 2)
Ley Nº 30077, Art. 3 (Delitos comprendidos)

“Artículo 279-C.- El que ilegítimamente fabrica, importa, exporta, deposita, transporta, comercializa o usa productos pirotécnicos de cualquier tipo, o los que vendan estos productos a menores de edad, serán reprimidos con pena privativa de libertad no menor de cuatro ni mayor de ocho años y trescientos sesenta y cinco días multa.

La pena será no menor de cinco ni mayor de diez años, si a causa de la fabricación, importación, depósito, transporte, comercialización y uso de productos pirotécnicos, se produjesen lesiones graves o muerte de personas.” (1)(2)

(1) Artículo incorporado por la Quinta Disposición Complementaria de la Ley N° 28627, publicada el 22 noviembre 2005.

(2) Artículo modificado por el Artículo 1 de la Ley Nº 30076, publicada el 19 agosto 2013, cuyo texto es el siguiente:

"Artículo 279-C. Tráfico de productos pirotécnicos

El que, sin estar debidamente autorizado, fabrica, importa, exporta, deposita, transporta, comercializa o usa productos pirotécnicos de cualquier tipo, o los que vendan estos productos a menores de edad, serán reprimidos con pena privativa de libertad no menor de cuatro ni mayor de ocho años y trescientos sesenta y cinco días-multa.

La pena será no menor de cinco ni mayor de diez años, si a causa de la fabricación, importación, depósito, transporte, comercialización y uso de productos pirotécnicos, se produjesen lesiones graves o muerte de personas." (*)

(*) El presente artículo quedó modificado por la Segunda Disposición Complementaria Modificatoria de la Ley N° 30299, publicada el 22 enero 2015, a partir de la publicación de su reglamento, cuyo texto es el siguiente:

“Artículo 279-C. Tráfico de productos pirotécnicos

El que, sin estar debidamente autorizado, fabrica, importa, exporta, deposita, transporta, comercializa o usa productos pirotécnicos de cualquier tipo, o los que vendan estos productos a menores de edad, serán reprimidos con pena privativa de libertad no menor de cuatro ni mayor de ocho años, trescientos sesenta y cinco días-multa e inhabilitación conforme al inciso 4 del artículo 36 del Código Penal.

La pena será no menor de cinco ni mayor de diez años, si a causa de la fabricación, importación, depósito, transporte, comercialización y uso de productos pirotécnicos, se produjesen lesiones graves o muerte de personas”.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

“Artículo 279-D.- Empleo, producción y transferencia de minas antipersonales

El que emplee, desarrolle, produzca, adquiera, almacene, conserve o transfiera a una persona natural o jurídica, minas antipersonales, será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años.”(*)

(*) Artículo incorporado por el Artículo 1 de la Ley N° 28824, publicada el 22 julio 2006.

CONCORDANCIAS:     Ley N° 28824, Art. 2 (Retención y/o transferencias permitidas)
Ley Nº 30077, Art. 3 (Delitos comprendidos)

“Artículo 279-E.- Ensamblado, comercialización y utilización, en el servicio público, de transporte de omnibuses sobre chasis de camión

El que sin cumplir con la normatividad vigente y/o sin contar con la autorización expresa, que para el efecto expida la autoridad competente, realice u ordene realizar a sus subordinados la actividad de ensamblado de ómnibus sobre chasis originalmente diseñado y fabricado para el transporte de mercancías con corte o alargamiento del chasis, será reprimido con pena privativa de la libertad no menor de cinco (5) ni mayor de diez (10) años.

Si el agente comercializa los vehículos referidos en el primer párrafo o utiliza éstos en el servicio público de transporte de pasajeros, como transportista o conductor, la pena privativa de la libertad será no menor de cuatro (4) ni mayor de ocho (8) años y, según corresponda, inhabilitación para prestar el servicio de transporte o conducir vehículos del servicio de transporte por el mismo tiempo de la pena principal.

Si como consecuencia de las conductas a que se refieren el primer y segundo párrafos, se produce un accidente de tránsito con consecuencias de muerte o lesiones graves para los pasajeros o tripulantes del vehículo, la pena privativa de la libertad será no menor de diez (10) ni mayor de veinte (20) años, además de las penas accesorias que correspondan.”(*)

(*) Artículo incorporado por el Artículo 8 de la Ley N° 29177, publicada el 03 enero 2008, el mismo que de conformidad con la Primera Disposición Transitoria entró en vigencia a los sesenta (60) días de la fecha de publicación de la citada Ley.

"Artículo 279-F.- Uso de armas en estado de ebriedad o drogadicción

El que, en lugar público o poniendo en riesgo bienes jurídicos de terceros y teniendo licencia para portar arma de fuego, hace uso, maniobra o de cualquier forma manipula la misma en estado de ebriedad, con presencia de alcohol en la sangre en proporción mayor de 0.5 gramos-litro o bajo el efecto de estupefacientes, drogas tóxicas, sustancias psicotrópicas o sintéticas será sancionado con pena privativa de libertad no menor de un año ni mayor de tres años e inhabilitación conforme al artículo 36, inciso 6.” (*)

(*) Artículo incorporado por el Artículo 2 de la Ley N° 29439, publicada el 19 noviembre 2009.

"Artículo 279-G.- Fabricación, comercialización, uso o porte de armas

El que, sin estar debidamente autorizado, fabrica, ensambla, modifica, almacena, suministra, comercializa, trafica, usa, porta o tiene en su poder, armas de fuego de cualquier tipo, municiones, accesorios o materiales destinados para su fabricación o modificación, será reprimido con pena privativa de libertad no menor de seis ni mayor de diez años, e inhabilitación conforme al inciso 6 del artículo 36 del Código Penal.

Será sancionado con la misma pena el que presta, alquila o facilita, siempre que se evidencie la posibilidad de su uso para fines ilícitos, las armas o bienes a los que se hacen referencia en el primer párrafo. La pena privativa de libertad será no menor de ocho ni mayor de doce años cuando las armas o bienes, dados en préstamo o alquiler, sean de propiedad del Estado.

En cualquier supuesto, si el agente es miembro de las Fuerzas Armadas, Policía Nacional del Perú o Instituto Nacional Penitenciario la pena será no menor de diez ni mayor de quince años.

El que trafica armas de fuego artesanales o materiales destinados para su fabricación, será reprimido con pena privativa de libertad no menor de seis ni mayor de quince años.

Para todos los supuestos se impondrá la inhabilitación conforme a los incisos 1), 2) y 4) del artículo 36 del Código Penal, y adicionalmente el inciso 8) si es miembro de las Fuerzas Armadas o Policía Nacional del Perú y con ciento ochenta a trescientos sesenta y cinco días - multa." (*)

(*) Artículo incorporado por el Artículo 3 del Decreto Legislativo N° 1244, publicado el 29 octubre 2016.

CAPITULO II DELITOS CONTRA LOS MEDIOS DE TRANSPORTE, COMUNICACION Y OTROS SERVICIOS PUBLICOS
############################################################################################

CONCORDANCIAS:     D.S. Nº 017-2008-JUS, Art. 43 (De los Procuradores Públicos Especializados en delitos contra el Orden Público)

Artículo 280 Atentado contra los medios de transporte colectivo o de comunicación
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, a sabiendas, ejecuta cualquier acto que pone en peligro la seguridad de naves, aeronaves, construcciones flotantes o de cualquier otro medio de transporte colectivo o de comunicación destinado al uso público, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años.

Si el hecho produce naufragio, varamiento, desastre, muerte o lesiones graves y el agente pudo prever estos resultados, la pena será no menor de ocho ni mayor de veinte años.

Artículo 281 Atentado contra la seguridad común
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no menor de tres ni mayor de ocho años, el que crea un peligro para la seguridad común, realizando cualquiera de las conductas siguientes:

1. Atenta contra fábricas, obras o instalaciones destinadas a la producción, transmisión, almacenamiento o provisión de electricidad o de sustancias energéticas, o contra instalaciones destinadas al servicio público de aguas corrientes.

2. Atenta contra la seguridad de los medios de telecomunicación pública o puestos al servicio de la seguridad de transportes destinados al uso público.

3. Dificulta la reparación de los desperfectos en las fábricas, obras o instalaciones a que se refieren los incisos anteriores.(*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28820, publicada el 22 julio 2006, cuyo texto es el siguiente:

“Artículo 281.- Atentado contra la seguridad común

Será reprimido con pena privativa de libertad no menor de seis ni mayor de diez años, el que crea un peligro para la seguridad común, realizando cualquiera de las conductas siguientes:

1. Atenta contra fábricas, obras o instalaciones destinadas a la producción, transmisión, almacenamiento o provisión de electricidad o de sustancias energéticas, o contra instalaciones destinadas al servicio público de aguas corrientes. (*)

(*) Inciso modificado por el Artículo Único de la Ley N° 29583, publicada el  18 septiembre 2010, cuyo texto es el siguiente:

"1. Atenta contra fábricas, obras, infraestructura, instalaciones o equipos destinados a la producción, transmisión, distribución, almacenamiento o provisión de saneamiento, electricidad, gas o telecomunicaciones." (*)

(*) Numeral modificado por el Artículo 2 del Decreto Legislativo N° 1245, publicado el 06 noviembre 2016, cuyo texto es el siguiente:

"1. Atenta contra fábricas, obras, infraestructura, instalaciones destinadas a la producción, transmisión, transporte, almacenamiento o provisión de saneamiento, electricidad, gas, hidrocarburos o sus productos derivados o telecomunicaciones."

2. Atenta contra la seguridad de los medios de telecomunicación pública o puestos al servicio de la seguridad de transportes destinados al uso público.

3. Dificulta la reparación de los desperfectos en las fábricas, obras o instalaciones a que se refieren los incisos anteriores." (*)

(*) Inciso modificado por el Artículo Único de la Ley N° 29583, publicada el 18 septiembre 2010, cuyo texto es el siguiente:

"3. Dificulta la reparación de los desperfectos en las fábricas, obras, infraestructura, instalaciones o equipos a que se refieren los incisos 1 y 2."

Artículo 282 Forma culposa
~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, por culpa, ocasiona alguno de los hechos de peligro previstos en los artículos 280 y 281 será reprimido con pena privativa de libertad no mayor de dos años.

Artículo 283 Entorpecimiento al funcionamiento de servicios públicos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, sin crear una situación de peligro común, impide, estorba o entorpece el normal funcionamiento de los transportes, o servicios públicos de comunicación, o de provisión de aguas, electricidad o de sustancias energéticas similares, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.

"En los casos en que el agente actúe con violencia y atente contra la integridad física de las personas o cause grave daño a la propiedad pública o privada, la pena privativa de la libertad será no menor de tres ni mayor de seis años.” (1)(2)

(1) Párrafo incorporado por el Artículo 1 de la Ley N° 27686, publicada el 19 marzo 2002.

(2) Artículo modificado por el Artículo 1 de la Ley N° 28820, publicada el 22 julio 2006, cuyo texto es el siguiente:

"Artículo 283.- Entorpecimiento al funcionamiento de servicios públicos

El que, sin crear una situación de peligro común, impide, estorba o entorpece el normal funcionamiento del transporte; o de los servicios públicos de comunicación, provisión de agua, electricidad, hidrocarburos o de sustancias energéticas similares, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años. (*)

(*) Párrafo modificado por el Artículo Único de la Ley N° 29583, publicada el 18 septiembre 2010, cuyo texto es el siguiente:

"Artículo 283.- Entorpecimiento al funcionamiento de servicios públicos

El que, sin crear una situación de peligro común, impide, estorba o entorpece el normal funcionamiento del transporte o de los servicios públicos de telecomunicaciones, saneamiento, electricidad, hidrocarburos o de sustancias energéticas similares, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años." (*)

(*) Primer párrafo modificado por el Artículo 2 del Decreto Legislativo N° 1245, publicado el 06 noviembre 2016, cuyo texto es el siguiente:

“Artículo 283.- Entorpecimiento al funcionamiento de servicios públicos

El que, sin crear una situación de peligro común, impide, estorba o entorpece el normal funcionamiento del transporte o de los servicios públicos de telecomunicaciones, de saneamiento, de electricidad, de gas, de hidrocarburos o de sus productos derivados, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años."

En los casos en que el agente actúe con violencia y atente contra la integridad física de las personas o cause grave daño a la propiedad pública o privada, la pena privativa de la libertad será no menor de seis ni mayor de ocho años."

Artículo 284 Abandono de servicio de transporte
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El conductor, capitán, comandante, piloto, técnico, maquinista o mecánico de cualquier medio de transporte, que abandona su respectivo servicio antes del término del viaje, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

Artículo 285 Sustitución o impedimento de funciones en medio de transporte
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, mediante violencia, intimidación o fraude, sustituye o impide el cumplimiento de sus funciones al capitán, comandante o piloto de un medio de transporte, será reprimido con pena privativa de libertad no menor de uno ni mayor de cinco años.

CAPITULO III DELITOS CONTRA LA SALUD PUBLICA
############################################

SECCION I
CONTAMINACION Y PROPAGACION

Artículo 286 Contaminación de aguas o sustancias destinadas al consumo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que envenena, contamina o adultera aguas o sustancias alimenticias o medicinales, destinadas al consumo, será reprimido con pena privativa de libertad no menor de tres ni mayor de diez años.

Si resultan lesiones graves o muerte y el agente pudo prever estos resultados, la pena será no menor de diez ni mayor de veinte años.(*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 29675, publicada el 12 abril 2011, cuyo texto es el siguiente:

“Artículo 286.- Contaminación o adulteración de bienes o insumos destinados al uso o consumo humano y alteración de la fecha de vencimiento

El que contamina o adultera bienes o insumos destinados al uso o consumo humano, o altera la fecha de vencimiento de los mismos, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años."

PROCESOS CONSTITUCIONALES

Artículo 287 Adulteración de sustancias o bienes destinados a uso público
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, de modo peligroso para la salud, adultera sustancias o bienes destinados al uso público, distintos a los especificados en el artículo 286, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años.

Si la adulteración consiste en el envenenamiento o contaminación de las sustancias mencionadas y resultan lesiones graves o muerte que el agente pudo prever, la pena será no menor de seis ni mayor de diez años. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 29675, publicada el 12 abril 2011, cuyo texto es el siguiente:

"Artículo 287.- Contaminación o adulteración de alimentos o bebidas y alteración de la fecha de vencimiento

El que contamina o adultera alimentos, bebidas o aguas destinadas al consumo humano, o altera la fecha de vencimiento de los mismos, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de diez años."

Artículo 288 Comercialización o tráfico de productos nocivos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, a sabiendas de que el consumo de un producto o su empleo normal o probable, puede comprometer la salud de las personas, lo pone en venta o en circulación o lo importa o toma en depósito, será reprimido con pena privativa de libertad no menor de dos ni mayor de seis años.

Si el agente sabía que el empleo o consumo del producto originaba un peligro de muerte, la pena será no menor de tres ni mayor de ocho años.

Cuando el agente actúa por culpa, la pena privativa de libertad será no mayor de dos años. (*)

(*) Artículo modificado por el Artículo 3 de la Ley N° 27729, publicada el 24 mayo 2002, cuyo texto es el siguiente:

Comercialización o tráfico de productos nocivos

“Artículo 288.- El que produce, vende, pone en circulación, importa o toma en depósito alimentos, preservantes, aditivos y mezclas destinadas al consumo humano, falsificados, corrompidos o dañados que pudieran comprometer la salud de las personas, será reprimido con pena privativa de libertad no menor de dos ni mayor de seis años.

La pena privativa de libertad será no menor de cuatro años si el agente hubiera utilizado sellos, etiquetas o cualquier distintivo de marcas de fábrica debidamente registradas o el nombre de productos conocidos.

Si el agente sabía que el empleo o consumo del producto originaba un peligro de muerte, la pena será no menor de cuatro ni mayor de ocho años.

Cuando el agente actúa por culpa, la pena privativa de libertad será no mayor de dos años.” (*)

(*) Artículo modificado por el Artículo Único de la Ley N° 28513, publicada el 23 mayo 2005, cuyo texto es el siguiente:

“Artículo 288.- Comercialización o tráfico de productos nocivos

El que produce, vende, pone en circulación, importa o toma en depósito alimentos, preservantes, aditivos y mezclas destinadas al consumo humano, falsificados, adulterados, corrompidos o dañados que pudieran comprometer la salud de las personas, será reprimido con pena privativa de libertad no menor de dos ni mayor de seis años.

Si se trata de sustancias medicinales que se comercializan vencido el plazo que garantiza su buen estado, la pena será no menor de cuatro ni mayor de ocho años y multa de ciento ochenta a trescientos sesenta y cinco días multa.

La pena privativa de libertad será no menor de cuatro ni mayor de ocho años si el agente hubiera utilizado sellos, etiquetas o cualquier distintivo de marcas de fábrica debidamente registradas o el nombre de productos conocidos.

Si el agente sabía que el empleo o consumo del producto originaba un peligro de muerte, la pena será no menor de cuatro ni mayor de ocho años.

Cuando el agente actúa por culpa, la pena privativa de libertad será no mayor de dos años." (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 29675, publicada el 12 abril 2011, cuyo texto es el siguiente:

"Artículo 288.- Producción, comercialización o tráfico ilícito de alimentos y otros productos destinados al uso o consumo humano

El que produce, vende, pone en circulación, importa o toma en depósito alimentos, aguas, bebidas o bienes destinados al uso o consumo humano, a sabiendas de que son contaminados, falsificados o adulterados, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años.

Cuando el agente actúa por culpa, la pena privativa de libertad será no mayor de dos años."

“Artículo 288-A.- El que comercializa alcohol metílico, conociendo o presumiendo su uso para fines de consumo humano, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años.

No es punible la comercialización de alcohol metílico para fines comprobadamente industriales o científicos.”

(*) Artículo incorporado por el Artículo 2 de la Ley Nº 27645, publicada el 23 enero 2002.

CONCORDANCIAS:     Ley  N° 28317, Art. 6 (Acciones de naturaleza penal)

“Artículo 288-B.- Uso de productos tóxicos o peligrosos

El que fabrica, importa, distribuye o comercializa productos o materiales tóxicos o peligrosos para la salud destinados al uso de menores de edad y otros consumidores, será reprimido con pena privativa de la libertad no menor de cuatro años ni mayor de ocho años:” (*)

(*) Artículo 288-B, agregado por el Artículo 5 de la Ley N° 28376, publicada el 10 noviembre 2004.

“Artículo 288-C.- Producción o comercialización de bebidas alcohólicas ilegales

El que produce o comercializa bebidas alcohólicas informales, adulteradas o no aptas para el consumo humano, según las definiciones señaladas en la Ley para Erradicar la Elaboración y Comercialización de Bebidas Alcohólicas Informales, Adulteradas o no Aptas para el Consumo Humano, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años.” (*)

(*) Artículo incorporado por la Única Disposición Complementaria Modificatoria de la Ley Nº 29632, publicada el 17 diciembre 2010.

Artículo 289 Propagación de enfermedad peligrosa o contagiosa 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, a sabiendas, propaga una enfermedad peligrosa o contagiosa para la salud de las personas, será reprimido con pena privativa de libertad no menor de tres ni mayor de diez años.

Si resultan lesiones graves o muerte y el agente pudo prever estos resultados, la pena será no menor de diez ni mayor de veinte años.

Artículo 290 Ejercicio ilegal de la medicina
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no mayor de dos años o con prestación de servicio comunitario de veinte a cincuentidós jornadas, el que, careciendo de título, realiza cualquiera de las acciones siguientes:

1. Anuncia, emite diagnósticos, prescribe, administra o aplica cualquier medio supuestamente destinado al cuidado de la salud, aunque obre de modo gratuito.

2. Expide dictámenes o informes destinados a sustentar el diagnóstico, la prescripción o la administración a que se refiere el inciso 1. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 27754, publicada el 14 junio 2002, cuyo texto es el siguiente:

Ejercicio ilegal de la medicina
“Artículo 290.- Será reprimido con pena privativa de libertad no menor de un año ni mayor de cuatro años o con prestación de servicio comunitario de veinte a cien jornadas, el que simulando calidad de médico u otra profesión de las ciencias médicas, que sin tener título profesional, realiza cualquiera de las acciones siguientes:

1. Anuncia, emite diagnósticos, prescribe, administra o aplica cualquier medio supuestamente destinado al cuidado de la salud, aunque obre de modo gratuito.

2. Expide dictámenes o informes destinados a sustentar el diagnóstico, la prescripción o la administración a que se refiere el inciso 1." (*)

(*) Artículo modificado por el Artículo Único de la Ley Nº 28538, publicada el 07 junio 2005, cuyo texto es el siguiente:

“Artículo 290.- Ejercicio ilegal de la medicina

Será reprimido con pena privativa de libertad no menor de un año ni mayor de cuatro años, el que simulando calidad de médico u otra profesión de las ciencias médicas, que sin tener título profesional, realiza cualquiera de las acciones siguientes:

1. Anuncia, emite diagnósticos, prescribe, administra o aplica cualquier medio supuestamente destinado al cuidado de la salud, aunque obre de modo gratuito.

2. Expide dictámenes o informes destinados a sustentar el diagnóstico, la prescripción o la administración a que se refiere el inciso 1.

La pena será no menor de dos ni mayor de cuatro años, si como consecuencia de las conductas referidas en los incisos 1 y 2 se produjera alguna lesión leve; y no menor de cuatro ni mayor de ocho años, si la lesión fuera grave en la víctima. En caso de muerte de la víctima, la pena privativa de la libertad será no menor de seis ni mayor de diez años."

Artículo 291 Ejercicio malicioso y desleal de la medicina
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, teniendo título, anuncia o promete la curación de enfermedades a término fijo o por medios secretos o infalibles, será reprimido con pena privativa de libertad no mayor de dos años o con prestación de servicio comunitario de veinte a cincuentidós jornadas.

Artículo 292 Violación de medicinas sanitarias
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que viola las medidas impuestas por la ley o por la autoridad para la introducción al país o la propagación de una enfermedad o epidemia o de una epizootía o plaga, será reprimido con pena privativa de libertad no menor de seis meses ni mayor de tres años y con noventa a ciento ochenta días-multa.

Artículo 293 Venta de animales de consumo peligroso
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, en lugares públicos, vende, preparados o no, animales alimentados con desechos sólidos, contraviniendo leyes, reglamentos o disposiciones establecidas, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años y ciento ochenta a trescientos sesenticinco días-multa.

Artículo 294 Venta de medicinas adulteradas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, teniendo autorización para la venta de sustancias medicinales, las entrega en especie, calidad o cantidad no correspondiente a la receta médica o distinta de la declarada o convenida o vencido el plazo que garantiza su buen estado, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años. (*)

(*) Artículo modificado por el Artículo Único de la Ley N° 28513, publicada el 23 mayo 2005, cuyo texto es el siguiente:

"Artículo 294.- Suministro infiel de medicamentos

El que, teniendo autorización para la venta de sustancias medicinales, las entrega en especie, calidad o cantidad no correspondiente a la receta médica o distinta de la declarada o convenida, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años.” (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 29675, publicada el 12 abril 2011, cuyo texto es el siguiente:

"Artículo 294.- Suministro infiel de productos farmacéuticos, dispositivos médicos o productos sanitarios

El que teniendo o no autorización para la venta de productos farmacéuticos, dispositivos médicos o productos sanitarios, a sabiendas, los entrega en especie, calidad o cantidad no correspondiente a la receta médica o distinta de la declarada o convenida, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.

Lo dispuesto en el párrafo precedente no será aplicable cuando el químico farmacéutico proceda conforme a lo dispuesto en el segundo párrafo del artículo 32 de la Ley 29459, Ley de los Productos Farmacéuticos, Dispositivos Médicos y Productos Sanitarios.”

“Artículo 294-A.- Falsificación, contaminación o adulteración de productos farmacéuticos, dispositivos médicos o productos sanitarios

El que falsifica, contamina o adultera productos farmacéuticos, dispositivos médicos o productos sanitarios, o altera su fecha de vencimiento, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de diez años y con ciento ochenta a trescientos sesenta y cinco días multa.

El que, a sabiendas, importa, comercializa, almacena, transporta o distribuye en las condiciones antes mencionadas productos farmacéuticos, dispositivos médicos o productos sanitarios, será reprimido con la misma pena." (*)

(*) Artículo incorporado  por el Artículo 2 de la Ley Nº 29675, publicada el 12 abril 2011.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

"Artículo 294-B.- Comercialización de productos farmacéuticos, dispositivos médicos o productos sanitarios sin garantía de buen estado

El que vende, importa o comercializa productos farmacéuticos, dispositivos médicos o productos sanitarios luego de producida su fecha de vencimiento, o el que para su comercialización los almacena, transporta o distribuye en esa condición, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años y con ciento ochenta a trescientos sesenta y cinco días multa."(*)

(*) Artículo incorporado  por el Artículo 2 de la Ley Nº 29675, publicada el 12 abril 2011.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

"Artículo 294-C.- Agravantes

Cuando alguno de los delitos previstos en los artículos 286, 287, 288, 294, 294-A y 294-B ocasiona lesiones graves o la muerte y el agente pudo prever, la pena privativa de libertad será no menor de ocho ni mayor de quince años.

Si el agente en los delitos previstos en los artículos 294-A y 294-B tiene la condición de director técnico, o quien haga sus veces, de un establecimiento farmacéutico o establecimiento de salud, será también reprimido con inhabilitación conforme a los numerales 1, 2 y 4 del artículo 36.” (*)

(*) Artículo incorporado  por el Artículo 2 de la Ley Nº 29675, publicada el 12 abril 2011.

Artículo 295 Formas culposas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando alguno de los delitos previstos en los artículos 286 a 289 se comete por culpa, la pena será privativa de libertad no mayor de dos años o de prestación de servicio comunitario de diez a treinta jornadas.

SECCION II
TRAFICO ILICITO DE DROGAS

CONCORDANCIAS:     Ley N° 27765, Art. 6

Ley Nº 30077, Art. 3 (Delitos comprendidos)

PROCESOS CONSTITUCIONALES
JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 296 Promoción o favorecimiento al Tráfico Ilícito de Drogas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que promueve, favorece o facilita el consumo ilegal de drogas tóxicas, estupefacientes o sustancias psicotrópicas, mediante actos de (*) RECTIFICADO POR FE DE ERRATAS fabricación o tráfico o las posea con este último fin, será reprimido con pena privativa de libertad no menor de ocho ni mayor de quince años, con ciento ochenta a trescientos sesenticinco días-multa e inhabilitación conforme al artículo 36, incisos 1, 2 y 4.

El que, a sabiendas, comercializa materias primas o insumos destinados a la elaboración de las sustancias de que trata el párrafo anterior, será reprimido con la misma pena. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28002, publicada el 17 junio 2003, cuyo texto es el siguiente:

"Artículo 296.- Promoción o favorecimiento al tráfico ilícito de drogas
El que promueve, favorece o facilita el consumo ilegal de drogas tóxicas, estupefacientes o sustancias psicotrópicas, mediante actos de fabricación o tráfico será reprimido con pena privativa de libertad no menor de ocho ni mayor de quince años y con ciento ochenta a trescientos sesenta y cinco días-multa, e inhabilitación conforme al artículo 36, incisos 1, 2 y 4.

El que posea drogas tóxicas, estupefacientes o sustancias psicotrópicas para su tráfico ilícito será reprimido con pena privativa de libertad no menor de seis ni mayor de doce años y con ciento veinte a ciento ochenta días-multa.

El que a sabiendas comercializa materias primas o insumos destinados a la elaboración ilegal de drogas será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años y con sesenta a ciento veinte días-multa."(*)

(*) Párrafo modificado por el Artículo 4 de la Ley N° 29037, publicada el 12 junio 2007, la misma que entró en vigencia, al día siguiente de la publicación de sus normas reglamentarias, según su Primera Disposición Final, cuyo texto es el siguiente:

“El que a sabiendas comercializa materias primas destinadas a la elaboración ilegal de drogas, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años y sesenta a ciento veinte días-multa.” (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 982, publicado el 22 julio 2007, cuyo texto es el siguiente:

“Artículo 296.- Promoción o favorecimiento al tráfico ilícito de drogas

El que promueve, favorece o facilita el consumo ilegal de drogas tóxicas, estupefacientes o sustancias psicotrópicas, mediante actos de fabricación o tráfico será reprimido con pena privativa de libertad no menor de ocho ni mayor de quince años y con ciento ochenta a trescientos sesenta y cinco días-multa, e inhabilitación conforme al artículo 36, incisos 1), 2) y 4).

El que posea drogas tóxicas, estupefacientes o sustancias psicotrópicas para su tráfico ilícito será reprimido con pena privativa de libertad no menor de seis ni mayor de doce años y con ciento veinte a ciento ochenta días-multa.

El que provee, produce, acopie o comercialice materias primas o insumos para ser destinados a la elaboración ilegal de drogas en cualquiera de sus etapas de maceración, procesamiento o elaboración y/o promueva, facilite o financie dichos actos, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años y con sesenta a ciento veinte días-multa. (*)RECTIFICADO POR FE DE ERRATAS

El que toma parte en una conspiración de dos o más personas para promover, favorecer o facilitar el tráfico ilícito de drogas, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años y con sesenta a ciento veinte días-multa.” (*)

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 296.- Promoción o favorecimiento al Tráfico Ilícito de Drogas y otros

El que promueve, favorece o facilita el consumo ilegal de drogas tóxicas, estupefacientes o sustancias psicotrópicas, mediante actos de fabricación o tráfico será reprimido con pena privativa de libertad no menor de ocho ni mayor de quince años y con ciento ochenta a trescientos sesenta y cinco días-multa, e inhabilitación conforme al artículo 36, incisos 1) , 2) y 4) .

El que posea drogas tóxicas, estupefacientes o sustancias psicotrópicas para su tráfico ilícito será reprimido con pena privativa de libertad no menor de seis ni mayor de doce años y con ciento veinte a ciento ochenta días-multa.

El que introduce al país, produce, acopie, provee, comercialice o transporte materias primas o sustancias químicas controladas o no controladas, para ser destinadas a la elaboración ilegal de drogas tóxicas, estupefacientes o sustancias psicotrópicas, en la maceración o en cualquiera de sus etapas de procesamiento, y/o promueva, facilite o financie dichos actos, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años y con sesenta a ciento veinte días-multa.

El que toma parte en una conspiración de dos o más personas para promover, favorecer o facilitar el tráfico ilícito de drogas, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años y con sesenta a ciento veinte días-multa." (*)(**)

(*) De conformidad con el Numeral 6 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de tráfico ilícito de drogas, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

(**) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1367, publicado el 29 julio 2018, cuyo texto es el siguiente:

“Artículo 296.- Promoción o favorecimiento al Tráfico Ilícito de Drogas y otros

El que promueve, favorece o facilita el consumo ilegal de drogas tóxicas, estupefacientes o sustancias psicotrópicas, mediante actos de fabricación o tráfico será reprimido con pena privativa de libertad no menor de ocho ni mayor de quince años y con ciento ochenta a trescientos sesenta y cinco días-multa, e inhabilitación conforme al artículo 36, incisos 1) , 2) y 4) .

El que posea drogas tóxicas, estupefacientes o sustancias psicotrópicas para su tráfico ilícito será reprimido con pena privativa de libertad no menor de seis ni mayor de doce años y con ciento veinte a ciento ochenta días-multa, e inhabilitación conforme al artículo 36, incisos 1) y 2).

El que introduce al país, produce, acopie, provee, comercialice o transporte materias primas o sustancias químicas controladas o no controladas, para ser destinadas a la elaboración ilegal de drogas tóxicas, estupefacientes o sustancias psicotrópicas, en la maceración o en cualquiera de sus etapas de procesamiento, y/o promueva, facilite o financie dichos actos, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años y con sesenta a ciento veinte días-multa, e inhabilitación conforme al artículo 36, incisos 1) y 2).

El que toma parte en una conspiración de dos o más personas para promover, favorecer o facilitar el tráfico ilícito de drogas, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años y con sesenta a ciento veinte días-multa, e inhabilitación conforme al artículo 36, incisos 1) y 2).”

CONCORDANCIAS:     Ley N° 26320, Arts. 2 y 4

PROCESOS CONSTITUCIONALES

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

"Artículo 296.-A.- El que interviene en la inversión, venta, pignoración, transferencia o posesión de las ganancias, cosas o bienes provenientes de aquellos o del beneficio económico obtenido del trafico ilícito de drogas, siempre que el agente hubiese conocido ese origen o lo hubiera sospechado, será reprimido con pena privativa de la libertad no menor de cinco ni mayor de diez años, con ciento veinte a trescientos días de multa e inhabilitación, conforme al artículo 36, incisos 1), 2) y 4).

En que compre, guarde, custodie, oculte o reciba dichas ganancias, cosas, bienes o beneficios conociendo su ilícito origen o habiéndolo sospechado, será reprimido con la misma pena". (*)

(*) Artículo incorporado por el Artículo 1 del Decreto Legislativo Nº 736, publicado el 12 noviembre 1991. Posteriormente dicho Decreto fue derogado por el Artículo 1 de la Ley Nº 25399, publicada el 10 febrero 1992.

Receptación

"Artículo 296-A.- El que interviene en la inversión, venta, pignoración, transferencia o posesión de las ganancias, cosas o bienes provenientes de aquellos o del beneficio económico obtenido del tráfico ilícito de drogas, siempre que el agente hubiese conocido ese origen o lo hubiera sospechado, será reprimido con pena privativa de la libertad no menor de ocho ni mayor de dieciocho años, y con ciento veinte a trescientos días-multa e inhabilitación, conforme al artículo 36, incisos 1, 2 y 4.

El que compre, guarde, custodie, oculte o reciba dichas ganancias, cosas, bienes o beneficios conociendo su ilícito origen o habiéndolo sospechado, será reprimido con la misma pena." (1)(2)

(1) Artículo incorporado (*) NOTA SPIJ por el Artículo 1 del Decreto Ley Nº 25428, publicado el 11 abril 1992.

(2) Artículo derogado por el Artículo 8 de la Ley N° 27765, publicada el 27 junio 2002. Posteriormente, la citada Ley fue derogada por la Única Disposición Complementaria Derogatoria del Decreto Legislativo Nº 1106, publicado el 19 abril 2012.

"Artículo 296-A.- Comercialización y cultivo de amapola y marihuana y sus siembra compulsiva
El que promueve, favorece, financia, facilita o ejecuta actos de siembra o cultivo de plantas de amapola de la especie papaver somníferum o marihuana de la especie cannabis sativa será reprimido con pena privativa de libertad no menor de ocho años ni mayor de quince años y con ciento ochenta a trescientos sesenta y cinco días-multa e inhabilitación conforme al artículo 36, incisos 1, 2 y 4.

El que comercializa o transfiere semillas de las especies a que alude el párrafo anterior será reprimido con pena privativa de libertad ni menor de cinco ni mayor de diez años y con ciento veinte a ciento ochenta días-multa.

La pena será privativa de libertad no menor de dos ni mayor de seis años y de noventa a ciento veinte días-multa cuando:

1. La cantidad de plantas sembradas o cultivadas no exceda de cien.

2. La cantidad de semillas no exceda de la requerida para sembrar el número de plantas que señala el inciso precedente.

Será reprimido con pena privativa de libertad no menor de veinticinco ni mayor de treinta y cinco años el que, mediante amenaza o violencia, obliga a otro a la siembra o cultivo o al procedimiento ilícito de plantas de coca, amapola de la especie papaver somníferum, o marihuana de la especie cannabis sativa.” (1)(2)

(1) Artículo incorporado por el Artículo 2 de la Ley N° 28002, publicada el 17 junio 2003.

(2) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 982, publicado el 22 julio 2007, cuyo texto es el siguiente:

“Artículo 296-A.- Comercialización y cultivo de amapola y marihuana y su siembra compulsiva

El que promueve, favorece, financia, facilita o ejecuta actos de siembra o cultivo de plantas de amapola o adormidera de la especie papaver somníferum o marihuana de la especie cannabis sativa será reprimido con pena privativa de libertad no menor de ocho años ni mayor de quince años y con ciento ochenta a trescientos sesenta y cinco días-multa e inhabilitación conforme al artículo 36, incisos 1, 2 y 4.

El que comercializa o transfiere semillas de las especies a que alude el párrafo anterior será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años y con ciento veinte a ciento ochenta días-multa. (*)RECTIFICADO POR FE DE ERRATAS

La pena será privativa de libertad no menor de dos ni mayor de seis años y de noventa a ciento veinte días-multa cuando:

1. La cantidad de plantas sembradas o cultivadas no exceda de cien.

2. La cantidad de semillas no exceda de la requerida para sembrar el número de plantas que señala el inciso precedente.

Será reprimido con pena privativa de libertad no menor de veinticinco ni mayor de treinta y cinco años el que, mediante amenaza o violencia, obliga a otro a la siembra o cultivo o al procesamiento ilícito de plantas de coca, amapola o adormidera de la especie papaver somníferum, o marihuana de la especie cannabis sativa.” (*)

(*) Artículo modificado por la Primera Disposición Complementaria Final de la Ley N° 30681, publicada el 17 noviembre 2017, cuyo texto es el siguiente:

“Artículo 296-A. Comercialización y cultivo de amapola y marihuana y su siembra compulsiva

El que promueve, favorece, financia, facilita o ejecuta actos de siembra o cultivo de plantas de amapola o adormidera de la especie papaver somníferum o marihuana de la especie cannabis sativa será reprimido con pena privativa de libertad no menor de ocho años ni mayor de quince años y con ciento ochenta a trescientos sesenta y cinco días-multa e inhabilitación conforme al artículo 36, incisos 1, 2 y 4.

El que comercializa o transfiere semillas de las especies a que alude el párrafo anterior será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años y con ciento veinte a ciento ochenta días-multa.

La pena será privativa de libertad no menor de dos ni mayor de seis años y de noventa a ciento veinte días-multa cuando:

1. La cantidad de plantas sembradas o cultivadas no exceda de cien.

2. La cantidad de semillas no exceda de la requerida para sembrar el número de plantas que señala el inciso precedente.

Será reprimido con pena privativa de libertad no menor de veinticinco ni mayor de treinta y cinco años el que, mediante amenaza o violencia, obliga a otro a la siembra o cultivo o al procesamiento ilícito de plantas de coca, amapola o adormidera de la especie papaver somníferum, o marihuana de la especie cannabis sativa.

Se excluye de los alcances de lo establecido en el presente artículo, cuando se haya otorgado licencia para la investigación, importación y/o comercialización y producción, del cannabis y sus derivados con fines medicinales y terapéuticos. De incumplirse con la finalidad de la licencia señalada se aplica la pena prevista en el presente artículo. Será reprimido con la pena máxima más el cincuenta por ciento de la misma al funcionario público que otorga irregularmente la licencia o autorización referida”. (*)(**)

(*) De conformidad con el Numeral 6 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de tráfico ilícito de drogas, tipificado en el presente artículo. Asimismo, se excluye el delito de comercialización y cultivo de amapola y marihuana y su siembra compulsiva tipificado en el tercer párrafo del presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

(**) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1367, publicado el 29 julio 2018, cuyo texto es el siguiente:

“Artículo 296-A. Comercialización y cultivo de amapola y marihuana y su siembra compulsiva

El que promueve, favorece, financia, facilita o ejecuta actos de siembra o cultivo de plantas de amapola o adormidera de la especie papaver somníferum o marihuana de la especie cannabis sativa será reprimido con pena privativa de libertad no menor de ocho años ni mayor de quince años y con ciento ochenta a trescientos sesenta y cinco días-multa e inhabilitación conforme al artículo 36, incisos 1), 2) y 4).

El que comercializa o transfiere semillas de las especies a que alude el párrafo anterior será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años y con ciento veinte a ciento ochenta días-multa, e inhabilitación conforme al artículo 36, incisos 1) y 2).

La pena será privativa de libertad no menor de dos ni mayor de seis años y de noventa a ciento veinte días-multa cuando:

1. La cantidad de plantas sembradas o cultivadas no exceda de cien.

2. La cantidad de semillas no exceda de la requerida para sembrar el número de plantas que señala el inciso precedente.

Será reprimido con pena privativa de libertad no menor de veinticinco ni mayor de treinta y cinco años, e inhabilitación conforme al artículo 36, incisos 1) y 2), el que, mediante amenaza o violencia, obliga a otro a la siembra o cultivo o al procesamiento ilícito de plantas de coca, amapola o adormidera de la especie papaver somníferum, o marihuana de la especie cannabis sativa. Se excluye de los alcances de lo establecido en el presente artículo, cuando se haya otorgado licencia para la investigación, importación y/o comercialización y producción, del cannabis y sus derivados con fines medicinales y terapéuticos. De incumplirse con la finalidad de la licencia señalada se aplica la pena prevista en el presente artículo. Será reprimido con la pena máxima más el cincuenta por ciento de la misma al funcionario público que otorga irregularmente la licencia o autorización referida”.

"Artículo 296.- B.- El que interviniera en el proceso de blanqueado o lavado de dinero proveniente del tráfico ilícito de drogas o del narcoterrorismo, ya sea convirtiéndolo en otros bienes, o transferiéndolo a otros países, bajo cualquier modalidad empleada por el sistema bancario o financiero o repatriándolo para su ingreso al circuito económico imperante en el país, de tal forma que ocultare su origen, su propiedad u otros valores potencialmente ilícitos, será reprimido con pena privativa de la libertad no  menor de seis ni mayor de doce años, con ciento cuarenta a trescientos sesenta y cinco días de multa e inhabilitación conforme al artículo 36, incisos 1), 2) y 4).

La figura delictiva descrita se grava sancionándose con el máximo de ley como mínimo, si el agente, siendo miembro del sistema bancario o financiero, actúa a sabiendas de la procedencia ilícita del dinero.

Las penas consideradas en los artículos precedentes se duplicarán si se comprueba que los ilícitos penales están vinculados con actividades terroristas.

En la investigación de los delitos previstos en este Decreto Legislativo no habrá reserva o secreto bancario o tributario alguno. El Ministerio Público, siempre que exista indicios razonables solicitará de oficio o a petición de la autoridad policial competente, el levantamiento de estas reservas asegurándose previamente de que la información obtenida sólo será utilizada en relación con la investigación financiera de los hechos previstos como trafico ilícito de drogas y/o su vinculación con el terrorismo". (*)

(*) Artículo incorporado por el Artículo 1 del Decreto Legislativo Nº 736, publicado el 12 noviembre 1991. Posteriormente dicho Decreto fue derogado por el Artículo 1 de la Ley Nº 25399, publicada el 10 febrero 1992.

"Artículo 296-B.- El que interviniere en el proceso de lavado de dinero proveniente del tráfico ilícito de drogas o del narcoterrorismo, ya sea convirtiéndolo en otros bienes, o transfiriéndolo a otros países, bajo cualquier modalidad empleada por el sistema bancario o financiero o repatriándolo para su ingreso al circuito económico imperante en el país, de tal forma que ocultare su origen, su propiedad u otros factores potencialmente ilícitos, será reprimido con pena privativa de la libertad no menor de diez ni mayor de veinticinco años, con ciento cuarenta a trescientos sesenticinco días-multa e inhabilitación conforme al Artículo 36, incisos 1, 2 y 4.

La figura delictiva descrita precedentemente se agrava sancionándose con el máximo de Ley como mínimo, si el agente, siendo miembro del Sistema Bancario o Financiero, actúa a sabiendas de la procedencia ilícita del dinero.

En los casos de ilícitos penales vinculados con actividades terroristas se reprimirán con el máximo de la pena.

En la investigación de los delitos previstos en este Decreto Ley no habrá reserva o secreto bancario o tributario alguno. El Ministerio Público, siempre que exista indicios razonables solicitará de oficio o a petición de la autoridad policial competente, el levantamiento de estas reservas, asegurándose previamente que la información obtenida sólo será utilizada en relación con la investigación financiera de los hechos previstos como tráfico ilícito de drogas y/o su vinculación en el terrorismo". (1)(2)

(1) Artículo incorporado (*) NOTA SPIJ por el Artículo 1 del Decreto Ley Nº 25428, publicado el 11 abril 1992.

(2) Artículo modificado por el Artículo Primero de la  Ley Nº 26223,  publicada el 21 agosto 1993, cuyo texto es el siguiente:

Lavado de dinero

"Artículo 296-B.- El que interviene en el proceso de lavado de dinero proveniente del tráfico ilícito de drogas o del narcoterrorismo, ya sea convirtiéndolo en otros bienes, o transfiriéndolo a otros países, bajo cualquier modalidad empleada por el sistema bancario o financiero o repatriándolo para su ingreso al circuito económico imperante en el país, de tal forma que ocultare su origen, su propiedad u otros factores potencialmente ilícitos, será reprimido con pena de cadena perpetua.

La misma pena de cadena perpetua se aplicará en los casos en que el agente esté vinculado con actividades terroristas, o siendo miembro del sistema bancario o financiero actúa a sabiendas de la procedencia ilícita del dinero.

En la investigación de los delitos previstos en esta ley, no habrá reserva o secreto bancario o tributario alguno. El Fiscal de la Nación, siempre que exista indicios razonables solicitará de oficio o a petición de la autoridad policial competente, el levantamiento de estas reservas, asegurándose previamente que la información obtenida sólo será utilizada en relación con la investigación financiera de los hechos previstos como tráfico ilícito de drogas y/o su vinculación en el terrorismo."

"La condición de miembro del directorio, gerente, socio, accionista, directivo titular o asociado de una persona jurídica de derecho privado, no constituye indicio suficiente de responsabilidad en la comisión del delito de lavado de dinero, en cuyo proceso penal se encuentre comprendido otro miembro de dicha persona jurídica." (1)(2)

(1) Párrafo adicionado por el Artículo Unico de la Ley Nº 27225, publicada el 17 diciembre 1999.

(2) Artículo derogado por el Artículo 8 de la Ley N° 27765, publicada el 27 junio 2002. Posteriormente, la citada Ley fue derogada por la Única Disposición Complementaria Derogatoria del Decreto Legislativo Nº 1106, publicado el 19 abril 2012.

“Artículo 296-B.- Tráfico ilícito de insumos químicos y productos

El que importa, exporta, fabrica, produce, prepara, elabora, transforma, almacena, posee, transporta, adquiere, vende o de cualquier modo transfiere insumos químicos o productos, sin contar con las autorizaciones o certificaciones respectivas, o contando con ellas hace uso indebido de las mismas, con el objeto de destinarlos a la producción, extracción o preparación ilícita de drogas, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años y con sesenta a ciento veinte días multa.” (*)

(*) Artículo incorporado por el Artículo 5 de la Ley N° 29037, publicada el 12 junio 2007, la misma que entró en vigencia, al día siguiente de la publicación de sus normas reglamentarias, según su Primera Disposición Final.

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 296-B.- Tráfico Ilícito de Insumos Químicos y Productos Fiscalizados.

El que importa, exporta, fabrica, produce, prepara, elabora, transforma, almacena, posee, transporta, adquiere, vende o de cualquier modo transfiere insumos químicos o productos fiscalizados, contando con las autorizaciones o certificaciones respectivas, hace uso indebido de las mismas, para ser destinadas a la elaboración ilegal de drogas tóxicas, estupefacientes o sustancias psicotrópicas, en cualquier etapa de su procesamiento, será reprimido con pena privativa de libertad no menor de siete ni mayor de doce años y con ciento veinte a ciento ochenta días multa e inhabilitación conforme al artículo 36, incisos 1, 2 y 4.

El que, contando con las autorizaciones o certificaciones respectivas para realizar actividades con Insumos Químicos y Productos Fiscalizados en zona de producción cocalera, emite reportes, declaraciones, informes de obligatorio cumplimiento u otros similares, conteniendo datos de identidad falsos o simulados del destinatario, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años y con ciento veinte a ciento ochenta días-multa e inhabilitación conforme al artículo 36, incisos 1, 2 y 4.(*) (*) RECTIFICADO POR FE DE ERRATAS

(*) De conformidad con el Numeral 6 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de tráfico ilícito de drogas, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

Siembra compulsiva de coca o amapola

"Artículo 296-C.- El que mediante amenaza o violencia y con fines ilícitos obligue a otro a la siembra de coca o amapola o a su procesamiento, será reprimido con pena de cadena perpetua." (1)(2)

(1) Artículo incorporado por el Artículo 2 de la Ley Nº 26223, publicada el 21 agosto 1993.

(2) Artículo derogado por el Artículo 3 de la Ley N° 28002, publicada el 17 junio 2003.

«Artículo 296-C.- Penalización de la resiembra

El propietario, posesionario o tercero, que haciendo uso de cualquier técnica de cultivo, resiembre parcial o totalmente con arbusto de coca, semillas y/o almácigos, aquellos predios de coca erradicados por el Estado, será reprimidos con pena privativa de libertad no menor de 3 ni mayor de 8 años.

Serán decomisados a favor del Estado, los predios que total o parcialmente estuvieran cultivados ilegalmente con plantas de coca, semillas y/o almácigos en áreas del territorio nacional, cualquiera sea la técnica utilizada para su cultivo, y no procedieran sus propietarios o posesionarios a sustituirlos o erradicarlos».(*)(**)

(*) Artículo incorporado por la Tercera Disposición Complementaria Modificatoria del Decreto Legislativo N° 1241, publicado el 26 septiembre 2015.

(**) De conformidad con el Numeral 6 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de tráfico ilícito de drogas, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

Comercialización y cultivo de plantaciones de adormidera

"Artículo 296-D.- El que ejecuta actos de cultivo, promoción, facilitación o financiación de plantaciones de adormidera, será reprimido con pena privativa de libertad no menor de ocho ni mayor de quince años, con ciento ochenta a trescientos sesenticinco días-multa e inhabilitación conforme al Artículo 36, incisos 1), 2) y 4).

Si la cantidad de plantas de que trata el párrafo anterior no excede de cien, el agente será reprimido con pena privativa de libertad no menor de dos ni mayor de ocho años, con trescientos sesenticinco a setecientos treinta días-multa e inhabilitación conforme al Artículo 36, incisos 1), 2) y 4).

El que transfiere o comerciliza semillas de adormidera será reprimido con la misma pena que establece el primer párrafo del presente artículo." (1)(2)

(1) Artículo incorporado por el Artículo Primero de la Ley Nº 26332 , publicada  el 24 junio 1994.

(2) Artículo derogado por el Artículo 3 de la Ley N° 28002, publicada el 17 junio 2003.

Artículo 297 
~~~~~~~~~~~~~

La pena será privativa de libertad no menor de quince años; de ciento ochenta a trescientos sesenticinco días-multa e inhabilitación conforme al artículo 36, incisos 1, 2, 4, 5 y 8, cuando:

1. El hecho es cometido por dos o más personas o el agente integra una organización destinada al tráfico de drogas.

2. El agente es funcionario o servidor público, encargado de la prevención o investigación de cualquier delito, o tiene el deber de aplicar penas o de vigilar su ejecución.

3. El agente tiene la profesión de educador o se desempeña como tal en cualquiera de los niveles de enseñanza.

4. El agente es médico, farmacéutico, químico, odontólogo o ejerce profesión sanitaria.

5. El hecho es cometido en el interior o en inmediaciones de un  establecimiento de enseñanza, centro asistencial, de salud, recinto deportivo, lugar de detención o reclusión.

6. El agente se vale para la comisión del delito de persona inimputable. (*)

(*) Artículo modificado por el Artículo 3 de la Ley Nº 26223, publicada el 21 agosto 1993, cuyo texto es el siguiente:

Formas agravadas

"Artículo 297.- La pena será privativa de libertad no menor de veinticinco años; de ciento ochenta a trescientos sesenticinco días-multa e inhabilitación conforme al artículo 36, incisos 1, 2, 4, 5 y 8, cuando:

1. El agente es funcionario o servidor público, encargado de la prevención o investigación de cualquier delito, o tiene el deber de aplicar penas o de vigilar su ejecución.

2. El agente tiene la profesión de educador o se desempeña como tal en cualquiera de los niveles de enseñanza.

3. El agente es médico, farmacéutico, químico, odontólogo o ejerce profesión sanitaria.

4. El hecho es cometido en el interior o en inmediaciones de un  establecimiento de enseñanza, centro asistencial, de salud, recinto deportivo, lugar de detención o reclusión.

5. El agente se vale o utiliza para la comisión del delito menores de edad o a cualquier otra persona inimputable.

6. El agente es autoridad pública elegida por sufragio popular.

"7. El hecho es cometido por tres o más personas o el agente activo integra una organización dedicada al Tráfico Ilícito de Drogas a nivel nacional o internacional." (*)

(*) Inciso 7 incorporado por el Artículo Unico de la  Ley Nº 26619, publicada el 09 junio 1996.

La  pena será de cadena perpetua cuando:

1. El agente actúa como cabecilla o dirigente de una organización destinada al tráfico ilícito
de drogas de nivel nacional o internacional.

2. El agente se vale del narcotráfico para financiar actividades de grupos terroristas.(*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28002, publicada el 17 junio 2003, cuyo texto es el siguiente:

"Artículo 297.- Formas agravadas
La pena será privativa de libertad no menor de quince ni mayor de veinticinco años, de ciento ochenta a trescientos sesenta y cinco días-multa e inhabilitación conforme al artículo 36, incisos 1, 2, 4, 5 y 8 cuando:

1. El agente comete el hecho abusando del ejercicio de la función pública.

2. El agente tiene la profesión de educador o se desempeña como tal en cualquiera de los niveles de enseñanza.

3. El agente es médico, farmacéutico, químico, odontólogo o ejerce otra profesión sanitaria. (*) RECTIFICADO POR FE DE ERRATAS

4. El hecho es cometido en el interior o en inmediaciones de un establecimiento de enseñanza, centro asistencial, de salud, recinto deportivo, lugar de detención o reclusión. (*) RECTIFICADO POR FE DE ERRATAS

5. El agente vende drogas a menores de edad, o los utiliza para la venta o emplea a una persona inimputable.

6. El hecho es cometido por tres o más personas, o en calidad de integrante de una organización dedicada al tráfico ilícito de drogas o que se dedique a la comercialización de insumos para su elaboración.(*)

(*) Numeral modificado por el Artículo 5 de la Ley Nº 29037, publicada el 12 junio 2007, la misma que entró en vigencia, al día siguiente de la publicación de sus normas reglamentarias, cuyo texto es el siguiente:

"6. El hecho es cometido por tres o más personas o en calidad de integrante de una organización dedicada al tráfico ilícito de drogas o de insumos químicos o productos para la elaboración ilícita de drogas."

7. La droga a comercializarse o comercializada excede las siguientes cantidades: veinte kilogramos de pasta básica de cocaína, diez kilogramos de clorhidrato de cocaína, cinco kilogramos de látex de opio o quinientos gramos de sus derivados, y cien kilogramos de marihuana o dos kilogramos de sus derivados.

La pena será privativa de libertad no menor de veinticinco ni mayor de treinta y cinco años cuando el agente actúa como jefe, dirigente o cabecilla de una organización dedicada al tráfico ilícito de drogas o insumos para su elaboración.

Igual pena se aplicará al agente que se vale del tráfico ilícito de drogas para financiar actividades terroristas."(*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 982, publicado el 22 julio 2007, cuyo texto es el siguiente:

“Artículo 297.- Formas agravadas

La pena será privativa de libertad no menor de quince ni mayor de veinticinco años, de ciento ochenta a trescientos sesenta y cinco días-multa e inhabilitación conforme al artículo 36, incisos 1), 2), 4), 5) y 8) cuando:

1. El agente comete el hecho abusando del ejercicio de la función pública.

2. El agente tiene la profesión de educador o se desempeña como tal en cualquiera de los niveles de enseñanza.

3. El agente es médico, farmacéutico, químico, odontólogo o ejerce otra profesión sanitaria.

4. El hecho es cometido en el interior o en inmediaciones de un establecimiento de enseñanza, centro asistencial, de salud, recinto deportivo, lugar de detención o reclusión.

5. El agente vende drogas a menores de edad, o los utiliza para la venta o emplea a una persona inimputable.

6. El hecho es cometido por tres o más personas, o en calidad de integrante de una organización dedicada al tráfico ilícito de drogas o que se dedique a la comercialización de insumos para su elaboración.(*)

(*) Numeral modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 30077, publicada el 20 agosto 2013, la misma que entró en vigencia el 1 de julio de 2014, cuyo texto es el siguiente:

"6. El hecho es cometido por tres o más personas o en calidad de integrante de una organización criminal dedicada al tráfico ilícito de drogas o que se dedique a la comercialización de insumos para su elaboración."

7. La droga a comercializarse o comercializada excede las siguientes cantidades: veinte kilogramos de pasta básica de cocaína, diez kilogramos de clorhidrato de cocaína, cinco kilogramos de látex de opio o quinientos gramos de sus derivados, y cien kilogramos de marihuana o dos kilogramos de sus derivados o quince gramos de éxtasis, conteniendo Metilendioxianfetamina - MDA, Metilendioximetanfetamina- MDMA, Metanfetamina o sustancias análogas.

La pena será privativa de libertad no menor de veinticinco ni mayor de treinta y cinco años cuando el agente actúa como jefe, dirigente o cabecilla de una organización dedicada al tráfico ilícito de drogas o insumos para su elaboración.

Igual pena se aplicará al agente que se vale del tráfico ilícito de drogas para financiar actividades terroristas.” (*)

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 297.- Formas agravadas.

La pena será privativa de libertad no menor de quince ni mayor de veinticinco años, de ciento ochenta a trescientos sesenta y cinco días-multa e inhabilitación conforme al artículo 36, incisos 1) , 2) , 4) , 5) y 8) cuando:

1. El agente comete el hecho abusando del ejercicio de la función pública.

2. El agente tiene la profesión de educador o se desempeña como tal en cualquiera de los niveles de enseñanza.

3. El agente es médico, farmacéutico, químico, odontólogo o ejerce otra profesión sanitaria.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

4. El hecho es cometido en el interior o en inmediaciones de un establecimiento de enseñanza, centro asistencial, de salud, recinto deportivo, lugar de detención o reclusión.

5. El agente vende drogas a menores de edad, o los utiliza para la venta o emplea a una persona inimputable.

6. El hecho es cometido por tres o más personas, o en calidad de integrante de una organización criminal dedicada al tráfico ilícito de drogas, o al desvío de sustancias químicas controladas o no controladas o de materias primas a que se refieren los Artículos 296 y 296-B.

PROCESOS CONSTITUCIONALES

7. La droga a comercializarse o comercializada excede las siguientes cantidades: veinte kilogramos de pasta básica de cocaína o sus derivados ilícitos, diez kilogramos de clorhidrato de cocaína, cinco kilogramos de látex de opio o quinientos gramos de sus derivados, y cien kilogramos de marihuana o dos kilogramos de sus derivados o quince gramos de éxtasis, conteniendo Metilendioxianfetamina - MDA, Metilendioximetanfetamina - MDMA, Metanfetamina o sustancias análogas.

La pena será privativa de libertad no menor de veinticinco ni mayor de treinta y cinco años cuando el agente actúa como jefe, dirigente o cabecilla de una organización dedicada al tráfico ilícito de drogas o insumos para su elaboración.

Igual pena se aplicará al agente que se vale del tráfico ilícito de drogas para financiar actividades terroristas." (*)(**)

PROCESOS CONSTITUCIONALES

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

(**) De conformidad con el Numeral 6 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de tráfico ilícito de drogas, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA
PROCESOS CONSTITUCIONALES

Artículo 298 Microcomercialización o microproducción
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si es pequeña la cantidad de droga o materia prima poseída, fabricada, extractada o preparada por el agente, la pena privativa  de libertad será no menor de dos ni mayor de ocho años, de trescientos sesenticinco a setecientos treinta días-multa e inhabilitación conforme al artículo 36, incisos 1, 2 y 4.

Si se ha distribuído la droga en pequeñas cantidades y directamente a consumidores individuales, no manifiestamente inimputables, la pena privativa de libertad será no menor de uno ni mayor de cuatro años, de ciento ochenta a trescientos sesenticinco días-multa e inhabilitación conforme al artículo 36º, incisos 1, 2 y 4.

"A efectos de la aplicación del presente artículo, se considera pequeña cantidad de droga hasta cien gramos de pasta básica de cocaína y derivados ilícitos, venticinco gramos de clorhidrato de cocaína, doscientos gramos de marihuana y veinte gramos de derivados de marihuana." (1)

"El Poder Ejecutivo, mediante Decreto Supremo determinará las cantidades correspondientes a las demás drogas." (1)(2)

(1) Párrafos adicionados por el Artículo 1 de la Ley Nº 26320, publicada el 02 junio 1994.

(2) Artículo modificado por el Artículo Único de la Ley N° 27817, publicada el 13 agosto 2002, cuyo texto es el siguiente:

“Artículo 298.- Micro-comercialización o Microproducción

Si es pequeña la cantidad de droga o materia prima poseída, fabricada, extractada o preparada por el agente, la pena privativa de la libertad será no menor de dos ni mayor de ocho años, de trescientos sesenta y cinco a setecientos treinta días-multa e inhabilitación conforme al artículo 36, incisos 1, 2 y 4.

Si se ha distribuido la droga en pequeñas cantidades y directamente a consumidores individuales, no manifiestamente inimputables, la pena privativa de la libertad será no menor de uno ni mayor de cuatro años, de ciento ochenta a trescientos sesenta y cinco días-multa e inhabilitación conforme al artículo 36 incisos  1, 2 y 4.

La pena será no menor de seis años y no mayor de doce años, si el agente se encuentra dentro de alguno de los supuestos contemplados en los incisos 2, 3, 4, 5 ó 6 del artículo 297, que precede, salvo que la pequeña cantidad de droga se entregue a personas manifiestamente inimputables.

A los efectos de la aplicación del presente artículo, se considera pequeña cantidad de droga hasta cincuenta gramos de pasta básica de cocaína y derivados ilícitos, veinticinco gramos de clorhidrato de cocaína, cinco gramos de opio o un gramo de sus derivados; ochenta gramos de marihuana o diez gramos de sus derivados.

El Poder Ejecutivo determinará mediante Decreto Supremo las cantidades correspondientes a las demás drogas y las de elaboración sintética.” (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28002, publicada el 17 junio 2003, cuyo texto es el siguiente:

"Artículo 298.- Microcomercialización o microproducción
La pena será privativa de libertad no menor de tres ni mayor de siete años y de ciento ochenta a trescientos sesenta días-multa cuando: (*) RECTIFICADO POR FE DE ERRATAS

1. La cantidad de droga fabricada, extractada, preparada, comercializada o poseída por el agente no sobrepase los cincuenta gramos de pasta básica de cocaína y derivados ilícitos, veinticinco gramos de clorhidrato de cocaína, cinco gramos de látex de opio o un gramo de sus derivados, cien gramos de marihuana o diez gramos de sus derivados.

El Poder Ejecutivo determinará mediante decreto supremo las cantidades correspondientes a las demás drogas y las de elaboración sintética.

2. Las materias primas o los insumos comercializados por el agente que no excedan de lo requerido para la elaboración de las cantidades de drogas señaladas en el inciso anterior.

La pena será privativa de libertad no menor de seis años ni mayor de diez años y de trescientos sesenta a setecientos días-multa cuando el agente ejecute el delito en las circunstancias previstas en los incisos 2, 3, 4, 5 o 6 del artículo 297 del Código Penal."(*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 982, publicado el 22 julio 2007, cuyo texto es el siguiente:

“Artículo 298.- Microcomercialización o microproducción

La pena será privativa de libertad no menor de tres ni mayor de siete años y de ciento ochenta a trescientos sesenta días-multa cuando:

1. La cantidad de droga fabricada, extractada, preparada, comercializada o poseída por el agente no sobrepase los cincuenta gramos de pasta básica de cocaína y derivados ilícitos, veinticinco gramos de clorhidrato de cocaína, cinco gramos de látex de opio o un gramo de sus derivados, cien gramos de marihuana o diez gramos de sus derivados o dos gramos de éxtasis, conteniendo Metilendioxianfetamina - MDA, Metilendioximetanfetamina - MDMA, Metanfetamina o sustancias análogas.

2. Las materias primas o los insumos comercializados por el agente que no excedan de lo requerido para la elaboración de las cantidades de drogas señaladas en el inciso anterior.

3. Se comercialice o distribuya pegamentos sintéticos que expelen gases con propiedades psicoactivas, acondicionados para ser destinados al consumo humano por inhalación.

La pena será privativa de libertad no menor de seis años ni mayor de diez años y de trescientos sesenta a setecientos días-multa cuando el agente ejecute el delito en las circunstancias previstas en los incisos 2, 3, 4, 5 o 6 del artículo 297 del Código Penal.”  (*)

(*) De conformidad con el Numeral 6 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de tráfico ilícito de drogas, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

PROCESOS CONSTITUCIONALES

Artículo 299 Posesión impune de droga
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que posee droga en dosis personal para su propio e inmediato consumo está exento de pena.

Para determinar la dosis personal, el Juez tendrá en cuenta la correlación peso-dosis, la pureza y la aprehensión de la droga. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28002, publicado el 17 junio 2003, cuyo texto es el siguiente:

"Artículo 299.- Posesión no punible
No es punible la posesión de droga para el propio e inmediato consumo, en cantidad que no exceda de cinco gramos de pasta básica de cocaína, dos gramos de clorhidrato de cocaína, ocho gramos de marihuana o dos gramos de sus derivados, un gramo de látex de opio o doscientos miligramos de sus derivados.

Se excluye de los alcances de lo establecido en el párrafo precedente la posesión de dos o más tipos de drogas.”(*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 982, publicado el 22 julio 2007, cuyo texto es el siguiente:

“Artículo 299.- Posesión no punible

No es punible la posesión de droga para el propio e inmediato consumo, en cantidad que no exceda de cinco gramos de pasta básica de cocaína, dos gramos de clorhidrato de cocaína, ocho gramos de marihuana o dos gramos de sus derivados, un gramo de látex de opio o doscientos miligramos de sus derivados o doscientos cincuenta miligramos de éxtasis, conteniendo Metilendioxianfetamina - MDA, Metilendioximetanfetamina - MDMA, Metanfetamina o sustancias análogas.

Se excluye de los alcances de lo establecido en el párrafo precedente la posesión de dos o más tipos de drogas.”   (*)

(*) Artículo modificado por la Primera Disposición Complementaria Final de la Ley N° 30681, publicada el 17 noviembre 2017, cuyo texto es el siguiente:

“Artículo 299. Posesión no punible

No es punible la posesión de droga para el propio e inmediato consumo, en cantidad que no exceda de cinco gramos de pasta básica de cocaína, dos gramos de clorhidrato de cocaína, ocho gramos de marihuana o dos gramos de sus derivados, un gramo de látex de opio o doscientos miligramos de sus derivados o doscientos cincuenta miligramos de éxtasis, conteniendo Metilendioxianfetamina - MDA, Metilendioximetanfetamina - MDMA, Metanfetamina o sustancias análogas.

Se excluye de los alcances de lo establecido en el párrafo precedente la posesión de dos o más tipos de drogas.

Tampoco será punible la posesión del cannabis y sus derivados con fines medicinales y terapéuticos, siempre que la cantidad sea la necesaria para el tratamiento del paciente registrado en el Ministerio de Salud, supervisado por el Instituto Nacional de Salud y la DIGEMID, o de un tercero que se encuentre bajo su cuidado o tutela, o para investigación según las leyes sobre la materia y las disposiciones que establezca el ente rector”.

Artículo 300 Suministro indebido de droga
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El médico, farmacéutico, químico, odontólogo u otro profesional sanitario que indebidamente receta, prescribe, administra o expende medicamento que contenga droga tóxica, estupefaciente o psicotrópica, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años e inhabilitación conforme al artículo 36, incisos 1, 2 y 4. (*)

(*) Artículo modificado por la Primera Disposición Complementaria Final de la Ley N° 30681, publicada el 17 noviembre 2017, cuyo texto es el siguiente:

“Artículo 300. Suministro indebido de droga

El médico, farmacéutico, químico, odontólogo u otro profesional sanitario que indebidamente receta, prescribe, administra o expende medicamento que contenga droga tóxica, estupefaciente o psicotrópica, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años e inhabilitación conforme al artículo 36, incisos 1, 2 y 4; a excepción del cannabis y sus derivados, con fines medicinales o terapéuticos, que no es punible, siempre que se suministre a pacientes que se registren en el registro a cargo del Ministerio de Salud, constituido para tal fin”.

CONCORDANCIAS:     Ley N° 26320, Arts. 2 y 4

Artículo 301 Coacción al consumo de droga
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, subrepticiamente, o con violencia o intimidación, hace consumir a otro una droga, será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años y con noventa a ciento ochenta días-multa.

Si el agente actúa con el propósito de estimular o difundir el uso de la droga, o si la víctima es una persona manifiestamente inimputable, la pena será no menor de ocho ni mayor de doce años y de ciento ochenta a trescientos sesenticinco días-multa. (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1351, publicado el 07 enero 2017, cuyo texto es el siguiente:

“Artículo 301.- Coacción al consumo de droga

El que, subrepticiamente, o con violencia o intimidación, hace consumir a otro una droga, será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años y con noventa a ciento ochenta días-multa.

Si el delito se comete en agravio de menores de edad, personas con discapacidad, mujeres en estado de gravidez o adulto mayor, la pena será no menor de ocho ni mayor de doce años y de ciento ochenta a trescientos sesenticinco(*)NOTA SPIJ días-multa.

Si se produce afectación grave a la salud física o mental de la víctima, la pena será no menor de doce ni mayor de quince años.” (*)

(*) De conformidad con el Numeral 6 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de tráfico ilícito de drogas, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

CONCORDANCIAS:     Ley N° 26320, Arts. 2 y 4

Artículo 302 Inducción o instigación al consumo de droga
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que instiga o induce a persona determinada para el consumo indebido de drogas, será reprimido con pena privativa de libertad, no menor de dos ni mayor de cinco años y noventa a ciento ochenta días-multa.

Si el agente actúa con propósito de lucro o si la víctima es persona manifiestamente inimputable, la pena será no menor de cinco ni mayor de ocho años y de ciento ochenta a trescientos sesenticinco días-multa. (*)

(*) De conformidad con el Numeral 6 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de tráfico ilícito de drogas, tipificado en el presente artículo. Asimismo, se excluye el delito de inducción o instigación al consumo de drogas tipificado en el primer párrafo del presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

CONCORDANCIAS:     Ley N° 26320, Arts. 2 y 4

Artículo 303 Pena de expulsión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El extranjero que haya cumplido la condena impuesta será expulsado del país, quedando prohibido su reingreso. (*)

(*) Artículo modificado por la Segunda Disposición Complementaria Modificatoria de la Ley N° 30219, publicado el 08 julio 2014, cuyo texto es el siguiente:

"Artículo 303.- Pena de expulsión

El extranjero que haya cumplido la pena privativa de libertad impuesta o se le haya concedido un beneficio penitenciario será expulsado del país, quedando prohibido su reingreso.”

CONCORDANCIAS:     Ley N° 27378, Art. 1, num. 6)

"CAPITULO IV
DELITOS CONTRA EL ORDEN MIGRATORIO

Artículo 303-A TRAFICO ILICITO DE PERSONAS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que ilícitamente y con el fin de obtener una ventaja patrimonial, para sí o para otro, ejecuta, promueve, favorece o facilita el ingreso o salida del país de terceras personas, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años, con ciento ochenta a trescientos sesenta y cinco días-multa e inhabilitación de uno a dos años conforme al Artículo 36, incisos 1, 2, 3 y 4.

La pena será privativa de libertad no menor de cuatro ni mayor de ocho años, con trescientos sesenta y cinco a setecientos treinta días-multa e inhabilitación de dos a cuatro años, conforme al Artículo 36, incisos 1, 2, 4 y 8 cuando:

1. El agente es funcionario o servidor público encargado de la administración y control migratorio, de la prevención o investigación de cualquier delito, o tiene el deber de aplicar penas o de vigilar su ejecución.

2. Las condiciones en que se transporte a las personas pongan en grave peligro su integridad física o psíquica." (1)(2)

(1) Capítulo IV) incorporado por el Artículo Unico de la Ley Nº 27202, publicada el 15 noviembre 1999.

(2) Artículo 303-A) modificado por el Artículo 2 de la Ley N° 28950, publicada el 16 enero 2007, cuyo texto es el siguiente:

"Artículo 303-A.- Tráfico ilícito de migrantes

El que promueve, favorece, financia o facilita la entrada o salida ilegal del país de otra persona, con el fin de obtener directa o indirectamente, lucro o cualquier otro beneficio para sí o para tercero, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años." (*)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

CONCORDANCIAS:     R. M. N° 2570-2006-IN-0105 (Institucionalizan el “Sistema de Registro y Estadística del delito de Trata de Personas y Afines (RETA)”

R. M. N° 129-2007-IN-0105 (Directiva “Procedimientos para el ingreso, registro, consulta y reporte de datos del Sistema de Registro y
Estadística del delito de Trata de personas y Afines (RETA)”)
Ley Nº 30077, Art. 3 (Delitos comprendidos)

"Artículo 303-B.- Formas agravadas del tráfico ilícito de migrantes

La pena será no menor de cinco ni mayor de ocho años de pena privativa de libertad e inhabilitación conforme al artículo 36 incisos 1, 2, 3, 4 y 5 del Código Penal, cuando:

1. El agente comete el hecho abusando del ejercicio de la función pública.

2. El agente es promotor, integrante o representante de una organización social, tutelar o empresarial, que aprovecha de esta condición y actividades para perpetrar este delito.

3. Exista pluralidad de víctimas.

4. La víctima tiene entre catorce y menos de dieciocho años de edad, o es incapaz.

5. El hecho es cometido por dos o más personas.

6. El agente es cónyuge, conviviente, adoptante, tutor, curador, pariente hasta el cuarto grado de consanguinidad o segundo de afinidad, o tiene a la víctima a su cuidado por cualquier motivo o habitan en el mismo hogar.

La pena será privativa de libertad no menor de 25 años, cuando:

1. Se produzca la muerte de la víctima, lesión grave que ponga en peligro la vida o la seguridad de los migrantes afectados;

2. Las condiciones de transporte ponen en grave peligro su integridad física o psíquica.

3. La víctima es menor de catorce años o padece, temporal o permanentemente, de alguna discapacidad física o mental.

4. El agente es parte de una organización criminal.”(*)

(*) Artículo incorporado por el Artículo 2 de la Ley N° 28950, publicada el 16 enero 2007.

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

CONCORDANCIAS:     R. M. N° 2570-2006-IN-0105 (Institucionalizan el “Sistema de Registro y Estadística del delito de Trata de Personas y Afines (RETA)”

R. M. N° 129-2007-IN-0105 (Directiva “Procedimientos para el ingreso, registro, consulta y reporte de datos del Sistema de Registro y
Estadística del delito de Trata de personas y Afines (RETA)”)
Ley Nº 30077, Art. 3 (Delitos comprendidos)

TITULO XIII DELITOS CONTRA LA ECOLOGIA (*)
==========================================

CAPITULO  UNICO DELITOS CONTRA LOS RECURSOS NATURALES Y EL MEDIO AMBIENTE
#########################################################################

(*) Título XIII modificado por el Artículo 3 de la Ley N° 29263, publicada el 02 octubre 2008.

Artículo 304 Contaminación del medio ambiente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, infringiendo las normas sobre protección del medio ambiente, lo contamina vertiendo residuos sólidos, líquidos, gaseosos o de cualquier otra naturaleza por encima de los límites establecidos, y que causen o puedan causar perjuicio o alteraciones en la flora, fauna y recursos hidrobiológicos, será reprimido con pena privativa de libertad, no menor de uno ni mayor de tres años o con ciento ochenta a trescientos sesenticinco días-multa.

Si el agente actuó por culpa, la pena será privativa de libertad no mayor de un año o prestación de servicio comunitario de diez a treinta jornadas.

Artículo 305 Formas agravadas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La pena será privativa de libertad no menor de dos ni mayor de cuatro años y  trescientos sesenticinco a setecientos treinta días-multa cuando: (*) RECTIFICADO POR FE DE ERRATAS

1. Los actos previstos en el artículo 304 ocasionan peligro para la salud de las personas o para sus bienes.

2. El perjuicio o alteración ocasionados adquieren un carácter catastrófico.

3. El agente actuó clandestinamente en el ejercicio de su actividad.

4. Los actos contaminantes afectan gravemente los recursos naturales que constituyen la base de la actividad económica.

Si, como efecto de la actividad contaminante, se producen lesiones graves o muerte, la pena será:

a) Privativa de libertad no menor de tres ni mayor de seis años y de trescientos sesenticinco a setecientos días-multa, en caso de lesiones graves.

b) Privativa de libertad no menor de cuatro ni mayor de ocho años y de setecientos treinta a mil cuatrocientos sesenta días-multa, en caso de muerte.

Artículo 306 Responsabilidad de funcionario público por otorgamiento ilegal de licencia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario público que otorga licencia de funcionamiento para cualquier actividad industrial o el que, a sabiendas, informa favorablemente para su otorgamiento sin observar las exigencias de las leyes y reglamentos sobre protección del medio ambiente, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años, e inhabilitación de uno a tres años conforme al artículo 36, incisos 1, 2 y 4.

Artículo 307 Incumplimiento de las normas sanitarias
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


El que deposita, comercializa o vierte desechos industriales o domésticos en lugares no autorizados o sin cumplir con las normas sanitarias y de protección del medio ambiente, será reprimido con pena privativa de libertad no mayor de dos años.

Cuando el agente es funcionario o servidor público, la pena será no menor de uno ni mayor de tres años, e inhabilitación de uno a dos años conforme al artículo 36, incisos 1, 2 y 4.

Si el agente actuó por culpa, la pena será privativa de libertad no mayor de un año.

Cuando el agente contraviene leyes, reglamentos o disposiciones establecidas y utiliza los desechos sólidos para la alimentación de animales destinados al consumo humano, la pena será no menor de dos ni mayor de cuatro años y de ciento ochenta a trescientos sesenticinco días-multa.

Ingreso ilegal al territorio nacional de residuos peligrosos

"Artículo 307-A.- El que ilegalmente ingresare al territorio nacional, en forma definitiva o en tránsito, creando un riesgo al equilibrio ambiental, residuos o desechos resultantes de un proceso de producción, extracción, transformación, utilización o consumo, que no hayan ingresado como insumos para procesos productivos calificados como peligrosos o tóxicos por la legislación especial sobre la materia, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años y de ciento cincuenta a trescientos días-multa.

Con igual pena se sancionará al funcionario público que autorice el ingreso al territorio nacional de desechos calificados como peligrosos o tóxicos por los dispositivos legales." (*)

(*) Artículo incorporado por el Artículo Unico de la Ley N° 26828, publicada el 30 junio 1997.

Artículo 308 Depredación de flora y fauna legalmente protegidas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que caza, captura, recolecta, extrae o comercializa especies de flora o fauna que están legalmente protegidas, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años.

La pena será no menor de dos ni mayor de cuatro años y de ciento ochenta a trescientos sesenticinco días-multa cuando:

1. El hecho se comete en período de producción de semillas o de reproducción o crecimiento de las especies.

2. El hecho se comete contra especies raras o en peligro de extinción.

3. El hecho se comete mediante el uso de explosivos o sustancias tóxicas.

Artículo 309 Extracción ilegal de especies acuáticas 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que extrae especies de flora o fauna acuática en épocas, cantidades y zonas que son prohibidas o vedadas o utiliza procedimientos de pesca o caza prohibidos, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años. (*)

(*) Artículo modificado por el Artículo Único de la Ley N° 28154, publicada el 07 enero 2004, cuyo texto es el siguiente:

“Artículo 309.- El que extrae especies de flora o fauna acuática en épocas, cantidades, talla y zonas que son prohibidas o vedadas o utiliza procedimientos de pesca o caza prohibidos, o métodos ilícitos, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años.” (*)

(*) Artículo modificado por el numeral 2 del Artículo 29 del Decreto Legislativo Nº 1084, publicado el 28 junio 2008, cuyo texto es el siguiente:

“Artículo 309.- Extracción ilegal de especies acuáticas

El que extrae especies de flora o fauna acuática en épocas, cantidades, talla y zonas que son prohibidas o vedadas; o captura especies sin contar con un Límite de Captura por Embarcación atribuido de acuerdo con la Ley de la materia o lo hace excediendo los límites impuestos; o utiliza procedimientos de pesca o caza prohibidos, o métodos ilícitos, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años.”

Artículo 310 Depredación de bosques protegidos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que destruye, quema, daña o tala, en todo o en parte, bosques u otras formaciones vegetales naturales o cultivadas que están legalmente protegidas, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años.

La pena será no menor de dos ni mayor de cuatro años y de noventa a ciento veinte días-multa, cuando:

1. Del delito resulta la disminución de aguas naturales, la erosión del suelo o la modificación del régimen climático.

2. El delito se realiza en lugares donde existen vertientes que abastecen de agua a un centro poblado o sistema de irrigación. (*)

(*) El nuevo texto del Artículo 310 modificado por el Artículo 3 de la Ley N° 29263, publicada el 02 octubre 2008, entró en vigencia a partir del 1 de enero de 2009, de conformidad con la Segunda Disposición Final de la citada Ley.

Artículo 311 Utilización indebida de tierras agrícolas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que utiliza tierras destinadas por autoridad competente al uso agrícola con fines de expansión urbana, de extracción o elaboración de materiales de construcción u otros usos específicos, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años.

El que valiéndose de anuncios en el propio terreno o a través de medio de comunicación social, ofrece en venta para fines urbanos u otro cualquiera, áreas agrícolas intangibles, será reprimido con la misma pena.

Artículo 312 Autorización de actividad contraria a los planes o usos previstos por la ley
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario público que autoriza un proyecto de urbanización para otra actividad no conforme con los planes o usos previstos por los dispositivos legales o el profesional que informa favorablemente, a sabiendas de su ilegalidad, será reprimido con pena privativa de libertad no mayor de dos años e inhabilitación de uno a dos años conforme al artículo 36, incisos 1, 2 y 4. (*) RECTIFICADO POR FE DE ERRATAS

Artículo 313 Alteración del ambiente o paisaje
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, contraviniendo las disposiciones de la autoridad competente, altera el ambiente natural o el paisaje urbano o rural, o  modifica la flora o fauna, mediante la construcción de obras o tala de árboles que dañan la armonía de sus elementos, será reprimido con pena privativa de libertad no mayor de dos años y con sesenta a noventa días-multa.

Artículo 314 Medida cautelar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El Juez Penal ordenará, como medida cautelar, la suspensión inmediata de la actividad contaminante, así como la clausura definitiva o temporal del establecimiento de que se trate de conformidad con el artículo 105 inciso 1, sin perjuicio de lo que pueda ordenar la autoridad en materia ambiental. (*)

(*) Título XIII modificado por el Artículo 3 de la Ley N° 29263, publicada el 02 octubre 2008, cuyo texto es el siguiente:

“TÍTULO XIII

DELITOS AMBIENTALES

CAPÍTULO I 
###########
DELITOS DE CONTAMINACIÓN

CONCORDANCIAS:     D.S.N° 007-2017-MINAM, Arts. 3 y 4

Artículo 304.- Contaminación del ambiente
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, infringiendo leyes, reglamentos o límites máximos permisibles, provoque o realice descargas, emisiones, emisiones de gases tóxicos, emisiones de ruido, filtraciones, vertimientos o radiaciones contaminantes en la atmósfera, el suelo, el subsuelo, las aguas terrestres, marítimas o subterráneas, que cause o pueda causar perjuicio, alteración o daño grave al ambiente o sus componentes, la calidad ambiental o la salud ambiental, según la calificación reglamentaria de la autoridad ambiental, será reprimido con pena privativa de libertad no menor de cuatro años ni mayor de seis años y con cien a seiscientos días-multa.

Si el agente actuó por culpa, la pena será privativa de libertad no mayor de tres años o prestación de servicios comunitarios de cuarenta a ochenta jornadas. (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1351, publicado el 07 enero 2017, cuyo texto es el siguiente:

“Artículo 304.- Contaminación del ambiente

El que, infringiendo leyes, reglamentos o límites máximos permisibles, provoque o realice descargas, emisiones, emisiones de gases tóxicos, emisiones de ruido, filtraciones, vertimientos o radiaciones contaminantes en la atmósfera, el suelo, el subsuelo, las aguas terrestres, marítimas o subterráneas, que cause o pueda causar perjuicio, alteración o daño grave al ambiente o sus componentes, la calidad ambiental o la salud ambiental, será reprimido con pena privativa de libertad no menor de cuatro años ni mayor de seis años y con cien a seiscientos días-multa.

Si el agente actuó por culpa, la pena será privativa de libertad no mayor de tres años o prestación de servicios comunitarios de cuarenta a ochenta jornadas.”

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 305.- Formas agravadas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La pena privativa de libertad será no menor de cuatro años ni mayor de siete años y con trescientos a mil días-multa si el agente incurre en cualquiera de los siguientes supuestos:

1. Falsea u oculta información sobre el hecho contaminante, la cantidad o calidad de las descargas, emisiones, filtraciones, vertimientos o radiaciones contaminantes referidos en el artículo 304, a la autoridad competente o a la institución autorizada para realizar labores de fiscalización o auditoría ambiental.

2. Obstaculiza o impide la actividad fiscalizadora de auditoría ordenada por la autoridad administrativa competente.

3. Actúa clandestinamente en el ejercicio de su actividad.

Si por efecto de la actividad contaminante se producen lesiones graves o muerte, la pena será:

1. Privativa de libertad no menor de cinco años ni mayor de ocho años y con seiscientos a mil días-multa, en caso de lesiones graves.

2. Privativa de libertad no menor de seis años ni mayor de diez años y con setecientos cincuenta a tres mil quinientos días-multa, en caso de muerte.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

Artículo 306.- Incumplimiento de las normas relativas al manejo de residuos sólidos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, sin autorización o aprobación de la autoridad competente, establece un vertedero o botadero de residuos sólidos que pueda perjudicar gravemente la calidad del ambiente, la salud humana o la integridad de los procesos ecológicos, será reprimido con pena privativa de libertad no mayor de cuatro años.

Si el agente actuó por culpa, la pena será privativa de libertad no mayor de dos años.

Cuando el agente, contraviniendo leyes, reglamentos o disposiciones establecidas, utiliza desechos sólidos para la alimentación de animales destinados al consumo humano, la pena será no menor de tres años ni mayor de seis años y con doscientos sesenta a cuatrocientos cincuenta días-multa.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

Artículo 307.- Tráfico ilegal de residuos peligrosos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que ingrese ilegalmente al territorio nacional, use, emplee, coloque, traslade o disponga sin la debida autorización, residuos o desechos tóxicos o peligrosos para el ambiente, resultantes de un proceso de producción, extracción, transformación, utilización o consumo, será reprimido con pena privativa de libertad no menor de cuatro años ni mayor de seis años y con trescientos a cuatrocientos días-multa.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

“Artículo 307-A.- Delito de minería ilegal

Será reprimido con pena privativa de libertad no menor de cuatro años ni mayor de ocho años y con cien a seiscientos días-multa, el que realice actividad de exploración, extracción, explotación u otros actos similares, de recursos minerales, metálicos o no metálicos, sin contar con la autorización de la entidad administrativa competente, que cause o pueda causar perjuicio, alteración o daño al ambiente o sus componentes, la calidad ambiental o la salud ambiental.

Si el agente actuó por culpa, la pena será privativa de libertad no mayor de tres años o con prestación de servicios comunitarios de cuarenta a ochenta jornadas." (*)

(*) Artículo incorporado por el Artículo Primero del Decreto Legislativo Nº 1102, publicado el 29 febrero 2012, que entró en vigencia a los quince días de su publicación.

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1351, publicado el 07 enero 2017, cuyo texto es el siguiente:

“Artículo 307-A.- Delito de minería ilegal

El que realice actividad de exploración, extracción, explotación u otro acto similar de recursos minerales metálicos y no metálicos sin contar con la autorización de la entidad administrativa competente que cause o pueda causar perjuicio, alteración o daño al ambiente y sus componentes, la calidad ambiental o la salud ambiental, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años y con cien a seiscientos días-multa.

La misma pena será aplicada al que realice actividad de exploración, extracción, explotación u otro acto similar de recursos minerales metálicos y no metálicos que se encuentre fuera del proceso de formalización, que cause o pueda causar perjuicio, alteración o daño al ambiente y sus componentes, la calidad ambiental o la salud ambiental.

Si el agente actuó por culpa, la pena será privativa de libertad, no mayor de tres o con prestación de servicios comunitarios de cuarenta a ochenta jornadas.”(*)

(*) De conformidad con la Única Disposición Complementaria Final del Decreto Legislativo N° 1351, publicado el 07 enero 2017, se dispone que están exentos de responsabilidad penal por la comisión del delito de minería ilegal establecido en el presente artículo, quienes se encuentren en los siguientes supuestos: a. El sujeto de formalización minera que no logra la autorización final de inicio o reinicio de operaciones mineras por culpa inexcusable o negligente del funcionario a cargo del proceso de formalización; b. El agente de los delitos de minería ilegal, que se inserte al Registro Integral de Formalización Minera, dentro del plazo establecido en el numeral 4.2 del artículo 4 del Decreto Legislativo Nª 1293.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

"Artículo 307-B.- Formas agravadas

La pena será no menor de ocho años ni mayor de diez años y con trescientos a mil días-multa, cuando el delito previsto en el anterior artículo se comete en cualquiera de los siguientes supuestos:

1. En zonas no permitidas para el desarrollo de actividad minera.

2. En áreas naturales protegidas, o en tierras de comunidades nativas, campesinas o indígenas.

3. Utilizando dragas, artefactos u otros instrumentos similares.

4. Si el agente emplea instrumentos u objetos capaces de poner en peligro la vida, la salud o el patrimonio de las personas.

5. Si se afecta sistemas de irrigación o aguas destinados al consumo humano.

6. Si el agente se aprovecha de su condición de funcionario o servidor público.

7. Si el agente emplea para la comisión del delito a menores de edad u otra persona inimputable." (*)

(*) Artículo incorporado por el Artículo Primero del Decreto Legislativo Nº 1102, publicado el 29 febrero 2012, que entró en vigencia a los quince días de su publicación.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

"Artículo 307-C.- Delito de financiamiento de la minería ilegal

El que financia la comisión de los delitos previstos en los artículos 307-A o sus formas agravadas, será reprimido con pena privativa de libertad no menor de cuatro años ni mayor de doce años y con cien a seiscientos días-multa." (*)

(*) Artículo incorporado por el Artículo Primero del Decreto Legislativo Nº 1102, publicado el 29 febrero 2012, que entró en vigencia a los quince días de su publicación.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

"Artículo 307-D.- Delito de obstaculización de la fiscalización administrativa

El que obstaculiza o impide la actividad de evaluación, control y fiscalización de la autoridad administrativa relacionada con la minería ilegal, será reprimido con pena privativa de la libertad no menor de cuatro años ni mayor de ocho años." (*)

(*) Artículo incorporado por el Artículo Primero del Decreto Legislativo Nº 1102, publicado el 29 febrero 2012, que entró en vigencia a los quince días de su publicación.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

"Artículo 307-E.- Actos preparatorios de minería ilegal

El que adquiere, vende, distribuye, comercializa, transporta, importa, posee o almacena insumos o maquinarias destinadas a la comisión de los delitos de minería ilegal, será reprimido con pena privativa de libertad no menor de tres años ni mayor de seis años y con cien a seiscientos días-multa." (1)(2)

(1) Artículo incorporado por el Artículo Primero del Decreto Legislativo Nº 1102, publicado el 29 febrero  2012, que entró en vigencia a los quince días de su publicación.

(2) Artículo modificado por la Única Disposición Complementaria Modificatoria del Decreto Legislativo Nº 1107, publicado el 20 abril 2012, cuyo texto es el siguiente:

“Artículo 307-E.- Tráfico ilícito de insumos químicos y maquinarias destinados a minería ilegal

El que, infringiendo las leyes y reglamentos, adquiere, vende, distribuye, comercializa, transporta, importa, posee o almacena insumos químicos, con el propósito de destinar dichos bienes a la comisión de los delitos de minería ilegal, será reprimido con pena privativa de libertad no menor de tres años ni mayor de seis años y con cien a seiscientos días-multa.

El que adquiere, vende, arrienda, transfiere o cede en uso bajo cualquier título, distribuye, comercializa, transporta, importa, posee o almacena maquinarias, a sabiendas de que serán destinadas a la comisión de los delitos de minería ilegal, será reprimido con pena privativa de libertad no menor de tres años ni mayor de seis años y con cien a seiscientos días-multa.”

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

"Artículo 307-F.- Inhabilitación

El agente de los delitos previstos en los artículos 307-A, 307-B, 307-C, 307-D y 307-E, será además sancionado, de conformidad con el artículo 36, inciso 4, con la pena de inhabilitación para obtener, a nombre propio o a través de terceros, concesiones mineras, de labor general, de beneficio o transporte de minerales metálicos o no metálicos, así como para su comercialización, por un periodo igual al de la pena principal”. (*)

(*) Artículo incorporado por el Artículo Primero del Decreto Legislativo Nº 1102, publicado el 29 febrero  2012, que entró en vigencia a los quince días de su publicación.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

CAPÍTULO II 
############
DELITOS CONTRA LOS RECURSOS NATURALES

CONCORDANCIAS:     D.S.N° 007-2017-MINAM, Arts. 5 y 6

Artículo 308.- Tráfico ilegal de especies de flora y fauna silvestre protegida
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que adquiere, vende, transporta, almacena, importa, exporta o reexporta productos o especímenes de especies de flora silvestre no maderable y/o fauna silvestre protegidas por la legislación nacional, sin un permiso o certificado válido, cuyo origen no autorizado conoce o puede presumir, será reprimido con pena privativa de libertad no menor de tres años ni mayor de cinco años y con ciento ochenta a cuatrocientos días-multa. (*)

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 308.- Tráfico ilegal de especies de flora y fauna silvestre

El que adquiere, vende, transporta, almacena, importa, exporta o reexporta productos o especímenes de especies de flora silvestre no maderable y/o fauna silvestre, sin un permiso o certificado válido, cuyo origen no autorizado conoce o puede presumir, será reprimido con pena privativa de libertad no menor de tres años ni mayor de cinco años y con ciento ochenta a cuatrocientos días-multa."

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

Artículo 308-A.- Tráfico ilegal de especies acuáticas de la flora y fauna silvestre protegidas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no menor de tres años ni mayor de cinco años y con ciento ochenta a cuatrocientos días-multa, el que adquiere, vende, transporta, almacena, importa, exporta o reexporta productos o especímenes de especies acuáticas de la flora y/o fauna silvestre protegidas por la legislación nacional bajo cualquiera de los siguientes supuestos:

1. Sin un permiso, licencia o certificado válido.

2. En épocas, cantidades, talla o zonas que son prohibidas o vedadas.(*)

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 308-A.- Tráfico ilegal de especies acuáticas de la flora y fauna silvestre Será reprimido con pena privativa de libertad no menor de tres años ni mayor de cinco años y con ciento ochenta a cuatrocientos días-multa, el que adquiere, vende, transporta, almacena, importa, exporta o reexporta productos o especímenes de especies acuáticas de la flora y/o fauna silvestre bajo cualquiera de los siguientes supuestos:

1. Sin un permiso, licencia o certificado válido.

2. En épocas, cantidades, talla o zonas que son prohibidas o vedadas."

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

Artículo 308-B.- Extracción ilegal de especies acuáticas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que extrae especies de flora o fauna acuática en épocas, cantidades, talla y zonas que son prohibidas o vedadas, o captura especies sin contar con el respectivo permiso o exceda el límite de captura por embarcación, asignado por la autoridad administrativa competente y la ley de la materia, o lo hace excediendo el mismo o utiliza explosivos, medios químicos u otros métodos prohibidos o declarados ilícitos, será reprimido con pena privativa de libertad no menor de tres años ni mayor de cinco años.(*)

(*) Artículo modificado por la Primera Disposición Complementaria Modificatoria del Decreto Legislativo N° 1393, publicado el 06 septiembre 2018, el mismo que entró en vigencia a los treinta días calendario desde su publicación, cuyo texto es el siguiente:

“Artículo 308-B.- Extracción y procesamiento ilegal de especies acuáticas

El que extrae especies de flora o fauna acuática en épocas, cantidades, talla y zonas que son prohibidas o vedadas, o captura especies o las procesa sin contar con el respectivo permiso o licencia o exceda el límite de captura por embarcación, asignado por la autoridad administrativa competente y la ley de la materia, o lo hace excediendo el mismo o utiliza explosivos, o embarcaciones construidas sin autorización o sin licencia, medios químicos u otros métodos prohibidos o declarados ilícitos, será reprimido con pena privativa de libertad no menor de tres años ni mayor de cinco años.”

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

Artículo 308-C.- Depredación de flora y fauna silvestre protegida
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que caza, captura, colecta, extrae o posee productos, raíces o especímenes de especies de flora y/o fauna silvestre protegidas por la legislación nacional, sin contar con la concesión, permiso, licencia o autorización u otra modalidad de aprovechamiento o extracción, otorgada por la autoridad competente, será reprimido con pena privativa de libertad no menor de tres años ni mayor de cinco años y con cincuenta a cuatrocientos días-multa.(*)

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 308-C.- Depredación de flora y fauna silvestre

El que caza, captura, colecta, extrae o posee productos, raíces o especímenes de especies de flora y/o fauna silvestre, sin contar con la concesión, permiso, licencia o autorización u otra modalidad de aprovechamiento o extracción, otorgada por la autoridad competente, será reprimido con pena privativa de libertad no menor de tres años ni mayor de cinco años y con cincuenta a cuatrocientos días-multa."

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

Artículo 308-D.- Tráfico ilegal de recursos genéticos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que adquiere, vende, transporta, almacena, importa, exporta o reexporta, de forma no autorizada, recursos genéticos de especies de flora y/o fauna silvestre protegidas por la legislación nacional, será reprimido con pena privativa de libertad no menor de tres años ni mayor de cinco años y con ciento ochenta a cuatrocientos días-multa.

La misma pena será aplicable para el que a sabiendas financia, de modo que sin su cooperación no se hubiera podido cometer las actividades señaladas en el primer párrafo, y asimismo al que las dirige u organiza.(*)

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 308-D.- Tráfico ilegal de recursos genéticos

El que adquiere, vende, transporta, almacena, importa, exporta o reexporta, de forma no autorizada, recursos genéticos de especies de flora y/o fauna silvestre, será reprimido con pena privativa de libertad no menor de tres años ni mayor de cinco años y con ciento ochenta a cuatrocientos días-multa.

La misma pena será aplicable para el que a sabiendas financia, de modo que sin su cooperación no se hubiera podido cometer las actividades señaladas en el primer párrafo, y asimismo al que las dirige u organiza."

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

Artículo 309.- Formas agravadas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En los casos previstos en los artículos 308, 308-A, 308-B y 308-C, la pena privativa de libertad será no menor de cuatro años ni mayor de siete años cuando el delito se cometa bajo cualquiera de los siguientes supuestos:

1. Cuando los especímenes, productos, recursos genéticos, materia del ilícito penal, provienen de áreas naturales protegidas de nivel nacional o de zonas vedadas para la extracción de flora y/o fauna silvestre, según corresponda.

2. Cuando los especímenes, productos o recursos genéticos materia del ilícito penal, provienen de las reservas intangibles de comunidades nativas o campesinas o pueblos indígenas en situación de aislamiento o de contacto inicial, según corresponda.

3. Cuando es un funcionario o servidor público que omitiendo funciones autoriza, aprueba o permite la realización de este hecho delictivo en su tipo básico, o permite la comercialización, adquisición o transporte de los recursos de flora y fauna ilegalmente obtenidos.

4. Mediante el uso de armas, explosivos o sustancias tóxicas. (*)

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 309.- Formas agravadas

En los casos previstos en los artículos 308, 308-A, 308-B y 308-C, la pena privativa de libertad será no menor de cuatro años ni mayor de siete años cuando el delito se cometa bajo cualquiera de los siguientes supuestos:

1. Cuando los especímenes, productos, recursos genéticos, materia del ilícito penal, provienen de áreas naturales protegidas de nivel nacional o de zonas vedadas para la extracción de flora y/o fauna silvestre, según corresponda.

2. Cuando los especímenes, productos o recursos genéticos materia del ilícito penal, provienen de las tierras o territorios en posesión o propiedad de comunidades nativas o campesinas; o, de las Reservas Territoriales o Reservas Indígenas para pueblos indígenas en situación de aislamiento o de contacto inicial, según corresponda.

3. Cuando es un funcionario o servidor público que omitiendo funciones autoriza, aprueba o permite la realización de este hecho delictivo en su tipo básico, o permite la comercialización, adquisición o transporte de los recursos de flora y fauna ilegalmente obtenidos.

CONCORDANCIAS:     D.S.N° 007-2017-MINAM, Arts. 7 y 8

4. Mediante el uso de armas, explosivos o sustancias tóxicas.

5. Cuando se trate de especies de flora y fauna silvestre o recursos genéticos protegidos por la legislación nacional."

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

Artículo 310.- Delitos contra los bosques o formaciones boscosas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no menor de tres años ni mayor de seis años y con prestación de servicios comunitarios de cuarenta a ochenta jornadas el que, sin contar con permiso, licencia, autorización o concesión otorgada por autoridad competente, destruye, quema, daña o tala, en todo o en parte, bosques u otras formaciones boscosas, sean naturales o plantaciones.(*)

(*) El presente Artículo entró en vigencia a partir del 1 de enero de 2009, de conformidad con la Segunda Disposición Final de la Ley N° 29263, publicada el 02 octubre 2008.

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 310.- Delitos contra los bosques o formaciones boscosas

Será reprimido con pena privativa de libertad no menor de cuatro años ni mayor de seis años y con prestación de servicios comunitarios de cuarenta a ochenta jornadas el que, sin contar con permiso, licencia, autorización o concesión otorgada por autoridad competente, destruye, quema, daña o tala, en todo o en parte, bosques u otras formaciones boscosas, sean naturales o plantaciones."

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

Artículo 310-A.- Tráfico ilegal de productos forestales maderables
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que adquiere, almacena, transforma, transporta, oculta, custodia, vende, embarca, desembarca, importa, exporta o reexporta productos o especímenes forestales maderables protegidos por la legislación nacional, cuyo origen ilícito conoce o puede presumir, será reprimido con pena privativa de libertad no menor de tres años ni mayor de seis años y con cien a seiscientos días-multa.

La misma pena será aplicable para el que a sabiendas financia, de modo que sin su cooperación no se hubiera podido cometer las actividades señaladas en el primer párrafo, y asimismo al que las dirige u organiza.

Está fuera del supuesto previsto en el primer párrafo, el que realiza los hechos previstos en el presente artículo, si sus acciones estuvieron basadas en una diligencia razonable y en información o documentos expedidos por la autoridad competente, aunque estos sean posteriormente declarados nulos o inválidos. (*)

(*) El presente Artículo entra en vigencia a partir del 1 de enero de 2009, de conformidad con la Segunda Disposición Final de la Ley N° 29263, publicada el 02 octubre 2008.

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 310-A.- Tráfico ilegal de productos forestales maderables

El que adquiere, acopia, almacena, transforma, transporta, oculta, custodia, comercializa, embarca, desembarca, importa, exporta o reexporta productos o especímenes forestales maderables, cuyo origen ilícito, conoce o puede presumir, será reprimido con pena privativa de libertad no menor de cuatro años ni mayor de siete años y con cien a seiscientos días-multa."

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

Artículo 310-B.- Obstrucción de procedimiento
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que obstruye, impide o traba una investigación, verificación, supervisión o auditoría, en relación con la extracción, transporte, transformación, venta, exportación, reexportación o importación de especímenes de flora y/o de fauna silvestre protegidas por la legislación nacional, será reprimido con pena privativa de libertad no menor de dos años ni mayor de cinco años.

La pena será privativa de libertad no menor de cuatro años ni mayor de ocho años para el que emplea intimidación o violencia contra un funcionario público o contra la persona que le presta asistencia, en el ejercicio de sus funciones, en relación con actividades de extracción y la venta de productos o especímenes forestales maderables. (*)

(*) El presente Artículo entra en vigencia a partir del 1 de enero de 2009, de conformidad con la Segunda Disposición Final de la Ley N° 29263, publicada el 02 octubre 2008.

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 310-B.- Obstrucción de procedimiento

El que obstruye, impide o traba una investigación, verificación, supervisión o auditoría, en relación con la extracción, transporte, transformación, venta, exportación, reexportación o importación de especímenes de flora y/o de fauna silvestre, será reprimido con pena privativa de libertad no menor de cuatro años ni mayor de siete años.

La pena será privativa de libertad no menor de cinco años ni mayor de ocho años para el que emplea intimidación o violencia contra un funcionario público o contra la persona que le presta asistencia, en el ejercicio de sus funciones."

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

Artículo 310-C.- Formas agravadas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

En los casos previstos en los artículos 310, 310-A y 310-B, la pena privativa de libertad será no menor de cinco años ni mayor de ocho años, bajo cualquiera de los siguientes supuestos:

1. Si se comete el delito al interior de tierras de comunidades nativas o campesinas o pueblos indígenas, áreas naturales protegidas, zonas vedadas, concesiones forestales y áreas de conservación privadas debidamente reconocidas por la autoridad competente.

2. Si como consecuencia de la conducta prevista en los artículos correspondientes se afecten vertientes que abastecen de agua a centros poblados, sistemas de irrigación o se erosione el suelo haciendo peligrar las actividades económicas del lugar.

3. Si el autor o partícipe es funcionario o servidor público.

4. Si el delito se comete respecto de especímenes que han sido marcados para realizar estudios o han sido reservados como semilleros, cuando se trate de especies protegidas por la legislación nacional.

5. Si el delito se comete con el uso de armas, explosivo o similar.

6. Si el delito se comete con el concurso de dos o más personas.

7. Si el delito es cometido por los titulares de concesiones forestales.

La pena privativa de libertad será no menor de seis años ni mayor de diez años cuando:

1. El delito es cometido por un agente que actúa en calidad de integrante, jefe, cabecilla o dirigente de una organización delictiva o banda destinada a perpetrar estos delitos.(*)

(*) Numeral modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 30077, publicada el 20 agosto 2013, la misma que entró en vigencia el 1 de julio de 2014, cuyo texto es el siguiente:

"1. El agente actúa como integrante de una organización criminal."

2. El autor causa lesiones graves o muerte durante la comisión del hecho delictivo o a consecuencia de dicho acto. (*)

(*) El presente Artículo entra en vigencia a partir del 1 de enero de 2009, de conformidad con la Segunda Disposición Final de la Ley N° 29263, publicada el 02 octubre 2008.

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 310-C.- Formas agravadas

En los casos previstos en los artículos 310, 310-A y 310-B, la pena privativa de libertad será no menor de ocho años ni mayor de diez años, bajo cualquiera de los siguientes supuestos:

1. Si se comete el delito al interior de tierras en propiedad o posesión de comunidades nativas, comunidades campesinas, pueblos indígenas, reservas indígenas; o en reservas territoriales o reservas indígenas a favor de pueblos indígenas en contacto inicial o aislamiento voluntario, áreas naturales protegidas, zonas vedadas, concesiones forestales o áreas de conservación privadas debidamente reconocidas por la autoridad competente.

2. Si como consecuencia de la conducta prevista en los artículos correspondientes se afecten vertientes que abastecen de agua a centros poblados, sistemas de irrigación o se erosione el suelo haciendo peligrar las actividades económicas del lugar.

3. Si el autor o partícipe es funcionario o servidor público.

4. Si el delito se comete respecto de especímenes que han sido marcados para realizar estudios o han sido reservados como semilleros.

5. Si el delito se comete con el uso de armas, explosivo o similar.

6. Si el delito se comete con el concurso de dos o más personas.

7. Si el delito es cometido por los titulares de concesiones forestales.

8. Si se trata de productos o especímenes forestales maderables protegidos por la legislación nacional.

La pena privativa de libertad será no menor de diez años ni mayor de doce años cuando:

1. El agente actúa como integrante de una organización criminal.

2. El autor causa lesiones graves o muerte durante la comisión del hecho delictivo o a consecuencia de dicho acto.

3. Si el hecho delictivo se realiza para cometer delitos tributarios, aduaneros y de lavados de activos.

4. Financie o facilite la comisión de estos delitos."

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

Artículo 311.- Utilización indebida de tierras agrícolas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, sin la autorización de cambio de uso, utiliza tierras destinadas por autoridad competente al uso agrícola con fines de expansión urbana, de extracción o elaboración de materiales de construcción u otros usos específicos, será reprimido con pena privativa de libertad no menor de dos años ni mayor de cuatro años.

La misma pena será para el que vende u ofrece en venta, para fines urbanos u otro cualquiera, tierras zonificadas como uso agrícola.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

PROCESOS CONSTITUCIONALES
JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 312.- Autorización de actividad contraria a los planes o usos previstos por la ley
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario o servidor público que autoriza o se pronuncia favorablemente sobre un proyecto de urbanización para otra actividad no conforme con los planes o usos previstos por los dispositivos legales o el profesional que informa favorablemente, será reprimido con pena privativa de libertad no menor de dos años ni mayor de cuatro años e inhabilitación de un año a tres años conforme al artículo 36 incisos 1, 2 y 4.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

CONCORDANCIAS:     D.S.N° 007-2017-MINAM, Arts. 7 y 8

Artículo 313.- Alteración del ambiente o paisaje
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, contraviniendo las disposiciones de la autoridad competente, altera el ambiente natural o el paisaje urbano o rural, o modifica la flora o fauna, mediante la construcción de obras o tala de árboles, será reprimido con pena privativa de libertad no mayor de cuatro años y con sesenta a noventa días-multa.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

CAPÍTULO III 
#############
RESPONSABILIDAD FUNCIONAL E INFORMACIÓN FALSA

CONCORDANCIAS:     D.S.N° 007-2017-MINAM, Arts. 7 y 8

Artículo 314.- Responsabilidad de funcionario público por otorgamiento ilegal de derechos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario público que sin observar leyes, reglamentos, estándares ambientales vigentes, por haber faltado gravemente a sus obligaciones funcionales, autoriza o se pronuncia favorablemente sobre el otorgamiento o renovación de autorización, licencia, concesión, permiso u otro derecho habilitante en favor de la obra o actividad a que se refiere el presente Título, será reprimido con pena privativa de libertad no menor de tres años ni mayor de seis años, e inhabilitación de un año a seis años conforme al artículo 36 incisos 1, 2 y 4.

La misma pena será para el funcionario público competente para combatir las conductas descritas en el presente Título y que, por negligencia inexcusable o por haber faltado gravemente a sus obligaciones funcionales, facilite la comisión de los delitos previstos en el presente Título. (*)

(*) Artículo modificado por el Artículo Segundo del Decreto Legislativo Nº 1102, publicado el 29 febrero 2012, que entró en vigencia a los quince días de su publicación, cuyo texto es el siguiente:

“Artículo 314.- Responsabilidad de funcionario público por otorgamiento ilegal de derechos

El funcionario público que sin observar leyes, reglamentos, estándares ambientales vigentes, por haber faltado gravemente a sus obligaciones funcionales, autoriza o se pronuncia favorablemente sobre el otorgamiento, renovación o cancelación de autorización, licencia, concesión, permiso u otro derecho habilitante en favor de la obra o actividad a que se refiere el presente Título, será reprimido con pena privativa de libertad no menor de tres años ni mayor de seis años, e inhabilitación de un año a seis años conforme al artículo 36 incisos 1, 2 y 4.

La misma pena será para el funcionario público competente para combatir las conductas descritas en el presente Título y que, por negligencia inexcusable o por haber faltado gravemente a sus obligaciones funcionales, facilite la comisión de los delitos previstos en el presente Título.” (*)

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 314.- Responsabilidad de funcionario público por otorgamiento ilegal de derechos

El funcionario público que sin observar leyes, reglamentos, estándares ambientales vigentes, por haber faltado gravemente a sus obligaciones funcionales, autoriza el otorgamiento, renovación o cancelación de autorización, licencia, concesión, permiso u otro derecho habilitante en favor de la obra o actividad a que se refiere el presente Título, será reprimido con pena privativa de libertad no menor de cuatro años ni mayor de siete años, e inhabilitación de un año a siete años conforme al artículo 36 incisos 1, 2 y 4.

El servidor público que sin observar leyes, reglamentos, estándares ambientales vigentes se pronuncia favorablemente en informes u otro documento de gestión sobre el otorgamiento, renovación o cancelación de autorización, licencia, concesión, permiso u otro derecho habilitante en favor de la obra o actividad a que se refiere el presente Título, será reprimido con pena privativa de libertad no menor de cuatro años ni mayor de siete años, e inhabilitación de un año a siete años conforme al artículo 36 incisos 1, 2 y 4.

La misma pena será para el funcionario público competente para combatir las conductas descritas en el presente Título y que, por negligencia inexcusable o por haber faltado gravemente a sus obligaciones funcionales, facilite la comisión de los delitos previstos en el presente Título."

Artículo 314-A.- Responsabilidad de los representantes legales de las personas jurídicas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los representantes legales de las personas jurídicas dentro de cuya actividad se cometan los delitos previstos en este Título serán responsables penalmente de acuerdo con las reglas establecidas en los artículos 23 y 27 de este Código.

Artículo 314-B.- Responsabilidad por información falsa contenida en informes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, conociendo o pudiendo presumir la falsedad o la inexactitud, suscriba o realice estudios, evaluaciones, auditorías ambientales, planes de manejo forestal u otro documento de gestión forestal, exigido conforme a ley, en los que se incorpore o avale información falsa o inexacta, será reprimido con pena privativa de libertad no menor de tres años ni mayor de cinco años. (*)

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 314-B.- Responsabilidad por información falsa contenida en informes

El que, conociendo o pudiendo presumir la falsedad o la inexactitud, suscriba, realice, inserte o hace insertar al procedimiento administrativo, estudios, evaluaciones, auditorías ambientales, planes de manejo forestal, solicitudes u otro documento de gestión forestal, exigido conforme a ley, en los que se incorpore o avale información falsa o inexacta, será reprimido con pena privativa de libertad no menor de cuatro años ni mayor de seis años, e inhabilitación de uno a seis años, conforme al inciso 2 y 4 del artículo 36.

Será reprimido con la misma pena todo aquel que, hace uso de un documento privado falso o falsificado o conteniendo información falsa como si fuese legítimo, con fines de evadir los procedimientos de control y fiscalización en materia forestal y de fauna silvestre relativos al presente Título, incluyendo los controles tributarios, aduaneros y otros."

CONCORDANCIAS:     D.S.N° 007-2017-MINAM, Arts. 3 y 4

CAPÍTULO IV 
############
MEDIDAS CAUTELARES Y EXCLUSIÓN O REDUCCIÓN DE PENAS

Artículo 314-C.- Medidas cautelares
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sin perjuicio de lo ordenado por la autoridad administrativa, el Juez dispondrá la suspensión inmediata de la actividad contaminante, extractiva o depredatoria, así como las otras medidas cautelares que correspondan.

En los delitos previstos en este Título, el Juez procederá a la incautación previa de los especímenes presuntamente ilícitos y de los aparatos o medios utilizados para la comisión del presunto ilícito. Asimismo, el Juez, a solicitud del Ministerio Público, ordenará el allanamiento o descerraje del lugar donde presuntamente se estuviere cometiendo el ilícito penal.

En caso de emitirse sentencia condenatoria, los especímenes ilícitos podrán ser entregados a una institución adecuada, según recomendación de la autoridad competente, y en caso de no corresponder, serán destruidos.

En ningún caso procederá la devolución de los ejemplares ilícitos al encausado.

Artículo 314-D.- Exclusión o reducción de penas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, encontrándose en una investigación fiscal a cargo del Ministerio Público o en el desarrollo de un proceso penal, proporcione información veraz, oportuna y significativa sobre la realización de un delito ambiental, podrá ser beneficiado en la sentencia con reducción de pena, tratándose de autores, y con exclusión de la misma para los partícipes, siempre y cuando la información proporcionada haga posible alguna de las siguientes situaciones:

1. Evitar la comisión del delito ambiental en el que interviene.

2. Promover el esclarecimiento del delito ambiental en el que intervino.

3. La captura del autor o autores del delito ambiental, así como de los partícipes.

El beneficio establecido en el presente artículo deberá ser concedido por los Jueces con criterio de objetividad y previa opinión del Ministerio Público.” (*)

(*) Artículo modificado por el Artículo Segundo del Decreto Legislativo Nº 1102, publicado el 29 febrero 2012, que entró en vigencia a los quince días de su publicación, cuyo texto es el siguiente:

“Artículo 314-D.- Exclusión o reducción de penas

El que, encontrándose en una investigación fiscal o en el desarrollo de un proceso penal, proporcione información veraz, oportuna y significativa sobre la realización de un delito ambiental, podrá ser beneficiado en la sentencia con reducción de pena, tratándose de autores, y con exclusión de la misma para los partícipes, siempre y cuando la información proporcionada haga posible alguna de las siguientes situaciones:

1. Evitar la comisión del delito ambiental en el que interviene.

2. Promover el esclarecimiento del delito ambiental en el que intervino.

3. La captura del autor o autores del delito ambiental, así como de los partícipes.

4. La desarticulación de organizaciones criminales vinculadas a la minería ilegal.

El beneficio establecido en el presente artículo deberá ser concedido por los Jueces con criterio de objetividad y previa opinión del Ministerio Público.”

TITULO XIV DELITOS CONTRA LA TRANQUILIDAD PUBLICA
=================================================

CONCORDANCIAS:     D.U. N° 052-2010, Art. 1, num. 1.2

CAPITULO I DELITOS CONTRA LA PAZ PUBLICA
########################################

CONCORDANCIAS:     D.S. Nº 017-2008-JUS, Art. 43 (De los Procuradores Públicos Especializados en delitos contra el Orden Público)

D.S Nº 009-2010-JUS (Aprueban Procedimiento para el pago de la reparación civil a favor del Estado en casos de procesos seguidos sobre
delitos de corrupción y otros delitos conexos)

Artículo 315 Disturbios
~~~~~~~~~~~~~~~~~~~~~~~

El que toma parte en una reunión tumultuaria, en la que se haya cometido colectivamente violencia contra las personas o contra las propiedades, será reprimido con pena privativa de libertad no mayor de dos años.

Si la provocación es para cometer delito contra la seguridad o tranquilidad públicas, la pena será no menor de tres ni mayor de seis años.(*)

(*) Artículo modificado por el Artículo 2 de la Ley N° 27686, publicada el 19 marzo 2002, cuyo texto es el siguiente:

Disturbios

“Artículo 315.- El que en una reunión tumultuaria, atenta contra la integridad física de las personas y/o mediante violencia causa grave daño a la propiedad pública o privada, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años.” (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28820, publicada el 22 julio 2006, cuyo texto es el siguiente:

"Artículo 315.- Disturbios

El que en una reunión tumultuaria, atenta contra la integridad física de las personas y/o mediante violencia causa grave daño a la propiedad pública o privada, será reprimido con pena privativa de libertad no menor de seis ni mayor de ocho años.

En los casos en que el agente utilice indebidamente prendas o símbolos distintivos de las Fuerzas Armadas o de la Policía Nacional del Perú, la pena privativa de la libertad será no menor de ocho ni mayor de diez años.” (*)

(*) Artículo modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 30037, publicada el 07 junio 2013, el mismo que entró en vigencia a los (90) días posteriores a su publicación, cuyo texto es el siguiente:

“Artículo 315.- Disturbios

El que en una reunión tumultuaria, atenta contra la integridad física de las personas y/o mediante violencia causa grave daño a la propiedad pública o privada, será reprimido con pena privativa de libertad no menor de seis ni mayor de ocho años.

Será sancionado con la misma pena cuando los actos descritos en el primer párrafo se produzcan con ocasión de un espectáculo deportivo, o en el área de influencia deportiva.

Si el atentado contra la integridad física de las personas causa la muerte, la conducta es calificada como asesinato, con la pena prevista en el artículo 108 del Código Penal.

En los actos en que el agente utilice indebidamente prendas o símbolos distintivos de las Fuerzas Armadas o de la Policía Nacional del Perú, la pena privativa de la libertad será no menor de ocho ni mayor de diez años.”  (*)

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 315.- Disturbios

El que en una reunión tumultuaria, atenta contra la integridad física de las personas y/o mediante violencia causa grave daño a la propiedad pública o privada, será reprimido con pena privativa de libertad no menor de seis ni mayor de ocho años.

Será sancionado con la misma pena cuando los actos descritos en el primer párrafo se produzcan con ocasión de un espectáculo deportivo, o en el área de influencia deportiva.

Constituyen circunstancias agravantes los siguientes supuestos:

1. Si en estos actos el agente utiliza indebidamente prendas o símbolos distintivos de las Fuerzas Armadas o de la Policía Nacional del Perú, la pena privativa de la libertad será no menor de ocho ni mayor de diez años.

2. Si el atentado contra la integridad física de las personas causa lesiones graves, será reprimido con la pena privativa de la libertad no menor de ocho años a doce años.

3. Si el atentado contra la integridad física de las personas causa la muerte, será reprimido con la pena privativa de la libertad no menor de quince años."

PROCESOS CONSTITUCIONALES

"Artículo 315-A. Delito de grave perturbación de la tranquilidad pública

El que perturbe gravemente la paz pública usando cualquier medio razonable capaz de producir alarma, será sancionado con pena privativa de libertad no menor de tres ni mayor de seis años.

Se considera perturbación grave a todo acto por el cual se difunda o ponga en conocimiento de la autoridad pública, medios de comunicación social o de cualquier otro por el cual pueda difundirse masivamente la noticia, la inminente realización de un hecho o situación falsa o inexistente, relacionado con un daño o potencial daño a la vida e integridad de las personas o de bienes públicos o privados.

Si el agente actúa en calidad de integrante de una organización criminal que, para lograr sus fines, cualesquiera que sean, utiliza como medio la amenaza de la comisión del delito de terrorismo, será sancionado con pena privativa de libertad no menor de seis ni mayor de diez años.”(*)

(*) Artículo incorporado por el Artículo 2 de la Ley Nº 30076, publicada el 19 agosto 2013.

Artículo 316 Apología
~~~~~~~~~~~~~~~~~~~~~

El que, públicamente, hace la apología de un delito o de la persona que haya sido condenada como su autor o partícipe, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

Si la apología se hace de delito contra la seguridad y tranquilidad públicas, contra el Estado y la defensa nacional, o contra los Poderes del Estado y el orden constitucional, la pena será no menor de cuatro ni mayor de seis años.

“Si la apología se hace del delito de terrorismo o de la persona que haya sido condenada como su autor o partícipe, la pena será no menor de seis ni mayor de doce años. Además se le impondrá el máximo de la pena de multa previsto en el artículo 42 e inhabilitación conforme a los incisos 2, 4, y 8 del artículo 36 del Código Penal”. (1)(2)

(1) Párrafo agregado por el Artículo Primero del Decreto Legislativo N° 924, publicado el 20 febrero 2003.

(2) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 982, publicado el 22 julio 2007, cuyo texto es el siguiente:

“Artículo 316.- Apología

El que públicamente hace la apología de un delito o de la persona que haya sido condenada como su autor o partícipe, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

1. Si la apología se hace de delito previsto en los artículos 152 al 153-A, 200, 273 al 279-D, 296 al 298, 315, 317, 318- A, 325 al 333; 346 al 350 o en la Ley Nº 27765, Ley Penal contra el Lavado de Activos o de la persona que haya sido condenada como su autor o partícipe, la pena será no menor de cuatro ni mayor de seis años, doscientos cincuenta días multa, e inhabilitación conforme a los incisos 2,4 y 8 del artículo 36 del Código Penal.

2. Si la apología se hace de delito de terrorismo o de la persona que haya sido condenada como su autor o partícipe, la pena será no menor de seis ni mayor de doce años. Si se realiza a través de medios de comunicación social o mediante el uso de tecnologías de la información y comunicaciones, como Internet u otros análogos, la pena será no menor de ocho ni mayor de quince años; imponiéndose trescientos sesenta días multa e inhabilitación conforme a los incisos 2, 4 y 8 del artículo 36 del Código Penal.”  (*)

(*) Artículo modificado por el Artículo Único de la Ley N° 30610, publicada el 19 julio 2017, cuyo texto es el siguiente:

“Artículo 316. Apología

El que públicamente exalta, justifica o enaltece un delito o a la persona condenada por sentencia firme como autor o partícipe, será reprimido con pena privativa de libertad no menor de un año ni mayor de cuatro años.

Si la exaltación, justificación o enaltecimiento se hace de delito previsto en los artículos 152 al 153-A, 200, 273 al 279-D, 296 al 298, 315, 317, 318-A, 325 al 333, 346 al 350 o de los delitos de lavado de activos, o de la persona que haya sido condenada por sentencia firme como autor o partícipe, la pena será no menor de cuatro años ni mayor de seis años, doscientos cincuenta días multa, e inhabilitación conforme a los incisos 2, 4 y 8 del artículo 36 del Código Penal."

CONCORDANCIAS:     Ley N° 27378, Art. 1, num. 4)

"Artículo 316-A. Apología del delito de terrorismo

Si la exaltación, justificación o enaltecimiento se hace del delito de terrorismo o de cualquiera de sus tipos, o de la persona que haya sido condenada por sentencia firme como autor o partícipe, la pena será no menor de cuatro años ni mayor de ocho años, trescientos días multa e inhabilitación conforme a los incisos 2, 4, 6 y 8 del artículo 36 del Código Penal.

Si la exaltación, justificación o enaltecimiento del delito de terrorismo se realiza: a) en ejercicio de la condición de autoridad, docente o personal administrativo de una institución educativa, o b) utilizando o facilitando la presencia de menores de edad, la pena será no menor de seis años ni mayor de diez años e inhabilitación, conforme a los incisos 1, 2, 4 y 9 del artículo 36 del Código Penal.

Si la exaltación, justificación o enaltecimiento se propaga mediante objetos, libros, escritos, imágenes visuales o audios, o se realiza a través de imprenta, radiodifusión u otros medios de comunicación social o mediante el uso de tecnologías de la información o de la comunicación, del delito de terrorismo o de la persona que haya sido condenada por sentencia firme como autor o partícipe de actos de terrorismo, la pena será no menor de ocho años ni mayor de quince años e inhabilitación, conforme a los incisos 1, 2, 4 y 9 del artículo 36 del Código Penal”.(*)(**)

(*) Artículo incorporado por el Artículo Único de la Ley N° 30610, publicada el 19 julio 2017.

(**) De conformidad con el Numeral 2 del Artículo 1 de la Ley N° 30794, publicada el 18 junio 2018, se establece como requisito para ingresar o reingresar a prestar servicios en el sector público, que el trabajador no haya sido condenado con sentencia firme, por el delito de apología del delito de terrorismo, tipificado en el presente artículo. La citada ley entra en vigencia a los noventa (90) días de su publicación, con la finalidad de que las entidades de la administración pública adecúen su procedimiento de selección de personal para incorporar el requisito señalado en el artículo 1 de la citada ley.

Artículo 317 Agrupación ilícita
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que forma parte de una agrupación de dos o más personas destinada a cometer delitos será reprimido, por el sólo hecho, de ser miembro de la agrupación, con pena privativa de libertad no menor de tres ni mayor de seis años.

Cuando la agrupación esté destinada a cometer los delitos de genocidio, contra la seguridad y tranquilidad públicas, contra el Estado y la defensa nacional o contra los Poderes del Estado y el orden constitucional, la pena será no menor de ocho años, de ciento ochenta a trescientos sesenticinco días-multa e inhabilitación conforme al artículo 36, incisos 1, 2 y 4. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28355, publicada el 06 octubre 2004, cuyo texto es el siguiente:

“Artículo 317.- Asociación ilícita

El que forma parte de una organización de dos o más personas destinada a cometer delitos será reprimido por el sólo hecho de ser miembro de la misma, con pena privativa de libertad no menor de tres ni mayor de seis años.

Cuando la organización esté destinada a cometer los delitos de genocidio, contra la seguridad y tranquilidad públicas, contra el Estado y la defensa nacional o contra los Poderes del Estado y el orden constitucional, la pena será no menor de ocho ni mayor de treinta y cinco años, de ciento ochenta a trescientos sesenta y cinco días-multa e inhabilitación conforme al artículo 36, incisos 1, 2 y 4."(*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 982, publicado el 22 julio 2007, cuyo texto es el siguiente:

“Artículo 317.- Asociación ilícita

El que forma parte de una organización de dos o más personas destinada a cometer delitos será reprimido por el sólo hecho de ser miembro de la misma, con pena privativa de libertad no menor de tres ni mayor de seis años.

Cuando la organización esté destinada a cometer los delitos previstos en los artículos 152 al 153-A, 200, 273 al 279-D, 296 al 298, 315, 317, 318-A, 319, 325 al 333; 346 al 350 o la Ley Nº 27765 (Ley Penal contra el Lavado de Activos), la pena será no menor de ocho ni mayor de quince años, de ciento ochenta a trescientos sesenta y cinco días-multa e inhabilitación conforme al artículo 36 incisos 1, 2 y 4, imponiéndose además, de ser el caso, las consecuencias accesorias del artículo 105 numerales 2) y 4), debiéndose dictar las medidas cautelares que correspondan para garantizar dicho fin”.(*) (*)RECTIFICADO POR FE DE ERRATAS

(*) Artículo modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 30077, publicada el 20 agosto 2013, la misma que entró en vigencia el 1 de julio de 2014, cuyo texto es el siguiente:

"Artículo 317. - Asociación ilícita

El que constituya, promueva o integre una organización de dos o más personas destinada a cometer delitos será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años.

La pena será no menor de ocho ni mayor de quince años, de ciento ochenta a trescientos sesenta y cinco días-multas e inhabilitación conforme a los incisos 1), 2) y 4) del artículo 36, imponiéndose además, de ser el caso, las consecuencias accesorias previstas en los incisos 2 y 4 del artículo 105, debiéndose dictar las medidas cautelares que correspondan, en los siguientes casos:

a) Cuando la organización esté destinada a cometer los delitos previstos en los artículos 106, 108, 116, 152, 153, 162, 183-A, 186, 188, 189, 195, 200, 202, 204, 207-B, 207-C, 222, 252, 253, 254, 279, 279-A, 279-B, 279-C, 279-D, 294-A, 294-B, 307-A, 307-B, 307-C, 307-D, 307-E, 310-A, 310-B, 310-C, 317-A, 319, 320, 321, 324, 382, 383, 384, 387, 393, 393-A, 394, 395, 396, 397, 397-A, 398, 399, 400, 401, 427 primer párrafo y en la Sección II del Capítulo III del Título XII del Libro Segundo del Código Penal; en los artículos 1, 2, 3, 4, 5 y 6 del Decreto Legislativo 1106, de lucha eficaz contra el lavado de activos y otros actos relacionados a la minería ilegal y crimen organizado y en la Ley 28008, Ley de los Delitos Aduaneros, y sus respectivas normas modificatorias.

b) Cuando el integrante fuera el líder, jefe o dirigente de la organización.

c) Cuando el agente es quien financia la organización." (*)

(*) Artículo modificado por la Única Disposición Complementaria Modificatoria del Decreto Legislativo N° 1181, publicado el 27 julio 2015, cuyo texto es el siguiente:

"Artículo 317.- Asociación ilícita

El que constituya, promueva o integre una organización de dos o más personas destinada a cometer delitos será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años. La pena será no menor de ocho ni mayor de quince años, de ciento ochenta a trescientos sesenta y cinco días-multas e inhabilitación conforme a los incisos 1), 2) y 4) del artículo 36, imponiéndose además, de ser el caso, las consecuencias accesorias previstas en los incisos 2 y 4 del artículo 105, debiéndose dictar las medidas cautelares que correspondan, en los siguientes casos:

a) Cuando la organización esté destinada a cometer los delitos previstos en los artículos 106, 108, 108-C, 108-D 116, 152, 153, 162, 183-A, 186, 188, 189, 195, 200, 202, 204, 207-B, 207-C, 222, 252, 253, 254, 279, 279-A, 279-B, 279-C, 279-D, 294-A, 294-B, 307- A, 307-B, 307-C, 307-D, 307-E, 310-A, 310-B, 310-C, 317-A, 319, 320, 321, 324, 382, 383, 384, 387, 393, 393-A, 394, 395, 396, 397, 397-A, 398, 399, 400, 401, 427 primer párrafo y en la Sección II del Capítulo III del Título XII del Libro Segundo del Código Penal; en los artículos 1, 2, 3, 4, 5 y 6 del Decreto Legislativo 1106, de lucha eficaz contra el lavado de activos y otros actos relacionados a la minería ilegal y crimen organizado y en la Ley 28008, Ley de los Delitos Aduaneros, y sus respectivas normas modificatorias.

b) Cuando el integrante fuera el líder, jefe o dirigente de la organización.

c) Cuando el agente es quién financia la organización.” (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1244, publicado el 29 octubre 2016, cuyo texto es el siguiente:

"Artículo 317.- Organización Criminal

El que promueva, organice, constituya, o integre una organización criminal de tres o más personas con carácter estable, permanente o por tiempo indefinido, que de manera organizada, concertada o coordinada, se repartan diversas tareas o funciones, destinada a cometer delitos será reprimido con pena privativa de libertad no menor de ocho ni mayor de quince años y con ciento ochenta a trescientos sesenta y cinco días - multa, e inhabilitación conforme al artículo 36, incisos 1), 2), 4) y 8).

La pena será no menor de quince ni mayor de veinte años y con ciento ochenta a trescientos sesenta y cinco días - multa, e inhabilitación conforme al artículo 36, incisos 1), 2), 4) y 8) en los siguientes supuestos:

Cuando el agente tuviese la condición de líder, jefe, financista o dirigente de la organización criminal. Cuando producto del accionar delictivo de la organización criminal, cualquiera de sus miembros causa la muerte de una persona o le causa lesiones graves a su integridad física o mental.”

CONCORDANCIAS:     D.S Nº 009-2010-JUS (Aprueban Procedimiento para el pago de la reparación civil a favor del Estado en casos de procesos seguidos sobre
delitos de corrupción y otros delitos conexos)

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

“Artículo 317-A.- Marcaje o reglaje

El que para cometer o facilitar la comisión de los delitos tipificados en los artículos 106, 107, 108, 121, 124-A, 152, 153, 170, 171, 172, 173, 173-A, 175, 176, 176-A, 177, 185, 186, 188, 189 o 200 del Código Penal, realiza actos de acopio de información; o realiza actos de vigilancia o seguimiento de personas; o tiene en su poder armas, vehículos, teléfonos u otros instrumentos para facilitar la comisión del delito, será sancionado con pena privativa de libertad no menor de tres ni mayor de seis años.

Constituye circunstancia agravante si el sujeto activo es funcionario o servidor público o mantiene o hubiese mantenido vínculo laboral con el sujeto pasivo o mantiene o hubiese mantenido con este último vínculo que lo impulse a depositar en él su confianza o utilice para su realización a un menor de edad. En estos casos la pena privativa de libertad será no menor de seis ni mayor de diez años.” (1)(2)

(1) Artículo incorporado por el Artículo Único de la Ley Nº 29859, publicada el 03 mayo 2012.

(2) Artículo modificado por el Artículo 1 de la Ley Nº 30076, publicada el 19 agosto 2013, cuyo texto es el siguiente:

"Artículo 317-A. Marcaje o reglaje

Será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años el que para cometer o facilitar la comisión de cualquiera de los delitos previstos en los artículos 106, 107, 108, 108-A, 121, 152, 153, 170, 171, 172, 173, 173-A, 175, 176, 176-A, 177, 185, 186, 188, 189 o 200 del Código Penal, acopia o entrega información, realiza vigilancia o seguimiento, o colabora en la ejecución de tales conductas mediante el uso de armas, vehículos, teléfonos u otros instrumentos idóneos.

La pena privativa de libertad será no menor de seis ni mayor de diez años cuando el agente:

1. Es funcionario o servidor público y aprovecha su cargo para la comisión del delito.

2. Mantiene o mantuvo vínculo laboral con la víctima u otro vínculo que la impulse a esta última a depositar su confianza en el agente.

3. Utilice a un menor de edad.

4. Labora, pertenece o está vinculado a una empresa del sistema financiero y, por razón de su cargo u oficio, tiene conocimiento de los ingresos económicos, operaciones bancarias u otros datos sobre el patrimonio de la víctima.

5. Actúa en condición de integrante de una organización criminal."

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

"Artículo 317-B. Banda Criminal

El que constituya o integre una unión de dos a más personas; que sin reunir alguna o algunas de las características de la organización criminal dispuestas en el artículo 317, tenga por finalidad o por objeto la comisión de delitos concertadamente; será reprimidos con una pena privativa de libertad de no menor de cuatro ni mayor de ocho años y con ciento ochenta a trescientos sesenta y cinco días - multa." (*)

(*) Artículo incorporado por el Artículo 3 del Decreto Legislativo N° 1244, publicado el 29 octubre 2016.

Artículo 318 Ofensas a la memoria de los muertos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no mayor de dos años:

1. El que profana el lugar en que reposa un muerto o públicamente lo ultraja.

2. El que turba un cortejo fúnebre.

3. El que sustrae un cadáver o una parte del mismo o sus cenizas o lo exhuma sin la correspondiente autorización.

“En el supuesto previsto en el inciso 3 del presente artículo, cuando el acto se comete con fines de lucro, la pena será privativa de libertad no menor de dos años ni mayor de cuatro años e inhabilitación conforme a los incisos 1, 2 y 4 del artículo 36 del Código Penal.” (*)

(*) Párrafo incorporado por la Cuarta Disposición Transitoria y Final de la Ley N° 28189, publicada el 18 marzo 2004.

“Artículo 318-A.- Delito de intermediación onerosa de órganos y tejidos

Será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años el que, por lucro y sin observar la ley de la materia, compra, vende, importa, exporta, almacena o transporta órganos o tejidos humanos de personas vivas o de cadáveres, concurriendo las circunstancias siguientes:

a) Utiliza los medios de prensa escritos o audiovisuales o base de datos o sistema o red de computadoras; o

b) Constituye o integra una organización ilícita para alcanzar dichos fines.(*)

(*) Literal modificado por la Primera Disposición Complementaria Modificatoria de la Ley Nº 30077, publicada el 20 agosto 2013, la misma que entró en vigencia el 1 de julio de 2014, cuyo texto es el siguiente:

"b. Constituye o integra una organización criminal para alcanzar dichos fines."

Si el agente es un profesional médico o sanitario o funcionario del sector salud, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años e inhabilitación conforme al artículo 36 incisos 1, 2, 4, 5 y 8.

Están exentos de pena el donatario o los que ejecutan los hechos previstos en el presente artículo si sus relaciones con la persona favorecida son tan estrechas como para excusar su conducta.” (*)

(*) Artículo incorporado por la Quinta Disposición Transitoria y Final de la Ley N° 28189, publicada el 18 marzo 2004.

CAPITULO II TERRORISMO (*)
##########################

(*) Capítulo II derogado por el Artículo 22 del Decreto Ley Nº 25475, publicado el  06 mayo 1992.

Artículo 319 
~~~~~~~~~~~~~

El que provoca, crea, o mantiene un estado de zozobra, alarma o terror en la población o en un sector de ella, realizando actos contra la vida, el cuerpo, la salud, la libertad, la seguridad personal o la integridad física de las personas, o contra el patrimonio de éstas, contra la seguridad de los edificios (*) RECTIFICADO POR FE DE ERRATAS públicos, vías o medios de comunicación o de transporte de cualquier índole, torres de energía o transmisión, instalaciones motrices o cualquier otro bien o servicio, empleando para tales efectos métodos violentos, armamentos, materias o artefactos explosivos o cualquier otro medio capaz de causar estragos o grave perturbación de la tranquilidad pública o afectar las relaciones internacionales o la seguridad social o estatal, será reprimido con pena privativa de la libertad no menor de diez años.

Artículo 320 
~~~~~~~~~~~~~

La pena será:

1.- Privativa de libertad no menor de quince años si el agente actúa en calidad de integrante de una organización que, para lograr sus fines, cualesquiera que sean, utiliza como medio el delito de terrorismo previsto en el artículo 319.

La pena será privativa de libertad no menor de veinte años cuando el agente pertenece a la organización en calidad de jefe, cabecilla o dirigente.

2.- Privativa de libertad no menor de dieciocho años, si como efecto del delito se producen lesiones en personas o daños en bienes públicos o privados.

3.- Privativa de libertad no menor de veinte años, si se hace participar a menores de edad en la comisión del delito.

4.- Privativa de libertad no menor de veinte años, si el daño en los bienes públicos o privados impide, total o parcialmente, la prestación de servicios esenciales para la población.

5.- Privativa de libertad no menor de veinte años, cuando con fines terroristas se extorsiona o secuestra personas para obtener excarcelaciones de detenidos o cualquier otra ventaja indebida por parte de la autoridad o particulares, o cuando con idéntica finalidad se apodera ilícitamente de medio de transporte aéreo, acuático o terrestre, sea nacional o extranjero, altera su itinerario, o si la extorsión o secuestro tiene como finalidad la obtención de dinero, bienes o cualquier otra ventaja.

6.- Privativa de libertad no menor de veinte años, si como efecto de la comisión de los hechos contenidos en el artículo 313 se producen lesiones graves o muerte, siempre que el agente haya podido prever (*) RECTIFICADO POR FE DE ERRATAS estos resultados.

Artículo 321 
~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no menor de diez años, el que de manera voluntaria obtiene, recaba o facilita cualquier acto de colaboración que favorezca la comisión de delitos comprendidos en este Capítulo o la realización de los fines de un grupo terrorista.

Son actos de colaboración:

1.- La información sobre personas y patrimonios, instalaciones, edificios públicos y privados, centros urbanos y cualquier otra que tenga significación para las actividades del grupo terrorista.

2.- La construcción, cesión o utilización de cualquier tipo de alojamiento o de otros elementos susceptibles de ser destinados a ocultar personas o servir de depósito para armas o explosivos, víveres, dinero u otras pertenencias relacionadas con los grupos terroristas o con sus víctimas.

3.- La ocultación o traslado de personas integradas a los grupos o vinculadas con sus actividades delictuosas, así como la prestación de cualquier tipo de ayuda que favorezca la fuga de aquéllas.

4.- La organización de cursos o centros de instrucción de grupos terroristas.

5.- La fabricación, adquisición, sustracción, almacenamiento o suministro de armas, municiones, sustancias u objetos explosivos, inflamables, asfixiantes o tóxicos.

6.- Cualquier forma de acción económica, ayuda o mediación hecha con la finalidad de financiar grupos o actividades terroristas.

Artículo 322 
~~~~~~~~~~~~~

Los que forman parte de una organización integrada por dos o más personas para instigar, planificar, propiciar, organizar, difundir o cometer actos de terrorismo, mediatos o inmediatos, previstos en este Capítulo, serán reprimidos, por el solo hecho de agruparse o asociarse, con pena privativa de libertad no menor de diez ni mayor de veinte años.

Artículo 323 
~~~~~~~~~~~~~

El funcionario o servidor público que prive a una persona de su libertad, ordenando o ejecutando acciones que tengan por resultado su desaparición, será reprimido con pena privativa de libertad no menor de quince años e inhabilitación conforme al artículo 36, incisos 1 y 2 (*) RECTIFICADO POR FE DE ERRATAS .

Artículo 324 
~~~~~~~~~~~~~

La exención y reducción de las penas, prevista en los artículos 20 y 21, son aplicables al agente que abandona voluntariamente su vinculación con la organización terrorista, disminuye considerablemente el peligro causado,. impide que el resultado se realice o proporciona información eficaz respecto de la misma y de sus cabecillas.  La pena será reducida hasta dos tercios por debajo del mínimo legal, eximida o remitida, según el caso. (*)

(*) Capítulo II derogado por el Artículo 22 del Decreto Ley Nº 25475, publicado el  06 mayo 1992.

"TITULO XIV-A
DELITOS CONTRA LA HUMANIDAD

CAPITULO I GENOCIDIO
####################

Artículo 319 Genocidio - Modalidades
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no menor de veinte años el que, con la intención de destruir, total o parcialmente, a un grupo nacional, étnico, social o religioso, realiza cualquiera de los actos siguientes:

1. Matanza de miembros del grupo.

2. Lesión grave a la integridad física o mental a los miembros del grupo.

3. Sometimiento del grupo a condiciones de existencia que hayan de acarrear su destrucción física de manera total o parcial.

4. Medidas destinadas a impedir los nacimientos en el seno del grupo.

5. Transferencia forzada de niños a otro grupo.

CONCORDANCIAS:     Ley N° 27378, Art. 1, num. 3)

Ley Nº 30077, Art. 3 (Delitos comprendidos)

CAPITULO II DESAPARICION FORZADA
################################

Artículo 320 Desaparición comprobada
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario o servidor público que prive a una persona de su libertad, ordenando o ejecutando acciones que tenga por resultado su desaparición debidamente comprobada, será reprimido con pena privativa de libertad no menor de quince años e inhabilitación, conforme al Artículo 36 incisos 1) y 2). (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1351, publicado el 07 enero 2017, cuyo texto es el siguiente:

“Artículo 320.- Desaparición forzada de personas

El funcionario o servidor público, o cualquier persona con el consentimiento o aquiescencia de aquel, que de cualquier forma priva a otro de su libertad y se haya negado a reconocer dicha privación de libertad o a dar información cierta sobre el destino o el paradero de la víctima, es reprimido con pena privativa de libertad no menor de quince ni mayor de treinta años e inhabilitación conforme al artículo 36 incisos 1) y 2).

La pena privativa de libertad es no menor de treinta ni mayor de treinta y cinco años, e inhabilitación conforme al artículo 36 incisos 1) y 2), cuando la víctima:

a) Tiene menos de dieciocho años o es mayor de sesenta años de edad.

b) Padece de cualquier tipo de discapacidad.

c) Se encuentra en estado de gestación.” (*) RECTIFICADO POR FE DE ERRATAS

CONCORDANCIAS:     Ley N° 27378, Art. 1, num. 3)

Ley Nº 30077, Art. 3 (Delitos comprendidos)

PROCESOS CONSTITUCIONALES

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA
JURISPRUDENCIA  DE LA CORTE INTERAMERICANA DE DERECHOS HUMANOS

CAPITULO III TORTURA
####################

Artículo 321 Tortura - Agravante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario o servidor público o cualquier persona, con el consentimiento o aquiescencia de aquél, que inflija a otro dolores o sufrimientos graves, sean físicos o mentales, o lo someta a condiciones o métodos que anulen su personalidad o disminuyan su capacidad física o mental, aunque no causen dolor físico o aflicción psíquica, con el fin de obtener de la víctima o de un tercero una confesión o información, o de castigarla por cualquier hecho que haya cometido o se sospeche que ha cometido, o de intimidarla o de coaccionarla, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años.

Si la tortura causa la muerte del agraviado o le produce lesión grave y el agente pudo prever este resultado, la pena privativa de libertad será respectivamente no menor de ocho ni mayor de veinte años, ni menor de seis ni mayor de doce años. (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1351, publicado el 07 enero 2017, cuyo texto es el siguiente:

“Artículo 321.- Tortura

El funcionario o servidor público, o cualquier persona con el consentimiento o aquiescencia de aquel, que inflige dolores o sufrimientos graves, sean físicos o mentales, a otra persona o la somete a cualquier método tendente a menoscabar su personalidad o disminuir su capacidad mental o física, es reprimido con pena privativa de libertad no menor de ocho ni mayor de catorce años.

La pena privativa de libertad es no menor de quince ni mayor de veinte años, cuando la víctima:

a. Resulte con lesión grave.

b. Tiene menos de dieciocho años o es mayor de sesenta años de edad.

c. Padece de cualquier tipo de discapacidad.

d. Se encuentra en estado de gestación.

e. Se encuentra detenida o recluida, y el agente abusa de su condición de autoridad para cometer el delito.

Si se produce la muerte de la víctima y el agente pudo prever ese resultado, la pena privativa de libertad es no menor de veinte ni mayor de veinticinco años.”

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

PROCESOS CONSTITUCIONALES
JURISPRUDENCIA  DE LA CORTE INTERAMERICANA DE DERECHOS HUMANOS

Artículo 322 Cooperación de profesional
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El médico o cualquier profesional sanitario que cooperara en la perpetración del delito señalado en el artículo anterior, será reprimido con la misma pena de los autores." (1)(2)

(1) Título XIV-A incorporado por el Artículo 1 de la Ley Nº 26926, publicada el 21 febrero 1998.

(2) De conformidad con el Artículo 5 de la Ley Nº 26926, publicada el 21 febrero 1998, los delitos a que se refiere este Título se tramitarán en la vía ordinaria y ante el fuero común. Asimismo, el Artículo 4 de la misma norma, señala que: 4.1. Cualquier persona puede pedir de inmediato el examen médico de la persona agraviada o de aquella imposibilitada de recurrir por sí misma a la autoridad; y, 4.2. Los médicos legistas deberán concurrir de inmediato para el reconocimiento de quien resulte víctima de la tortura, sin perjuicio del derecho del denunciante de acudir a cualquier médico para su verificación.

CONCORDANCIAS:     Ley N° 27378, Art. 1, num. 3)

“Capítulo IV

DISCRIMINACIÓN

Artículo 323 Discriminación de personas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que discrimina a otra persona o grupo de personas, por su diferencia racial, étnica, religiosa o sexual, será reprimido con prestación de servicios a la comunidad de treinta a sesenta jornadas o limitación de días libres de veinte a sesenta jornadas.

Si el agente es funcionario público la pena será prestación de servicios a la comunidad de sesenta a ciento veinte jornadas e inhabilitación por tres años, conforme al inciso 2) del Artículo 36.” (1)(2)

(1) Capítulo IV incorporado por el Artículo 1 de la Ley Nº 27270, publicada el 29 mayo 2000.

(2) Artículo 323) modificado por el Artículo Único de la Ley N° 28867, publicada el 09 agosto 2006, cuyo texto es el siguiente:

“DISCRIMINACIÓN

Artículo 323.- El que, por sí o mediante terceros, discrimina a una o más personas o grupo de personas, o incita o promueve en forma pública actos discriminatorios, por motivo racial, religioso, sexual, de factor genético, filiación, edad, discapacidad, idioma, identidad étnica y cultural, indumentaria, opinión política o de cualquier índole, o condición económica, con el objeto de anular o menoscabar el reconocimiento, goce o ejercicio de los derechos de la persona, será reprimido con pena privativa de libertad no menor de dos años, ni mayor de tres o con prestación de servicios a la comunidad de sesenta a ciento veinte jornadas.

Si el agente es funcionario o servidor público la pena será no menor de dos, ni mayor de cuatro años e inhabilitación conforme al inciso 2) del artículo 36.

La misma pena privativa de libertad se impondrá si la discriminación se ha materializado mediante actos de violencia física o mental.” (*)

(*) Artículo modificado por la Cuarta Disposición Complementaria Modificatoria de la Ley N° 30096, publicada el 22 octubre 2013, cuyo texto es el siguiente:

"Artículo 323. Discriminación

El que, por sí o mediante terceros, discrimina a una o más personas o grupo de personas, o incita o promueve en forma pública actos discriminatorios, por motivo racial, religioso, sexual, de factor genético, filiación, edad, discapacidad, idioma, identidad étnica y cultural, indumentaria, opinión política o de cualquier índole, o condición económica, con el objeto de anular o menoscabar el reconocimiento, goce o ejercicio de los derechos de la persona, será reprimido con pena privativa de libertad no menor de dos años ni mayor de tres o con prestación de servicios a la comunidad de sesenta a ciento veinte jornadas.

Si el agente es funcionario o servidor público, la pena será no menor de dos ni mayor de cuatro años e inhabilitación conforme al numeral 2 del artículo 36.

La misma pena privativa de libertad señalada en el párrafo anterior se impondrá si la discriminación se ha materializado mediante actos de violencia física o mental, o si se realiza a través de las tecnologías de la información o de la comunicación.” (*)

(*) Artículo modificado por el Artículo 4 de la Ley N° 30171, publicada el 10 marzo 2014, cuyo texto es el siguiente:

“Artículo 323. Discriminación e incitación a la discriminación

El que, por sí o mediante terceros, discrimina a una o más personas o grupo de personas, o incita o promueve en forma pública actos discriminatorios, por motivo racial, religioso, sexual, de factor genético, filiación, edad, discapacidad, idioma, identidad étnica y cultural, indumentaria, opinión política o de cualquier índole, o condición económica, con el objeto de anular o menoscabar el reconocimiento, goce o ejercicio de los derechos de la persona, será reprimido con pena privativa de libertad no menor de dos años, ni mayor de tres o con prestación de servicios a la comunidad de sesenta a ciento veinte jornadas.

Si el agente es funcionario o servidor público la pena será no menor de dos, ni mayor de cuatro años e inhabilitación conforme al numeral 2 del artículo 36.

La misma pena privativa de libertad señalada en el párrafo anterior se impondrá si la discriminación, la incitación o promoción de actos discriminatorios se ha materializado mediante actos de violencia física o mental o a través de internet u otro medio análogo.” (*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo N° 1323, publicado el 06 enero 2017, cuyo texto es el siguiente:

“Artículo 323.- Discriminación e incitación a la discriminación

El que, por sí o mediante terceros, realiza actos de distinción, exclusión, restricción o preferencia que anulan o menoscaban el reconocimiento, goce o ejercicio de cualquier derecho de una persona o grupo de personas reconocido en la ley, la Constitución o en los tratados de derechos humanos de los cuales el Perú es parte, basados en motivos raciales, religiosos, nacionalidad, edad, sexo, orientación sexual, identidad de género, idioma, identidad étnica o cultural, opinión, nivel socio económico, condición migratoria, discapacidad, condición de salud, factor genético, filiación, o cualquier otro motivo, será reprimido con pena privativa de libertad no menor de dos ni mayor de tres años, o con prestación de servicios a la comunidad de sesenta a ciento veinte jornadas.

Si el agente actúa en su calidad de servidor civil, o se realiza el hecho mediante actos de violencia física o mental, a través de internet u otro medio análogo, la pena privativa de libertad será no menor de dos ni mayor de cuatro años e inhabilitación conforme a los numerales 1 y 2 del artículo 36.”

CONCORDANCIAS:     Ley N° 28983 (Ley de igualdad de oportunidades entre mujeres y hombres)

PROCESOS CONSTITUCIONALES
JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

“CAPÍTULO V

Artículo 324 MANIPULACIÓN GENÉTICA
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Toda persona que haga uso de cualquier técnica de manipulación genética con la finalidad de clonar seres humanos, será reprimido con pena privativa de la libertad no menor de seis ni mayor de ocho años e inhabilitación conforme al Artículo 36, incisos 4 y 8.” (*)

(*) Capítulo V incorporado por el Artículo 1 de la Ley Nº 27636, publicada el 16 enero 2002.

TITULO  XV DELITOS CONTRA EL ESTADO Y LA  DEFENSA NACIONAL
==========================================================

CAPITULO  I ATENTADOS CONTRA LA SEGURIDAD NACIONAL Y  TRAICION A LA PATRIA
##########################################################################

Artículo 325 Atentado contra la integridad nacional
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que practica un acto dirigido a someter a la República, en todo o en parte, a la dominación extranjera o a hacer independiente una parte de la misma, será reprimido con pena privativa de libertad no menor de quince años.

Artículo 326 Participación en grupo armado dirigido por extranjero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que forma parte de un grupo armado dirigido o asesorado por extranjero, organizado dentro o fuera del país, para actuar en el territorio nacional, será reprimido con pena privativa de libertad no menor de seis ni mayor de diez años.

Artículo 327 Destrucción o alteración de hitos fronterizos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que destruye o altera las señales que marcan los límites del territorio de la República o hace que éstos se confundan, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años.

Artículo 328 Formas agravadas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no menor de cinco años el que realiza cualquiera de las acciones siguientes:

1. Acepta del invasor un empleo, cargo o comisión o dicta providencias encaminadas a afirmar al gobierno del invasor.

2. Celebra o ejecuta con algún Estado, sin cumplir las disposiciones constitucionales, tratados o actos de los que deriven o puedan derivar una guerra con el Perú.

3. Admite tropas o unidades de guerra extranjeras en el país.

Artículo 329 Inteligencia desleal con Estado extranjero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que entra en inteligencia con los representantes o agentes de un Estado extranjero, con el propósito de provocar una guerra contra la República, será reprimido con pena privativa de libertad no menor de veinte años.

Artículo 330 Revelación de secretos nacionales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que revela o hace accesible a un Estado extranjero o a sus agentes o al público, secretos que el interés de la República exige guardarlos, será reprimido con pena privativa de libertad no menor de cinco ni mayor de quince años.

Si el agente obra por lucro o por cualquier otro móvil innoble, la pena será no menor de diez años.

Cuando el agente actúa por culpa, la pena será no mayor de cuatro años.

Artículo 331 Espionaje
~~~~~~~~~~~~~~~~~~~~~~

El que espía para comunicar o comunica o hace accesibles a un Estado extranjero o al público, hechos, disposiciones u objetos mantenidos en secreto por interesar a la defensa nacional, será reprimido con pena privativa de libertad no menor de quince años.

Si el agente obró por culpa la pena será no mayor de cinco años.

Artículo 331-A 
~~~~~~~~~~~~~~~

El que por cualquier medio revela, reproduce, exhibe, difunde o hace accesible en todo o en parte, el contenido de información y/o actividades secretas del Sistema de Defensa Nacional, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años e inhabilitación de conformidad con el artículo 36, incisos 1, 2, y 4 de este Código.

El que proporcione o haga accesible a terceros, sin la autorización pertinente, las informaciones y/o actividades a que se refiere el párrafo anterior, será reprimido con pena privativa de libertad no menor de seis ni mayor de doce años e inhabilitación de conformidad con el artículo 36, incisos 1, 2, y 4 de este Código. (*)

(*) Artículo incorporado por el Artículo 1 del Decreto Legislativo Nº 762, publicado el 15 noviembre 1991. Posteriormente el Decreto Legislativo N° 762, fue derogado por el Artículo 1 de la Ley Nº 25399, publicada el 10 febrero 1992.

Artículo 332 Favorecimiento bélico a Estado extranjero-Favorecimiento agravado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que entrega a un Estado extranjero bienes destinados a la defensa nacional o le favorece mediante servicios o socorros que pueda debilitarla, será reprimido con pena privativa de libertad no menor de quince años.

Si el agente actúa por lucro o por cualquier otro móvil innoble la pena será no menor de veinte años.

Artículo 333 Provocación pública a la desobediencia militar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que provoca públicamente a la desobediencia de una orden militar  o a la violación de los deberes propios del servicio o al rehusamiento o deserción, será reprimido con pena privativa de libertad no mayor de cuatro años.

Artículo 334 Expatriación
~~~~~~~~~~~~~~~~~~~~~~~~~

Los delitos previstos en los artículos 325, 326, 329, 330, 331 y 332 serán sancionados, además, con expatriación. Se excluyen de esta pena las modalidades culposas. (1)(2)

(1) Confrontar con la Cuarta Disposición Final y Transitoria de la Constitución de 1993 y la Convención Americana sobre Derechos Humanos Art. 22 num. 5  .

(2) Artículo derogado por el Artículo 4 de la Ley N° 29460, publicada el 27 noviembre 2009.

CAPITULO  II DELITOS QUE  COMPROMETEN LAS  RELACIONES EXTERIORES  DEL  ESTADO
#############################################################################

Artículo 335 Violación de inmunidad de Jefe de Estado o de Agente Diplomático
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que viola las inmunidades del Jefe de un Estado o de algún agente diplomático, o ultraja en la persona de éstos a un Estado extranjero, o arrebata o degrada los emblemas de la soberanía de una Nación amiga en acto de menosprecio, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años.

Artículo 336 Atentado contra persona que goza de protección internacional
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que atenta, en territorio de la República, contra la vida, la salud o la libertad de una persona que goza de protección internacional, será reprimido, en caso de atentado contra la vida, con pena privativa de libertad no menor de diez ni mayor de quince años y, en los demás casos, con pena privativa de libertad no menor de cinco ni mayor de diez años.

Artículo 337 Violación de la soberanía extranjera
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que viola la soberanía de un Estado extranjero, practicando en su territorio actos indebidos o penetra en el mismo contraviniendo las normas del Derecho Internacional, será reprimido con pena privativa de libertad no mayor de cinco años.

Artículo 338 Conjuración contra un Estado extranjero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, en territorio de la República, practica actos destinados a alterar por la violencia la organización política de un Estado extranjero, será reprimido con pena privativa de libertad no mayor de cinco años.

Si el agente obra por lucro o por cualquier móvil innoble, la pena será no menor de cinco ni mayor de diez años.

Artículo 339 Actos hostiles contra Estado extranjero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que practica, sin aprobación del gobierno, actos hostiles contra un Estado extranjero, dando motivo al peligro de una declaración de guerra contra la República o expone a sus habitantes a vejaciones o represalias contra sus personas o bienes o altera las relaciones amistosas del Estado Peruano con otro, será reprimido con pena privativa de libertad no menor de dos ni mayor de ocho años.

Si el agente obra por cualquier otro móvil o cuando de los actos hostiles resulta la guerra, la pena será no menor de ocho años y de ciento ochenta a trescientos sesenticinco días-multa.

Artículo 340 Violación de Tratados o Convenciones de Paz
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que viola los tratados o convenciones de paz vigentes entre el Perú y otros Estados o las treguas o los armisticios, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

Artículo 341 Espionaje militar en perjuicio de Estado extranjero
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, en territorio peruano, recoge informaciones militares para un Estado extranjero, en perjuicio de otro Estado, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.

Artículo 342 Ejecución de actos de autoridad extranjera en el territorio nacional
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, prescindiendo de la intervención de la autoridad competente, ejecuta o manda ejecutar actos de autoridad de un país extranjero o de un organismo internacional en el territorio de la República, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años e inhabilitación de uno a tres años conforme al artículo 36, incisos 1 y 2.

Artículo 343 Actos de hostilidad ordenados por beligerantes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, con ocasión de guerra en que la República haya declarado su neutralidad, practica actos destinados a realizar en el país las medidas de hostilidad ordenadas por los beligerantes, será reprimido con pena privativa de libertad no mayor de dos años.

CAPITULO III DELITOS CONTRA LOS SIMBOLOS Y VALORES DE LA PATRIA
###############################################################

Artículo 344 Ultraje a Símbolos, próceres o héroes de la Patria
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, públicamente o por cualquier medio de difusión, ofende, ultraja, vilipendia o menosprecia, por obra o por expresión verbal, los símbolos de la Patria o la memoria de los próceres o héroes que nuestra historia consagra, será reprimido con pena privativa de libertad no mayor de cuatro años y con sesenta a ciento ochenta días-multa.

El que publica o difunde, por cualquier medio el mapa del Perú con alteración de sus límites, será reprimido con la misma pena.

Artículo 345 Actos de menosprecio contra los símbolos, próceres o héroes patrios
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, por acto de menosprecio, usa como marca de fábrica, en estampados de vestimentas o de cualquier otra manera, los símbolos de la Patria o la imagen de los próceres y héroes, será reprimido con pena privativa de libertad no mayor de un año, o con prestación de servicio comunitario de veinte a treinta jornadas.

TITULO  XVI DELITOS  CONTRA  LOS  PODERES  DEL  ESTADO Y  EL  ORDEN  CONSTITUCIONAL
===================================================================================

CAPITULO  I REBELION,  SEDICION  Y  MOTIN
#########################################

CONCORDANCIAS:     Ley N° 27770 (Otorgamiento de beneficios penales y penitenciarios a aquellos que cometen delitos graves contra la Administración
Pública)

(*) De conformidad con la Cuarta Disposición Final del Decreto Legislativo Nº 961, publicado el 11 enero 2006, los delitos previstos en el Capítulo I del Título XVI del Libro Segundo del Código Penal vigente, no son de aplicación a los miembros de las Fuerzas Armadas y Policía Nacional que cometan estos ilícitos en el ejercicio de su función y en acto de servicio.

Artículo 346 Rebelión
~~~~~~~~~~~~~~~~~~~~~

El que se alza en armas para variar la forma de gobierno, deponer al gobierno legalmente constituído o suprimir o modificar el régimen constitucional, será reprimido con pena privativa de libertad no menor de diez ni mayor de veinte años y expatriación. (1)(2)

(1) Confrontar con la Cuarta Disposición Final y Transitoria de la Constitución de 1993 y la Convención Americana sobre Derechos Humanos Art. 22 num. 5  .

(2) Artículo modificado por el Artículo 1 de la Ley N° 29460, publicada el 27 noviembre 2009, cuyo texto es el siguiente:

"Artículo 346.- Rebelión

El que se alza en armas para variar la forma de gobierno, deponer al gobierno legalmente constituido o suprimir o modificar el régimen constitucional, será reprimido con pena privativa de libertad no menor de diez ni mayor de veinte años.”

Artículo 347 Sedición
~~~~~~~~~~~~~~~~~~~~~

El que, sin desconocer al gobierno legalmente constituído, se alza en armas para impedir que la autoridad ejerza libremente sus funciones o para evitar el cumplimiento de las leyes o resoluciones o impedir las elecciones generales, parlamentarias, regionales o locales, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años.

PROCESOS CONSTITUCIONALES

Artículo 348 Motín
~~~~~~~~~~~~~~~~~~

El que, en forma tumultuaria, empleando violencia contra las personas o fuerza en las cosas, se atribuye los derechos del pueblo y peticiona en nombre de éste para exigir de la autoridad la ejecución u omisión de un acto propio de sus funciones, será reprimido con pena privativa de libertad no menor de uno ni mayor de seis años.

PROCESOS CONSTITUCIONALES

Artículo 349 Conspiración para una rebelión, sedición o motín
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que toma parte en una conspiración de dos o más personas para cometer delitos de rebelión, sedición o motín, será reprimido con pena privativa de libertad no mayor de la mitad del máximo de la señalada para el delito que se trataba de perpetrar.

Artículo 350 Seducción, usurpación y retención ilegal de mando
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que seduce a tropas, usurpa el mando de las mismas, el mando de un buque o aeronave de guerra o de una plaza fuerte o puesto de guardia, o retiene ilegalmente un mando político o militar con el fin de cometer rebelión, sedición o motín, será reprimido con pena privativa de libertad no mayor a los dos tercios del máximo de la señalada para el delito que se trataba de perpetrar.

CAPITULO II DISPOSICIONES COMUNES
#################################

Artículo 351 Exención de la pena y responsabilidad de promotores
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los rebeldes, sediciosos o amotinados que se someten a la autoridad legítima o se disuelven antes de que ésta les haga intimaciones, o lo hacen a consecuencia de ellas, sin haber causado otro mal que la perturbación momentánea del orden, están exentos de pena. Se exceptúan a los promotores o directores, quienes serán reprimidos con pena privativa de libertad no mayor de la mitad del máximo de la señalada para el delito que se trataba de perpetrar.

Artículo 352 Omisión de resistencia a rebelión, sedición o motín
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario o servidor público que, pudiendo hacerlo, no oponga resistencia a una rebelión, sedición o motín, será reprimido con pena privativa de libertad no mayor de cuatro años.

Artículo 353 Inhabilitación
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los funcionarios, servidores públicos o miembros de las Fuerzas Armadas o de la Policía Nacional, que sean culpables de los delitos previstos en este Título, serán reprimidos, además, con inhabilitación de uno a cuatro años conforme al artículo 36, incisos 1, 2 y 8.

TITULO  XVII DELITOS  CONTRA  LA  VOLUNTAD  POPULAR
===================================================

CAPITULO  UNICO (*) 
####################
(*) Mención modificada por el Artículo 1 de la Ley N° 30997, publicada el 27 agosto 2019, cuyo texto es el siguiente:

"CAPITULO I"

DELITOS  CONTRA  EL  DERECHO  DE  SUFRAGIO

(*) De conformidad con el Artículo Primero de la Resolución de la Fiscalía de la Nación N° 011-2006-MP-FN, publicada el 11 enero 2006, se precisa que las Fiscalías Provinciales y Superiores Penales y/o Mixtas a nivel nacional, son competentes, en el ámbito de su jurisdicción, para conocer denuncias sobre los Delitos Electorales, del Título XVI de la presente Ley Nº 26859 - Ley Orgánica de Elecciones y el Título XVII del presente Código, Delitos contra la Voluntad Popular y otros ilícitos conexos o derivados de los mismos.

Artículo 354 Perturbación o impedimento de proceso electoral
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, con violencia o amenaza, perturba o impide que se desarrolle un proceso electoral general, parlamentario, regional o local, será reprimido con pena privativa de libertad no menor de tres ni mayor de diez años. (*)

(*) Artículo modificado por el Artículo Único de la Ley Nº 29287, publicada el 06 diciembre 2008, cuyo texto es el siguiente:

"Artículo 354.- Perturbación o impedimento de proceso electoral

El que, con violencia o amenaza, perturba o impide que se desarrolle un proceso electoral general, parlamentario, regional o municipal, o los procesos de revocatoria o referéndum será reprimido con pena privativa de libertad no menor de tres ni mayor de diez años."

Artículo 355 Impedimento del ejercicio de derecho de sufragio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, mediante violencia o amenaza, impide a un elector ejercer su derecho de sufragio o le obliga a hacerlo en un sentido determinado, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 356 Inducción a no votar o hacerlo en sentido determinado
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, mediante dádivas, ventajas o promesas trata de inducir a un elector a no votar o a votar en un sentido determinado, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

Artículo 357 Suplantación de votante
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que suplanta a otro votante o vota más de una vez en la misma elección o sufraga sin tener derecho, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

Artículo 358 Publicidad ilegal del sentido del voto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El elector que da a publicidad el sentido de su voto en el acto electoral, será reprimido con pena privativa de libertad no mayor de un año o con prestación de servicio comunitario de veinte a treinta jornadas.

Artículo 359 Atentados contra el derecho de sufragio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Será reprimido con pena privativa de libertad no menor de dos ni mayor de ocho años el que, con propósito de impedir o alterar el resultado de un proceso electoral, realiza cualquiera de las acciones siguientes: (*)

(*) Párrafo modificado por el Artículo Único de la Ley Nº 29287, publicada el 06 diciembre 2008, cuyo texto es el siguiente:

"Artículo 359.- Atentados contra el derecho de sufragio

Será reprimido con pena privativa de libertad no menor de dos ni mayor de ocho años el que, con propósito de impedir o alterar el resultado de un proceso o favorecer o perjudicar a un candidato u organización política, realiza cualquiera de las acciones siguientes:"

1. Inserta o hace insertar o suprime o hace suprimir, indebidamente, nombres en la formación de un registro electoral.

2.Falsifica o destruye, de cualquier modo, en todo o en parte un registro electoral, libretas electorales o actas de escrutinio u oculta, retiene o hace desaparecer los documentos mencionados, de manera que el hecho pueda dificultar la elección o falsear su resultado.

3. Sustrae, destruye o sustituye ánforas utilizadas en una elección antes de realizarse el escrutinio.

4. Sustrae, destruye o sustituye cédulas de sufragio que fueron depositadas por los electores.

5. Altera, de cualquier manera, el resultado de una elección o torna imposible la realización del escrutinio.

6. Recibe, siendo miembro de una mesa de sufragio, el voto de un ciudadano no incluído en la lista de electores de esa mesa o rechaza injustificadamente el voto de un elector incluído en dicha lista.

7. Despoja a un ciudadano, indebidamente, de su libreta electoral o la  retiene con el propósito de impedirle que sufrague.

"8) Realiza cambio de domicilio o induce a realizarlo a una circunscripción distinta al de su residencia habitual, induciendo a error en la formación del Registro Electoral.” (*)

(*) Numeral adicionado por el Artículo Único de la Ley Nº 29287, publicada el 06 diciembre 2008.

"CAPÍTULO II
DELITOS CONTRA LA PARTICIPACIÓN DEMOCRÁTICA (*)

(*) Capítulo II incorporado por el Artículo 2 de la Ley N° 30997, publicada el 27 agosto 2019.

Artículo 359-A.- Financiamiento prohibido de organizaciones políticas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


El que, de manera directa o indirecta, solicita, acepta, entrega o recibe aportes, donaciones, contribuciones o cualquier otro tipo de beneficio proveniente de fuente de financiamiento legalmente prohibida, conociendo o debiendo conocer su origen, en beneficio de una organización política o alianza electoral, registrada o en proceso de registro, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años y con sesenta a ciento ochenta días multa, e inhabilitación conforme al artículo 36, incisos 1, 2, 3 y 4, del Código Penal. La pena privativa de libertad será no menor de cuatro ni mayor de seis años y con cien a trescientos días multa, e inhabilitación conforme al artículo 36, incisos 1, 2, 3 y 4, del Código Penal, si el delito es cometido por el candidato, tesorero, responsable de campaña o administrador de hecho o derecho de los recursos de una organización política, siempre que conozca o deba conocer la fuente de financiamiento legalmente prohibida.

La pena privativa de libertad será no menor de cinco ni mayor de ocho años y con ciento veinte a trescientos cincuenta días multa, e inhabilitación conforme al artículo 36, incisos 1, 2, 3 y 4, del Código Penal, si:

a) El valor del aporte, donación o financiamiento involucrado es superior a cincuenta (50) unidades impositivas tributarias (UIT).

b) El agente comete el delito como integrante de una organización criminal o persona vinculada a ella o actúe por encargo de la misma.

Artículo 359-B.- Falseamiento de la información sobre aportaciones, ingresos y gastos de organizaciones políticas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


El tesorero, el responsable de campaña, el representante legal, el administrador de hecho o de derecho, o el miembro de la organización política que, con pleno conocimiento, proporciona información falsa en los informes sobre aportaciones e ingresos recibidos o en aquellos referidos a los gastos efectivos de campaña electoral o en la información financiera anual que se entrega a la entidad supervisora será reprimido con pena privativa de libertad no menor de dos ni mayor de seis años e inhabilitación conforme al artículo 36, incisos 1, 2, 3 y 4, del Código Penal.

Artículo 359-C.- Fuentes de financiamiento legalmente prohibidas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Son fuentes de financiamiento legalmente prohibidas aquellas que provengan de:

1. Cualquier entidad de derecho público o empresa de propiedad del Estado o con participación de este, distintas del financiamiento público directo o indirecto a las organizaciones políticas.

2. Los aportes anónimos dinerarios superiores a dos (2) unidades impositivas tributarias.

3. Personas naturales condenadas con sentencia consentida o ejecutoriada, o con mandato de prisión preventiva vigente por delitos contra la administración pública, tráfico ilícito de drogas, minería ilegal, tala ilegal, trata de personas, lavado de activos o terrorismo, según información obtenida a través del procedimiento de la ley sobre la Ventanilla Única de Antecedentes para Uso Electoral, en lo que resulte aplicable. La prohibición se extiende hasta diez (10) años después de cumplida la condena.

4. Los que provengan de personas jurídicas nacionales o extranjeras sancionadas penal o administrativamente en el país o en el extranjero por la comisión de un delito, o que se les haya sancionado conforme a lo señalado en la Ley 30424, Ley que regula la responsabilidad administrativa de las personas jurídicas, o se les haya aplicado las consecuencias accesorias previstas en el presente código.”(*)

(*) Capítulo II incorporado por el Artículo 2 de la Ley N° 30997, publicada el 27 agosto 2019.

Artículo 360 Inhabilitación
~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario o servidor público o miembro de las Fuerzas Armadas o de la Policía Nacional que incurra en uno de los delitos previstos en este Título sufrirá, además, inhabilitación de uno a tres años conforme al artículo 36, incisos 1 y 2.

TITULO  XVIII DELITOS CONTRA  LA ADMINISTRACION PUBLICA
=======================================================

CAPITULO  I DELITOS COMETIDOS POR PARTICULARES
##############################################

SECCION  I
USURPACION DE AUTORIDAD, TITULOS Y HONORES

CONCORDANCIAS:     Ley N° 27765, Art. 6

Ley N° 27770

Artículo 361 
~~~~~~~~~~~~~

El que usurpa una función pública sin título o nombramiento o la facultad de dar órdenes militares o el que hallándose destituido o suspendido de su cargo continúa ejerciéndolo o el que ejerce funciones correspondientes a cargo diferente del que tiene, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años, e inhabilitación de uno a dos años conforme al artículo 36, incisos 1 y 2. (*)

(*) Artículo modificado por el Artículo 1 del Decreto Ley Nº 25444, publicado el 23 abril 1992, cuyo texto es el siguiente:

Usurpación de función pública

"Artículo 361.- El que, sin título o nombramiento, usurpa una función pública, o la facultad de dar órdenes militares o policiales, o el que hallándose destituido, cesado, suspendido o subrogado de su cargo continúa ejerciéndolo, o el que ejerce funciones correspondientes a cargo diferente del que tiene, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de siete años, e inhabilitación de uno a dos años conforme al artículo 36, incisos 1 y 2.

Si para perpetrar la comisión del delito, el agente presta resistencia o se enfrenta a las Fuerzas del Orden, la pena será privativa de libertad no menor de cinco ni mayor de ocho años."

Artículo 362 Ostentación de distintivos de función o cargos que no ejerce
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, públicamente, ostenta insignias o distintivos de una función o cargo que no ejerce o se arroga grado académico, título profesional u honores que no le corresponden, será reprimido con pena privativa de libertad no mayor de un año o con prestación de servicio comunitario de diez a veinte jornadas.

Artículo 363 Ejercicio ilegal de profesión
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, con falso título o el titulado que sin reunir los requisitos legales, ejerce profesión que los requiera, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 27754, publicada el 14 junio 2002, cuyo texto es el siguiente:

Ejercicio ilegal de profesión
"Artículo 363.- El que, con falso título o el que sin reunir los requisitos legales, ejerce profesión que los requiera, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.” (*)

(*) Artículo modificado por el Artículo Único de la Ley Nº 28538, publicada el 07 junio 2005, cuyo texto es el siguiente:

"Artículo 363.- Ejercicio ilegal de profesión

El que ejerce profesión sin reunir los requisitos legales requeridos, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.

El que ejerce profesión con falso título, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años.

La pena será no menor de cuatro ni mayor de ocho años, si el ejercicio de la profesión se da en el ámbito de la función pública o prestando servicios al Estado bajo cualquier modalidad contractual.”

Artículo 364 Participación en ejercicio ilegal de la profesión 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El profesional que ampara con su firma el trabajo de quien no tiene título para ejercerlo, será reprimido con pena privativa de libertad no mayor de cuatro años e inhabilitación de uno a tres años conforme al artículo 36º,  incisos 1 y 2.

SECCION  II
VIOLENCIA  Y  RESISTENCIA  A  LA  AUTORIDAD

Artículo 365 Violencia contra la autoridad para obligarle a algo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, sin alzamiento público, mediante violencia o amenaza, impide a una autoridad o a un funcionario o servidor público ejercer sus funciones o le obliga a practicar un determinado acto de sus funciones o le estorba en el ejercicio de éstas, será reprimido con pena privativa de libertad no mayor de dos años.

Artículo 366 Violencia contra la autoridad para impedir el ejercicio de sus funciones
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que emplea intimidación o violencia contra un funcionario público o contra la persona que le presta asistencia en virtud de un deber legal o ante requerimiento de aquél, para impedir o trabar la ejecución de un acto propio del legítimo ejercicio de sus funciones, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años. (*)

(*) Artículo modificado por el Artículo Único de la Ley N° 27937, publicada el 12 febrero 2003, cuyo texto es el siguiente:

“Artículo 366.- Violencia contra la autoridad para impedir el ejercicio de sus funciones

El que emplea intimidación o violencia contra un funcionario público o contra la persona que le presta asistencia en virtud de un deber legal o ante requerimiento de aquél, para impedir o trabar la ejecución de un acto propio de legítimo ejercicio de sus funciones, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años o con prestación de servicio comunitario de ochenta a ciento cuarenta jornadas."

PROCESOS CONSTITUCIONALES
JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 367 Formas agravadas 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


En los casos de los artículos 365 y 366 la pena privativa de libertad será no menor de tres ni mayor de seis años cuando:

1. El hecho se comete a mano armada.
2. El hecho se realiza por dos o más personas.
3. El autor es funcionario o servidor público.
4. El autor ocasiona una lesión grave que haya podido prever.

Si el agraviado muere y el agente pudo prever este resultado, la pena será privativa de libertad no menor de cinco ni mayor de quince años.(*)

(*) Artículo modificado por el Artículo Único de la Ley N° 27937, publicada el 12 febrero 2003, cuyo texto es el siguiente:

"Artículo 367.- Formas agravadas

En los casos de los artículos 365 y 366, la pena privativa de libertad será no menor de tres ni mayor de seis años cuando:

1. El hecho se realiza por dos o más personas.

2. El autor es funcionario o servidor público.

La pena privativa de libertad será no menor de cuatro ni mayor de siete años cuando:

1. El hecho se comete a mano armada.

2. El autor causa una lesión grave que haya podido prever.

Si el agraviado muere y el agente pudo prever este resultado, la pena será privativa de libertad no menor de siete ni mayor de quince años:”(*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28878, publicada el 17 agosto 2006, cuyo texto es el siguiente:

"Artículo 367.- Formas agravadas

En los casos de los artículos 365 y 366, la pena privativa de libertad será no menor de tres ni mayor de seis años cuando:

1. El hecho se realiza por dos o más personas.

2. El autor es funcionario o servidor público.

La pena privativa de libertad será no menor de cuatro ni mayor de siete años cuando:

1. El hecho se comete a mano armada.

2. El autor causa una lesión grave que haya podido prever.

3. El hecho se realiza en contra de un miembro de la Policía Nacional o de las Fuerzas Armadas, Magistrado del Poder Judicial o del Ministerio Público, en el ejercicio de sus funciones.

Si el agraviado muere y el agente pudo prever este resultado, la pena será privativa de libertad no menor de siete ni mayor de quince años.”(*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 982, publicado el 22 julio 2007, cuyo texto es el siguiente:

“Artículo 367.- Formas agravadas

En los casos de los artículos 365 y 366, la pena privativa de libertad será no menor de cuatro ni mayor de ocho años cuando:

1. El hecho se realiza por dos o más personas.

2. El autor es funcionario o servidor público.

La pena privativa de libertad será no menor de seis ni mayor de doce años cuando:

1. El hecho se comete a mano armada.

2. El autor causa una lesión grave que haya podido prever.

3. El hecho se realiza en contra de un miembro de la Policía Nacional o de las Fuerzas Armadas, Magistrado del Poder Judicial o del Ministerio Público, en el ejercicio de sus funciones.

4. El hecho se realiza para impedir la erradicación o destrucción de cultivos ilegales, o de cualquier medio o instrumento destinado a la fabricación o transporte ilegal de drogas tóxicas, estupefacientes o sustancias psicotrópicas.

5. El hecho se comete respecto a investigaciones o juzgamiento por los delitos de terrorismo, tráfico ilícito de drogas, lavado de activos, secuestro, extorsión y trata de personas.

Si como consecuencia del hecho se produce la muerte de una persona y el agente pudo prever este resultado, la pena será privativa de libertad no menor de diez ni mayor de quince años.” (*)

(*) Artículo modificado por el Artículo 2 de la Ley Nº 30054, publicada el 30 junio 2013, cuyo texto es el siguiente:

"Artículo 367.- Formas agravadas

En los casos de los artículos 365 y 366, la pena privativa de libertad será no menor de cuatro ni mayor de ocho años cuando:

1. El hecho se realiza por dos o más personas.

2. El autor es funcionario o servidor público.

La pena privativa de libertad será no menor de ocho ni mayor de doce años cuando:

1. El hecho se comete a mano armada.

2. El autor causa una lesión grave que haya podido prever.

3. El hecho se realiza en contra de un miembro de la Policía Nacional o de las Fuerzas Armadas, magistrado del Poder Judicial o del Ministerio Público, miembro del Tribunal Constitucional o autoridad elegida por mandato popular, en el ejercicio de sus funciones.

4. El hecho se realiza para impedir la erradicación o destrucción de cultivos ilegales, o de cualquier medio o instrumento destinado a la fabricación o transporte ilegal de drogas tóxicas, estupefacientes o sustancias psicotrópicas.

5. El hecho se comete respecto a investigaciones o juzgamiento por los delitos de terrorismo, tráfico ilícito de drogas, lavado de activos, secuestro, extorsión y trata de personas.

Si como consecuencia del hecho se produce la muerte de una persona y el agente pudo prever este resultado, la pena será privativa de libertad no menor de doce ni mayor de quince años.”

Artículo 368 Desobediencia o resistencia a la  autoridad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que desobedece o resiste la orden impartida por un funcionario público en el ejercicio de sus atribuciones, salvo que se trate de la propia detención, será reprimido con pena privativa de libertad no mayor de dos años. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 29439, publicada el 19 noviembre 2009, cuyo texto es el siguiente:

"Artículo 368.- Resistencia o desobediencia a la autoridad

El que desobedece o resiste la orden legalmente impartida por un funcionario público en el ejercicio de sus atribuciones, salvo que se trate de la propia detención, será reprimido con pena privativa de libertad no menor de seis meses ni mayor de dos años.

Cuando se desobedezca la orden de realizarse un análisis de sangre o de otros fluidos corporales que tenga por finalidad determinar el nivel, porcentaje o ingesta de alcohol, drogas tóxicas estupefacientes, sustancias psicotrópicas o sintéticas, la pena privativa de la libertad será no menor de seis meses ni mayor de cuatro años o prestación de servicios comunitarios de setenta a ciento cuarenta jornadas."(*)

(*) Artículo modificado por el Artículo 4 de la Ley N° 30862, publicada el 25 octubre 2018, cuyo texto es el siguiente:

"Artículo 368.- Resistencia o desobediencia a la autoridad

El que desobedece o resiste la orden legalmente impartida por un funcionario público en el ejercicio de sus atribuciones, salvo que se trate de la propia detención, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años.

Cuando se desobedezca la orden de realizarse un análisis de sangre o de otros fluidos corporales que tenga por finalidad determinar el nivel, porcentaje o ingesta de alcohol, drogas tóxicas estupefacientes, sustancias psicotrópicas o sintéticas, la pena privativa de libertad será no menor de cuatro ni mayor de siete años o prestación de servicios comunitarios de setenta a ciento cuarenta jornadas. Cuando se desobedece o resiste una medida de protección dictada en un proceso originado por hechos que configuran violencia contra las mujeres o contra integrantes del grupo familiar será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años."

“Artículo 368-A.- Ingreso indebido de equipos o sistema de comunicación, fotografía y/o filmación en centros de detención o reclusión

El que indebidamente ingresa, intenta ingresar o permite el ingreso a un centro de detención o reclusión, equipos o sistema de comunicación, fotografía y/o filmación o sus componentes que permitan la comunicación telefónica celular o fija, radial, vía internet u otra análoga del interno, así como el registro de tomas fotográficas o de video, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años.

Si el agente se vale de su condición de autoridad, abogado defensor, servidor o funcionario público para cometer o permitir que se cometa el hecho punible descrito, la pena privativa será no menor de seis ni mayor de ocho años e inhabilitación, conforme al artículo 36, incisos 1 y 2, del presente Código.” (*)

(*) Artículo incorporado por el Artículo Único de la Ley Nº 29867, publicada el 22 mayo 2012, la misma que entró en vigencia a los sesenta días calendario de su publicación en el diario oficial El Peruano.

(*) Artículo modificado por la Cuarta Disposición Complementaria Modificatoria del Decreto Legislativo N° 1182, publicado el 27 julio 2015, cuyo texto es el siguiente:

“Artículo 368-A.- Ingreso indebido de equipos o sistema de comunicación, fotografía y/o filmación en centros de detención o reclusión

El que indebidamente ingresa, intenta ingresar o permite el ingreso a un centro de detención o reclusión, equipos o sistema de comunicación, fotografía y/o filmación o sus componentes que permitan la comunicación telefónica celular o fija, radial, vía internet u otra análoga del interno, así como el registro de tomas fotográficas, de video, o proporcionen la señal para el acceso a internet desde el exterior del establecimiento penitenciario será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años.

Si el agente se vale de su condición de autoridad, abogado defensor, servidor o funcionario público para cometer o permitir que se cometa el hecho punible descrito, la pena privativa será no menor de seis ni mayor de ocho años e inhabilitación, conforme al artículo 36, incisos 1 y 2, del presente Código.”

“Artículo 368-B.- Ingreso indebido de materiales o componentes con fines de elaboración de equipos de comunicación en centros de detención o reclusión

El que indebidamente ingresa, intenta ingresar o permite el ingreso a un centro de detención o reclusión, materiales o componentes que puedan utilizarse en la elaboración de antenas, receptores u otros equipos que posibiliten o faciliten la comunicación telefónica celular o fija, radial, vía internet u otra análoga del interno, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.

Si el agente se vale de un menor de edad o de su condición de autoridad, abogado defensor, servidor o funcionario público para cometer o permitir que se cometa el hecho punible descrito, la pena privativa será no menor de tres ni mayor de seis años e inhabilitación, conforme al artículo 36, incisos 1 y 2, del presente Código.” (*)

(*) Artículo incorporado por el Artículo Único de la Ley Nº 29867, publicada el 22 mayo 2012, la misma que entró en vigencia a los sesenta días calendario de su publicación en el diario oficial El Peruano.

“Artículo 368-C.- Sabotaje de los equipos de seguridad y de comunicación en establecimientos penitenciarios

El que dentro de un centro de detención o reclusión vulnera, impide, dificulta, inhabilita o de cualquier otra forma imposibilite el funcionamiento de los equipos de seguridad y/o de comunicación en los establecimientos penitenciarios, será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años.

Si el agente se vale de un menor de edad o de su condición de autoridad, abogado defensor, servidor o funcionario público para cometer o permitir que se cometa el hecho punible descrito, la pena privativa será no menor de ocho ni mayor de diez años e inhabilitación, conforme al artículo 36, incisos 1 y 2, del presente Código.” (*)

(*) Artículo incorporado por el Artículo Único de la Ley Nº 29867, publicada el 22 mayo 2012, la misma que entró en vigencia a los sesenta días calendario de su publicación en el diario oficial El Peruano.

“Artículo 368-D.- Posesión indebida de teléfonos celulares o, armas, municiones o materiales explosivos, inflamables, asfixiantes o tóxicos en establecimientos penitenciarios

La persona privada de libertad en un centro de detención o reclusión, que posea o porte un arma de fuego o arma blanca, municiones o materiales explosivos, inflamables, asfixiantes o tóxicos, será reprimida con pena privativa de libertad no menor de ocho ni mayor de quince años.

Si el agente posee, porta, usa o trafica con un teléfono celular o fijo o cualquiera de sus accesorios que no esté expresamente autorizado, la pena privativa de libertad será no menor de tres ni mayor de ocho años.

Si se demuestra que del uso de estos aparatos se cometió o intentó cometer un ilícito penal, la pena será no menor de diez ni mayor de quince años.” (*)

(*) Artículo incorporado por el Artículo Único de la Ley Nº 29867, publicada el 22 mayo 2012, la misma que entró en vigencia a los sesenta días calendario de su publicación en el diario oficial El Peruano.

“Artículo 368-E.- Ingreso indebido de armas, municiones o materiales explosivos, inflamables, asfixiantes o tóxicos en establecimientos penitenciarios

El que indebidamente ingresa, intenta ingresar o permite el ingreso a un centro de detención o reclusión, un arma de fuego o arma blanca, municiones o materiales explosivos, inflamables, asfixiantes o tóxicos para uso del interno, será reprimido con pena privativa de libertad no menor de ocho ni mayor de quince años.

Si el agente se vale de un menor de edad o de su condición de autoridad, abogado defensor, servidor o funcionario público para cometer o permitir que se cometa el hecho punible descrito, la pena privativa será no menor de diez ni mayor de veinte años e inhabilitación, conforme al artículo 36, incisos 1 y 2, del presente Código.” (*)

(*) Artículo incorporado por el Artículo Único de la Ley Nº 29867, publicada el 22 mayo 2012, la misma que entró en vigencia a los sesenta días calendario de su publicación en el diario oficial El Peruano.

Artículo 369 Violencia contra autoridades elegidas
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que impide a los Senadores o Diputados o a los miembros de las Asambleas Regionales o a los Alcaldes o Regidores el ejercicio de las funciones propias de sus cargos, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

Si el agente es funcionario o servidor público sufrirá, además, inhabilitación de uno a tres años conforme al artículo 36, incisos 1 y 2. (*)

(*) Artículo modificado por el Artículo Único de la Ley N° 29519, publicada el 16 abril 2010, cuyo texto es el siguiente:

“Artículo 369.- Violencia contra autoridades elegidas
El que, mediante violencia o amenaza, impide a una autoridad elegida en un proceso electoral general, parlamentario, regional o municipal juramentar, asumir o ejercer sus funciones será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años. Si el agente es funcionario o servidor público sufrirá, además, inhabilitación de uno a tres años conforme al artículo 36, incisos 1, 2 y 8.”

Artículo 370 Atentado contra la conservación e identidad de objeto
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que destruye o arranca envolturas, sellos o marcas puestos por la autoridad para conservar o identificar un objeto, será reprimido con pena privativa de libertad no mayor de dos años o con prestación de servicio comunitario de veinte a treinta jornadas.

Artículo 371 Negativa a colaborar con la administración de justicia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testigo, perito, traductor o intérprete que, siendo legalmente requerido, se abstiene de comparecer o prestar la declaración, informe o servicio respectivo, será reprimido con pena privativa de libertad no mayor de dos años o con prestación de servicio comunitario de veinte a treinta jornadas.

El perito, traductor o intérprete será sancionado, además, con inhabilitación de seis meses a dos años conforme al artículo 36, incisos 1, 2 y 4.

Artículo 372 Atentado contra documentos que sirven de prueba en el proceso
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que sustrae, oculta, cambia, destruye o inutiliza objetos, registros o documentos destinados a servir de prueba ante la autoridad competente que sustancia un proceso, confiados a la custodia de un funcionario o de otra persona, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

Si la destrucción o inutilización es por culpa, la pena será privativa de libertad no mayor de un año o prestación de servicio comunitario de veinte a cuarenta jornadas.

Artículo 373 Sustracción de objetos requisados por autoridad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que sustrae objetos requisados por la autoridad, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.

SECCION  III
DESACATO

Artículo 374 Desacato
~~~~~~~~~~~~~~~~~~~~~

El que amenaza, injuria o de cualquier otra manera ofende la dignidad o el decoro de un funcionario público a causa del ejercicio de sus funciones o al tiempo de ejercerlas, será reprimido con pena privativa de libertad no mayor de tres años.

Si el ofendido es Presidente de uno de los Poderes del Estado, la pena será no menor de dos ni mayor de cuatro años. (*)

(*) Artículo derogado por el Artículo Único de la Ley N° 27975, publicada el 29 mayo 2003.

Artículo 375 Perturbación del orden en el lugar donde la autoridad ejerce su función
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que causa desorden en la sala de sesiones del Congreso o de las Cámaras Legislativas, de las Asambleas Regionales, de los Consejos Municipales o de los Tribunales de Justicia u otro lugar donde las autoridades públicas ejercen sus funciones o el que entra armado en dichos lugares, será reprimido con pena privativa de libertad no mayor de un año o con prestación de servicio comunitario de veinte a treinta jornadas.

CAPITULO  II DELITOS COMETIDOS POR FUNCIONARIOS PUBLICOS
########################################################

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

CONCORDANCIAS:     Ley N° 27378, Art. 1, num. 2)
D.S. Nº 015-2003-JUS, Art. 210.5 (Aprueban el Reglamento del Código de Ejecución Penal)

D.S. N° 007-2012-JUS, Art. 10 (Aprueban arancel para la prestación del Servicio No Gratuito de Defensa Pública)

SECCION  I
ABUSO DE AUTORIDAD

Artículo 376 Abuso de autoridad
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario público que, abusando de sus atribuciones, comete u ordena, en perjuicio de alguien, un acto arbitrario cualquiera, será reprimido con pena privativa de libertad no mayor de dos años. (*)

(*) Artículo modificado por la Sétima Disposición Final de la Ley N° 28165, publicada el 10 enero 2004, cuyo texto es el siguiente:

“Artículo 376.- Abuso de autoridad

El funcionario público que, abusando de sus atribuciones, comete u ordena, en perjuicio de alguien, un acto arbitrario cualquiera, será reprimido con pena privativa de libertad no mayor de dos años.

Cuando los hechos deriven de un procedimiento de cobranza coactiva, la pena será no menor de dos ni mayor de cuatro años."(*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 29703, publicada el 10 junio 2011, cuyo texto es el siguiente:

“Artículo 376.- Abuso de autoridad

El funcionario público que, abusando de sus atribuciones, comete u ordena un acto arbitrario que cause perjuicio a alguien será reprimido con pena privativa de libertad no mayor de tres años.

Si los hechos derivan de un procedimiento de cobranza coactiva, la pena privativa de libertad será no menor de dos ni mayor de cuatro años."

PROCESOS CONSTITUCIONALES

“Artículo 376-A.- Abuso de autoridad condicionando ilegalmente la entrega de bienes y servicios

El que, valiéndose de su condición de funcionario o servidor público, condiciona la distribución de bienes o la prestación de servicios correspondientes a programas públicos de apoyo o desarrollo social, con la finalidad de obtener ventaja política y/o electoral de cualquier tipo en favor propio o de terceros, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal.” (*)

(*) Artículo reubicado y reformado por el Artículo 2 de la Ley N° 28355, publicada el 06 octubre 2004.

“Artículo 376-B.- Otorgamiento ilegítimo de derechos sobre inmuebles

El funcionario público que, en violación de sus atribuciones u obligaciones, otorga ilegítimamente derechos de posesión o emite títulos de propiedad sobre bienes de dominio público o bienes de dominio privado estatal, o bienes inmuebles de propiedad privada, sin cumplir con los requisitos establecidos por la normatividad vigente, será reprimido con pena privativa de libertad, no menor de cuatro ni mayor de seis años.

Si el derecho de posesión o título de propiedad se otorga a personas que ilegalmente ocupan o usurpan los bienes inmuebles referidos en el primer párrafo, la pena privativa de libertad será no menor de cinco ni mayor de ocho años”. (*)

(*) Artículo incorporado por la Quinta Disposición Complementaria Transitoria de la Ley N° 30327, publicada el 21 mayo 2015.

Artículo 377 Omisión, rehusamiento o demora de actos funcionales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario público que, ilegalmente, omite, rehusa o retarda algún acto de su cargo, será reprimido con pena privativa de libertad no mayor de dos años y con treinta a sesenta días-multa. (*)

(*) Artículo modificado por la Primera Disposición Complementaria Modificatoria de la Ley N° 30364, publicada el 23 noviembre 2015, cuyo texto es el siguiente:

"Artículo 377. Omisión, rehusamiento o demora de actos funcionales

El funcionario público que, ilegalmente, omite, rehúsa o retarda algún acto de su cargo será reprimido con pena privativa de libertad no mayor de dos años y con treinta a sesenta días-multa.

Cuando la omisión, rehusamiento o demora de actos funcionales esté referido a una solicitud de garantías personales o caso de violencia familiar, la pena será privativa de libertad no menor de dos ni mayor de cinco años."

Artículo 378 Denegación o deficiente apoyo policial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El policía que rehusa, omite o retarda, sin causa justificada, la prestación de un auxilio legalmente requerido por la autoridad civil competente, será reprimido con pena privativa de libertad no mayor de dos años.

Si la prestación de auxilio es requerida por un particular en situación de peligro, la pena será no menor de dos ni mayor de cuatro años.(*)

(*) Artículo modificado por la Primera Disposición Complementaria Modificatoria de la Ley N° 30364, publicada el 23 noviembre 2015, cuyo texto es el siguiente:

"Artículo 378. Denegación o deficiente apoyo policial

El policía que rehúsa, omite o retarda, sin causa justificada, la prestación de un auxilio legalmente requerido por la autoridad civil competente, será reprimido con pena privativa de libertad no mayor de dos años.

Si la prestación de auxilio es requerida por un particular en situación de peligro, la pena será no menor de dos ni mayor de cuatro años.

La pena prevista en el párrafo segundo se impondrá, si la prestación de auxilio está referida a una solicitud de garantías personales o un caso de violencia familiar”.

Artículo 379 Requerimiento indebido de la fuerza pública
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario público que requiere la asistencia de la fuerza pública para oponerse a la ejecución de disposiciones u órdenes legales de la autoridad o contra la ejecución de sentencia o mandato judicial, será reprimido con pena privativa de libertad no mayor de tres años.

Artículo 380 Abandono de cargo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario o servidor público que, con daño del servicio, abandona su cargo sin haber cesado legalmente en el desempeño del mismo, será reprimido con pena privativa de libertad no mayor de dos años.

Si el agente incita al abandono colectivo del trabajo a los funcionarios o servidores públicos la pena será privativa de libertad no mayor de tres años.

Artículo 381 Nombramiento o aceptación ilegal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario público que hace un nombramiento para cargo público a persona en quien no concurren los requisitos legales, será reprimido con sesenta a ciento veinte días-multa.

El que acepta el cargo sin contar con los requisitos legales será reprimido con la misma pena.

SECCION  II
CONCUSION

CONCORDANCIAS:     Ley N° 27770 (Sobre Beneficios Penales y Penitenciarios) aplicable a todas las modalidades de este delito
D.S. Nº 017-2008-JUS, Arts. 46 y 49 (Decreto Supremo que aprueba el Reglamento del Decreto Legislativo Nº 1068 del Sistema de Defensa
Jurídica del Estado)

D.S Nº 009-2010-JUS (Aprueban Procedimiento para el pago de la reparación civil a favor del Estado en casos de procesos seguidos sobre
delitos de corrupción y otros delitos conexos)

Artículo 382 Concusión
~~~~~~~~~~~~~~~~~~~~~~

El funcionario o servidor público que, abusando de su cargo, obliga o induce a una persona a dar o prometer indebidamente, para sí o para otro, un bien o un beneficio patrimonial, será reprimido con pena privativa de libertad no menor de dos ni mayor de ocho años. (*)

(*) Artículo modificado por el Artículo único de la Ley N° 30111, publicada el 26 noviembre 2013, cuyo texto es el siguiente:

“Artículo 382. Concusión

El funcionario o servidor público que, abusando de su cargo, obliga o induce a una persona a dar o prometer indebidamente, para sí o para otro, un bien o un beneficio patrimonial, será reprimido con pena privativa de libertad no menor de dos ni mayor de ocho años y con ciento ochenta a trescientos sesenta y cinco días-multa." (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1243, publicado el 22 octubre 2016, cuyo texto es el siguiente:

“Artículo 382. Concusión

El funcionario o servidor público que, abusando de su cargo, obliga o induce a una persona a dar o prometer indebidamente, para sí o para otro, un bien o un beneficio patrimonial, será reprimido con pena privativa de libertad no menor de dos ni mayor de ocho años; inhabilitación, según corresponda, conforme a los incisos 1, 2 y 8 del artículo 36; y, con ciento ochenta a trescientos sesenta y cinco días-multa.” (*)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

Artículo 383 Cobro indebido
~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario o servidor público que, abusando de su cargo, exige o hace pagar o entregar contribuciones o emolumentos no debidos o en cantidad que excede a la tarifa legal, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años. (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1243, publicado el 22 octubre 2016, cuyo texto es el siguiente:

“Artículo 383. Cobro indebido

El funcionario o servidor público que, abusando de su cargo, exige o hace pagar o entregar contribuciones o emolumentos no debidos o en cantidad que excede a la tarifa legal, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años e inhabilitación, según corresponda, conforme a los incisos 1, 2 y 8 del artículo 36.”

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

PROCESOS CONSTITUCIONALES

Artículo 384 
~~~~~~~~~~~~~

El funcionario o servidor público que, en los contratos, suministros, licitaciones, concurso de precios, subastas o en cualquier otra operación semejante en la que intervenga por razón de su cargo o comisión especial defrauda al Estado o empresa del Estado o sociedades de economía mixta u órganos sostenidos por el Estado, concentrándose con los interesados en los convenios, ajustes, liquidaciones o suministros, será reprimido con pena privativa de libertad no menor de tres ni mayor de quince años.(*)

(*) Artículo modificado por el Artículo 2 de la Ley Nº 26713, publicada el 27 diciembre 1996, cuyo texto es el siguiente:

Colusión

"Artículo 384.- El funcionario o servidor público que, en los contratos, suministros, licitaciones, concurso de precios, subastas o cualquier otra operación semejante en la que intervenga por razón de su cargo o comisión especial defrauda al Estado o entidad u organismo del Estado, según ley, concertándose con los interesados en los convenios, ajustes, liquidaciones o suministros será reprimido con pena privativa de libertad no menor de tres ni mayor de quince años."(*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 29703, publicada el 10 junio 2011, cuyo texto es el siguiente:

“Artículo 384.- Colusión

El funcionario o servidor público que, interviniendo por razón de su cargo o comisión especial en cualquiera de las contrataciones o negocios públicos mediante concertación ilegal con los interesados, defraudare patrimonialmente (*) al Estado o entidad u organismo del Estado, según ley, será reprimido con pena privativa de libertad no menor de seis ni mayor de quince años.”(**)

(*) De conformidad con el Resolutivo 1 del Expediente Nº 00017-2011-PI-TC, publicado el 07 junio 2012, se declara FUNDADA la demanda de inconstitucionalidad en el extremo referido a la modificación del presente artículo a través de la ley Nº 29703 y en consecuencia nulo y carente de todo efecto la expresión “patrimonialmente”.

(**) Artículo modificado por el Artículo Único de la Ley Nº 29758, publicada el 21 julio 2011, cuyo texto es el siguiente: 

“Artículo 384. Colusión simple y agravada

El funcionario o servidor público que, interviniendo directa o indirectamente, por razón de su cargo, en cualquier etapa de las modalidades de adquisición o contratación pública de bienes, obras o servicios, concesiones o cualquier operación a cargo del Estado concerta con los interesados para defraudar al Estado o entidad u organismo del Estado, según ley, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años.

El funcionario o servidor público que, interviniendo directa o indirectamente, por razón de su cargo, en las contrataciones y adquisiciones de bienes, obras o servicios, concesiones o cualquier operación a cargo del Estado mediante concertación con los interesados, defraudare patrimonialmente al Estado o entidad u organismo del Estado, según ley, será reprimido con pena privativa de libertad no menor de seis ni mayor de quince años. (*)

(*) Artículo modificado por el Artículo único de la Ley N° 30111, publicada el 26 noviembre 2013, cuyo texto es el siguiente:

"Artículo 384. Colusión simple y agravada

El funcionario o servidor público que, interviniendo directa o indirectamente, por razón de su cargo, en cualquier etapa de las modalidades de adquisición o contratación pública de bienes, obras o servicios, concesiones o cualquier operación a cargo del Estado concierta con los interesados para defraudar al Estado o entidad u organismo del Estado, según ley, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años y con ciento ochenta a trescientos sesenta y cinco días-multa.

El funcionario o servidor público que, interviniendo directa o indirectamente, por razón de su cargo, en las contrataciones y adquisiciones de bienes, obras o servicios, concesiones o cualquier operación a cargo del Estado mediante concertación con los interesados, defraudare patrimonialmente al Estado o entidad u organismo del Estado, según ley, será reprimido con pena privativa de libertad no menor de seis ni mayor de quince años y con trescientos sesenta y cinco a setecientos treinta días-multa."  (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1243, publicado el 22 octubre 2016, cuyo texto es el siguiente:

“Artículo 384. Colusión simple y agravada

El funcionario o servidor público que, interviniendo directa o indirectamente, por razón de su cargo, en cualquier etapa de las modalidades de adquisición o contratación pública de bienes, obras o servicios, concesiones o cualquier operación a cargo del Estado concierta con los interesados para defraudar al Estado o entidad u organismo del Estado, según ley, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años; inhabilitación, según corresponda, conforme a los incisos 1, 2 y 8 del artículo 36; y, con ciento ochenta a trescientos sesenta y cinco días-multa.

El funcionario o servidor público que, interviniendo directa o indirectamente, por razón de su cargo, en las contrataciones y adquisiciones de bienes, obras o servicios, concesiones o cualquier operación a cargo del Estado mediante concertación con los interesados, defraudare patrimonialmente al Estado o entidad u organismo del Estado, según ley, será reprimido con pena privativa de libertad no menor de seis ni mayor de quince años; inhabilitación, según corresponda, conforme a los incisos 1, 2 y 8 del artículo 36; y, con trescientos sesenta y cinco a setecientos treinta días-multa.”  (*)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

Artículo 385 Patrocinio ilegal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, valiéndose de su calidad de funcionario o servidor público, patrocina intereses de particulares ante la administración pública, será reprimido con pena privativa de libertad no mayor de dos años o con prestación de servicio comunitario de veinte a cuarenta jornadas.

Artículo 386 
~~~~~~~~~~~~~

Las disposiciones de los artículos 384 y 385  son aplicables a los peritos, árbitros y contadores particulares, respecto de los bienes en cuya tasación, adjudicación o partición intervienen, respecto de los pertenecientes a sus pupilos o testamentarias.(*)

(*) Artículo modificado por la Tercera Disposición Modificatoria de la Ley Nº 26572, publicada el 05 enero 1996, cuyo texto es el siguiente:

"Artículo 386.- Las disposiciones de los artículos 384 y 385 son aplicables a los peritos y contadores particulares, respecto de los bienes en cuya tasación, adquisición o partición intervienen, respecto de los pertenecientes a sus pupilos o testamentarías. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 26643, publicada el 26 junio 1996, cuyo texto es el siguiente:

Responsabilidad de peritos, árbitros y contadores particulares

Artículo 386.- Las disposiciones de los Artículos 384 y 385  son aplicables a los Peritos, Arbitros y Contadores Particulares, respecto de los bienes en cuya tasación, adjudicación o partición intervienen; y, a los tutores, curadores y albaceas, respecto de los pertenecientes a incapaces o testamentarías.

SECCION  III
PECULADO

CONCORDANCIAS:     Ley N° 27770 (Sobre Beneficios Penales y Penitenciarios) aplicable en todas las modalidades de este delito, excepto en la forma culposa
D.S. Nº 017-2008-JUS, Arts. 46 y 49 (Decreto Supremo que aprueba el Reglamento del Decreto Legislativo Nº 1068 del Sistema de Defensa
Jurídica del Estado)

D.S Nº 009-2010-JUS (Aprueban Procedimiento para el pago de la reparación civil a favor del Estado en casos de procesos seguidos sobre
delitos de corrupción y otros delitos conexos)

Artículo 387 
~~~~~~~~~~~~~

El funcionario o servidor público que se apropia o utiliza, en cualquier forma, para sí o para otro, caudales o efectos cuya percepción, administración o custodia le estén confiados por razón de su cargo, será reprimido con pena privativa de la libertad no menor de dos ni mayor de ocho años.

Si el agente, por culpa, da ocasión a que se efectúe por otra persona la sustracción de caudales o efectos será reprimido con pena privativa de libertad no mayor de dos años o con prestación de servicio comunitario de veinte a cuarenta jornadas. (*)

(*) Artículo modificado por el Artículo Unico de la Ley Nº 26198, publicada el 13 junio 1993, cuyo texto es el siguiente:

Peculado

"Artículo 387.- El funcionario o servidor público que se apropia o utiliza, en cualquier forma, para sí o para otro, caudales o efectos cuya percepción, administración o custodia le estén confiados por razón de su cargo, será reprimido con pena privativa de la libertad no menor de dos ni mayor de ocho años.

Constituye circunstancia agravante si los caudales o efectos estuvieran destinados a fines asistenciales o a programas de apoyo social. En estos casos, la pena privativa de la libertad será no menor de cuatro ni mayor de diez años.

Si el agente, por culpa, da ocasión a que se efectúe por otra persona la sustracción de caudales o efectos será reprimido con pena privativa de libertad no mayor de dos años o con prestación de servicios comunitarios de veinte a cuarenta jornadas. Constituye circunstancia agravante si los caudales o efectos estuvieran destinados a fines asistenciales o a programas de apoyo social. En estos casos, la pena privativa de libertad será no menor de tres ni mayor de cinco años." (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 29703, publicada el 10 junio 2011, cuyo texto es el siguiente:

“Artículo 387.- Peculado doloso y culposo

El funcionario o servidor público que se apropia o utiliza en cualquier forma, o consiente que un tercero se apropie o utilice caudales o efectos públicos, cuya percepción, administración o custodia le estén confiados por razón de su cargo, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años.

Cuando el valor de lo apropiado o utilizado sobrepase diez unidades impositivas tributarias, será reprimido con pena privativa de libertad no menor de ocho ni mayor de doce años.

Si los caudales o efectos, independientemente de su valor, estuvieran destinados a fines asistenciales o a programas de apoyo social, la pena privativa de libertad será no menor de ocho ni mayor de doce años.

Si el agente, por culpa, da ocasión a que se efectúe por otra persona la sustracción de caudales o efectos, será reprimido con pena privativa de libertad no mayor de dos años o con prestación de servicios comunitarios de veinte a cuarenta jornadas. Si los caudales o efectos, independientemente de su valor, estuvieran destinados a fines asistenciales o a programas de desarrollo o apoyo social, la pena privativa de libertad será no menor de tres ni mayor de cinco años.” (*)

(*) Artículo modificado por el Artículo Único de la Ley Nº 29758, publicada el 21 julio 2011, cuyo texto es el siguiente:

"Artículo 387. Peculado doloso y culposo

El funcionario o servidor público que se apropia o utiliza, en cualquier forma, para sí o para otro, caudales o efectos cuya percepción, administración o custodia le estén confiados por razón de su cargo, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años.

Cuando el valor de lo apropiado o utilizado sobrepase diez unidades impositivas tributarias, será reprimido con pena privativa de libertad no menor de ocho ni mayor de doce años.

Constituye circunstancia agravante si los caudales o efectos estuvieran destinados a fines asistenciales o a programas de apoyo social. En estos casos, la pena privativa de libertad será no menor de ocho ni mayor de doce años.

Si el agente, por culpa, da ocasión a que se efectúe por otra persona la sustracción de caudales o efectos, será reprimido con pena privativa de libertad no mayor de dos años o con prestación de servicios comunitarios de veinte a cuarenta jornadas. Constituye circunstancia agravante si los caudales o efectos estuvieran destinados a fines asistenciales o a programas de apoyo social. En estos casos, la pena privativa de libertad será no menor de tres ni mayor de cinco años." (*)

(*) Artículo modificado por el Artículo único de la Ley N° 30111, publicada el 26 noviembre 2013, cuyo texto es el siguiente:

"Artículo 387. Peculado doloso y culposo

El funcionario o servidor público que se apropia o utiliza, en cualquier forma, para sí o para otro, caudales o efectos cuya percepción, administración o custodia le estén confiados por razón de su cargo, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años y con ciento ochenta a trescientos sesenta y cinco días-multa.

Cuando el valor de lo apropiado o utilizado sobrepase diez unidades impositivas tributarias, será reprimido con pena privativa de libertad no menor de ocho ni mayor de doce años y con trescientos sesenta y cinco a setecientos treinta días-multa.

Constituye circunstancia agravante si los caudales o efectos estuvieran destinados a fines asistenciales o a programas de apoyo social. En estos casos, la pena privativa de libertad será no menor de ocho ni mayor de doce años y con trescientos sesenta y cinco a setecientos treinta días-multa.

Si el agente, por culpa, da ocasión a que se efectúe por otra persona la sustracción de caudales o efectos, será reprimido con pena privativa de libertad no mayor de dos años y con prestación de servicios comunitarios de veinte a cuarenta jornadas. Constituye circunstancia agravante si los caudales o efectos estuvieran destinados a fines asistenciales o a programas de apoyo social. En estos casos, la pena privativa de libertad será no menor de tres ni mayor de cinco años y con ciento cincuenta a doscientos treinta días-multa."  (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1243, publicado el 22 octubre 2016, cuyo texto es el siguiente:

“Artículo 387. Peculado doloso y culposo

El funcionario o servidor público que se apropia o utiliza, en cualquier forma, para sí o para otro, caudales o efectos cuya percepción, administración o custodia le estén confiados por razón de su cargo, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años; inhabilitación, según corresponda, conforme a los incisos 1, 2 y 8 del artículo 36; y, con ciento ochenta a trescientos sesenta y cinco días-multa.

Cuando el valor de lo apropiado o utilizado sobrepase diez unidades impositivas tributarias, será reprimido con pena privativa de libertad no menor de ocho ni mayor de doce años; inhabilitación, según corresponda, conforme a los incisos 1, 2 y 8 del artículo 36; y, con trescientos sesenta y cinco a setecientos treinta días-multa.

Constituye circunstancia agravante si los caudales o efectos estuvieran destinados a fines asistenciales o a programas de apoyo o inclusión social. En estos casos, la pena privativa de libertad será no menor de ocho ni mayor de doce años; inhabilitación, según corresponda, conforme a los incisos 1, 2 y 8 del artículo 36; y, con trescientos sesenta y cinco a setecientos treinta días-multa.

Si el agente, por culpa, da ocasión a que se efectúe por otra persona la sustracción de caudales o efectos, será reprimido con pena privativa de libertad no mayor de dos años y con prestación de servicios comunitarios de veinte a cuarenta jornadas. Constituye circunstancia agravante si los caudales o efectos estuvieran destinados a fines asistenciales o a programas de apoyo o inclusión social. En estos casos, la pena privativa de libertad será no menor de tres ni mayor de cinco años y con ciento cincuenta a doscientos treinta días-multa.” (*)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el primer párrafo del presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA
PROCESOS CONSTITUCIONALES

Artículo 388 Peculado por uso
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario o servidor público que, para fines ajenos al servicio usa o permite que otro use vehículos, máquinas o cualquier otro instrumento de trabajo pertenecientes a la administración pública o que se hallan bajo su guarda, será reprimido con pena privativa de libertad no mayor de cuatro años.

Esta disposición es aplicable al contratista de una obra pública o a sus empleados cuando los efectos indicados pertenecen al Estado o a cualquier dependencia pública.

No están comprendidos en este artículo los vehículos motorizados destinados al servicio personal por razón del cargo. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 29703, publicada el 10 junio 2011, cuyo texto es el siguiente:

“Artículo 388.- Peculado de uso

El funcionario o servidor público que, para fines ajenos al servicio, usa o permite que otro use vehículos, máquinas u otros instrumentos de trabajo pertenecientes a la administración pública o que se hallan bajo su guarda, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.

Esta disposición es aplicable al contratista de una obra pública o a sus empleados, cuando los efectos indicados en el párrafo anterior pertenecen al Estado o a cualquier dependencia pública, independientemente del grado de afectación de la obra.

No están comprendidos los vehículos motorizados destinados al servicio personal por razón del cargo.” (*)

(*) Artículo modificado por el Artículo Único de la Ley Nº 29758, publicada el 21 julio 2011, cuyo texto es el siguiente:

"Artículo 388. Peculado de uso

El funcionario o servidor público que, para fines ajenos al servicio, usa o permite que otro use vehículos, máquinas o cualquier otro instrumento de trabajo pertenecientes a la administración pública o que se hallan bajo su guarda, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.

Esta disposición es aplicable al contratista de una obra pública o a sus empleados cuando los efectos indicados pertenecen al Estado o a cualquier dependencia pública.

No están comprendidos en este artículo los vehículos motorizados destinados al servicio personal por razón del cargo." (*)

(*) Artículo modificado por el Artículo Único de la Ley N° 30111, publicada el 26 noviembre 2013, cuyo texto es el siguiente:

"Artículo 388. Peculado de uso

El funcionario o servidor público que, para fines ajenos al servicio, usa o permite que otro use vehículos, máquinas o cualquier otro instrumento de trabajo pertenecientes a la administración pública o que se hallan bajo su guarda, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años y con ciento ochenta a trescientos sesenta y cinco días-multa.

Esta disposición es aplicable al contratista de una obra pública o a sus empleados cuando los efectos indicados pertenecen al Estado o a cualquier dependencia pública.

No están comprendidos en este artículo los vehículos motorizados destinados al servicio personal por razón del cargo."(*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1243, publicado el 22 octubre 2016, cuyo texto es el siguiente:

“Artículo 388. Peculado de uso

El funcionario o servidor público que, para fines ajenos al servicio, usa o permite que otro use vehículos, máquinas o cualquier otro instrumento de trabajo pertenecientes a la administración pública o que se hallan bajo su guarda, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años; inhabilitación, según corresponda, conforme a los incisos 1, 2 y 8 del artículo 36; y, con ciento ochenta a trescientos sesenta y cinco días-multa.

Esta disposición es aplicable al contratista de una obra pública o a sus empleados cuando los efectos indicados pertenecen al Estado o a cualquier dependencia pública.

No están comprendidos en este artículo los vehículos motorizados destinados al servicio personal por razón del cargo.”

Artículo 389 
~~~~~~~~~~~~~

El funcionario o servidor público que da al dinero o bienes que administra, una aplicación diferente de aquella a la que están destinados, será reprimido con pena privativa de libertad no mayor de tres años.

Si resulta dañado o entorpecido el servicio respectivo, la pena será no menor de dos ni mayor de cinco años. (*)

(*) Artículo modificado por el Artículo Único de la Ley Nº 26198, publicada el 13 junio 1993, cuyo texto es el siguiente:

"Artículo 389.- El funcionario o servidor público que da al dinero o bienes que administra, una aplicación diferente de aquella a la que están destinados, será reprimido con  pena privativa de libertad no mayor de tres años.

Si resulta dañado o entorpecido el servicio respectivo, la pena será no menor de dos ni mayor de cinco años.

Constituye circunstancia agravante, si el dinero o bienes que administra estuvieran destinados a fines asistenciales o a programas de apoyo social. En estos casos, la pena privativa  de la libertad será no menor de tres ni mayor de ocho años." (*)

(*) Artículo modificado por el Artículo Unico de la Ley Nº 27151, publicada el 07 julio 1999, cuyo texto es el siguiente:

Malversación

"Artículo 389.- El funcionario o servidor público que da al dinero o bienes que administra una aplicación definitiva diferente de aquella a los que están destinados, afectando el servicio o la función encomendada, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

Si el dinero o bienes que administra corresponden a programas de apoyo social, de desarrollo o asistenciales y son destinados a una aplicación definitiva diferente, afectando el servicio o la función encomendada, la pena privativa de libertad será no menor de tres años ni mayor de ocho años." (*)

(*) Artículo modificado por el Artículo único de la Ley N° 30111, publicada el 26 noviembre 2013, cuyo texto es el siguiente:

"Artículo 389. Malversación

El funcionario o servidor público que da al dinero o bienes que administra una aplicación definitiva diferente de aquella a los que están destinados, afectando el servicio o la función encomendada, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años y con ciento ochenta a trescientos sesenta y cinco días-multa.

Si el dinero o bienes que administra corresponden a programas de apoyo social, de desarrollo o asistenciales y son destinados a una aplicación definitiva diferente, afectando el servicio o la función encomendada, la pena privativa de libertad será no menor de tres ni mayor de ocho años y con trescientos sesenta y cinco días-multa." (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1243, publicado el 22 octubre 2016, cuyo texto es el siguiente:

“Artículo 389. Malversación

El funcionario o servidor público que da al dinero o bienes que administra una aplicación definitiva diferente de aquella a los que están destinados, afectando el servicio o la función encomendada, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años; inhabilitación, según corresponda, conforme a los incisos 1, 2 y 8 del artículo 36; y, con ciento ochenta a trescientos sesenta y cinco días-multa.

Si el dinero o bienes que administra corresponden a programas de apoyo social, de desarrollo o asistenciales y son destinados a una aplicación definitiva diferente, afectando el servicio o la función encomendada, la pena privativa de libertad será no menor de tres ni mayor de ocho años; inhabilitación, según corresponda, conforme a los incisos 1, 2 y 8 del artículo 36; y, trescientos sesenta y cinco a setecientos treinta días multa.” (*)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

Artículo 390 Retardo injustificado de pago
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario o servidor público que, teniendo fondos expeditos, demora injustificadamente un pago ordinario o decretado por la autoridad competente, será reprimido con pena privativa de libertad no mayor de dos años.

Artículo 391 Rehusamiento a entrega de bienes depositados o puestos en custodia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario o servidor público que, requerido con las formalidades de ley por la autoridad competente, rehusa entregar dinero, cosas o efectos depositados o puestos bajo su custodia o administración, será reprimido con pena privativa de libertad no mayor de dos años.

Artículo 392 
~~~~~~~~~~~~~

Están sujetos a lo prescrito en los artículos 387 a 389, los que administran o custodian dinero perteneciente a las entidades de beneficencia o similares, así como los administradores o depositarios de dinero o bienes embargados o depositados por orden de autoridad competente, aunque pertenezcan a particulares. (*)

(*) Artículo modificado por el Artículo Unico de la Ley Nº 26198, publicada el 13 junio 1993, cuyo texto es el siguiente:

Extensión de punibilidad

"Artículo 392.- Están sujetos a lo prescrito en los artículos 387 a 389, los que administran o custodian dineros pertenecientes a las entidades de beneficencia o similares, los administradores o depositarios de dinero o bienes embargados o depositados por orden de autoridad competente, aunque pertenezcan a particulares, así como todas las personas o representantes legales de personas jurídicas que administren o custodien dineros o bienes destinados a fines asistenciales o a programas de apoyo social." (*)

(*) Artículo modificado por la Sétima Disposición Final de la Ley N° 28165, publicada el 10 enero 2004, cuyo texto es el siguiente:

"Artículo 392.- Extensión del tipo

Están sujetos a lo prescrito en los artículos 387 a 389, los que administran o custodian dinero perteneciente a las entidades de beneficencia o similares, los ejecutores coactivos, administradores o depositarios de dinero o bienes embargados o depositados por orden de autoridad competente, aunque pertenezcan a particulares, así como todas las personas o representantes legales de personas jurídicas que administren o custodien dinero o bienes destinados a fines asistenciales o a programas de apoyo social.”

SECCION  IV
CORRUPCION DE FUNCIONARIOS

CONCORDANCIAS:     Ley N° 27770 (Ley Sobre Beneficios Penales y Penitenciarios) aplicable a todas las modalidades de este delito, incluidas las
cometidas por particulares
D.S. Nº 017-2008-JUS, Arts. 46 y 49 (Decreto Supremo que aprueba el Reglamento del Decreto Legislativo Nº 1068 del Sistema de Defensa
Jurídica del Estado)

D.S Nº 009-2010-JUS (Aprueban Procedimiento para el pago de la reparación civil a favor del Estado en casos de procesos seguidos sobre
delitos de corrupción y otros delitos conexos)

Artículo 393 Cohecho propio
~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario o servidor público que solicita o acepta donativo, promesa o cualquier otra ventaja, para realizar u omitir un acto en violación de sus obligaciones o el que las acepta a consecuencia de haber faltado a sus deberes, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28355, publicada el 06 octubre 2004, cuyo texto es el siguiente:

"Artículo 393.- Cohecho pasivo propio

El funcionario o servidor público que acepte o reciba donativo, promesa o cualquier otra ventaja o beneficio, para realizar u omitir un acto en violación de sus obligaciones o el que las acepta a consecuencia de haber faltado a ellas, será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal.

El funcionario o servidor público que solicita, directa o indirectamente, donativo, promesa o cualquier otra ventaja o beneficio, para realizar u omitir un acto en violación de sus obligaciones o a consecuencia de haber faltado a ellas, será reprimido con pena privativa de libertad no menor de seis ni mayor de ocho años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal.

El funcionario o servidor público que condiciona su conducta funcional derivada del cargo o empleo a la entrega o promesa de donativo o ventaja, será reprimido con pena privativa de libertad no menor de ocho ni mayor de diez años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal." (*)

(*) Artículo modificado por el Artículo único de la Ley N° 30111, publicada el 26 noviembre 2013, cuyo texto es el siguiente:

"Artículo 393. Cohecho pasivo propio

El funcionario o servidor público que acepte o reciba donativo, promesa o cualquier otra ventaja o beneficio, para realizar u omitir un acto en violación de sus obligaciones o el que las acepta a consecuencia de haber faltado a ellas, será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal y con ciento ochenta a trescientos sesenta y cinco días-multa.

El funcionario o servidor público que solicita, directa o indirectamente, donativo, promesa o cualquier otra ventaja o beneficio, para realizar u omitir un acto en violación de sus obligaciones o a consecuencia de haber faltado a ellas, será reprimido con pena privativa de libertad no menor de seis ni mayor de ocho años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal y con trescientos sesenta y cinco a setecientos treinta días-multa.

El funcionario o servidor público que condiciona su conducta funcional derivada del cargo o empleo a la entrega o promesa de donativo o ventaja, será reprimido con pena privativa de libertad no menor de ocho ni mayor de diez años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal y con trescientos sesenta y cinco a setecientos treinta días-multa."  (*)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

PROCESOS CONSTITUCIONALES

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

“Artículo 393-A. Soborno internacional pasivo

El funcionario o servidor público de otro Estado o funcionario de organismo internacional público que acepta, recibe o solicita, directa o indirectamente, donativo, promesa o cualquier otra ventaja o beneficio, para realizar u omitir un acto en el ejercicio de sus funciones oficiales, en violación de sus obligaciones, o las acepta como consecuencia de haber faltado a ellas, para obtener o retener un negocio u otra ventaja indebida, en la realización de actividades económicas internacionales, será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años.” (1)(2)

(1) Artículo incorporado por el Artículo 2 de la Ley Nº 29703, publicada el 10 junio 2011.

(2) Artículo modificado por el Artículo único de la Ley N° 30111, publicada el 26 noviembre 2013, cuyo texto es el siguiente:

"Artículo 393-A. Soborno internacional pasivo

El funcionario o servidor público de otro Estado o funcionario de organismo internacional público que acepta, recibe o solicita, directa o indirectamente, donativo, promesa o cualquier otra ventaja o beneficio, para realizar u omitir un acto en el ejercicio de sus funciones Oficiales, en violación de sus obligaciones, o las acepta como consecuencia de haber faltado a ellas, para obtener o retener un negocio u otra ventaja indebida, en la realización de actividades económicas internacionales, será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años y con trescientos sesenta y cinco a setecientos treinta días-multa."(*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1243, publicado el 22 octubre 2016, cuyo texto es el siguiente:

“Artículo 393-A. Soborno internacional pasivo

El funcionario o servidor público de otro Estado o funcionario de organismo internacional público que acepta, recibe o solicita, directa o indirectamente, donativo, promesa o cualquier otra ventaja o beneficio, para realizar u omitir un acto en el ejercicio de sus funciones Oficiales, en violación de sus obligaciones, o las acepta como consecuencia de haber faltado a ellas, para obtener o retener un negocio u otra ventaja indebida, en la realización de actividades económicas internacionales, será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años; inhabilitación, según corresponda, conforme a los incisos 1, 2 y 8 del artículo 36; y, con trescientos sesenta y cinco a setecientos treinta días-multa.”  (*)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

Artículo 394 Cohecho impropio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario o servidor público que solicita o acepta donativo, promesa o cualquier otra ventaja indebida para practicar un acto propio de su cargo, sin faltar a su obligación, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28355, publicada el 06 octubre 2004, cuyo texto es el siguiente:

"Artículo 394.- Cohecho pasivo impropio

El funcionario o servidor público que acepte o reciba donativo, promesa o cualquier otra ventaja o beneficio indebido para realizar un acto propio de su cargo o empleo, sin faltar a su obligación, o como consecuencia del ya realizado, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal.

El funcionario o servidor público que solicita, directa o indirectamente, donativo, promesa o cualquier otra ventaja indebida para realizar un acto propio de su cargo o empleo, sin faltar a su obligación, o como consecuencia del ya realizado, será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal."(*)

(*) Artículo modificado por el Artículo Único de la Ley N° 30111, publicada el 26 noviembre 2013, cuyo texto es el siguiente:

"Artículo 394. Cohecho pasivo impropio

El funcionario o servidor público que acepte o reciba donativo, promesa o cualquier otra ventaja o beneficio indebido para realizar un acto propio de su cargo o empleo, sin faltar a su obligación, o como consecuencia del ya realizado, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal y con ciento ochenta a trescientos sesenta y cinco días-multa.

El funcionario o servidor público que solicita, directa o indirectamente, donativo, promesa o cualquier otra ventaja indebida para realizar un acto propio de su cargo o empleo, sin faltar a su obligación, o como consecuencia del ya realizado, será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal y con trescientos sesenta y cinco a setecientos treinta días-multa."  (*)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

“Artículo 394-A.- El que, valiéndose de su condición de funcionario o servidor público, condiciona la distribución de bienes o la prestación de servicios correspondientes a programas públicos de apoyo o desarrollo social, con la finalidad de obtener ventaja política y/o electoral de cualquier tipo en favor propio o de terceros, será reprimido con pena privativa de libertad no menor de tres (3) ni mayor de seis (6) años, e inhabilitación por igual tiempo a la condena conforme a los incisos 1), 2) y 4) del Artículo 36 del Código Penal.” (1)(2)

(1) Artículo incorporado por el Artículo 1 de la Ley N° 27722, publicada el 14 mayo 2002.

(2) Artículo reubicado y reformado como Artículo 376-A en la Sección de los Delitos de Abuso de Autoridad del presente Código, por disposición del Artículo 2 de la Ley N° 28355, publicada el 06 octubre 2004.

Artículo 395 
~~~~~~~~~~~~~

El Juez, Arbitro, Fiscal o miembro del tribunal administrativo o de cualquier otro análogo que solicita o acepta donativo, promesa o cualquier otra ventaja, a sabiendas que se lo hacen con el fin de influir en la decisión de un asunto que esté sometido a su conocimiento, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de diez años. (*)

(*) Artículo modificado por el Artículo 1 del Decreto Ley Nº 25489, publicado el 10 mayo 1992, cuyo texto es el siguiente:

"Artículo 395.- El Juez, Arbitro, Fiscal, miembro del Tribunal Administrativo, Perito o cualquier otro análogo  que solicite y/o acepte donativo, promesa u otra ventaja, a sabiendas que se lo hacen con el fin de influir en la decisión de un asunto que esté sometido a su conocimiento, será reprimido con pena privativa de libertad no menor de seis ni mayor de quince años e inhabilitación conforme a los incisos 1), 2) y 4) del Artículo 36 del Código Penal y con ciento ochenta a trescientos sesenticinco días de multa. (*) RECTIFICADO POR FE DE ERRATAS

La inhabilitación que como accesoria de la pena privativa de libertad se imponga al agente del delito será puesta en conocimiento del Colegio de Abogados del lugar en donde se encuentre inscrito para que la Junta Directiva, bajo responsabilidad, proceda en el plazo de cinco (05) días a suspender la colegiación respectiva". (*)

(*) Artículo modificado por la Tercera Disposición Modificatoria de la Ley Nº 26572, publicada el 05 enero 1996, cuyo texto es el siguiente:

"Artículo 395.- El Juez, Fiscal, Miembro de Tribunal Administrativo, Perito o cualquier otro análogo que solicite y/o acepte donativo, promesa u otra ventaja, a sabiendas que se lo hacen con el fin de influir en la decisión de un asunto que esté sometido a su conocimiento, será reprimido con pena privativa de  libertad no menor de seis ni mayor de quince años e inhabilitación conforme a los incisos 1), 2) y 4) del Artículo 36 del Código Penal y con ciento ochenta a trescientos sesenta y cinco días de multa.

La inhabilitación que como accesoria a la pena privativa de libertad se imponga al agente del delito será puesta en conocimiento del Colegio de Abogados del lugar en donde se encuentre inscrito para que la Junta Directiva, bajo responsabilidad, proceda en el plazo de cinco (05) días a suspender la colegiación respectiva." (*)

(*) Artículo modificado por el Artículo 1 de  la Ley Nº 26643, publicada el 26 junio 1996, cuyo texto es el siguiente:

Corrupción pasiva

Artículo 395.- El Magistrado, Arbitro, Fiscal, Perito, Miembro de Tribunal Administrativo o cualquier otro análogo que solicite y/o acepte donativo, promesa o cualquier otra ventaja, a sabiendas que es hecha con el fin de influir en la decisión de un asunto que esté sometido a su conocimiento, será reprimido con pena privativa de libertad no menor de seis ni mayor de quince años e inhabilitación conforme a los incisos 1), 2) y 4) del Artículo 36º del Código Penal y con ciento ochenta a trescientos sesenta y cinco días-multa .

La inhabilitación que como accesoria a la pena privativa de libertad se imponga al agente del delito, será puesta en conocimiento del Colegio respectivo en donde se encuentra inscrito el agente, para que dentro de cinco (5) días proceda a suspender la colegiación respectiva, bajo responsabilidad. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28355, publicada el 06 octubre 2004, cuyo texto es el siguiente:

"Artículo 395.- Cohecho pasivo específico

El Magistrado, Árbitro, Fiscal, Perito, Miembro de Tribunal Administrativo o cualquier otro análogo a los anteriores que bajo cualquier modalidad acepte o reciba donativo, promesa o cualquier otra ventaja o beneficio, a sabiendas que es hecho con el fin de influir o decidir en asunto sometido a su conocimiento o competencia, será reprimido con pena privativa de libertad no menor de seis ni mayor de quince años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal y con ciento ochenta a trescientos sesenta y cinco días-multa.

El Magistrado, Árbitro, Fiscal, Perito, Miembro de Tribunal Administrativo o cualquier otro análogo a los anteriores que bajo cualquier modalidad solicite, directa o indirectamente, donativo, promesa o cualquier otra ventaja o beneficio, con el fin de influir en la decisión de un asunto que esté sometido a su conocimiento, será reprimido con pena privativa de libertad no menor de ocho ni mayor de quince años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal y con trescientos sesenta y cinco a setecientos días-multa."   (*)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

PROCESOS CONSTITUCIONALES

“Artículo 395-A.- Cohecho pasivo propio en el ejercicio de la función policial

El miembro de la Policía Nacional que acepta o recibe donativo, promesa o cualquier otra ventaja o beneficio, para sí o para otro, para realizar u omitir un acto en violación de sus obligaciones derivadas de la función policial o el que las acepta a consecuencia de haber faltado a ellas, será sancionado con pena privativa de libertad no menor de cinco ni mayor de diez años e inhabilitación conforme a los incisos 1, 2 y 8 del artículo 36.

El miembro de la Policía Nacional que solicita, directa o indirectamente, donativo, promesa o cualquier otra ventaja o beneficio, para realizar u omitir un acto en violación de sus obligaciones derivadas de la función policial o a consecuencia de haber faltado a ellas, será reprimido con pena privativa de libertad no menor de seis ni mayor de diez años e inhabilitación conforme a los incisos 1, 2 y 8 del artículo 36 del Código Penal.

El miembro de la Policía Nacional que condiciona su conducta funcional a la entrega o promesa de donativo o cualquier otra ventaja o beneficio, será reprimido con pena privativa de libertad no menor de ocho ni mayor de doce años e inhabilitación conforme a los incisos 1, 2 y 8 artículo 36 del Código Penal.”(*)

(*) Artículo incorporado por el Artículo 3 del Decreto Legislativo N° 1351, publicado el 07 enero 2017.

“Artículo 395-B.- Cohecho pasivo impropio en el ejercicio de la función policial

El miembro de la Policía Nacional que acepta o recibe donativo, promesa o cualquier otra ventaja o beneficio indebido para realizar u omitir un acto propio de su función, sin faltar a su obligación, o como consecuencia del acto ya realizado u omitido, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de siete años e inhabilitación conforme a los incisos 1, 2 y 8 del artículo 36.

El miembro de la Policía Nacional que solicita, directa o indirectamente, donativo, promesa o cualquier otra ventaja indebida para realizar u omitir un acto propio de su función, sin faltar a su obligación, o como consecuencia del acto ya realizado u omitido, será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años e inhabilitación conforme a los incisos 1, 2 y 8 del artículo 36.”(*)

(*) Artículo incorporado por el Artículo 3 del Decreto Legislativo N° 1351, publicado el 07 enero 2017.

Artículo 396 Corrupción de auxiliares jurisdiccionales-Corrupción pasiva atenuada
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Si en el caso del artículo 395, el agente es secretario judicial o auxiliar de justicia o desempeña algún cargo similar, la pena será privativa de libertad no mayor de cuatro años. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28355, publicada el 06 octubre 2004, cuyo texto es el siguiente:

"Artículo 396.- Corrupción pasiva de auxiliares jurisdiccionales

Si en el caso del artículo 395, el agente es secretario judicial, relator, especialista, auxiliar jurisdiccional o cualquier otro análogo a los anteriores, será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal." (*)

(*) Artículo modificado por el Artículo único de la Ley N° 30111, publicada el 26 noviembre 2013, cuyo texto es el siguiente:

"Artículo 396. Corrupción pasiva de auxiliares jurisdiccionales

Si en el caso del artículo 395, el agente es secretario judicial, relator, especialista, auxiliar jurisdiccional o cualquier otro análogo a los anteriores, será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal y con ciento ochenta a trescientos sesenta y cinco días-multa." (*)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

Artículo 397 
~~~~~~~~~~~~~

El funcionario o servidor público que, directa o indirectamente o por acto simulado, se interesa en cualquier contrato u operación en que interviene por razón de su cargo, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años.(*)

(*) Artículo modificado por el Artículo Unico de la Ley Nº 27074, publicada el 26 marzo 1999, cuyo texto es el siguiente:

Aprovechamiento indebido de cargo

"Artículo 397.- El funcionario o servidor público que indebidamente en forma directa o indirecta o por acto simulado se interesa por cualquier contrato u operación en que interviene por razón de su cargo, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28355, publicada el 06 octubre 2004, cuyo texto es el siguiente:

"Artículo 397.- Cohecho activo genérico

El que, bajo cualquier modalidad, ofrece, da o promete a un funcionario o servidor público donativo, promesa, ventaja o beneficio para que realice u omita actos en violación de sus obligaciones, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años.

El que, bajo cualquier modalidad, ofrece, da o promete donativo, ventaja o beneficio para que el funcionario o servidor público realice u omita actos propios del cargo o empleo, sin faltar a su obligación, será reprimido con pena privativa de libertad no menor de tres ni mayor de cinco años." (*)

(*) Artículo modificado por el Artículo único de la Ley N° 30111, publicada el 26 noviembre 2013, cuyo texto es el siguiente:

"Artículo 397. Cohecho activo genérico

El que, bajo cualquier modalidad, ofrece, da o promete a un funcionario o servidor público donativo, promesa, ventaja o beneficio para que realice u omita actos en violación de sus obligaciones, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años y con trescientos sesenta y cinco a setecientos treinta días-multa.

El que, bajo cualquier modalidad, ofrece, da o promete donativo, ventaja o beneficio para que el funcionario o servidor público realice u omita actos propios del cargo o empleo, sin faltar a su obligación, será reprimido con pena privativa de libertad no menor de tres ni mayor de cinco años y con trescientos sesenta y cinco a setecientos treinta días-multa." (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1243, publicado el 22 octubre 2016, cuyo texto es el siguiente:

“Artículo 397. Cohecho activo genérico

El que, bajo cualquier modalidad, ofrece, da o promete a un funcionario o servidor público donativo, promesa, ventaja o beneficio para que realice u omita actos en violación de sus obligaciones, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años; inhabilitación, según corresponda, conforme a los incisos 1, 2 y 8 del artículo 36; y, con trescientos sesenta y cinco a setecientos treinta días-multa.

El que, bajo cualquier modalidad, ofrece, da o promete donativo, ventaja o beneficio para que el funcionario o servidor público realice u omita actos propios del cargo o empleo, sin faltar a su obligación, será reprimido con pena privativa de libertad no menor de tres ni mayor de cinco años; inhabilitación, según corresponda, conforme a los incisos 1, 2 y 8 del artículo 36; y, con trescientos sesenta y cinco a setecientos treinta días-multa.”(*)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

"Artículo 397- A.- Cohecho activo transnacional

El que, bajo cualquier modalidad, ofrezca, otorgue o prometa directa o indirectamente a un funcionario o servidor público de otro Estado o funcionario de organismo internacional público donativo, promesa, ventaja o beneficio indebido que redunde en su propio provecho o en el de otra persona, para que dicho servidor o funcionario público realice u omita actos propios de su cargo o empleo, en violación de sus obligaciones o sin faltar a su obligación para obtener o retener un negocio u otra ventaja indebida en la realización de actividades económicas o comerciales internacionales, será reprimido con pena privativa de la libertad no menor de cinco años ni mayor de ocho años.” (1)(2)

(1) Artículo incorporado por el Artículo 1 de la Ley N° 29316, publicada el 14 enero 2009.

(2) Artículo modificado por el Artículo Único de la Ley N° 30111, publicada el 26 noviembre 2013, cuyo texto es el siguiente:

"Artículo 397-A. Cohecho activo transnacional

El que, bajo cualquier modalidad, ofrezca, otorgue o prometa directa o indirectamente a un funcionario o servidor público de otro Estado o funcionario de organismo internacional público donativo, promesa, ventaja o beneficio indebido que redunde en su propio provecho o en el de otra persona, para que dicho servidor o funcionario público realice u omita actos propios de su cargo o empleo, en violación de sus obligaciones o sin faltar a su obligación para obtener o retener un negocio u otra ventaja indebida en la realización de actividades económicas o comerciales internacionales, será reprimido con pena privativa de la libertad no menor de cinco años ni mayor de ocho años y con trescientos sesenta y cinco a setecientos treinta días-multa." (*)

(*) De conformidad con el Artículo 3 de la Ley N° 30424, publicada el 21 abril 2016, señala que las personas jurídicas a que se hace referencia en el artículo 2 de la citada Ley son responsables administrativamente por el delito de cohecho activo transnacional, previsto en el presente artículo, cuando este haya sido cometido en su nombre o por cuenta de ellas y en su beneficio, directo o indirecto, por lo establecido en el citado artículo. La referida norma entra en vigencia el 1 de enero del 2018.

(*) De conformidad con el Artículo 5 de la Ley N° 30424, publicada el 21 abril 2016, establece que el juez aplica, según corresponda, las medidas administrativas contra las personas jurídicas que resultaren administrativamente responsables de la comisión del delito de cohecho activo transnacional, tipificado en el presente artículo, detalladas en el citado artículo. La referida norma entra en vigencia el 1 de enero del 2018.

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1243, publicado el 22 octubre 2016, cuyo texto es el siguiente:

“Artículo 397-A. Cohecho activo transnacional

El que, bajo cualquier modalidad, ofrezca, otorgue o prometa directa o indirectamente a un funcionario o servidor público de otro Estado o funcionario de organismo internacional público donativo, promesa, ventaja o beneficio indebido que redunde en su propio provecho o en el de otra persona, para que dicho servidor o funcionario público realice u omita actos propios de su cargo o empleo, en violación de sus obligaciones o sin faltar a su obligación para obtener o retener un negocio u otra ventaja indebida en la realización de actividades económicas o comerciales internacionales, será reprimido con pena privativa de la libertad no menor de cinco años ni mayor de ocho años; inhabilitación, según corresponda, conforme a los incisos 1, 2 y 8 del artículo 36; y, con trescientos sesenta y cinco a setecientos treinta días-multa.”(*)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

CONCORDANCIAS CON EL TLC PERÚ - ESTADOS UNIDOS DE NORTEAMÉRICA

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

Artículo 398 
~~~~~~~~~~~~~

El que hace donativos, promesas o cualquier otra ventaja a un Juez, Arbitro, Fiscal o miembro de tribunal administrativo o de cualquier otro análogo, con el objeto de influir en la decisión de un proceso pendiente de fallo, será reprimido con pena privativa de libertad no menor de tres ni mayor de ocho años.

Cuando el donativo, la promesa o cualquier otra ventaja se hace a un testigo, perito, traductor o intérprete, la pena será no menor de dos ni mayor de cuatro años. (*)

(*) Artículo modificado por la Tercera Disposición Modificatoria de la Ley Nº 26572, publicada el 05 enero 1996, cuyo texto es el siguiente:

"Artículo 398.- El que hace donativos, promesas o cualquier otra ventaja a un Juez, Fiscal o miembro de tribunal administrativo o de cualquier otro análogo, con el objeto de influir en la decisión de un proceso pendiente de fallo, será reprimido con pena privativa de libertad no menor de tres ni mayor de ocho años.  Cuando el donativo, la promesa o cualquier otra ventaja se hace a un testigo, perito, traductor o intérprete, la pena será no menor de dos ni mayor de cuatro años." (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 26643, publicada el 26 junio 1996, cuyo texto es el siguiente:

Corrupción activa

"Artículo 398.- El que hace donativo, promesa o cualquier otra ventaja a un Magistrado, Arbitro, Fiscal, Miembro de Tribunal Administrativo o de cualquier otro análogo, con el objeto de influir en la decisión de un proceso pendiente de fallo, será reprimido con pena privativa de libertad, no menor de tres ni mayor de ocho años. Cuando el donativo, la promesa o cualquier otra ventaja se hace a un testigo, perito, traductor o intérprete, la pena será no menor de dos ni mayor de cuatro años." (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28355, publicada el 06 octubre 2004, cuyo texto es el siguiente:

"Artículo 398.- Cohecho activo específico

El que, bajo cualquier modalidad, ofrece, da o promete donativo, ventaja o beneficio a un Magistrado, Fiscal, Perito, Árbitro, Miembro de Tribunal Administrativo o análogo con el objeto de influir en la decisión de un asunto sometido a su conocimiento o competencia, será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años e inhabilitación accesoria conforme a los incisos 2, 3, y 4 del artículo 36 del Código Penal.

Cuando el donativo, promesa, ventaja o beneficio se ofrece o entrega a un secretario, relator, especialista, auxiliar jurisdiccional, testigo, traductor o intérprete o análogo, la pena privativa de libertad será no menor de cuatro ni mayor de ocho años e inhabilitación accesoria conforme a los incisos 2, 3 y 4 del artículo 36 del Código Penal.

Si el que ofrece, da o corrompe es abogado o forma parte de un estudio de abogados, la pena privativa de libertad será no menor de cinco ni mayor de ocho años e inhabilitación accesoria conforme a los incisos 1, 2, 3 y 8 del Código Penal y con ciento ochenta a trescientos sesenta y cinco días-multa." (*)

(*) Artículo modificado por el Artículo único de la Ley N° 30111, publicada el 26 noviembre 2013, cuyo texto es el siguiente:

"Artículo 398. Cohecho activo específico

El que, bajo cualquier modalidad, ofrece, da o promete donativo, ventaja o beneficio a un Magistrado, Fiscal, Perito, Árbitro, Miembro de Tribunal administrativo o análogo con el objeto de influir en la decisión de un asunto sometido a su conocimiento o competencia, será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años e inhabilitación accesoria conforme a los incisos 2, 3 y 4 del artículo 36 del Código Penal y con trescientos sesenta y cinco a setecientos treinta días-multa.

Cuando el donativo, promesa, ventaja o beneficio se ofrece o entrega a un secretario, relator, especialista, auxiliar jurisdiccional, testigo, traductor o intérprete o análogo, la pena privativa de libertad será no menor de cuatro ni mayor de ocho años e inhabilitación accesoria conforme a los incisos 2, 3 y 4 del artículo 36 del Código Penal y con trescientos sesenta y cinco a setecientos treinta días-multa.

Si el que ofrece, da o corrompe es abogado o forma parte de un estudio de abogados, la pena privativa de libertad será no menor de cinco ni mayor de ocho años e inhabilitación accesoria conforme a los incisos 1, 2, 3 y 8 del Código Penal y con trescientos sesenta y cinco a setecientos treinta días-multa." (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1243, publicado el 22 octubre 2016, cuyo texto es el siguiente:

“Artículo 398. Cohecho activo específico

El que, bajo cualquier modalidad, ofrece, da o promete donativo, ventaja o beneficio a un Magistrado, Fiscal, Perito, Árbitro, Miembro de Tribunal administrativo o análogo con el objeto de influir en la decisión de un asunto sometido a su conocimiento o competencia, será reprimido con pena privativa de libertad no menor de cinco ni mayor de ocho años; inhabilitación, según corresponda, conforme a los incisos 1, 2 y 8 del artículo 36; y, con trescientos sesenta y cinco a setecientos treinta días-multa.

Cuando el donativo, promesa, ventaja o beneficio se ofrece o entrega a un secretario, relator, especialista, auxiliar jurisdiccional, testigo, traductor o intérprete o análogo, la pena privativa de libertad será no menor de cuatro ni mayor de ocho años; inhabilitación, según corresponda, conforme a los incisos 1, 2, 3 y 4 del artículo 36; y, con trescientos sesenta y cinco a setecientos treinta días-multa.

Si el que ofrece, da o corrompe es abogado o forma parte de un estudio de abogados, la pena privativa de libertad será no menor de cinco ni mayor de ocho años; inhabilitación, según corresponda, conforme a los incisos 2, 3, 4 y 8 del artículo 36; y, con trescientos sesenta y cinco a setecientos treinta días-multa.” (*)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

"Artículo 398-A.- Si en el caso del Artículo 398, el agente del delito de corrupción de un Juez, Arbitro, Fiscal, Miembro de Tribunal Administrativo, Auxiliar de Justicia o cualquier otro análogo, es abogado, la pena será privativa de libertad no menor de cinco ni mayor de diez años e inhabilitación conforme a los incisos 4) u 8) del Artículo 36 del Código Penal y con ciento ochenta a trescientos sesenticinco días multa. (*) RECTIFICADO POR FE DE ERRATAS

Cuando el donativo, la promesa o cualquier otra ventaja la hace el abogado a un testigo, perito, traductor o intérprete, la pena será no menor de cuatro ni mayor de ocho años e inhabilitación conforme al inciso 4) del Artículo 36 y con noventa a ciento veinte días multa". (1)(2)

(1) Artículo incorporado por el Artículo 2 del Decreto Ley Nº 25489, publicado el 10 mayo 1992.

(2) Artículo modificado por la Tercera Disposición Modificatoria de la Ley Nº 26572, publicada el 05 enero 1996, cuyo texto es el siguiente:

"Artículo 398-A (art. 2 D.L. 25489).- Si en el caso del Artículo 398, el agente del delito de corrupción es un Juez, Fiscal, Miembro de Tribunal Administrativo, Auxiliar de Justicia  o de cualquier otro análogo, es abogado, la pena será privativa de libertad no menor de cinco ni mayor de diez años e inhabilitación conforme a los incisos 4) a 8) del Artículo 36 del Código Penal y con ciento ochenta a trescientos sesenta y cinco días multa.

Cuando el donativo, la promesa o cualquier otra ventaja la hace el abogado a un testigo, perito, traductor o intérprete, la pena será no menor de cuatro ni mayor de ocho años e inhabilitación conforme al inciso 4) del Artículo 36 y con noventa a ciento veinte días multa”. (*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 26643 , publicada el 26 junio 1996, cuyo texto es el siguiente:

Corrupción activa de abogado

"Artículo 398-A.- Si en el caso del Artículo 398, es Abogado el Agente del delito de corrupción de un Magistrado, Arbitro, Fiscal, Miembro de Tribunal Administrativo o de cualquier otro análogo, la pena será privativa de libertad no menor de cinco ni mayor de diez años e inhabilitación conforme a los incisos 1) a 8) del Artículo 36º del Código Penal, y con ciento ochenta a trescientos sesenta y cinco días-multa.

Cuando el donativo, la promesa o cualquier otra ventaja la hace el abogado a un Testigo, Perito, Traductor, Intérprete o cualquier otro auxiliar jurisdiccional, la pena privativa de libertad será no menor de cuatro ni mayor de ocho años  e inhabilitación conforme al inciso 4) del artículo 36º del Código Penal, y con noventa a ciento veinte días-multa." (*)

(*) Artículo derogado por el Artículo 4 de la Ley N° 28355, publicada el 06 octubre 2004.

“Artículo 398-A.- Cohecho activo en el ámbito de la función policial

El que, bajo cualquier modalidad, ofrece, da o promete a un miembro de la Policía Nacional donativo o cualquier ventaja o beneficio para que realice u omita actos en violación de sus obligaciones derivadas de la función policial, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años.

El que, bajo cualquier modalidad, ofrece, da o promete a un miembro de la Policía Nacional donativo o cualquier ventaja o beneficio para que realice u omita actos propios de la función policial, sin faltar a las obligaciones que se derivan de ella, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años.”(*)

(*) Artículo incorporado por el Artículo 3 del Decreto Legislativo N° 1351, publicado el 07 enero 2017.

Inhabilitación del ejercicio de la abogacía

"Artículo 398 B.- La inhabilitación que como accesoria de la pena privativa de libertad prevista en el artículo anterior se imponga a los autores del delito de corrupción de magistrados será puesta en conocimiento de la Corte Superior de Justicia respectiva y del Fiscal Superior Decano para que en el caso del inciso 8 del artículo 36 se proceda a anular el asiento de inscripción en el Libro de Registro de Títulos; así como del Colegio de Abogados del Distrito Judicial correspondiente y de la Federación Nacional de Colegios de Abogados del Perú, en el plazo de cinco (05) días para la suspensión o anulación de la colegiación."  (*) RECTIFICADO POR FE DE ERRATAS

Igualmente, la inhabilitación impuesta de acuerdo al inciso 8 del Artículo 36 será puesta en conocimiento de la Universidad que otorgó el Título Profesional de Abogado al sentenciado, para que el Rectorado respectivo, en el plazo de ocho (08) días, proceda a su cancelación. (1)(2)(3)

(1) Artículo incorporado por el Artículo 2 del Decreto Ley Nº 25489, publicado el 10 mayo 1992.

(2) Actualmente Junta de Decanos de los Colegios de Abogados del Perú según Decreto Ley Nº 25892, publicado el 27 noviembre 1992.

(3) Artículo derogado por el Artículo 4 de la Ley N° 28355, publicada el 06 octubre 2004.

“Artículo 398-B.- Inhabilitación

En los supuestos del artículo 398-A, cuando el agente corrompa a un miembro de la Policía Nacional en el ejercicio sus funciones, siempre que éstas correspondan al tránsito o seguridad vial, se le impondrá además inhabilitación consistente en la cancelación o incapacidad definitiva, según sea el caso, para obtener autorización para conducir, de conformidad con el inciso 7 del artículo 36.”(*)

(*) Artículo incorporado por el Artículo 3 del Decreto Legislativo N° 1351, publicado el 07 enero 2017.

Artículo 399 Corrupción activa de funcionario
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que trata de corromper a un funcionario o servidor público con dádivas, promesas o ventajas de cualquier clase para que haga u omita algo en violación de sus obligaciones, será reprimido con pena privativa de libertad no menor de tres ni mayor de cinco años.

Si el agente trata de corromper para que el funcionario o servidor público haga u omita un acto propio de sus funciones, sin faltar a sus obligaciones, la pena será no menor de dos ni mayor de cuatro años. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28355, publicada el 06 octubre 2004, cuyo texto es el siguiente:

"Artículo 399.- Negociación incompatible o aprovechamiento indebido de cargo

El funcionario o servidor público que indebidamente en forma directa o indirecta o por acto simulado se interesa, en provecho propio o de tercero, por cualquier contrato u operación en que interviene por razón de su cargo, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal." (*)

(*) Artículo modificado por el Artículo Único de la Ley N° 30111, publicada el 26 noviembre 2013, cuyo texto es el siguiente:

"Artículo 399. Negociación incompatible o aprovechamiento indebido de cargo

El funcionario o servidor público que indebidamente en forma directa o indirecta o por acto simulado se interesa, en provecho propio o de tercero, por cualquier contrato u operación en que interviene por razón de su cargo, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal y con ciento ochenta a trescientos sesenta y cinco días-multa." (*)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

Artículo 400 Tráfico de influencias
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, invocando influencias, reales o simuladas, recibe, hace dar o prometer para sí o para un tercero, donativo o promesa o cualquier otra ventaja con el ofrecimiento de interceder ante un funcionario o servidor público que esté conociendo o haya conocido, un caso judicial o administrativo, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años. (*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 28355, publicada el 06 octubre 2004, cuyo texto es el siguiente:

"Artículo 400.- Tráfico de influencias

El que, invocando o teniendo influencias reales o simuladas recibe, hace dar o prometer para sí o para un tercero, donativo o promesa o cualquier otra ventaja o beneficio con el ofrecimiento de interceder ante un funcionario o servidor público que ha de conocer, esté conociendo o haya conocido un caso judicial o administrativo, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años.

Si el agente es un funcionario o servidor público, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal."(*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 29703, publicada el 10 junio 2011, cuyo texto es el siguiente:

“Artículo 400.- Tráfico de influencias

El que solicita, recibe, hace dar o prometer, para sí o para otro, donativo, promesa, cualquier ventaja o beneficio, por el ofrecimiento real de interceder ante un funcionario o servidor público que haya conocido, esté conociendo o vaya a conocer un caso judicial o administrativo será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años. Si el agente es funcionario o servidor público, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años e inhabilitación conforme a los numerales 1 y 2 del artículo 36 del Código Penal.” (*)

(*) Artículo modificado por el Artículo Único de la Ley Nº 29758, publicada el 21 julio 2011, cuyo texto es el siguiente:

"Artículo 400. Tráfico de influencias

El que, invocando o teniendo influencias reales o simuladas, recibe, hace dar o prometer para sí o para un tercero, donativo o promesa o cualquier otra ventaja o beneficio con el ofrecimiento de interceder ante un funcionario o servidor público que ha de conocer, esté conociendo o haya conocido un caso judicial o administrativo, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años.

Si el agente es un funcionario o servidor público, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal." (*)

(*) Artículo modificado por el Artículo único de la Ley N° 30111, publicada el 26 noviembre 2013, cuyo texto es el siguiente:

"Artículo 400. Tráfico de influencias

El que, invocando o teniendo influencias reales o simuladas, recibe, hace dar o prometer para sí o para un tercero, donativo o promesa o cualquier otra ventaja o beneficio con el ofrecimiento de interceder ante un funcionario o servidor público que ha de conocer, esté conociendo o haya conocido un caso judicial o administrativo, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años y con ciento ochenta a trescientos sesenta y cinco días-multa.

Si el agente es un funcionario o servidor público, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal y con trescientos sesenta y cinco a setecientos treinta días-multa." (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1243, publicado el 22 octubre 2016, cuyo texto es el siguiente:

“Artículo 400. Tráfico de influencias

El que, invocando o teniendo influencias reales o simuladas, recibe, hace dar o prometer para sí o para un tercero, donativo o promesa o cualquier otra ventaja o beneficio con el ofrecimiento de interceder ante un funcionario o servidor público que ha de conocer, esté conociendo o haya conocido un caso judicial o administrativo, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años; inhabilitación, según corresponda, conforme a los incisos 2, 3, 4 y 8 del artículo 36; y con ciento ochenta a trescientos sesenta y cinco días-multa.

Si el agente es un funcionario o servidor público, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de ocho años; inhabilitación, según corresponda, conforme a los incisos 1, 2 y 8 del artículo 36; y, con trescientos sesenta y cinco a setecientos treinta días-multa.” (*)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

Artículo 401 Enriquecimiento ilícito
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El funcionario o servidor público que, por razón de su cargo, se enriquece ilícitamente, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años.

“Se considera que existe indicio de enriquecimiento ilícito, cuando el aumento del patrimonio y/o del gasto económico personal del funcionario o servidor público, en consideración a su declaración jurada de bienes y rentas, es notoriamente superior al que normalmente haya podido tener en virtud de sus sueldos o emolumentos percibidos, o de los incrementos de su capital, o de sus ingresos por cualquier otra causa lícita.” (1)(2)

(1) Párrafo incorporado por el Artículo 7 de la Ley Nº 27482, publicada el 15 junio 2001.

(2) Artículo modificado por el Artículo 1 de la Ley N° 28355, publicada el 06 octubre 2004, cuyo texto es el siguiente:

"Artículo 401.- Enriquecimiento ilícito

El funcionario o servidor público que ilícitamente incrementa su patrimonio, respecto de sus ingresos legítimos durante el ejercicio de sus funciones y que no pueda justificar razonablemente, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal.

Si el agente es un funcionario público que haya ocupado cargos de alta dirección en las entidades u organismos de la administración pública o empresas estatales, o esté sometido a la prerrogativa del antejuicio y la acusación constitucional, la pena será no menor de ocho ni mayor de dieciocho años e inhabilitación conforme a los incisos 1 y 2 del artículo 36 del Código Penal.

Se considera que existe indicio de enriquecimiento ilícito cuando el aumento del patrimonio y/o del gasto económico personal del funcionario o servidor público, en consideración a su declaración jurada de bienes y rentas, es notoriamente superior al que normalmente haya podido tener en virtud de sus sueldos o emolumentos percibidos, o de los incrementos de su capital, o de sus ingresos por cualquier otra causa lícita.”(*)

(*) Artículo modificado por el Artículo 1 de la Ley Nº 29703, publicada el 10 junio 2011, cuyo texto es el siguiente:

"Artículo 401.- Enriquecimiento ilícito

El funcionario o servidor público que, durante el ejercicio de sus funciones, incrementa ilícitamente su patrimonio respecto de sus ingresos legítimos, será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años.

Si el agente es un funcionario público que ha ocupado cargos de alta dirección en las entidades u organismos de la administración pública o empresas estatales, o está sometido a la prerrogativa del antejuicio y la acusación constitucional, la pena privativa de libertad será no menor de ocho ni mayor de dieciocho años. Se considera que existe indicio de enriquecimiento ilícito cuando el aumento del patrimonio o del gasto económico personal del funcionario o servidor público, en consideración a su declaración jurada de bienes y rentas, es notoriamente superior al que normalmente haya podido tener en virtud de sus sueldos o emolumentos percibidos o de los incrementos de su capital o de sus ingresos por cualquier otra causa lícita.” (*)

(*) Artículo modificado por el Artículo Único de la Ley Nº 29758, publicada el 21 julio 2011, cuyo texto es el siguiente:

"Artículo 401. Enriquecimiento ilícito

El funcionario o servidor público que, abusando de su cargo, incrementa ilícitamente su patrimonio respecto de sus ingresos legítimos será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años.

Si el agente es un funcionario público que ha ocupado cargos de alta dirección en las entidades, organismos o empresas del Estado, o está sometido a la prerrogativa del antejuicio y la acusación constitucional, la pena privativa de libertad será no menor de diez ni mayor de quince años.

Se considera que existe indicio de enriquecimiento ilícito cuando el aumento del patrimonio o del gasto económico personal del funcionario o servidor público, en consideración a su declaración jurada de bienes y rentas, es notoriamente superior al que normalmente haya podido tener en virtud de sus sueldos o emolumentos percibidos o de los incrementos de su capital o de sus ingresos por cualquier otra causa lícita." (*)

(*) Artículo modificado por el Artículo único de la Ley N° 30111, publicada el 26 noviembre 2013, cuyo texto es el siguiente:

"Artículo 401. Enriquecimiento ilícito

El funcionario o servidor público que, abusando de su cargo, incrementa ilícitamente su patrimonio respecto de sus ingresos legítimos será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años y con trescientos sesenta y cinco a setecientos treinta días-multa.

Si el agente es un funcionario público que ha ocupado cargos de alta dirección en las entidades, organismos o empresas del Estado, o está sometido a la prerrogativa del antejuicio y la acusación constitucional, la pena privativa de libertad será no menor de diez ni mayor de quince años y con trescientos sesenta y cinco a setecientos treinta días-multa.

Se considera que existe indicio de enriquecimiento ilícito cuando el aumento del patrimonio o del gasto económico personal del funcionario o servidor público, en consideración a su declaración jurada de bienes y rentas, es notoriamente superior al que normalmente haya podido tener en virtud de sus sueldos o emolumentos percibidos o de los incrementos de su capital o de sus ingresos por cualquier otra causa lícita.” (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1243, publicado el 22 octubre 2016, cuyo texto es el siguiente:

“Artículo 401. Enriquecimiento ilícito

El funcionario o servidor público que, abusando de su cargo, incrementa ilícitamente su patrimonio respecto de sus ingresos legítimos será reprimido con pena privativa de libertad no menor de cinco ni mayor de diez años; inhabilitación, según corresponda, conforme a los incisos 1, 2 y 8 del artículo 36; y, con trescientos sesenta y cinco a setecientos treinta días-multa.

Si el agente es un funcionario público que ha ocupado cargos de alta dirección en las entidades, organismos o empresas del Estado, o está sometido a la prerrogativa del antejuicio y la acusación constitucional, será reprimido con pena privativa de libertad será no menor de diez ni mayor de quince años; inhabilitación, según corresponda, conforme a los incisos 1, 2 y 8 del artículo 36; y, con trescientos sesenta y cinco a setecientos treinta días-multa.

Se considera que existe indicio de enriquecimiento ilícito cuando el aumento del patrimonio o del gasto económico personal del funcionario o servidor público, en consideración a su declaración jurada de bienes y rentas, es notoriamente superior al que normalmente haya podido tener en virtud de sus sueldos o emolumentos percibidos o de los incrementos de su capital o de sus ingresos por cualquier otra causa lícita.” (*)

(*) De conformidad con el Acápite vi del Literal b) del Artículo 11 del Decreto Legislativo N° 1264, publicado el 11 diciembre 2016, se dispone que no podrán acogerse al Régimen temporal y sustitutorio del impuesto a la renta, los delitos previstos en el presente artículo; disposición que entró en vigencia a partir del 1 de enero de 2017.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Decomiso

"Artículo 401 A.- En todo caso, los donativos, dádivas o presentes serán decomisados." (*)

(*) Artículo incorporado por el Artículo 2 del Decreto Ley Nº 25489, publicado el 10 mayo 1992.

Adjudicación al Estado de bienes decomisados

"Artículo 401-B.- Los bienes decomisados e incautados durante la investigación policial y proceso judicial, serán puestos a disposición del Ministerio de Justicia; el que los asignará para su uso en servicio oficial o (*) RECTIFICADO POR FE DE ERRATAS  del Poder Judicial y el Ministerio Público, en su caso, bajo responsabilidad."

De dictarse sentencia judicial absolutoria se dispondrá la devolución del bien a su propietario.

Los bienes decomisados o incautados definitivamente serán adjudicados al Estado y afectados en uso a los mencionados organismos públicos. Aquellos bienes que no sirvan para este fin serán vendidos en pública subasta y su producto constituirá ingresos del Tesoro Público. (*)

(*) Artículo incorporado por el Artículo 2 del Decreto Ley Nº 25489, publicado el 10 mayo 1992.

CONCORDANCIAS:      D.S. Nº 029-2001-JUS (Comisión de Administración de Bienes Incautados y Decomisados - COMABID)

“Artículo 401-C. Multa aplicable a las personas jurídicas

Cuando las personas jurídicas señaladas en el artículo 2 de la Ley que regula la responsabilidad administrativa autónoma de las personas jurídicas por el delito de cohecho activo transnacional resulten responsables por el delito previsto en el artículo 397-A, el juez impone la medida de multa, conforme al literal a del artículo 5 de la citada norma, sin perjuicio de las demás medidas administrativas allí previstas que resulten aplicables".(*)

(*) Artículo incorporado por la Primera Disposición Complementaria Modificatoria de la Ley N° 30424, publicada el 21 abril 2016, la misma que entra en vigencia el 1 de enero del 2018. Posteriormente, mediante la Única Disposición Complementaria Derogatoria del Decreto Legislativo N° 1352, publicado el 07 enero 2017, se dispuso la derogación de la Primera Disposición Complementaria Modificatoria de la Ley Nº 30424.

CAPITULO  III DELITOS CONTRA LA ADMINISTRACION DE JUSTICIA
##########################################################

SECCION I

DELITOS CONTRA LA FUNCION JURISDICCIONAL

Artículo 402 Denuncia calumniosa
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que denuncia a la autoridad un hecho punible, a sabiendas que no se ha cometido, o el que simula pruebas o indicios de su comisión que puedan servir de motivo para un proceso penal o el que falsamente se atribuye delito no cometido o que ha sido cometido por otro, será reprimido con pena privativa de libertad no mayor de tres años.

"Cuando la simulación directa o indirecta de pruebas o indicios de su comisión sea efectuada por miembros de la Policía Nacional u otro funcionario o servidor público encargado de la prevención del delito, y que puedan servir de sustento para un proceso penal por tráfico ilícito de drogas, la pena privativa de libertad será no menor de tres ni mayor de seis años." (*)

(*) Párrafo adicionado por el Artículo Unico de la Ley Nº 27225, publicada el 17 diciembre 1999.

(*) Artículo modificado por el Artículo Único del Decreto Legislativo N° 1237, publicado el 26 septiembre 2015, cuyo texto es el siguiente:

"Artículo 402.- Denuncia calumniosa

El que denuncia a la autoridad un hecho punible, a sabiendas de que no se ha cometido o que ha sido cometido por persona distinta a la denunciada, o el que simula o adultera pruebas o indicios de su comisión que puedan servir de motivo para un proceso penal o el que falsamente se atribuye delito no cometido o que ha sido cometido por otro, será reprimido con pena privativa de libertad no mayor de tres años y con ciento ochenta a trescientos sesenta y cinco días-multa.

Cuando la simulación o adulteración directa o indirecta de pruebas o indicios sea efectuada por miembros de la Policía Nacional u otro funcionario o servidor público encargado de la prevención del delito, y que puedan servir de sustento para un proceso penal por tráfico ilícito de drogas, la pena será privativa de libertad no menor de tres ni mayor de seis años y trescientos sesenta y cinco a setecientos treinta días-multa.”

CONCORDANCIAS:     Ley N° 28611, Art. 149, num. 149.2

Artículo 403 Ocultamiento de menor a las investigaciones
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que oculta a un menor de edad a las investigaciones de la justicia o de la que realiza la autoridad competente, será reprimido con pena privativa de libertad no menor de uno ni mayor de tres años.

Artículo 404 
~~~~~~~~~~~~~

El que sustrae a una persona de la persecución penal o de la ejecución de una pena o de otra medida ordenada por la justicia, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.

Si el agente sustrae al autor de los delitos contra la seguridad y tranquilidad públicas, contra el Estado y la defensa nacional o contra los Poderes del Estado y el orden constitucional, la pena será privativa de libertad no menor de cinco ni mayor de ocho años y de ciento ochenta a trescientos sesenticinco días-multa.

Si el autor del encubrimiento personal es funcionario o servidor público encargado de la investigación del delito o de la custodia del delincuente, la pena será privativa de libertad no menor de seis ni mayor de diez años. (*)

(*) Artículo modificado por el Artículo 1 del Decreto Legislativo Nº 747, publicado el 12 noviembre 1991, cuyo texto es el siguiente:

"Artículo 404.- El que sustrae una persona de la persecución penal o a la ejecución de una pena o de otra medida ordenada por la justicia, será reprimido con pena privada de libertad no menor de dos años mayor de cuatro años.

Si el Agente sustrae al autor de los delitos contra la Tranquilidad Pública, contra el Estado y la Defensa Nacional, contra los Poderes del Estado y el Orden Constitucional o de Tráfico Ilícito de Drogas, la pena será privativa de libertad no menor de siete ni mayor de diez años y de ciento ochenta a trescientos sesenta y cinco días multa.

Si el autor del encubrimiento personal es funcionario o servidor público encargado de la investigación del delito o de la custodia del delincuente, la pena será privada de libertad no menor de diez ni mayor de quince años". (*)

(*) De conformidad con el Artículo 1 de la Ley Nº 25399, publicada el 10 febrero 1992, quedó derogado el Decreto Legislativo Nº 747, recobrando vigencia el texto original, conforme al Artículo 3 de la citada Ley, cuyo texto es el siguiente:

Artículo 404.- El que sustrae a una persona de la persecución penal o de la ejecución de una pena o de otra medida ordenada por la justicia, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.

Si el agente sustrae al autor de los delitos contra la seguridad y tranquilidad públicas, contra el Estado y la defensa nacional o contra los Poderes del Estado y el orden constitucional, la pena será privativa de libertad no menor de cinco ni mayor de ocho años y de ciento ochenta a trescientos sesenticinco días-multa.

Si el autor del encubrimiento personal es funcionario o servidor público encargado de la investigación del delito o de la custodia del delincuente, la pena será privativa de libertad no menor de seis ni mayor de diez años. (*)

(*) Artículo modificado por el Artículo 1 del Decreto Ley N° 25429, publicado el 11 abril 1992, cuyo texto es el siguiente:

Encubrimiento personal

"Artículo 404.- El que sustrae a una persona de la persecución penal o a la ejecución de una pena o de otra medida ordenada por la justicia, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años.

Si el Agente sustrae al autor de los delitos contra la Tranquilidad Pública, contra el Estado y la Defensa Nacional, contra los Poderes del Estado y el Orden Constitucional o de Tráfico Ilícito de Drogas, la pena privativa de libertad será no menor de siete ni mayor de diez años y de ciento ochenta a trescientos sesenticinco días-multa.

Si el autor del encubrimiento personal es funcionario o servidor público encargado de la investigación del delito o de la custodia del delincuente, la pena será privativa de libertad no menor de diez ni mayor de quince años." (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 982, publicado el 22 julio 2007, cuyo texto es el siguiente:

“Artículo 404.- Encubrimiento personal

El que sustrae a una persona de la persecución penal o a la ejecución de una pena o de otra medida ordenada por la justicia, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años.

Si el Agente sustrae al autor de los delitos previstos en los artículos 152 al 153 A, 200, 273 al 279-D, 296 al 298, 315, 317, 318- A, 325 al 333; 346 al 350, en la Ley Nº 27765 (Ley Penal contra el Lavado de Activos) o en el Decreto Ley Nº 25475 (Establecen la penalidad para los delitos de terrorismo y los procedimientos para la investigación, la instrucción y el juicio), la pena privativa de libertad será no menor de siete ni mayor de diez años y de ciento ochenta a trescientos sesenticinco días-multa.

Si el autor del encubrimiento personal es funcionario o servidor público encargado de la investigación del delito o de la custodia del delincuente, la pena será privativa de libertad no menor de diez ni mayor de quince años.”

CONCORDANCIAS:     D.S Nº 009-2010-JUS (Aprueban Procedimiento para el pago de la reparación civil a favor del Estado en casos de procesos seguidos sobre
delitos de corrupción y otros delitos conexos)

Artículo 405 Encubrimiento real
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que dificulta la acción de la justicia procurando la desaparición de las huellas o prueba del delito u ocultando los efectos del mismo, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.(*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 982, publicado el 22 julio 2007, cuyo texto es el siguiente:

“Artículo 405.- Encubrimiento real

El que dificulta la acción de la justicia procurando la desaparición de las huellas o prueba del delito u ocultando los efectos del mismo, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años. Si el hecho se comete respecto a los delitos previstos en los artículos 152 al 153 A, 200, 273 al 279-D, 296 al 298, 315, 317, 318- A, 325 al 333; 346 al 350 o en el Decreto Ley Nº 25475 (Establecen la penalidad para los delitos de terrorismo o los procedimientos para la investigación, la instrucción y el juicio), la pena privativa de libertad será no menor de siete ni mayor de diez años y de ciento ochenta a trescientos sesenta y cinco días multa.”

CONCORDANCIAS:     D.S Nº 009-2010-JUS (Aprueban Procedimiento para el pago de la reparación civil a favor del Estado en casos de procesos seguidos sobre
delitos de corrupción y otros delitos conexos)

Artículo 406 Excusa absolutoria
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Están exentos de pena los que ejecutan cualquiera de los hechos previstos en los artículos 404º y 405º si sus relaciones con la persona favorecida son tan estrechas como para excusar su conducta.

Artículo 407 Omisión de denuncia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que omite comunicar a la autoridad las noticias que tenga acerca de la comisión de algún delito, cuando esté obligado a hacerlo por su profesión o empleo, será reprimido con pena privativa de libertad no mayor de dos años.

Si el hecho punible no denunciado tiene señalado en la ley pena privativa de libertad superior a cinco años, la pena será no menor de dos ni mayor de cuatro años. (*)

(*) Artículo modificado por el Artículo Único de la Ley N° 28516, publicada el 23 mayo 2005, cuyo texto es el siguiente:

“Artículo 407.- Omisión de denuncia

El que omite comunicar a la autoridad las noticias que tenga acerca de la comisión de algún delito, cuando esté obligado a hacerlo por su profesión o empleo, será reprimido con pena privativa de libertad no mayor de dos años.

Si el hecho punible no denunciado tiene señalado en la ley pena privativa de libertad superior a cinco años, la pena será no menor de dos ni mayor de cuatro años.

Si la omisión está referida a los delitos de genocidio, tortura o desaparición forzada, la pena será no menor de dos ni mayor de seis años.”

Artículo 408 Fuga en accidente de tránsito
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


El que, después de un accidente automovilístico o de otro similar en el que ha tenido parte y del que han resultado lesiones o muerte, se aleja del lugar para sustraerse a su identificación o para eludir las comprobaciones necesarias o se aleja por razones atendibles, pero omite dar cuenta inmediata a la autoridad, será reprimido con pena privativa de libertad no mayor de tres años y con noventa a ciento veinte días-multa.(*)

(*) Artículo modificado por el Artículo 1 de la Ley N° 29439, publicada el 19 noviembre 2009, cuyo texto es el siguiente:

"Artículo 408.- Fuga del lugar del accidente de tránsito

El que, después de un accidente automovilístico o de otro similar en el que ha tenido parte y del que han resultado lesiones o muerte, se aleja del lugar para sustraerse a su identificación o para eludir las comprobaciones necesarias o se aleja por razones atendibles, pero omite dar cuenta inmediata a la autoridad, será reprimido con pena privativa de libertad no menor de seis meses ni mayor de cuatro años y con noventa a ciento veinte días- multa.”

Artículo 409 Falsedad en juicio
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El testigo, perito, traductor o intérprete que, en un procedimiento judicial, hace falsa declaración sobre los hechos de la causa o emite dictamen, traducción o interpretación falsos, será reprimido con pena privativa de libertad, no menor de dos ni mayor de cuatro años.

Si el testigo, en su declaración, atribuye a una persona haber cometido un delito, a sabiendas que es inocente, la pena será no menor de dos ni mayor de seis años.

El Juez puede atenuar la pena hasta límites inferiores al mínimo legal o eximir de sanción, si el agente rectifica espontáneamente su falsa declaración antes de ocasionar perjuicio.

“Artículo 409-A.- Obstrucción de la justicia

El que mediante el uso de fuerza física, amenaza, ofrecimiento o concesión de un beneficio indebido, impide u obstaculiza se preste un testimonio o la aportación de pruebas o induce a que se preste un falso testimonio o pruebas falsas, será sancionado con pena privativa de libertad no menor de tres ni mayor de cinco años.

Si el hecho se comete respecto en la investigación preliminar o proceso penal por delito previsto en los artículos 152 al 153-A, 200, 296 al 298 o en la Ley Nº 27765 (Ley Penal contra el Lavado de Activos), la pena privativa de libertad será no menor de cinco ni mayor de ocho años y de ciento ochenta a trescientos sesenta y cinco días multa.”(*)

(*) Artículo adicionado por el Artículo 2 del Decreto Legislativo N° 982, publicado el 22 julio 2007.

“Artículo 409-B.- Revelación indebida de identidad

El que indebidamente revela la identidad de un colaborador eficaz, testigo, agraviado o perito protegido, Agente Encubierto o especial, o información que permita su identificación, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años.

Cuando el Agente es funcionario o servidor público y por el ejercicio de su cargo tiene acceso a la información, la pena será no menor de cinco ni mayor de siete años, e inhabilitación conforme al artículo 36, incisos 1, 2 y 4.”

(*) Artículo adicionado por el Artículo 2 del Decreto Legislativo N° 982, publicado el 22 julio 2007.

Artículo 410 Avocamiento ilegal de proceso en trámite
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La autoridad que, a sabiendas, se avoque a procesos en trámite ante el órgano jurisdiccional, será reprimida con pena privativa de libertad no mayor de dos años e inhabilitación conforme al artículo 36,  incisos 1, 2 y 4.

Artículo 411 Falsa declaración en procedimiento administrativo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, en un procedimiento administrativo, hace una falsa declaración en relación a hechos o circunstancias que le corresponde probar, violando la presunción de veracidad establecida por ley, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

Artículo 412 Expedición de prueba o informe falso en proceso judicial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, legalmente requerido en causa judicial en la que no es parte, expide una prueba o un informe falsos, niega o calla la verdad, en todo o en parte, será reprimido con pena privativa de libertad no mayor de tres años.

Artículo 413 Evasión mediante violencia o amenaza
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, estando legalmente privado de su libertad, se evade por medio de violencia o amenaza, será reprimido con pena privativa de libertad no mayor de tres años.

Artículo 414 Favorecimiento a la fuga
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, por violencia, amenaza o astucia, hace evadir a un preso, detenido o interno o le presta asistencia en cualquier forma para evadirse, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.

Si el agente que hace evadir, o presta asistencia para tal efecto, es funcionario o servidor público, la pena será privativa de libertad no menor de tres ni mayor de ocho años.

Si el agente actuó por culpa, la pena será no mayor de un año.

Artículo 415 Amotinamiento de detenido o interno
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El detenido o interno que se amotina atacando a un funcionario del establecimiento o a cualquier persona encargada de su custodia, u obligando por la violencia o amenaza a un funcionario del establecimiento o a cualquier persona encargada de su custodia a practicar o abstenerse de un acto, con el fin de evadirse, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.(*)

(*) Artículo modificado por el Artículo Único de la Ley Nº 29867, publicada el 22 mayo 2012, la misma que entró en vigencia a los sesenta días calendario de su publicación en el diario oficial El Peruano, cuyo texto es el siguiente:

“Artículo 415.- Amotinamiento de detenido o interno

El detenido o interno que se amotina atacando a un funcionario del establecimiento o a cualquier persona encargada de su custodia, u obligando por la violencia o amenaza a un funcionario del establecimiento o a cualquier persona encargada de su custodia a practicar o abstenerse de un acto, con el fin de evadirse, será reprimido con pena privativa de libertad no menor de cuatro ni mayor de seis años.

Si el agente provoca un motín, disturbio o cualquier violación contra la integridad física de cualquier persona o de la seguridad interna o externa del recinto, será reprimido con pena privativa de libertad no menor de seis ni mayor de ocho años.

Los cabecillas o dirigentes del motín serán sancionados con la pena señalada, aumentada en una tercera parte.”

Artículo 416 Fraude procesal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, por cualquier medio fraudulento, induce a error a un funcionario o servidor público para obtener resolución contraria a la ley, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.

PROCESOS CONSTITUCIONALES
JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 417 Ejercicio arbitrario de derecho. Justicia por propia mano
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, con el fin de ejercer un derecho, en lugar de recurrir a la autoridad, se hace justicia arbitrariamente por si mismo, será reprimido con prestación de servicio comunitario de veinte a cuarenta jornadas.

“Artículo 417-A.- Insolvencia provocada

El responsable civil por un hecho delictivo que, con posterioridad a la realización del mismo y con la finalidad de eludir total o parcialmente el cumplimiento de la reparación civil correspondiente, realiza actos de disposición o contrae obligaciones que disminuyan su patrimonio, haciéndose total o parcialmente insolvente, será reprimido con pena privativa de libertad no menor de dos años ni mayor de cuatro.

La misma pena se aplicará a quien como representante de una persona jurídica, con posterioridad a la realización de un hecho delictivo, dispone de los bienes de su representada, con la finalidad de eludir total o parcialmente la imposición de una consecuencia accesoria en el proceso penal respectivo.

Si el hecho se comete respecto a proceso por delito previsto en los artículos 152 al 153 A, 200, 296 al 298, en la Ley Nº 27765, Ley Penal contra el Lavado de Activos o en el Decreto Ley Nº 25475 (Establecen la penalidad para los delitos de terrorismo y los procedimientos para la investigación, la instrucción y el juicio), será sancionado con pena privativa de libertad no menor de tres ni mayor de seis años.”(*)

(*) Artículo adicionado por el Artículo 2 del Decreto Legislativo N° 982, publicado el 22 julio 2007.

SECCION  II
PREVARICATO

Artículo 418 Fallo o dictamen ilegal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El Juez o el Fiscal que, a sabiendas, dicta resolución o emite dictamen, contrarios al texto expreso y claro de la ley o cita pruebas inexistentes o hechos falsos, o se apoya en leyes supuestas o derogadas, será reprimido con pena privativa de libertad no menor de tres ni mayor de cinco años. (*)

(*) Artículo modificado por el Artículo Único de la Ley N° 28492, publicada el 12 abril 2005, cuyo texto es el siguiente:

"Artículo 418.- Prevaricato

El Juez o el Fiscal que dicta resolución o emite dictamen, manifiestamente contrarios al texto expreso y claro de la ley, o cita pruebas inexistentes o hechos falsos, o se apoya en leyes supuestas o derogadas, será reprimido con pena privativa de libertad no menor de tres ni mayor de cinco años."

Artículo 419 Detención ilegal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El Juez que, maliciosamente o sin motivo legal, ordena la detención de una persona o no otorga la libertad de un detenido o preso, que debió decretar, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.

Artículo 420 Prohibición de conocer un proceso que patrocinó
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El Juez o Fiscal que conoce en un proceso que anteriormente patrocinó como abogado, será reprimido con pena privativa de libertad no mayor de dos años.

Artículo 421 Patrocinio indebido de abogado o mandatario judicial
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El Abogado o mandatario judicial que, después de haber patrocinado o representado a una parte en un proceso judicial o administrativo, asume la defensa o representación de la parte contraria en el mismo proceso, será reprimido con pena privativa de libertad no mayor de dos años.

SECCION  III
DENEGACION Y RETARDO DE JUSTICIA

Artículo 422 Negativa a administrar justicia
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El Juez que se niega a administrar justicia o que elude juzgar bajo pretexto de defecto o deficiencia de la ley, será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

Artículo 423 Negativa al cumplimiento de obligaciones de notario y auxiliares jurisdiccionales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El notario o secretario de juzgado o fiscalía o cualquier otro auxiliar de justicia que se niega a cumplir las obligaciones que legalmente le corresponde, será reprimido con pena privativa de libertad no mayor de un año, o con treinta a sesenta días-multa.

Artículo 424 Omisión de ejercicio de la acción penal
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El Fiscal que omite ejercitar la acción penal será reprimido con pena privativa de libertad no menor de uno ni mayor de cuatro años.

CAPITULO  IV DISPOSICIONES COMUNES
##################################

Artículo 425 Funcionario o servidor público
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Se consideran funcionarios o servidores públicos:

1. Los que están comprendidos en la carrera administrativa.

2. Los que desempeñan cargos políticos o de confianza, incluso si emanan de elección popular.

3.- Los de empresas del Estado o sociedades de economía mixta y de organismos sometidos por el Estado. (*)

(*) Numeral modificado por el Artículo 1 de la Ley Nº 26713, publicada el 27 diciembre 1996, cuyo texto es el siguiente:

"3.Todo aquel que independientemente del régimen laboral en que se encuentre, mantiene vínculo laboral o contractual de cualquier naturaleza con entidades u organismos del Estado y que en virtud de ello ejerce funciones en dichas entidades u organismos."

4. Los administradores y depositarios de caudales embargados o depositados por autoridad competente, aunque pertenezcan a particulares.

5. Los miembros de las Fuerzas Armadas y Policía Nacional.

6. Los demás indicados por la Constitución Política y la ley. (*)

(*) Artículo modificado por el Artículo Único de la Ley N° 30124, publicada el 13 diciembre 2013, cuyo texto es el siguiente:

“Artículo 425. Funcionario o servidor público

Son funcionarios o servidores públicos:

1. Los que están comprendidos en la carrera administrativa.

2. Los que desempeñan cargos políticos o de confianza, incluso si emanan de elección popular.

3. Todo aquel que, independientemente del régimen laboral en que se encuentre, mantiene vínculo laboral o contractual de cualquier naturaleza con entidades u organismos del Estado, incluidas las empresas del Estado o sociedades de economía mixta comprendidas en la actividad empresarial del Estado, y que en virtud de ello ejerce funciones en dichas entidades u organismos.

4. Los administradores y depositarios de caudales embargados o depositados por autoridad competente, aunque pertenezcan a particulares.

5. Los miembros de las Fuerzas Armadas y Policía Nacional.

6. Los designados, elegidos o proclamados, por autoridad competente, para desempeñar actividades o funciones en nombre o al servicio del Estado o sus entidades.

7. Los demás indicados por la Constitución Política y la ley.”

CONCORDANCIAS:     D.S. N° 004-2014-JUS, Art. 5 (Obligación de guardar secreto de la identidad del agente encubierto)

Artículo 426 Inhabilitación
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Los delitos previstos en los Capítulos II y III de este Título, serán sancionados, además, con pena de inhabilitación de uno a tres años conforme al artículo 36, incisos 1 y 2.(*)

(*) Artículo modificado por el Artículo Único de la Ley Nº 29758, publicada el 21 julio 2011, cuyo texto es el siguiente:

"Artículo 426. Inhabilitación accesoria y especial

Los delitos previstos en el capítulo II de este Título se sancionan, además, con pena de inhabilitación accesoria, con igual tiempo de duración que la pena principal, de conformidad con el artículo 36, incisos 1 y 2.

Los delitos previstos en el capítulo III de este Título se sancionan, además, con pena de inhabilitación de uno a tres años, de conformidad con el artículo 36, incisos 1 y 2.” (*)

(*) Artículo modificado por el Artículo 2 del Decreto Legislativo N° 1243, publicado el 22 octubre 2016, cuyo texto es el siguiente:

“Artículo 426. Inhabilitación

Los delitos previstos en los Capítulos II y III de este Título, que no contemplan la pena de inhabilitación, son sancionados, además, conforme a los incisos 1, 2, 4 y 8 del artículo 36, según corresponda, y el artículo 38.”

TITULO  XIX DELITOS CONTRA LA FE PUBLICA
========================================

CAPITULO  I FALSIFICACION DE DOCUMENTOS EN GENERAL
##################################################

CONCORDANCIAS:     D.S. Nº 096-2007-PCM, Art.  8 (Central de Riesgo Administrativo)

Artículo 427 Falsificación de documentos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que hace, en todo o en parte, un documento falso o adultera uno verdadero que pueda dar origen a derecho u obligación o servir para probar un hecho, con el propósito de utilizar el documento, será reprimido, si de su uso puede resultar algún perjuicio, con pena privativa de libertad no menor de dos ni mayor de diez años y con treinta a noventa días-multa si se trata de un documento público, registro público, título auténtico o cualquier otro trasmisible por endoso o al portador y con pena privativa de libertad no menor de dos ni mayor de cuatro años, y con ciento ochenta a trescientos sesenticinco días-multa, si se trata de un documento privado.

CONCORDANCIAS:     Ley Nº 30077, Art. 3 (Delitos comprendidos)

El que hace uso de un documento falso o falsificado, como si fuese legítimo, siempre que de su uso pueda resultar algún perjuicio, será reprimido, en su caso, con las mismas penas.

PROCESOS CONSTITUCIONALES
JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA

Artículo 428 Falsedad ideológica
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que inserta o hace insertar, en instrumento público, declaraciones falsas concernientes a hechos que deban probarse con el documento, con el objeto de emplearlo como si la declaración fuera conforme a la verdad, será reprimido, si de su uso puede resultar algún perjuicio, con pena privativa de libertad no menor de tres ni mayor de seis años y con ciento ochenta a trescientos sesenticinco días-multa.

El que hace uso del documento como si el contenido fuera exacto, siempre que de su uso pueda resultar algún perjuicio, será reprimido, en su caso, con las mismas penas.

JURISPRUDENCIA DE LA CORTE SUPREMA DE JUSTICIA
PROCESOS CONSTITUCIONALES

“Artículo 428- B.- Falsedad en el reporte de los volúmenes de pesca capturados.-

El que, estando incluido dentro del régimen de Límites Máximos de Captura por Embarcación establecido por Ley, inserta o hace insertar en cualquier documento donde se consigne la información referente a los volúmenes de captura, información falsa o distinta respecto al volumen realmente capturado, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años y con ciento ochenta a trescientos sesenta y cinco días-multa.

Con igual pena será reprimido quien altera o ayuda a la alteración de los instrumentos de pesaje con los que se calcula los volúmenes de pesca capturados, si dicha alteración tiene la finalidad de consignar un volumen distinto al realmente capturado.” (*)

(*) Artículo incorporado por el numeral 3 del Artículo 29 del Decreto Legislativo Nº 1084, publicado el 28 junio 2008.

Artículo 429 Omisión de consignar declaraciones en documentos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que omite en un documento público o privado declaraciones que deberían constar o expide duplicados con igual omisión, al tiempo de ejercer una función y con el fin de dar origen a un hecho u obligación, será reprimido con pena privativa de libertad no menor de uno ni mayor de seis años.

Artículo 430 Supresión, destrucción u ocultamiento de documentos
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que suprime, destruye u oculta un documento, en todo o en parte de modo que pueda resultar perjuicio para otro, será reprimido con la pena señalada en los artículos 427 y 428, según sea el caso.

Artículo 431 Expedición de certificado médico falso
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El médico que, maliciosamente, expide un certificado falso respecto a la existencia o no existencia, presente o pasada, de enfermedades físicas o mentales, será reprimido con pena privativa de libertad no mayor de tres años e inhabilitación de uno a dos años conforme al artículo 36, incisos 1 y 2.

Cuando se haya dado la falsa certificación con el objeto que se admita o interne a una persona en un hospital para enfermos mentales, la pena será privativa de libertad no menor de tres ni mayor de seis años e inhabilitación de dos a cuatro años conforme al artículo 36, incisos 1 y 2.

El que haga uso malicioso de la certificación, según el caso de que se trate, será reprimido con las mismas penas privativas de libertad.

“Artículo 431-A.- El que, con el propósito de gozar de los beneficios o coberturas del Seguro Obligatorio de Accidentes de Tránsito, incita a la simulación o simula la ocurrencia de accidentes de tránsito o la intervención en éstos de personas que no tienen la condición de ocupantes o terceros no ocupantes del vehículo automotor interviniente en dichos accidentes o simula lesiones corporales que no se han producido o que se han producido en grado manifiestamente menor al indicado en la documentación policial o médica correspondiente, será reprimido con pena privativa de la libertad no menor de tres (3) ni mayor de seis (6) años.

Si el agente es efectivo de la Policía Nacional del Perú o del Cuerpo General de Bomberos Voluntarios del Perú, agente o intermediario de seguros, profesional médico o funcionario de un establecimiento de salud público o privado, la pena privativa de la libertad será no menor de tres (3) ni mayor de seis (6) años, imponiéndosele además la pena accesoria de inhabilitación para el ejercicio del cargo por un periodo similar a la pena principal.”(*)

(*) Artículo incorporado por el Artículo 3 de la Ley N° 28839, publicada el 24 julio 2006.

Artículo 432 Inhabilitación
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando algunos de los delitos previstos en este Capítulo sea cometido por un funcionario o servidor público o notario, con abuso de sus funciones, se le impondrá, además, la pena de inhabilitación de uno a tres años conforme al artículo 36, incisos 1 y 2.

Artículo 433 Equiparación a documento público
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Para los efectos de este Capítulo se equiparan a documento público, los testamentos ológrafo y cerrado, los títulos-valores y los títulos de crédito transmisibles por endoso o al portador.

CONCORDANCIAS:     D.S. Nº 096-2007-PCM, Art.  8 (Central de Riesgo Administrativo)

CAPITULO  II FALSIFICACION DE SELLOS, TIMBRES Y MARCAS OFICIALES
################################################################

Artículo 434 Fabricación o falsificación de sellos o timbres oficiales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que fabrica, fraudulentamente, o falsifica sellos o timbres oficiales de valor, especialmente estampillas de correos, con el objeto de emplearlos o hacer que los empleen otras personas o el que da a dichos sellos o timbres oficiales ya usados la apariencia de validez para emplearlos nuevamente, será reprimido con pena privativa de libertad no menor de dos ni mayor de cinco años y con noventa a ciento ochenta días-multa.

Cuando el agente emplea como auténticos o todavía válidos los sellos o timbres oficiales de valor que son falsos, falsificados o ya usados, la pena será privativa de libertad no menor de uno ni mayor de tres años y de sesenta a noventa días-multa.

Artículo 435 Fabricación fraudulenta o falsificación de marcas o contraseñas oficiales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que fabrica, fraudulentamente, o falsifica marcas o contraseñas oficiales destinadas a hacer constar el resultado de un examen de la autoridad o la concesión de un permiso o la identidad de un objeto o el que a sabiendas de su procedencia ilícita hace uso de tales marcas, será reprimido con pena privativa de libertad no mayor de tres años.

Artículo 436 Inhabilitación
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Cuando el agente de alguno de los delitos comprendidos en este Capítulo es funcionario o servidor público, será reprimido, además, con pena de inhabilitación de uno a tres años conforme al artículo 36 incisos 1 y 2.

Artículo 437 Marcas y sellos extranjeros equiparados a los nacionales
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Las disposiciones de este Capítulo son aplicables a los sellos, marcas oficiales y timbres de procedencia extranjera.

CAPITULO  III DISPOSICIONES COMUNES
###################################

Artículo 438 Falsedad genérica
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que de cualquier otro modo que no esté especificado en los Capítulos precedentes, comete falsedad simulando, suponiendo, alterando la verdad intencionalmente y con perjuicio de terceros, por palabras, hechos o usurpando nombre, calidad o empleo que no le corresponde, suponiendo viva a una persona fallecida o que no ha existido o viceversa, será reprimido con pena privativa de libertad no menor de dos ni mayor de cuatro años.

“Artículo 438-A.- Falsedad genérica agravada

El que otorgue, expida u oferte certificados, diplomas u otras constancias que atribuyan grado académico, título profesional, título de segunda especialidad profesional, nivel de especialización u otra capacidad análoga, sin que el beneficiario haya llevado efectivamente los estudios correspondientes, será reprimido con pena privativa de libertad no menor de tres ni mayor de cinco años y sesenta a ciento cincuenta días-multa.”(*)

(*) Artículo incorporado por el Artículo 3 del Decreto Legislativo N° 1351, publicado el 07 enero 2017.

Artículo 439 Fabricación o tenencia de instrumentos para falsificar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

El que, a sabiendas, fabrica o introduce en el territorio de la República o conserva en su poder máquinas, cuños, marcas o cualquier otra clase de útiles o instrumentos, destinados a la falsificación de timbres, estampillas, marcas oficiales o cualquier especie valorada, será reprimido con pena privativa de libertad no menor de tres ni mayor de seis años.
