.. Código Penal documentation master file, created by
   sphinx-quickstart on Fri Jul  3 13:20:27 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Código Penal
============

.. toctree::
   :maxdepth: 5
   :caption: Contents:

   libros/libroii

Índices y tablas
================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
